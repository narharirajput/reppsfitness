package com.reppsfitness.widget.searchable_spinner;

/**
 * Created by Md Farhan Raja on 2/23/2017.
 */

public interface OnSpinerItemClick
{
    public void onClick(String item,int position);
}