package com.reppsfitness.client;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class AddCommentActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_comment)
    EditText et_comment;
    @BindView(R.id.btn_submit_comment)
    Button btnSubmitComment;
    boolean isAddComment;
    String scheduleDayID, comment;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAddComment = getIntent().getBooleanExtra("isAddComment", false);
        scheduleDayID = getIntent().getStringExtra("scheduleDayID");
        comment = getIntent().getStringExtra("comment");
        user = getIntent().getParcelableExtra("user");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(isAddComment ? "Add comment" : "Edit comment");
        et_comment.setHint(isAddComment ? "Add comment here" : "Edit comment");
        if (comment != null && !TextUtils.isEmpty(comment)) {
            et_comment.setText(comment);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_add_comment;
    }

    @OnClick(R.id.btn_submit_comment)
    public void onViewClicked() {
        String scheduleComment = et_comment.getText().toString().trim();
        if (TextUtils.isEmpty(scheduleComment)) {
            et_comment.setError("Enter Comment");
            et_comment.requestFocus();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "updateWorkoutScheduleComment");
        params.put("scheduleDayID", scheduleDayID);
        params.put("scheduleComment", scheduleComment);
        params.put("clientID", user.getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().updateWorkoutScheduleComment(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
