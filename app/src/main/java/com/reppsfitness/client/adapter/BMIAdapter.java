package com.reppsfitness.client.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.client.EditBMIActivity;
import com.reppsfitness.model.Bmi;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class BMIAdapter extends RecyclerView.Adapter<BMIAdapter.RecyclerViewHolder> {
    Context mContext;
    List<Bmi> bmiList;

    public BMIAdapter(Context mContext, List<Bmi> bmiList) {
        this.mContext = mContext;
        this.bmiList = bmiList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_bmi, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Drawable drawable = holder.ivCalendar.getDrawable();
        drawable.setColorFilter(holder.white, PorterDuff.Mode.SRC_IN);
        Bmi bmi = bmiList.get(position);
        holder.tvWorkoutName.setText(bmi.getBMIDate());
        Bundle extras = new Bundle();
        extras.putSerializable("editbmi", bmi);
        holder.itemView.setOnClickListener(view -> holder.itemView.getContext().startActivity(new Intent(holder.itemView.getContext(), EditBMIActivity.class).putExtra("isAddBMI", false).putExtra("bmi", bmi)
                .putExtra("user", ((ClientHomeActivity) mContext).user)));
    }


    @Override
    public int getItemCount() {
        return bmiList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_calendar)
        ImageView ivCalendar;
        @BindView(R.id.tv_workout_name)
        TextView tvWorkoutName;
        @BindView(R.id.iv_next)
        ImageView ivNext;
        @BindColor(R.color.white)
        int white;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
