package com.reppsfitness.client.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.exercise.AddExerciseActivity;
import com.reppsfitness.model.Day;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.squint.DiagonalView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleDayListAdapter extends RecyclerView.Adapter<ScheduleDayListAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<Day> dayList;
    private String currDate, tomorrowDate;

    public ScheduleDayListAdapter(Context mContext, List<Day> dayList) {
        this.mContext = mContext;
        this.dayList = dayList;
        SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Calendar c = Calendar.getInstance();
        currDate = simpleDateFormatDate.format(c.getTime());
        c.add(Calendar.DAY_OF_YEAR, 1);
        tomorrowDate = simpleDateFormatDate.format(c.getTime());
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_day_row, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Day day = dayList.get(position);
        String date = Utils.ymdTodmy(day.getDate());
        String strDay;
        holder.tvDate.setTag(position);
        if (date.matches(currDate)) {
            holder.rr_bg.setBackground(holder.rect_bg_green);
            strDay = "Today";
            holder.tvDate.setText("Today, " + date);
        } else if (date.matches(tomorrowDate)) {
            strDay = "Tomorrow";
            holder.tvDate.setText("Tomorrow, " + date);
            holder.rr_bg.setBackground(null);
        } else {
            strDay = "Day " + String.valueOf(position + 1);
            holder.rr_bg.setBackground(null);
            holder.tvDate.setText("Day " + String.valueOf(position + 1) + ", " + date);
        }
        if (day.getIsWeekDay() == 1) {
            if (day.getIsSessionAllotedDay() == 1) {
                if (day.getExercise() == null || TextUtils.isEmpty(day.getExercise())) {
                    holder.tvExercises.setText("Exercise not assigned");
                    holder.tvExercises.setTextColor(Color.GRAY);
                    holder.iv_diagonal.setImageDrawable(holder.dialogal_bg_grey);
//            holder.diagonalView.setBackground(Utils.changeVectorColor(mContext, holder.diagonalView, R.color.day_grey));
                    holder.rr_day.setBackground(new ColorDrawable(holder.day_grey));
                } else {
                    holder.iv_diagonal.setImageDrawable(holder.dialogal_bg);
                    holder.tvExercises.setText(day.getExercise());
                    holder.tvExercises.setTextColor(Color.BLACK);
//            holder.diagonalView.setBackground(Utils.changeVectorColor(mContext, holder.diagonalView, R.color.toolbar_color));
                    holder.rr_day.setBackground(new ColorDrawable(holder.toolbar_color));
                }
                holder.itemView.setOnClickListener(view -> {
                    int clickPos = position;
                    if (holder.tvDate.getTag() != null)
                        clickPos = Integer.parseInt(holder.tvDate.getTag().toString());
                    Day day1 = dayList.get(clickPos);
                    if (day1.getIsWeekDay() == 1 && day1.getIsSessionAllotedDay() == 1 &&
                            day1.getExercise() != null && !TextUtils.isEmpty(day1.getExercise())) {
//                    mContext.startActivity(new Intent(mContext, ViewClientWorkoutActivity.class)
                        mContext.startActivity(new Intent(mContext, AddExerciseActivity.class)
                                .putExtra("day", day1)
                                .putExtra("user", ((ClientHomeActivity) mContext).user)
                                .putExtra("dayText", strDay)
                                .putExtra("isFromAdmin", ((ClientHomeActivity) mContext).isFromAdmin));
                    }
                });
            } else {
                holder.tvExercises.setText("Not a Session Day");
                holder.tvExercises.setTextColor(Color.GRAY);
                holder.iv_diagonal.setImageDrawable(holder.dialogal_bg_grey);
                holder.rr_day.setBackground(new ColorDrawable(holder.day_grey));
            }
        } else {
            holder.tvExercises.setText("Off Day");
            holder.tvExercises.setTextColor(Color.GRAY);
            holder.iv_diagonal.setImageDrawable(holder.dialogal_bg_grey);
            holder.rr_day.setBackground(new ColorDrawable(holder.day_grey));
        }
        holder.iv_next.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_exercises)
        TextView tvExercises;
        @BindView(R.id.iv_next)
        ImageView iv_next;
        @BindView(R.id.iv_diagonal)
        ImageView iv_diagonal;
        @BindView(R.id.rr_bg)
        RelativeLayout rr_bg;
        @BindView(R.id.diagonalView)
        DiagonalView diagonalView;
        @BindView(R.id.rr_day)
        RelativeLayout rr_day;
        @BindDrawable(R.drawable.rect_bg_green)
        Drawable rect_bg_green;
        @BindDrawable(R.drawable.dialogal_bg_grey)
        Drawable dialogal_bg_grey;
        @BindDrawable(R.drawable.dialogal_bg)
        Drawable dialogal_bg;
        @BindColor(R.color.day_grey)
        int day_grey;
        @BindColor(R.color.toolbar_color)
        int toolbar_color;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}