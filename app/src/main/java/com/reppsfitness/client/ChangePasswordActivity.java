package com.reppsfitness.client;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class ChangePasswordActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_oldPass)
    EditText etOldPass;
    @BindView(R.id.et_newPass)
    EditText etNewPass;
    @BindView(R.id.et_confirmPass)
    EditText etConfirmPass;
    User user;
    boolean isAdminProfile = false;
    Map<String, String> params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        toolbarTitle.setText ("Change password");
        user = getIntent ().getParcelableExtra ("user");
        isAdminProfile = getIntent ().getBooleanExtra ("isAdminProfile", false); // true= to change admin password
        if (!app.getPreferences ().isClient ()) {
            if (!isAdminProfile) {
                etOldPass.setText ("");
                etOldPass.setVisibility (View.GONE);
            }
        }
        Log.e ("uid ", "onCreate: uid" + app.getPreferences ().getLoggedInUser ().getData ().userID);
    }

    @OnClick({R.id.btn_cancel, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.btn_cancel:
                finish ();
                break;
            case R.id.btn_save:
                String oldPass = etOldPass.getText ().toString ().trim ();
                String newPass = etNewPass.getText ().toString ().trim ();
                String confirmPass = etConfirmPass.getText ().toString ().trim ();
                if (app.getPreferences ().isClient ()) {
                    if (TextUtils.isEmpty (oldPass)) {
                        etOldPass.setError ("Enter Old Password");
                        etOldPass.requestFocus ();
                        return;
                    }
                }
                if (isAdminProfile) {
                    if (TextUtils.isEmpty (oldPass)) {
                        etOldPass.setError ("Enter Old Password");
                        etOldPass.requestFocus ();
                        return;
                    }
                }
                if (TextUtils.isEmpty (newPass)) {
                    etNewPass.setError ("Enter New Password");
                    etNewPass.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (confirmPass)) {
                    etConfirmPass.setError ("Enter Password again");
                    etConfirmPass.requestFocus ();
                    return;
                }
                if (!newPass.matches (confirmPass)) {
                    Utils.showSimpleialog (mContext, "New password does not matches with confirm password.", "OK");
                    return;
                }
                if (isAdminProfile) {   // => true= to change admin password
                    params = new HashMap<> ();
                    params.put ("xAction", "changeAdminPassword");
                    params.put ("oldPass", oldPass);
                    params.put ("newPass", newPass);
                    params.put ("confirmPass", confirmPass);
                    params.put ("isFromAdmin", "0");
                    params.put ("userID", "" + app.getPreferences ().getLoggedInUser ().getData ().userID);
                } else {
                    params = new HashMap<> ();
                    params.put ("xAction", "changeClientPassword");
                    params.put ("oldPass", oldPass);
                    params.put ("newPass", newPass);
                    params.put ("confirmPass", confirmPass);
                    params.put ("isFromAdmin", app.getPreferences ().isClient () ? "0" : "1");  // 0 if client change his password ; 1 if admin change clients password
                    if (user==null || user.getClientID () == null)
                        params.put ("clientID", "" + app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
                    else params.put ("clientID", "" + user.getClientID ());
                }
                if (cd.isConnectingToInternet ()) {
                    CustomProgressDialog pd = new CustomProgressDialog (mContext);
                    pd.show ();
                    app.getApiRequestHelper ().changeClientPassword (params, new ApiRequestHelper.OnRequestComplete () {
                        @Override
                        public void onSuccess(Object object) {
                            if (pd.isShowing ()) pd.dismiss ();
                            LoginResponse response = (LoginResponse) object;
                            if (response != null) {
                                if (response.getCount () > 0) {
                                    if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                        Utils.showLongToast (mContext, response.getMsg ());
                                    }
                                    finish ();
                                } else {
                                    if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                        Utils.showLongToast (mContext, response.getMsg ());
                                    }
                                }
                            } else {
                                Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                            }
                        }

                        @Override
                        public void onFailure(String apiResponse) {
                            if (pd.isShowing ()) pd.dismiss ();
                            Utils.showLongToast (mContext, apiResponse);
                        }
                    });
                } else {
                    Utils.alert_dialog (mContext);
                }
                break;
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_change_password;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case android.R.id.home:
                finish ();
                break;
        }
        return true;
    }
}
