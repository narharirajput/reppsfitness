package com.reppsfitness.client;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.model.Bmi;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class EditBMIActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {
    @BindView(R.id.edit_weight)
    EditText editWeight;
    @BindView(R.id.edit_date)
    TextView edit_date;
    @BindView(R.id.body_flat)
    EditText bodyFlat;
    @BindView(R.id.edit_chest)
    EditText editChest;
    @BindView(R.id.edit_abdominal)
    EditText editAbdominal;
    @BindView(R.id.edit_thigh)
    EditText editThigh;
    @BindView(R.id.edit_tricep)
    EditText editTricep;
    @BindView(R.id.edit_head)
    EditText editHead;
    @BindView(R.id.edit_neck)
    EditText editNeck;
    @BindView(R.id.edit_arm_relaxed)
    EditText editArmRelaxed;
    @BindView(R.id.edit_arm_flexed)
    EditText editArmFlexed;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_bottom)
    LinearLayout llBottom;
    boolean isAddBMI = false;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.btn_save)
    Button btnSave;
    String clientBmiId;
    String clientId;
    User user;

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_edit_bmi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAddBMI = getIntent().getBooleanExtra("isAddBMI", false);
        user = getIntent().getParcelableExtra("user");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText(isAddBMI ? "Add BMI" : "Edit BMI");
//        boolean isClient = app.getPreferences().isClient();
//        editWeight.setFocusable(isClient);
//        bodyFlat.setFocusable(isClient);
//        editChest.setFocusable(isClient);
//        editAbdominal.setFocusable(isClient);
//        editThigh.setFocusable(isClient);
//        editTricep.setFocusable(isClient);
//        editHead.setFocusable(isClient);
//        editNeck.setFocusable(isClient);
//        editArmRelaxed.setFocusable(isClient);
//        editArmFlexed.setFocusable(isClient);
//        llBottom.setVisibility(isClient ? View.VISIBLE : View.GONE);

        if (isAddBMI) {

            edit_date.setText (Utils.getCurrentDate ());
            edit_date.setOnClickListener (v -> {
                Calendar now = Calendar.getInstance ();
                DatePickerDialog datepickerdialog = DatePickerDialog.newInstance (EditBMIActivity.this, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
                datepickerdialog.setThemeDark (false); //set dark them for dialog?
                datepickerdialog.vibrate (true); //vibrate on choosing date?
                datepickerdialog.dismissOnPause (true); //dismiss dialog when onPause() called?
                datepickerdialog.showYearPickerFirst (false); //choose year first?
                datepickerdialog.setMaxDate (now);
                datepickerdialog.setAccentColor (getResources ().getColor (R.color.app_primary_color));//(Color.parseColor ("#9C27A0")); // custom accent color
                datepickerdialog.setTitle ("Select a BMI date"); //dialog title
                datepickerdialog.show (this.getFragmentManager (), "Datepickerdialog"); //show dialog
            });

            //add
            editWeight.setText("");
            bodyFlat.setText("");
            editChest.setText("");
            editAbdominal.setText("");
            editThigh.setText("");
            editTricep.setText("");
            editHead.setText("");
            editNeck.setText("");
            editArmRelaxed.setText("");
            editArmFlexed.setText("");
        } else {
            //update
            Bmi bmi = (Bmi) getIntent().getSerializableExtra("bmi");
            if (bmi != null) {
                edit_date.setText (bmi.getBMIDate ());
                edit_date.setEnabled (false);
                editWeight.setText(bmi.getWeight());
                bodyFlat.setText(bmi.getBodyFlat());
                editChest.setText(bmi.getChest());
                editAbdominal.setText(bmi.getAbdominal());
                editThigh.setText(bmi.getThigh());
                editTricep.setText(bmi.getTricep());
                editHead.setText(bmi.getHead());
                editNeck.setText(bmi.getNeck());
                editArmRelaxed.setText(bmi.getArmRelaxed());
                editArmFlexed.setText(bmi.getArmFlexed());
                clientBmiId = bmi.getClientBMIID();
                clientId = bmi.getClientID();
            }
        }
    }

    private void addBmi() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "updateClientBMIDetails");
        if (app.getPreferences().isClient())
            params.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID()); //app.getPreferences().getLoggedInUser().getData().getClientID()
        else
            params.put("clientID", user.getClientID()); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("weight", editWeight.getText() != null ? editWeight.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("bodyFlat", bodyFlat.getText() != null ? bodyFlat.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("chest", editChest.getText() != null ? editChest.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("abdominal", editAbdominal.getText() != null ? editAbdominal.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("thigh", editThigh.getText() != null ? editThigh.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("tricep", editTricep.getText() != null ? editTricep.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("head", editHead.getText() != null ? editHead.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("neck", editNeck.getText() != null ? editNeck.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("armRelaxed", editArmRelaxed.getText() != null ? editArmRelaxed.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("armFlexed", editArmFlexed.getText() != null ? editArmFlexed.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("bmiDate", edit_date.getText() != null ? edit_date.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().addBmi(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(addBmiData -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "success");
                        if (addBmiData != null) {
                            if (addBmiData.getCount() != null && addBmiData.getCount() > 0) {
                                if (addBmiData.getMsg() != null && !TextUtils.isEmpty(addBmiData.getMsg())) {
                                    Utils.showLongToast(mContext, addBmiData.getMsg());
                                }
                                finish();
                            } else {
                                if (addBmiData.getMsg() != null && !TextUtils.isEmpty(addBmiData.getMsg())) {
                                    Utils.showLongToast(mContext, addBmiData.getMsg());
                                }
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
        //Log.e ("updateClientBMIDetails ", "addBmi: "+params );
    }


    private void updateBmi() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "updateClientBMIDetails");
        if (app.getPreferences().isClient())
            params.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID()); //app.getPreferences().getLoggedInUser().getData().getClientID()
        else
            params.put("clientID", user.getClientID()); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("clientBMIID", clientBmiId); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("weight", editWeight.getText() != null ? editWeight.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("bodyFlat", bodyFlat.getText() != null ? bodyFlat.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("chest", editChest.getText() != null ? editChest.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("abdominal", editAbdominal.getText() != null ? editAbdominal.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("thigh", editThigh.getText() != null ? editThigh.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("tricep", editTricep.getText() != null ? editTricep.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("head", editHead.getText() != null ? editHead.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("neck", editNeck.getText() != null ? editNeck.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("armRelaxed", editArmRelaxed.getText() != null ? editArmRelaxed.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("armFlexed", editArmFlexed.getText() != null ? editArmFlexed.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        params.put("bmiDate", edit_date.getText() != null ? edit_date.getText().toString() : ""); //app.getPreferences().getLoggedInUser().getData().getClientID()
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().addBmi(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(addBmiData -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "success");
                        if (addBmiData != null) {
                            if (addBmiData.getCount() != null && addBmiData.getCount() > 0) {
                                if (addBmiData.getMsg() != null && !TextUtils.isEmpty(addBmiData.getMsg())) {
                                    Utils.showLongToast(mContext, addBmiData.getMsg());
                                }
                                finish();
                            } else {
                                if (addBmiData.getMsg() != null && !TextUtils.isEmpty(addBmiData.getMsg())) {
                                    Utils.showLongToast(mContext, addBmiData.getMsg());
                                }
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
        //Log.e ("updateClientBMIDetails ", "addBmi: "+params );

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @OnClick({R.id.btn_cancel, R.id.btn_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_save:
//                if (TextUtils.isEmpty(editWeight.getText().toString())) {
//                    editWeight.setError("Enter Weight");
//                    editWeight.requestFocus();
//                    return;
//                }
//                if (TextUtils.isEmpty(bodyFlat.getText().toString())) {
//                    bodyFlat.setError("Enter Body Fat");
//                    bodyFlat.requestFocus();
//                    return;
//                }
                String weight = editWeight.getText().toString();
                String flat = bodyFlat.getText().toString();
                String chest = editChest.getText().toString();
                String abdominal = editAbdominal.getText().toString();
                String thigh = editThigh.getText().toString();
                String tricep = editTricep.getText().toString();
                String head = editHead.getText().toString();
                String neck = editNeck.getText().toString();
                String armRelaxed = editArmRelaxed.getText().toString();
                String armFlexed = editArmFlexed.getText().toString();
                if (TextUtils.isEmpty(weight) && TextUtils.isEmpty(flat) && TextUtils.isEmpty(chest) &&
                        TextUtils.isEmpty(abdominal) && TextUtils.isEmpty(thigh) && TextUtils.isEmpty(tricep) &&
                        TextUtils.isEmpty(head) && TextUtils.isEmpty(neck) && TextUtils.isEmpty(armRelaxed) &&
                        TextUtils.isEmpty(armFlexed)) {
                    Utils.showLongToast(mContext, "Please add all BMI details");
                    return;
                }
                if (isAddBMI) {
                    addBmi();
                } else {
                    updateBmi();
                }
                break;
        }
    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        edit_date.setText (year+"-"+monthOfYear+"-"+dayOfMonth);

    }
}
