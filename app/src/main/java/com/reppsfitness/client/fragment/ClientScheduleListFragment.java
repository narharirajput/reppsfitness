package com.reppsfitness.client.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.AddClientActivity;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.adapter.ClientScheduleDayListAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.client.adapter.ScheduleDayListAdapter;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/5/2017.
 */

public class ClientScheduleListFragment extends BaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView tv_error;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private RecyclerView.SmoothScroller smoothScroller;
    private LinearLayoutManager layoutManager;
    SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
    private Date pkgEndDate;
    private String isRegular, isTermApply;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);
        recyclerView.setHasFixedSize (true);
//        CustomLayoutManager layoutManager = new CustomLayoutManager(getActivity());
        layoutManager = new LinearLayoutManager (mContext, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager (layoutManager);
        smoothScroller = new LinearSmoothScroller (mContext) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };
        fab.setVisibility (((ClientHomeActivity) mContext).isFromAdmin ? View.VISIBLE : View.GONE);
        tv_error.setText ("Exercise schedule not assigned.");
        getClientProfile ();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_client_schedule_list;
    }

    @Override
    public void onResume() {
        super.onResume ();
        getClientScheduleDayList ();
    }

    //Managed to displayed client's schedule list when checked by client or admin
    private void getClientScheduleDayList() {
        Map<String, String> params = new HashMap<> ();
        params.put ("xAction", "getClientScheduleDayList");
        params.put ("clientID", ((ClientHomeActivity) mContext).user.getClientID ());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getClientScheduleDayList (params, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    WorkoutScheduleResponse response = (WorkoutScheduleResponse) object;
                    if (response != null) {
                        if (response.getCount () > 0 && response.getDayList () != null && response.getDayList ().size () > 0) {
                            SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat ("dd/MM/yyyy", Locale.getDefault ());
                            Calendar c = Calendar.getInstance ();
                            String currDate = simpleDateFormatDate.format (c.getTime ());
                            ArrayList<Day> dayList = response.getDayList ();
                            if (!((ClientHomeActivity) mContext).isFromAdmin) {       // Client
                                recyclerView.setAdapter (new ScheduleDayListAdapter (mContext, dayList));
                            } else {                                                  // Admin
                                recyclerView.setAdapter (new ClientScheduleDayListAdapter (mContext, response.getDayList ()));
                            }
                            int scrollPosition = 0;
                            for (int i = 0; i < dayList.size (); i++) {
                                Day day = dayList.get (i);
                                String date = Utils.ymdTodmy (day.getDate ());
                                if (date.matches (currDate)) {
                                    scrollPosition = i;
                                    break;
                                }
                            }
                            smoothScroller.setTargetPosition (scrollPosition);
                            layoutManager.startSmoothScroll (smoothScroller);
                            tv_error.setVisibility (View.GONE);
                        } else {
                            tv_error.setVisibility (View.VISIBLE);
                            if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                tv_error.setText (response.getMsg ());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Utils.showLongToast (mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
//        startActivity(new Intent(getActivity(), CreateWorkoutSchedule.class)
//                .putExtra("user", ((ClientHomeActivity) mContext).user));
        startAddClientWorkoutScheduleActivity ();
    }

    public void startAddClientWorkoutScheduleActivity() {
        //This client's Package is expired. Upgrade plan to create workout schedule.
        if (((ClientHomeActivity) mContext).user.getPkgEndDate () != null) {
            if (((ClientHomeActivity) mContext).user.getPkgEndDate ().equals ("0000-00-00")) {
                displayMaterialDialog ("Workout schedule cannot be assigned unless package is purchased.\nDo you wish to buy package?");
                return;
            }
            String str_pkgEndDate = ((ClientHomeActivity) mContext).user.getPkgEndDate ();
            try {
                Date pkgEndDate = formatter.parse (str_pkgEndDate);
                if (new Date ().after (pkgEndDate)) {
                    displayMaterialDialog ("Client's Package is expired. Upgrade plan and allot session to create workout schedule.\nDo you wish to upgrade package?");
                } else {
                    if (isTermApply != null && isRegular != null && isTermApply.equals ("0") && isRegular.equals ("0")) {
                        startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
                    } else {
                        if (((ClientHomeActivity) mContext).user.getIsSessionAlloted () != null) {
                            if (((ClientHomeActivity) mContext).user.getIsSessionAlloted ().equals ("1") || isRegular.equals ("0")) {
                                startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
                            } else {
                                Log.e ("in", "eslse");
                                displayMaterialDialog ("To assign workout, first allot session to user.\nDo you wish to proceed? ");
                            }
                        } else {
                            displayMaterialDialog ("To assign workout, first allot session to user.\nDo you wish to proceed? ");
                        }
                    }
                }
            } catch (ParseException e) {
                e.printStackTrace ();
            }
        } else {
            startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
        }
    }

    private void displayMaterialDialog(String message) {
        new MaterialDialog.Builder (mContext).content (message).positiveText ("Ok").onPositive ((dialog, which) -> {
            ((ClientHomeActivity) mContext).displayPackagesFrag ();
            dialog.dismiss ();
        }).negativeText ("Cancel").onNegative (null).show ();
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<> ();
        map.put ("xAction", "getClientProfile");
        map.put ("clientID", ((ClientHomeActivity) mContext).user.getClientID ());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getClientProfile (map, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    ClientProfileData clientProfileData = (ClientProfileData) object;
                    if (clientProfileData != null && clientProfileData.getCount () > 0) {
                        ClientProfile clientProfile = clientProfileData.getClientProfile ();
                        String str_pkgEndDate = clientProfile.getPkgEndDate ();
                        String str_pkgStartDate = clientProfile.getPkgStartDate ();
                        ((ClientHomeActivity) mContext).user.setPkgEndDate (str_pkgEndDate);
                        ((ClientHomeActivity) mContext).user.setPkgStartDate (str_pkgStartDate);
                        isRegular = clientProfile.getIsRegular ();
                        isTermApply = clientProfile.getIsTermApply ();
                        if (str_pkgEndDate != null && !str_pkgEndDate.equals ("0000-00-00")) {
                            try {
                                pkgEndDate = formatter.parse (str_pkgEndDate);
                            } catch (ParseException e) {
                                e.printStackTrace ();
                            }
                        }

                        if (!app.getPreferences ().isClient ()) {
                            if (((ClientHomeActivity) mContext).user.getPkgEndDate () != null) {
                                if (((ClientHomeActivity) mContext).user.getPkgEndDate ().equals ("0000-00-00")) {
                                    fab.setVisibility (View.GONE);
                                    displayMaterialDialog ("Workout schedule cannot be assigned unless package is purchased.\nDo you wish to buy package?");
                                    return;
                                }
                                //String str_pkgEndDate = ((ClientHomeActivity) mContext).user.getPkgEndDate ();
                                try {
                                    Date pkgEndDate = formatter.parse (str_pkgEndDate);
                                    if (new Date ().after (pkgEndDate)) {
                                        fab.setVisibility (View.GONE);
                                        displayMaterialDialog ("Client's Package is expired. Upgrade plan and allot session to create workout schedule.\nDo you wish to upgrade package?");
                                        return;
                                    } else {
                                        if (isTermApply != null && isRegular != null && isTermApply.equals ("0") && isRegular.equals ("0")) {
                                            fab.setVisibility (View.VISIBLE);

                                            // startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
                                        } else {
                                            if (((ClientHomeActivity) mContext).user.getIsSessionAlloted () != null) {
                                                if (((ClientHomeActivity) mContext).user.getIsSessionAlloted ().equals ("1") || isRegular.equals ("0")) {
                                                    fab.setVisibility (View.VISIBLE);
                                                    // startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
                                                } else {
                                                    Log.e ("in", "eslse");
                                                    fab.setVisibility (View.GONE);
                                                    displayMaterialDialog ("To assign workout, first allot session to user.\nDo you wish to proceed? ");
                                                    return;
                                                }
                                            } else {
                                                fab.setVisibility (View.GONE);
                                                displayMaterialDialog ("To assign workout, first allot session to user.\nDo you wish to proceed? ");
                                                return;
                                            }
                                        }
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace ();
                                }
                            } else {
                                fab.setVisibility (View.VISIBLE);
                                //startActivity (new Intent (mContext, AddClientActivity.class).putExtra ("isAddClient", false).putExtra ("user", ((ClientHomeActivity) mContext).user));
                            }
                        }/*else {
                            Date pkgEndDate = null;
                            try {
                                pkgEndDate = formatter.parse (str_pkgEndDate);
                            } catch (ParseException e) {
                                e.printStackTrace ();
                            }
                            if (new Date ().after (pkgEndDate)) {
                                fab.setVisibility (View.GONE);
                                displayMaterialDialog ("Your Package is expired. Please upgrade plan.\nDo you wish to upgrade package?");
                                return;
                            }
                        }*/
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }
}
