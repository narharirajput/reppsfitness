package com.reppsfitness.client.fragment;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.ChangeSessionActivity;
import com.reppsfitness.R;
import com.reppsfitness.adapter.SessionDaysAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.client.SettingsActivity;
import com.reppsfitness.expandable_list.ExpandableRegularPackage;
import com.reppsfitness.expandable_list.ExpandableTermPackage;
import com.reppsfitness.expandable_list.RegularAdapter;
import com.reppsfitness.expandable_list.TermAdapter;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.Plans;
import com.reppsfitness.model.PlansData;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.ItemOffsetDecoration;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class ClientPackageFragment extends BaseFragment {
    @BindColor(R.color.white)
    int img_bg_color;
    @BindView(R.id.ivCallTo)
    ImageView ivCallTo;
    @BindView(R.id.iv_arrow)
    ImageView iv_arrow;
    @BindView(R.id.imageView2)
    ImageView iv_currentPackage;
    @BindView(R.id.rr_noImage)
    RelativeLayout rrNoImage;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_name)
    CustomTextViewRegular tvName;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.recyclerViewSessionDays)
    RecyclerView recyclerViewSessionDays;
    @BindView(R.id.rv_term)
    RecyclerView rvTerm;
    @BindView(R.id.tv_regular)
    TextView tvRegular;
    @BindView(R.id.tv_term)
    TextView tvTerm;
    @BindView(R.id.tv_start_date)
    TextView tv_start_date;
    @BindView(R.id.tv_end_date)
    TextView tv_end_date;

    @BindView(R.id.tv_session_allotted)
    TextView tv_session_allotted;
    @BindView(R.id.tv_session_attended)
    TextView tv_session_attended;
    @BindView(R.id.ll_currentPackage)
    LinearLayout llCurrentPackage;
    @BindView(R.id.ll_currentPackage_regular)
    LinearLayout ll_currentPackage_regular;
    @BindView(R.id.ll_currentPackage_term)
    LinearLayout ll_currentPackage_term;
    boolean isFromAdmin = false;
    @BindView(R.id.tv_currentPlanTitle)
    TextView tvCurrentPlanTitle;
    @BindView(R.id.tv_current_package)
    TextView tvCurrentPackage;
    @BindView(R.id.tv_currentPlanPrice)
    TextView tvCurrentPlanPrice;
    @BindDrawable(R.drawable.no_image)
    Drawable no_image;
    @BindView(R.id.btn_changeSession)
    Button btnChangeSession;
    @BindView(R.id.vw_divider)
    View vwDivider;
    @BindView(R.id.tv_sessionTime)
    TextView tvSessionTime;
    /*  @BindView(R.id.tv_sessionDays)
      TextView tv_sessionDays;*/
    @BindView(R.id.ll_sessionTime)
    LinearLayout llSessionTime;
    @BindView(R.id.ll_btn_session_details)
    LinearLayout btnSessionDetails;
    @BindView(R.id.ll_packageInfo)
    LinearLayout ll_packageInfo;
    private RegularItem mRegularItem;
    private String sessionTime = "";
    SimpleDateFormat ymdSdf = new SimpleDateFormat ("yyyy-MM-dd");
    SimpleDateFormat dmySdf = new SimpleDateFormat ("dd-MMM-yy");
    SimpleDateFormat dsmsySdf = new SimpleDateFormat ("dd MMM yy");
    private String packageID = "", reqPackageID = "", exDate;
    private Date pkgEndDate;
    Date exDateObj = null;
    private TermItem mTermItem;
    boolean isExpanded = false;
    public static boolean isResume = false;
    SimpleDateFormat spf = new SimpleDateFormat ("yyyy-MM-dd");

//    private String sessionDays;

    //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);
        setRecyclerViewProperties (recyclerView);
        setRecyclerViewProperties (rvTerm);
        recyclerView.addItemDecoration (new ItemOffsetDecoration (getActivity (), R.dimen.size_0));
        recyclerViewSessionDays.setLayoutManager (new GridLayoutManager (mContext, 2));
        rvTerm.addItemDecoration (new ItemOffsetDecoration (getActivity (), R.dimen.size_0));
    }
    /*public static void itemDecor(FragmentActivity activity){
        recyclerView.addItemDecoration (new ItemOffsetDecoration (activity, R.dimen.size_0));
    }*/

    @Override
    public void onResume() {
        super.onResume ();
        if (((ClientHomeActivity) mContext).user != null) {
            isFromAdmin = ((ClientHomeActivity) mContext).isFromAdmin;
            if (!isFromAdmin) ivCallTo.setVisibility (View.GONE);
        }
        btnSessionDetails.setVisibility (View.VISIBLE);  // code by hari.. if package is not purchased
        ll_packageInfo.setVisibility (View.VISIBLE);
//        getClientPackagePlans();
        getClientProfile ();
        //iv_arrow.setBackground (getResources ().getDrawable (R.drawable.ic_keyboard_arrow_down_white_24dp));
        //iv_arrow.getDrawable ().mutate ().setColorFilter (img_bg_color, PorterDuff.Mode.SRC_IN);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_client_package;
    }

    //@OnClick({R.id.rr_noImage, R.id.iv_profile, R.id.tv_name})
    @OnClick({R.id.btn_edit_profile, R.id.btn_session_detail, R.id.iv_arrow})
    public void onViewClicked(View view) {
        int id = view.getId ();
        if (id == R.id.btn_edit_profile) {
            startActivity (new Intent (mContext, SettingsActivity.class).putExtra ("user", ((ClientHomeActivity) mContext).user));
        }
        if (id == R.id.btn_session_detail || id == R.id.iv_arrow) {
            if (isExpanded) {
                Utils.setFadeVisible (llCurrentPackage, false);
                iv_arrow.setBackground (getResources ().getDrawable (R.drawable.ic_keyboard_arrow_down_white_24dp));
                llCurrentPackage.setVisibility (View.GONE);
            } else {
                Utils.setFadeVisible (llCurrentPackage, true);
                iv_arrow.setBackground (getResources ().getDrawable (R.drawable.ic_keyboard_arrow_up_white_24dp));
                llCurrentPackage.setVisibility (View.VISIBLE);
            }
            isExpanded = !isExpanded; // reverse
        }
    }

    @OnClick({R.id.ivCallTo})
    public void onImageViewClicked(View view) {
        mContext.startActivity (new Intent (Intent.ACTION_DIAL, Uri.parse ("tel:" + ((ClientHomeActivity) mContext).user.getClientMobile ())));
    }

    @OnClick(R.id.btn_changeSession)
    public void onViewClicked() {
        if (isFromAdmin) {
            if (btnChangeSession.getText ().toString ().equalsIgnoreCase ("Allot session")) {
                Intent intent = new Intent (mContext, ChangeSessionActivity.class);
                intent.putExtra ("regularItem", mRegularItem);
                intent.putExtra ("user", ((ClientHomeActivity) mContext).user);
                intent.putExtra ("packageID", packageID);
                startActivity (intent);
            } else {
                Intent intent = new Intent (mContext, ChangeSessionActivity.class);
                intent.putExtra ("regularItem", mRegularItem);
                intent.putExtra ("user", ((ClientHomeActivity) mContext).user);
                intent.putExtra ("sessionTime", sessionTime);
                intent.putExtra ("packageID", packageID);
//                intent.putExtra("sessionDays", sessionDays);
                startActivity (intent);
            }
        }
    }

    private void setCurrentPackage(Object currentPackage) {
        if (currentPackage != null) {
            if (currentPackage instanceof RegularItem) {
                RegularItem regularItem = (RegularItem) currentPackage;
                //llCurrentPackage.setVisibility (View.VISIBLE);
                llCurrentPackage.setVisibility (View.VISIBLE);
                ll_currentPackage_regular.setVisibility (View.VISIBLE);
                ll_currentPackage_term.setVisibility (View.GONE);
                tvCurrentPlanTitle.setText (regularItem.getPackageName ());
                tvCurrentPlanPrice.setText ("₹ " + regularItem.getPackageAmount ());
                Glide.with (mContext).load (regularItem.getPackageImage ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (iv_currentPackage);

                if (pkgEndDate != null && exDateObj != null) {
                    if (new Date ().before (exDateObj)) {
                        tvCurrentPlanPrice.setText ("₹ " + regularItem.getPackageAmount ());
                    } else {
                        tvCurrentPlanPrice.setText ("EXPIRED");
                        tvCurrentPlanPrice.setTextColor (getResources ().getColor (R.color.yellow));
                    }
                } else {
                    btnSessionDetails.setVisibility (View.INVISIBLE);
                }
            } else if (currentPackage instanceof TermItem) {
                TermItem termItem = (TermItem) currentPackage;
                llCurrentPackage.setVisibility (View.VISIBLE);
                ll_currentPackage_regular.setVisibility (View.GONE);
                ll_currentPackage_term.setVisibility (View.VISIBLE);
                tvCurrentPlanTitle.setText (termItem.getPackageName ());
                Glide.with (mContext).load (termItem.getPackageImage ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (iv_currentPackage);

                if (pkgEndDate != null && exDateObj != null) {
                    if (new Date ().before (exDateObj)) {
                        tvCurrentPlanPrice.setText ("₹ " + termItem.getPackageAmount ());
                    } else {
                        tvCurrentPlanPrice.setText ("EXPIRED");
                        tvCurrentPlanPrice.setTextColor (getResources ().getColor (R.color.yellow));
                    }
                } else {
                    btnSessionDetails.setVisibility (View.INVISIBLE);
                }
                tvCurrentPlanPrice.setText ("₹ " + termItem.getPackageAmount ());
            }
        } else {
            llCurrentPackage.setVisibility (View.INVISIBLE);
            ll_currentPackage_regular.setVisibility (View.GONE);
            ll_currentPackage_term.setVisibility (View.GONE);
            btnSessionDetails.setVisibility (View.INVISIBLE);
        }
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<> ();
        map.put ("xAction", "getClientProfile");
        map.put ("clientID", ((ClientHomeActivity) mContext).user.getClientID ());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getClientProfile (map, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();

                    ClientProfileData data = (ClientProfileData) object;
                    Log.e (TAG, "onSuccess: " + data);
                    if (data != null && data.getCount () > 0) {
                        ClientProfile profile = data.getClientProfile ();
                        Log.e (TAG, "profile: " + profile);
                        String str_pkgEndDate = profile.getPkgEndDate ();
                        String str_pkgStartDate = profile.getPkgStartDate ();
                        Gson gson = new Gson ();
                        String tmp = gson.toJson (profile);
                        Log.e (TAG, "onSuccess: " + tmp);
                        ClientHomeActivity.clientHomeActivity.user = gson.fromJson (tmp, User.class);
                        tvName.setText (((ClientHomeActivity) mContext).user.getClientFName () + " " + ((ClientHomeActivity) mContext).user.getClientLName ());
                        if (((ClientHomeActivity) mContext).user.getClientProfilePic () != null) {
                            Glide.with (mContext).load (((ClientHomeActivity) mContext).user.getClientProfilePic ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
                            rrNoImage.setVisibility (View.GONE);
                            ivProfile.setVisibility (View.VISIBLE);
                        } else {
                            rrNoImage.setVisibility (View.VISIBLE);
                            ivProfile.setVisibility (View.GONE);
                        }
                        btnChangeSession.setVisibility (isFromAdmin ? View.VISIBLE : View.GONE);
                        if (str_pkgEndDate != null && !str_pkgEndDate.equals ("0000-00-00")) {
                            try {
                                Date pkgStartDate = ymdSdf.parse (str_pkgStartDate);
                                String startDate = dsmsySdf.format (pkgStartDate);
                                pkgEndDate = ymdSdf.parse (str_pkgEndDate);
                                String endDate = dsmsySdf.format (pkgEndDate);
                                if (!TextUtils.isEmpty (startDate) && !TextUtils.isEmpty (endDate)) {
                                    if (profile.getIsTermApply ().equals ("0") && profile.getIsRegular ().equals ("0"))  //  code by ajit
                                        tvCurrentPackage.setText (startDate);
                                    else tvCurrentPackage.setText (startDate + " - " + endDate);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace ();
                            }
                        }
//                        if (profile.getSessionAllottedTime() != null &&
//                                profile.getIsSessionAllotted() != null && profile.getIsSessionAllotted().equals("1")) {
//                            sessionTime = profile.getSessionAllottedTime();
//                            tvSessionTime.setText(sessionTime);
//                        } else {
//                            tvSessionTime.setText(" - ");
//                        }
//                        if (profile.getSessionDays() != null && !TextUtils.isEmpty(profile.getSessionDays())) {
//                            sessionDays = profile.getSessionDays();
//                            tv_sessionDays.setText(sessionDays.toUpperCase());
//                            tv_sessionDays.setVisibility(View.VISIBLE);
//                        } else {
//                            tv_sessionDays.setVisibility(View.GONE);
//                        }
                        if (profile.getPackageID () != null && !TextUtils.isEmpty (profile.getPackageID ()) && !profile.getPackageID ().equals ("0")) {
                            packageID = profile.getPackageID ();

                            getClientPackagePlans ();
                        } else {
                            getClientPackagePlans ();
                        }
                        if (profile.getIsRegular () != null && profile.getIsTermApply () != null && !TextUtils.isEmpty (profile.getIsRegular ()) && !TextUtils.isEmpty (profile.getIsTermApply ())) {
                            Date newDate = null;
                            try {
                                newDate = spf.parse (profile.getPkgEndDate ());
                                spf = new SimpleDateFormat ("dd/MM/yyyy");
                                exDate = spf.format (newDate);
                                exDateObj = Utils.curFormater.parse (exDate);
                            } catch (ParseException e) {
                                e.printStackTrace ();
                            }

                            if (profile.getIsRegular ().equals ("1")) { // || profile.getIsTermApply().equals("1")
                                if (profile.getClientSessionDays () != null && !TextUtils.isEmpty (profile.getClientSessionDays ())) {
                                    String sessionDays = profile.getClientSessionDays ();
                                    Log.e (TAG, "onSuccess: " + sessionDays);
                                   /* tv_sessionDays.setText (sessionDays.toUpperCase ());
                                    tv_sessionDays.setVisibility (View.VISIBLE);*/
                                    ll_currentPackage_regular.setVisibility (View.VISIBLE);
                                    ll_currentPackage_term.setVisibility (View.GONE);
                                    String[] days = sessionDays.split (",");
                                    recyclerViewSessionDays.setAdapter (new SessionDaysAdapter (mContext, days));
                                    //recyclerViewSessionDays.setVisibility (View.VISIBLE);
                                    //tv_sessionDays.setVisibility (View.GONE);
                                    //ll_currentPackage_regular.setVisibility (View.VISIBLE);
                                    //ll_currentPackage_term.setVisibility (View.GONE);
                                } else {
                                    ll_currentPackage_regular.setVisibility (View.GONE);

                                    //tv_sessionDays.setVisibility (View.GONE);
                                    //recyclerViewSessionDays.setVisibility (View.GONE);
                                    //ll_currentPackage_regular.setVisibility (View.GONE);
                                    //ll_currentPackage_term.setVisibility (View.VISIBLE);
                                }
                                if (profile.getPackageID () != null && !TextUtils.isEmpty (profile.getPackageID ()) && !profile.getPackageID ().equals ("0")) {
                                    if (app.getPreferences ().isClient ()) {
                                        btnChangeSession.setVisibility (View.GONE);
                                    } else {
                                        if (profile.getIsSessionAllotted () != null && profile.getIsSessionAllotted ().equals ("1")) {
                                            btnChangeSession.setText ("Change session");
                                            if (new Date ().before (exDateObj)) {
                                                btnChangeSession.setVisibility (View.VISIBLE);
                                            } else {
                                                btnChangeSession.setVisibility (View.GONE);
                                            }
                                        } else {
                                            //btnChangeSession.setVisibility (View.VISIBLE);
                                            btnChangeSession.setText ("Allot session");
                                            tvCurrentPackage.setText ("Session not allotted");
                                            recyclerViewSessionDays.setVisibility (View.GONE);
                                            if (new Date ().before (exDateObj)) {
                                                btnChangeSession.setVisibility (View.VISIBLE);
                                            } else {
                                                btnChangeSession.setVisibility (View.GONE);
                                            }
                                        }
                                    }
                                }
                            } else //if (profile.getIsTermApply().equals("0") && profile.getIsRegular().equals("0")) {
                            {
                                btnChangeSession.setVisibility (View.GONE);
                                if (profile.getTotalSessionRemainning () != null && profile.getNoOfSessionAllotted () != null) {
                                    //recyclerViewSessionDays.setVisibility (View.GONE);
                                    ll_currentPackage_regular.setVisibility (View.GONE);
                                    ll_currentPackage_term.setVisibility (View.VISIBLE);
                                    //int attended = Integer.parseInt (profile.getTotalSessionAllotted ()) - Integer.parseInt (profile.getTotalSessionRemainning ());
                                    String startDate = "";
                                    String endDate = "";

                                    Date pkgStartDate = null;
                                    Date pkgEndDate = null;
                                    try {
                                        pkgStartDate = ymdSdf.parse (str_pkgStartDate);
                                        startDate = dsmsySdf.format (pkgStartDate);
                                        pkgEndDate = ymdSdf.parse (str_pkgEndDate);
                                        endDate = dsmsySdf.format (pkgEndDate);
                                    } catch (ParseException e) {
                                        e.printStackTrace ();
                                    }

                                    tv_session_attended.setText ("Session attended : " + profile.getTotalSessionAttended ());
                                    tv_session_allotted.setText ("Session allotted   : " + profile.getTotalSessionAllotted ());
                                    tv_start_date.setText ("Start date : " + startDate);
                                    if (profile.getIsTermApply ().equals ("1"))
                                        tv_end_date.setText ("End date   : " + endDate);
                                    else tv_end_date.setVisibility (View.GONE);
                                   /* tv_sessionDays.setText ("Session: " + profile.getTotalSessionRemainning () + "/" + profile.getTotalSessionAllotted ());
                                    tv_sessionDays.setVisibility (View.VISIBLE);*/
                                }
                            }
                            //Commented by Swapnil
//                            if (profile.getIsTermApply().equals("1") && profile.getIsRegular().equals("0") && profile.getPackageID().equals("5")) {
//                                Log.e(TAG, "onSuccess5: ");
//                                btnChangeSession.setVisibility(View.GONE);//code by ajit
//                            }
                        }
//                        if (profile.getClientSessionDays() != null && profile.getClientSessionDays().size() > 0 &&
//                                profile.getClientSessionTimes() != null && profile.getClientSessionTimes().size() > 0 &&
//                                profile.getClientSessionDays().size() == profile.getClientSessionTimes().size()) {
//                            for (int i = 0; i < profile.getClientSessionDays().size(); i++) {
//                            }
//                            for (int j = 0; j < profile.getClientSessionTimes().size(); j++) {
//                            }
//                        }
                        if (profile.getReqPackageID () != null && !TextUtils.isEmpty (profile.getReqPackageID ())) {
                            reqPackageID = profile.getReqPackageID ();
                        }

                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void setRecyclerViewProperties(RecyclerView recyclerView) {
        recyclerView.setLayoutManager (new LinearLayoutManager (mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled (false);
        //recyclerView.addItemDecoration (new ItemOffsetDecoration (getActivity (), R.dimen.size_1));
    }

    public void getClientPackagePlans() {
        Map<String, String> params = new HashMap<> ();
//        params.put("xAction", "getClientPackagePlans");
        params.put ("xAction", "getPackagePlans");
//        params.put("clientID", ((ClientHomeActivity) mContext).user.getClientID());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            Log.e (TAG, "getClientPackagePlans: ");
            pd.show ();
            app.getApiRequestHelper ().packages (params, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    tvRegular.setText ("Regular packages");
                    tvRegular.setVisibility (View.VISIBLE);
                    PlansData plansData = (PlansData) object;
                    Log.e (TAG, "onSuccess: " + object);
                    if (plansData != null) {
                        if (plansData.getCount () > 0) {
                            if (plansData.getData () != null) {
                                Plans data = plansData.getData ();
                                Log.e (TAG, "onSuccess: " + data);
                                List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<> ();
                                List<ExpandableTermPackage> expandableTermPackages = new ArrayList<> ();
                                List<RegularItem> listPackages = data.getRegular ();
                                List<TermItem> listPackagesterm = data.getTerm ();
                                boolean isAnyPackageBuy = false, isAnyPackageRequested = false;
                                if (listPackages != null && listPackages.size () > 0) {
                                    for (int i = 0; i < listPackages.size (); i++) {
                                        RegularItem regularItem = listPackages.get (i);
                                        if (regularItem.getPackageID ().equals (packageID)) {
                                            isAnyPackageBuy = true;
                                            mRegularItem = regularItem;
                                            Log.e (TAG, "onSuccess: " + regularItem);
                                            setCurrentPackage (regularItem);
                                        }
                                        if (!TextUtils.isEmpty (reqPackageID) && regularItem.getPackageID ().equals (reqPackageID)) {
                                            isAnyPackageRequested = true;
                                        }
                                        List<RegularItem> regularItemList = new ArrayList<> ();
                                        Log.e (TAG, "onSuccess: " + regularItemList);
                                        regularItemList.add (regularItem);
                                        regularPackageArrayList.add (new ExpandableRegularPackage (regularItem.getPackageName (), regularItem.getPackageAmount (), regularItemList, regularItem.getPackageImage (), regularItem.getPackageID ()));
                                    }
                                }
                                boolean isAnyTermBuy = false, isAnyTermRequested = false;
                                if (listPackagesterm != null && listPackagesterm.size () > 0) {
                                    tvTerm.setText ("Terms packages");
                                    tvTerm.setVisibility (View.VISIBLE);
                                    for (int j = 0; j < listPackagesterm.size (); j++) {
                                        TermItem termItem = listPackagesterm.get (j);
//                                        List<TermItem> termItemList = new ArrayList<TermItem>();
//                                        termItemList.add(termItem);
//                                        expandableTermPackages.add(new ExpandableTermPackage(termItem.getPackageName(), termItem.getPackageAmount(), termItemList));
                                        if (termItem.getPackageID ().equals (packageID)) {
                                            isAnyTermBuy = true;
                                            mTermItem = termItem;
                                            setCurrentPackage (mTermItem);
                                        }
                                        if (!TextUtils.isEmpty (reqPackageID) && termItem.getPackageID ().equals (reqPackageID)) {
                                            isAnyTermRequested = true;
                                        }
                                        List<TermItem> termItemList = new ArrayList<> ();
                                        termItemList.add (termItem);
                                        expandableTermPackages.add (new ExpandableTermPackage (termItem.getPackageName (), termItem.getPackageAmount (), termItemList,termItem.getPackageImage (), termItem.getPackageID ()));
                                    }

                                    if (expandableTermPackages.size () > 0) {
//                                        rvTerm.setAdapter(new TermAdapter(ClientPackageFragment.this, expandableTermPackages, isFromAdmin, isAnyTermBuy));
                                        rvTerm.setNestedScrollingEnabled (false);
                                        rvTerm.setVisibility (View.VISIBLE);
                                        tvTerm.setText ("Terms packages");
                                    } else {
                                        rvTerm.setVisibility (View.GONE);
                                        tvTerm.setText ("");
                                    }
                                }
                                int isTermPkgBuy = -1;
                                if (isAnyPackageBuy || isAnyPackageRequested) {
                                    isTermPkgBuy = 0;
                                } else if (isAnyTermBuy || isAnyTermRequested) {
                                    isTermPkgBuy = 1;
                                }
                                if (isTermPkgBuy == -1 || (!isAnyPackageBuy && isAnyPackageRequested) || (!isAnyTermBuy && isAnyTermRequested)) {
                                    btnSessionDetails.setVisibility (View.INVISIBLE);  // code by hari.. if package is not purchased
                                    ll_packageInfo.setVisibility (View.INVISIBLE);
                                }
                                if (listPackages != null && listPackages.size () > 0) {
                                    recyclerView.setAdapter (new RegularAdapter (ClientPackageFragment.this, regularPackageArrayList, isAnyPackageBuy, packageID, pkgEndDate, reqPackageID, isAnyPackageRequested, isTermPkgBuy, isAnyTermBuy, isAnyTermRequested));
                                }
                                if (expandableTermPackages.size () > 0) {
                                    rvTerm.setAdapter (new TermAdapter (ClientPackageFragment.this, expandableTermPackages, isAnyTermBuy, packageID, pkgEndDate, reqPackageID, isAnyTermRequested, isTermPkgBuy, isAnyPackageBuy, isAnyPackageRequested));
                                }
//                                btnChangeSession.setText(data.getIsSessionAlloted() == 0 ? "Allot session" :
//                                        "CHANGE SESSION");
//                                if (data.getIsSessionAlloted() == 1 && data.getSessionTime() != null && !app.getPreferences().isClient()) {
//                                    llSessionTime.setVisibility(View.VISIBLE);
//                                    vwDivider.setVisibility(View.VISIBLE);
//                                    sessionTime = data.getSessionTime();
//                                    tvSessionTime.setText(sessionTime);
//                                } else if (data.getIsSessionAlloted() == 0 && !app.getPreferences().isClient()) {
//                                    llSessionTime.setVisibility(View.VISIBLE);
//                                    vwDivider.setVisibility(View.VISIBLE);
//                                    tvSessionTime.setText("-");
//                                }
                            } else {
                                if (plansData.getMsg () != null && !TextUtils.isEmpty (plansData.getMsg ())) {
                                    Utils.showLongToast (mContext, plansData.getMsg ());
                                }
                            }
                        } else {
                            if (plansData.getMsg () != null && !TextUtils.isEmpty (plansData.getMsg ())) {
                                Utils.showLongToast (mContext, plansData.getMsg ());
                            } else {
                                Utils.showLongToast (mContext, "Unable to get Packages/Plans. Please try again...");
                            }
                        }
                    } else {
                        Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Log.e ("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }
}
