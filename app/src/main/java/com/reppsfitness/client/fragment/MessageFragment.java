package com.reppsfitness.client.fragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.adapter.MessageAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.Notification;
import com.reppsfitness.model.NotificationData;
import com.reppsfitness.utils.RecyclerItemLongClickListener;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

import static android.content.ContentValues.TAG;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class MessageFragment extends BaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView tv_error;
    ArrayList<Notification> multiselect_list = new ArrayList<>();
    boolean isMultiSelect = false;
    private ArrayList<Notification> notificationList;
    private MessageAdapter messageAdapter;

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.addOnItemTouchListener(new RecyclerItemLongClickListener(mContext, recyclerView, new RecyclerItemLongClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.e("in", "onItemClick");
//                if (isMultiSelect)
                multi_select(position);
            }

            @Override
            public void onItemLongClick(View view, int position) {
                Log.e("in", "onItemLongClick");
            }
        }));

        if (((ClientHomeActivity) mContext).actionDelete != null) {
            ((ClientHomeActivity) mContext).actionDelete.setOnMenuItemClickListener(menuItem -> {
                List<String> notificationIds = new ArrayList<>();
                for (int i = 0; i < multiselect_list.size(); i++) {
                    notificationIds.add(multiselect_list.get(i).getNotificationID());
                }
                String str_notificationIDs = notificationIds.toString();
                str_notificationIDs = str_notificationIDs.replace("[", "");
                str_notificationIDs = str_notificationIDs.replace("]", "");
                clearNotifications(str_notificationIDs);
                Log.e("multiselect_list", new Gson().toJson(multiselect_list));
                return super.onOptionsItemSelected(((ClientHomeActivity) mContext).actionDelete);
            });

        }
    }

    public void multi_select(int position) {
        if (multiselect_list.contains(notificationList.get(position)))
            multiselect_list.remove(notificationList.get(position));
        else
            multiselect_list.add(notificationList.get(position));
        if (multiselect_list.size() > 0 && ((ClientHomeActivity) mContext).actionDelete != null) {
            ((ClientHomeActivity) mContext).actionDelete.setVisible(true);
        } else {
            ((ClientHomeActivity) mContext).actionDelete.setVisible(false);
            isMultiSelect = false;
        }
        refreshAdapter();
    }

    public void refreshAdapter() {
        messageAdapter.selected_notificationList = multiselect_list;
        messageAdapter.notificationList = notificationList;
        messageAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        Log.e(TAG, "onResume: ");
        super.onResume();
        getNotificationList();
    }

    private void getNotificationList() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getNotificationList");
        params.put("receiverType", "2");
        params.put("receiverID", ((ClientHomeActivity) mContext).user.getClientID());
        Log.e(TAG, "getNotificationList: "+params );
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getNotificationList(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "success");
                    NotificationData notificationData = (NotificationData) object;
                    if (notificationData != null) {
                        if (notificationData.getCount() > 0 && notificationData.getData().size() > 0) {
                            notificationList = notificationData.getData();
                            messageAdapter = new MessageAdapter(mContext, notificationList);
                            recyclerView.setAdapter(messageAdapter);
                            tv_error.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            if (notificationData.getMsg() != null && !TextUtils.isEmpty(notificationData.getMsg())) {
                                tv_error.setText(notificationData.getMsg());
                            }
                            tv_error.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        tv_error.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void clearNotifications(String notificationIDs) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "clearNotifications");
        params.put("notificationID", notificationIDs);
        //params.put("clientID", ((ClientHomeActivity) mContext).user.getClientID());  //removed as discuss with backend person
        Log.e(TAG, "getNotificationList: "+params );
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().clearNotifications(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "success");
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            getNotificationList();
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }
}