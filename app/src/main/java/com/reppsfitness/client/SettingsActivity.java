package com.reppsfitness.client;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData;
import com.reppsfitness.AddClientPackageActivity;
import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewMedium;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class SettingsActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.ivCallTo)
    ImageView ivCallTo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    @BindView(R.id.ll_toolbar)
    LinearLayout llToolbar;
    @BindView(R.id.rr_noImage)
    RelativeLayout rrNoImage;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_addPhoto)
    CustomTextViewMedium tvAddPhoto;
    @BindDrawable(R.drawable.no_image)
    Drawable no_image;
    private String dob;
    public User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        toolbarTitle.setText ("Settings");
        if (app.getPreferences ().isClient ()) {
            if (app.getPreferences ().getLoggedInUser ().getData () != null) {
                User data = app.getPreferences ().getLoggedInUser ().getData ();
                if (data.getClientProfilePic () != null) {
                    Glide.with (mContext).load (data.getClientProfilePic ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
                    rrNoImage.setVisibility (View.GONE);
                    ivProfile.setVisibility (View.VISIBLE);
                } else {
                    rrNoImage.setVisibility (View.VISIBLE);
                    ivProfile.setVisibility (View.GONE);
                }
                ivCallTo.setVisibility (View.GONE);
                etFirstName.setText (data.getClientFName ());
                etLastName.setText (data.getClientLName ());
                etEmail.setText (data.getClientEmail ());
                if (data.getClientDOB () != null && !TextUtils.isEmpty (data.getClientDOB ()))
                    etDob.setText (Utils.ymdTodmy (data.getClientDOB ()));
                etMobile.setText (data.getClientMobile ());
                dob = data.getClientDOB ();
                if (data.getClientGender ().equals ("1")) {
                    rbMale.setChecked (true);
                } else if (data.getClientGender ().equals ("2")) {
                    rbFemale.setChecked (true);
                }
            }
        } else {
            user = getIntent ().getParcelableExtra ("user");
            if (user.getClientProfilePic () != null) {
                Glide.with (mContext).load (user.getClientProfilePic ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
                rrNoImage.setVisibility (View.GONE);
                ivProfile.setVisibility (View.VISIBLE);
            } else {
                rrNoImage.setVisibility (View.VISIBLE);
                ivProfile.setVisibility (View.GONE);
            }
            etFirstName.setText (user.getClientFName ());
            etLastName.setText (user.getClientLName ());
            etEmail.setText (user.getClientEmail ());
            etDob.setText (Utils.ymdTodmy (user.getClientDOB ()));
            etMobile.setText (user.getClientMobile ());
            dob = user.getClientDOB ();
            if (user.getClientGender ().equals ("1")) {
                rbMale.setChecked (true);
            } else if (user.getClientGender ().equals ("2")) {
                rbFemale.setChecked (true);
            }
        }
    }

    @OnClick({R.id.et_dob, R.id.btn_changePass, R.id.btn_save, R.id.rr_noImage, R.id.iv_profile, R.id.tv_addPhoto, R.id.ivCallTo})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.et_dob:
                Calendar dobCal = Calendar.getInstance ();
                DatePickerDialog dpd = DatePickerDialog.newInstance ((view12, year, monthOfYear, dayOfMonth) -> {
                    String date = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                    etDob.setText (date);
                }, dobCal.get (Calendar.YEAR), dobCal.get (Calendar.MONTH), dobCal.get (Calendar.DAY_OF_MONTH));
                Calendar mMaxDate = Calendar.getInstance ();
                int mMaxYear = mMaxDate.get (Calendar.YEAR);
                mMaxYear = mMaxYear - 18;
                mMaxDate.set (Calendar.YEAR, mMaxYear);
                dpd.setMaxDate (mMaxDate);
                dpd.show (getFragmentManager (), "Datepickerdialog");
                break;
//            case R.id.et_joinDate:
//                Calendar cal = Calendar.getInstance();
//                DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
//                            String date = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
//                            joinDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
//                            etJoinDate.setText(date);
//                        },
//                        cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
//                ).show(getFragmentManager(), "Datepickerdialog");
//                break;
            case R.id.btn_changePass:
                Intent intent = new Intent (mContext, ChangePasswordActivity.class);
                intent.putExtra ("user", user);
                intent.putExtra ("isAdminProfile", false);
                startActivity (intent);
                break;
            case R.id.rr_noImage:
                selectPhoto ();
                break;
            case R.id.iv_profile:
                selectPhoto ();
                break;
            case R.id.ivCallTo:
                mContext.startActivity (new Intent (Intent.ACTION_DIAL, Uri.parse ("tel:" + user.getClientMobile ())));
                break;
            case R.id.tv_addPhoto:
                selectPhoto ();
                break;
            case R.id.btn_save:
                String first_name = etFirstName.getText ().toString ().trim ();
                String last_name = etLastName.getText ().toString ().trim ();
                String email = etEmail.getText ().toString ().trim ();
                String mobile = etMobile.getText ().toString ().trim ();
                String str_dob = etDob.getText ().toString ().trim ();
                if (TextUtils.isEmpty (first_name)) {
                    etFirstName.setError ("Enter First Name");
                    etFirstName.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (last_name)) {
                    etLastName.setError ("Enter Last Name");
                    etLastName.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (email)) {
                    etEmail.setError ("Enter Email");
                    etEmail.requestFocus ();
                    return;
                }
                if (!Utils.isValidEmail (email)) {
                    etEmail.setError ("Enter Valid Email");
                    etEmail.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (mobile)) {
                    etMobile.setError ("Enter Mobile Number");
                    etMobile.requestFocus ();
                    return;
                }
                if (!android.util.Patterns.PHONE.matcher (mobile).matches () || mobile.length () != 10) {
                    etMobile.setError ("Enter Valid Mobile no");
                    etMobile.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (str_dob)) {
                    etDob.setError ("Select Date of Birth");
                    etDob.requestFocus ();
                    return;
                }
                if (rgGender.getCheckedRadioButtonId () == -1) {
                    Utils.showLongToast (mContext, "Select Gender");
                    return;
                }
                Map<String, String> params = new HashMap<> ();
                params.put ("xAction", "updateClientProfile");
                params.put ("clientFName", first_name);
                params.put ("clientLName", last_name);
                params.put ("clientMobile", mobile);
//                params.put("clientEmail", email);
                params.put ("clientDOB", dob);
                params.put ("clientGender", rbMale.isChecked () ? "1" : "2");
                if (app.getPreferences ().isClient ()) {
                    if (app.getPreferences ().getLoggedInUser ().getData ().getUserID () != null)
                        params.put ("clientID", "" + app.getPreferences ().getLoggedInUser ().getData ().getUserID ());
                    else
                        params.put ("clientID", "" + app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
                } else {
                    params.put ("clientID", "" + user.getClientID ());
                    params.put ("userID", "" + app.getPreferences ().getLoggedInUser ().getData ().getUserID ());
                }
                if (cd.isConnectingToInternet ()) {
                    CustomProgressDialog pd = new CustomProgressDialog (mContext);
                    pd.show ();
                    app.getApiRequestHelper ().updateClientProfile (params, new ApiRequestHelper.OnRequestComplete () {
                        @Override
                        public void onSuccess(Object object) {
                            if (pd.isShowing ()) pd.dismiss ();
                            LoginResponse response = (LoginResponse) object;
                            if (response != null) {
                                if (response.getCount () > 0) {
                                    if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                        Utils.showLongToast (mContext, response.getMsg ());
                                    }
                                    if (app.getPreferences ().isClient ()) {
                                        app.getPreferences ().setLoggedInUser (response);
//                                        if (response.getData() != null && response.getData().getUserType() != null &&
//                                                response.getData().getUserType().equals("2")) {
                                        app.getPreferences ().setClient (true);
                                        User data = response.getData ();
                                        data.setUserID (data.getClientID ());
                                        response.setData (data);
                                        app.getPreferences ().setLoggedInUser (response);
//                                        }
                                    } else {
                                        if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing ()) {
                                            Gson gson = new Gson ();
                                            String tmp = gson.toJson (response.getData ());
                                            ClientHomeActivity.clientHomeActivity.user = gson.fromJson (tmp, User.class);
                                        }
                                        if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing ()) {
                                            Gson gson = new Gson ();
                                            String tmp = gson.toJson (response.getData ());
                                            AddClientPackageActivity.addClientPackageActivity.user = gson.fromJson (tmp, User.class);
                                        }
                                    }
                                    finish ();
                                } else {
                                    if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                        Utils.showLongToast (mContext, response.getMsg ());
                                    }
                                }
                            } else {
                                Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                            }
                        }

                        @Override
                        public void onFailure(String apiResponse) {
                            if (pd.isShowing ()) pd.dismiss ();
                            Utils.showLongToast (mContext, apiResponse);
                        }
                    });
                } else {
                    Utils.alert_dialog (mContext);
                }
                break;
        }
    }

    private void selectPhoto() {
        new MaterialDialog.Builder (mContext).items ("Capture from Camera", "Select from Gallery").itemsCallback ((dialog, view1, which, text) -> {
            if (which == 0) {
                RxPaparazzo.single (SettingsActivity.this).usingCamera ().subscribeOn (Schedulers.io ()).observeOn (AndroidSchedulers.mainThread ()).subscribe (response -> {
                    // See response.resultCode() doc
                    if (response.resultCode () != RESULT_OK) {
                        return;
                    }
                    FileData data = response.data ();
                    uploadProfilePicture (data.getFile ());
                });
            } else if (which == 1) {
                RxPaparazzo.single (SettingsActivity.this).usingGallery ().subscribeOn (Schedulers.io ()).observeOn (AndroidSchedulers.mainThread ()).subscribe (response -> {
                    // See response.resultCode() doc
                    if (response.resultCode () != RESULT_OK) {
                        return;
                    }
                    FileData data = response.data ();
                    uploadProfilePicture (data.getFile ());
                });
            }
        }).show ();
    }

    private void uploadProfilePicture(File file) {
        MultipartBody.Builder builder = new MultipartBody.Builder ();
        builder.setType (MultipartBody.FORM);
        builder.addFormDataPart ("xAction", "uploadProfilePicture");
        builder.addFormDataPart ("userType", "2");
        if (app.getPreferences ().isClient ())
            builder.addFormDataPart ("userID", app.getPreferences ().getLoggedInUser ().getData ().getUserID ());
        else builder.addFormDataPart ("userID", "" + user.getClientID ());
        builder.addFormDataPart ("userImage", file.getName (), RequestBody.create (MediaType.parse ("image/png"), file));
        MultipartBody multipartBody = builder.build ();
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().uploadProfilePicture (multipartBody, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    LoginResponse userData = (LoginResponse) object;
                    if (userData != null) {
                        if (userData.getCount () > 0) {
                            if (userData.getMsg () != null && !TextUtils.isEmpty (userData.getMsg ())) {
                                Utils.showLongToast (mContext, userData.getMsg ());
                            }
                            rrNoImage.setVisibility (View.GONE);
                            ivProfile.setVisibility (View.VISIBLE);
                            Glide.with (mContext).load (file).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
                            //Updating profile image in the preferences
                            if (app.getPreferences ().isClient ()) {
                                if (userData.getData ().getUserImage () != null) {
                                    LoginResponse loggedInUser = app.getPreferences ().getLoggedInUser ();
                                    User data = loggedInUser.getData ();
                                    data.setClientProfilePic (userData.getData ().getUserImage ());
                                    loggedInUser.setData (data);
                                    app.getPreferences ().setLoggedInUser (loggedInUser);
                                }
                            } else {
                                if (userData.getData ().getUserImage () != null) {
                                    if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing ()) {
                                        ClientHomeActivity.clientHomeActivity.user.setClientProfilePic (userData.getData ().getUserImage ());
                                    }
                                    if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing ()) {
                                        AddClientPackageActivity.addClientPackageActivity.user.setClientProfilePic (userData.getData ().getUserImage ());
                                    }
                                }
                            }
                        } else {
                            if (userData.getMsg () != null && !TextUtils.isEmpty (userData.getMsg ())) {
                                Utils.showLongToast (mContext, userData.getMsg ());
                            }
                        }
                    } else {
                        Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
//                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast (mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case android.R.id.home:
                finish ();
                break;
        }
        return true;
    }
}
