package com.reppsfitness.client;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reppsfitness.BaseActivity;
import com.reppsfitness.LoginActivity;
import com.reppsfitness.R;
import com.reppsfitness.RequestSessionActivity;
import com.reppsfitness.client.fragment.ClientPackageFragment;
import com.reppsfitness.client.fragment.ClientScheduleListFragment;
import com.reppsfitness.client.fragment.MessageFragment;
import com.reppsfitness.fragment.BMIFragment;
import com.reppsfitness.fragment.ClientListFragment;
import com.reppsfitness.model.User;
import com.reppsfitness.session_cal.ClientSessionsFragment;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;

public class ClientHomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.btn_workoutPlan)
    Button btn_workoutPlan;
    @BindView(R.id.btn_notifications)
    Button btn_notifications;
    @BindView(R.id.btn_bmi)
    Button btn_bmi;
    @BindView(R.id.btn_paymentPlans)
    Button btn_paymentPlans;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    public boolean isFromAdmin;
    public User user;

    @BindDrawable(R.drawable.no_image)
    Drawable no_image;
    public static ClientHomeActivity clientHomeActivity;
    @BindView(R.id.ll_tabs)
    LinearLayout llTabs;
    public MenuItem actionDelete;

    public static final String TAG = "tag";
    public int workoutPlanFlag, messageFlag, bmiFlag, packageFlag;
    Fragment fragmentA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        clientHomeActivity = this;
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        isFromAdmin = getIntent ().getBooleanExtra ("isFromAdmin", false);
        workoutPlanFlag = 1;
        btnBackground ();
        if (isFromAdmin) {
            getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
            getSupportActionBar ().setHomeAsUpIndicator (R.drawable.ic_arrow_back_white_24dp);
            user = getIntent ().getParcelableExtra ("user");
            navigationView.setVisibility (View.GONE);
            drawer.setDrawerLockMode (DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//            drawerToggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//            drawerToggle.setDrawerIndicatorEnabled(false);
//            drawerToggle.syncState();
        } else {
            getSupportActionBar ().setDisplayHomeAsUpEnabled (false);
            getSupportActionBar ().setHomeAsUpIndicator (R.drawable.ic_menu_white_24dp);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle (this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener (toggle);
            toggle.syncState ();
            navigationView.setNavigationItemSelectedListener (this);
            try {
                user = app.getPreferences ().getLoggedInUser ().getData ();
            } catch (Exception e) {
                e.printStackTrace ();
            }
            TextView tvName = navigationView.getHeaderView (0).findViewById (R.id.tv_name);
            ImageView ivProfile = navigationView.getHeaderView (0).findViewById (R.id.iv_profile);
            tvName.setText (user.getClientFName () + "" + user.getClientLName ());
            tvName.setOnClickListener (view -> startActivity (new Intent (mContext, SettingsActivity.class)));
            if (!TextUtils.isEmpty (user.getClientProfilePic ()))
                Glide.with (mContext).load (user.getClientProfilePic ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
            ivProfile.setOnClickListener (view -> startActivity (new Intent (mContext, SettingsActivity.class)));
        }
        Utils.replaceFragment (ClientHomeActivity.this, new ClientScheduleListFragment (), true);
        toolbarTitle.setText ("Workout Schedule");
        toolbar.setTitleTextColor (Color.WHITE);
//        tintViewDrawable(btnBmi);
//        tintViewDrawable(btnPaymentPlans);
        if (getIntent ().getBooleanExtra ("isFromNotification", false)) {
            Utils.replaceFragment (ClientHomeActivity.this, new MessageFragment (), true);
            toolbarTitle.setText ("Notifications");
            messageFlag = 1;
            btnBackground ();
        }

        if (getIntent ().getBooleanExtra ("isFromSession", false)) {
            Utils.replaceFragment (ClientHomeActivity.this, new ClientPackageFragment (), true);
            toolbarTitle.setText ("Packages");
            llTabs.setVisibility (View.VISIBLE);
            packageFlag = 1;
            btnBackground ();
        }
    }

    @Override
    protected void onResume() {
        super.onResume ();
        Log.e (TAG, "onResume: opened");
        if (!isFromAdmin) {
            user = app.getPreferences ().getLoggedInUser ().getData ();
            TextView tvName = navigationView.getHeaderView (0).findViewById (R.id.tv_name);
            tvName.setOnClickListener (view -> startActivity (new Intent (mContext, SettingsActivity.class)));
            ImageView ivProfile = navigationView.getHeaderView (0).findViewById (R.id.iv_profile);
            ivProfile.setOnClickListener (view -> startActivity (new Intent (mContext, SettingsActivity.class)));
            tvName.setText (user.getClientFName () + " " + user.getClientLName ());
            if (!TextUtils.isEmpty (user.getClientProfilePic ()))
                Glide.with (mContext).load (user.getClientProfilePic ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (ivProfile);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        fragmentA = getSupportFragmentManager ().findFragmentByTag ("MyFragment");
        int id = item.getItemId ();
        if (id == R.id.nav_workout_schedule) {
            if (!(fragmentA instanceof ClientScheduleListFragment)) {
                Utils.replaceFragment (ClientHomeActivity.this, new ClientScheduleListFragment (), true);
                toolbarTitle.setText ("Workout Schedule");
                llTabs.setVisibility (View.VISIBLE);
                workoutPlanFlag = 1;
                btnBackground ();
            }
        } else if (id == R.id.nav_notifications) {
            if (!(fragmentA instanceof MessageFragment)) {
                Utils.replaceFragment (ClientHomeActivity.this, new MessageFragment (), true);
                toolbarTitle.setText ("Notifications");
                llTabs.setVisibility (View.VISIBLE);
                messageFlag = 1;
                btnBackground ();
            }
        } else if (id == R.id.nav_sessions) {
//            startActivity(new Intent(mContext, ClientSessionsActivity.class));
            if (!(fragmentA instanceof ClientSessionsFragment)) {
                Utils.replaceFragment (ClientHomeActivity.this, new ClientSessionsFragment (), true);
                toolbarTitle.setText ("Schedule");
                llTabs.setVisibility (View.GONE);
            }
        } else if (id == R.id.nav_session_request) {
            startActivity (new Intent (mContext, RequestSessionActivity.class));
        } else if (id == R.id.nav_packages) {
            if (!(fragmentA instanceof ClientPackageFragment)) {
                Utils.replaceFragment (ClientHomeActivity.this, new ClientPackageFragment (), true);
                toolbarTitle.setText ("Packages");
                llTabs.setVisibility (View.VISIBLE);
                packageFlag = 1;
                btnBackground ();
            }
        } else if (id == R.id.nav_bmi) {
            if (!(fragmentA instanceof BMIFragment)) {
                toolbarTitle.setText ("BMI");
                Utils.replaceFragment (ClientHomeActivity.this, new BMIFragment (), true);
                bmiFlag = 1;
                btnBackground ();
            }
        } else if (id == R.id.nav_settings) {
            startActivity (new Intent (mContext, SettingsActivity.class));
        } else if (id == R.id.nav_log_out) {
            logout ();
        }
        drawer.closeDrawer (GravityCompat.START);
        return true;
    }

    @OnClick({R.id.btn_workoutPlan, R.id.btn_notifications, R.id.btn_bmi, R.id.btn_paymentPlans})
    public void onViewClicked(View view) {
        fragmentA = getSupportFragmentManager ().findFragmentByTag ("MyFragment");
        switch (view.getId ()) {
            case R.id.btn_workoutPlan:
                if (!(fragmentA instanceof ClientScheduleListFragment)) {
                    navigationView.setCheckedItem (R.id.nav_workout_schedule);
                    Utils.replaceFragment (ClientHomeActivity.this, new ClientScheduleListFragment (), true);
                    toolbarTitle.setText ("Workout Schedule");
                    workoutPlanFlag = 1;
                    btnBackground ();
                }
                break;
            case R.id.btn_notifications:
                if (!(fragmentA instanceof MessageFragment)) {
                    navigationView.setCheckedItem (R.id.nav_notifications);
                    Utils.replaceFragment (ClientHomeActivity.this, new MessageFragment (), true);
                    toolbarTitle.setText ("Notifications");
                    messageFlag = 1;
                    btnBackground ();
                }
                break;
            case R.id.btn_bmi:
                if (!(fragmentA instanceof BMIFragment)) {
                    navigationView.setCheckedItem (R.id.nav_bmi);
                    Utils.replaceFragment (ClientHomeActivity.this, new BMIFragment (), true);
                    toolbarTitle.setText ("BMI");
                    bmiFlag = 1;
                    btnBackground ();
                }
                break;
            case R.id.btn_paymentPlans:
                if (!(fragmentA instanceof ClientPackageFragment)) {
                    navigationView.setCheckedItem (R.id.nav_packages);
                    Utils.replaceFragment (ClientHomeActivity.this, new ClientPackageFragment (), true);
                    toolbarTitle.setText ("Packages");
                    packageFlag = 1;
                    btnBackground ();
                }
                break;
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_client_home;
    }

    public void displayPackagesFrag() {
        Utils.replaceFragment (ClientHomeActivity.this, new ClientPackageFragment (), true);
        toolbarTitle.setText ("Packages");
        packageFlag = 1;
        btnBackground ();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen (GravityCompat.START)) {
            drawer.closeDrawer (GravityCompat.START);
        } else {
            if (isFromAdmin) finish ();
            else Utils.closeApp (mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater ().inflate (R.menu.menu_delete, menu);
        actionDelete = menu.findItem (R.id.action_delete);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case android.R.id.home:
                finish ();
                break;
        }
        return true;
    }

    public void logout() {
        final Dialog dialog = new Dialog (mContext, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature (Window.FEATURE_NO_TITLE);
        dialog.setContentView (R.layout.custom_dialog_logout);
        dialog.setCanceledOnTouchOutside (false);
        dialog.getWindow ().setBackgroundDrawableResource (android.R.color.transparent);
        dialog.getWindow ().setSoftInputMode (WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.findViewById (R.id.btn_ok).setOnClickListener (view -> {
            app.getPreferences ().logOutUser ();
            startActivity (new Intent (mContext, LoginActivity.class));

            NotificationManager notificationManager = (NotificationManager) getSystemService (Context.
                    NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancelAll ();
            }

            dialog.dismiss ();
            ((Activity) mContext).finish ();

        });
        dialog.findViewById (R.id.btn_cancel).setOnClickListener (view -> dialog.dismiss ());
        dialog.show ();
    }

    private void btnBackground() {
        if (workoutPlanFlag == 1) {
            workoutPlanFlag = 0;
            btn_workoutPlan.setBackgroundResource (R.drawable.after_clicked_color);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_bmi.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_paymentPlans.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (messageFlag == 1) {
            messageFlag = 0;
            btn_workoutPlan.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.after_clicked_color);
            btn_bmi.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_paymentPlans.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (bmiFlag == 1) {
            bmiFlag = 0;
            btn_workoutPlan.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_bmi.setBackgroundResource (R.drawable.after_clicked_color);
            btn_paymentPlans.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (packageFlag == 1) {
            packageFlag = 0;
            btn_workoutPlan.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_bmi.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_paymentPlans.setBackgroundResource (R.drawable.after_clicked_color);
        }
    }
}