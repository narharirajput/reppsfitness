package com.reppsfitness.session_cal.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 11/11/17.
 */

public class SessionResponse {
    List<SessionData> sessionDataList = new ArrayList<>();

    public List<SessionData> getSessionDataList() {
        return sessionDataList;
    }

    public void setSessionDataList(List<SessionData> sessionDataList) {
        this.sessionDataList = sessionDataList;
    }
}
