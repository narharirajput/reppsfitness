package com.reppsfitness.session_cal.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 11/11/17.
 */

public class SessionData {
    String time;
    List<SessionModel> sessionModelList = new ArrayList<>();

    public SessionData(String time, List<SessionModel> sessionModelList) {
        this.time = time;
        this.sessionModelList = sessionModelList;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<SessionModel> getSessionModelList() {
        return sessionModelList;
    }

    public void setSessionModelList(List<SessionModel> sessionModelList) {
        this.sessionModelList = sessionModelList;
    }
}
