package com.reppsfitness.session_cal.model;

/**
 * Created by Mayur Gangurde on 11/11/17.
 */

public class SessionModel {
    String id;

    public SessionModel(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
