package com.reppsfitness.session_cal.apiclient;

import android.support.annotation.NonNull;

import com.reppsfitness.utils.Utils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Mayur Gangurde on 12/11/17.
 */

public class SessionClient {
    public SessionService sessionService;

    public SessionClient() {
        createRestAdapter();
    }

    private void createRestAdapter() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create());
        Retrofit retrofit = builder.client(getClient().build()).build();
        sessionService = retrofit.create(SessionService.class);
    }

    @NonNull
    private OkHttpClient.Builder getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
// add your other interceptors …

// add logging as last interceptor
        httpClient.interceptors().add(logging);
        return httpClient;
    }
}
