package com.reppsfitness.session_cal.apiclient;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Mayur Gangurde on 12/11/17.
 */

public interface SessionService {
    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<String> call_api(@FieldMap Map<String, String> params);
}
