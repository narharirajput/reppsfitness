package com.reppsfitness.session_cal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.SessionDetailsActivity;
import com.reppsfitness.session_cal.adapters.BaseTableAdapter;
import com.reppsfitness.session_cal.apiclient.SessionClient;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.session_cal.json_model.SessionRes;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.id.text1;

public class SessionsActivity extends BaseActivity {
    Calendar calendar = Calendar.getInstance();
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_toolbar)
    LinearLayout llToolbar;
    @BindView(R.id.imgLeft)
    ImageView imgLeft;
    @BindView(R.id.txtDay)
    TextView txtDay;
    @BindView(R.id.txtYear)
    TextView txtYear;
    @BindView(R.id.imgRight)
    ImageView imgRight;
    @BindView(R.id.table)
    TableFixHeaders tableFixHeaders;
    String json = "{\"timeList\":[{\"sessionFromTime\":\"8:00\",\"sessionToTime\":\"9:00\"},{\"sessionFromTime\":\"9:00\",\"sessionToTime\":\"10:00\"}],\"occupiedSession\":{\"09:00-10:00\":[{\"sessionDate\":\"1-1-2017\",\"alloted\":5,\"clientLimit\":\"7\"},{\"sessionDate\":\"5-1-2017\",\"alloted\":3,\"clientLimit\":\"7\"},{\"sessionDate\":\"8-1-2017\",\"alloted\":2,\"clientLimit\":\"7\"},{\"sessionDate\":\"12-1-2017\",\"alloted\":7,\"clientLimit\":\"7\"}],\"08:00-09:00\":[{\"0\":\"1-1-2017\",\"alloted\":6,\"clientLimit\":\"6\"},{\"sessionDate\":\"5-1-2017\",\"alloted\":5,\"clientLimit\":\"6\"},{\"sessionDate\":\"8-1-2017\",\"alloted\":3,\"clientLimit\":\"6\"},{\"sessionDate\":\"12-1-2017\",\"alloted\":5,\"clientLimit\":\"6\"}]}}\n";
    List<String> timeList = new ArrayList<>();
    int daysCount;
    SessionClient sessionClient;
    private Call<String> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Session");
        imgLeft.setOnClickListener(view -> {
            calendar.add(Calendar.MONTH, -1);
            daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (call != null && call.isExecuted()) call.cancel();
            getSessionSetting();
        });
        imgRight.setOnClickListener(view -> {
            calendar.add(Calendar.MONTH, 1);
            daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            if (call != null && call.isExecuted()) call.cancel();
            getSessionSetting();
        });

        sessionClient = new SessionClient();
        getSessionSetting();
        Log.e("month", "" + calendar.get(Calendar.MONTH) + "||" + calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        daysCount = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        setTableAdapter(null);
    }

    private void getSessionSetting() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getSessionSetting");
        map.put("month", String.valueOf(calendar.get(Calendar.MONTH) + 1));
        map.put("year", String.valueOf(calendar.get(Calendar.YEAR)));
        for (String key : map.keySet()) {
            System.out.println(key + "=" + map.get(key));
        }
        call = sessionClient.sessionService.call_api(map);
        CustomProgressDialog cpd = new CustomProgressDialog(mContext);
        cpd.show();
        if (cd.isConnectingToInternet()) {
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (cpd.isShowing()) cpd.dismiss();
                    try {
                        if (response.body() != null) {
                            com.reppsfitness.session_cal.json_model.SessionResponse sessionResponse =
                                    new Gson().fromJson(response.body(), com.reppsfitness.session_cal.json_model.SessionResponse.class);
                            SessionRes data1 = sessionResponse.getData();
//                    Log.e("keyset", data1.getSessionDateList().keySet().toString());
                            timeList = data1.getTimeList();
                            Log.e("timeList", timeList.toString());
                            List<List<DayDetails>> list = new ArrayList<>();
//                    for (String key : data1.getSessionDateList().keySet()) {
                            for (String key : timeList) {
                                List<DayDetails> items = data1.getSessionDateList() != null ?
                                        data1.getSessionDateList().get(key) : null;
                                List<DayDetails> tempItemsList = new ArrayList<>();
                                for (int i = 0; i < daysCount; i++) {
                                    DayDetails dayDetails = new DayDetails();
                                    dayDetails.setSessionDate("" + String.valueOf(i + 1));
                                    tempItemsList.add(dayDetails);
                                }
                                if (items != null && items.size() > 0) {
                                    for (DayDetails item1 : items) {
                                        int day = Integer.parseInt(Utils.dmyTod(item1.getSessionDate()));
                                        for (int i = 0; i < tempItemsList.size(); i++) {
                                            if (day == Integer.parseInt(tempItemsList.get(i).getSessionDate())) {
                                                DayDetails dayDetails = tempItemsList.get(i);
                                                dayDetails.setAlloted(item1.getAlloted());
                                                dayDetails.setClientLimit(item1.getClientLimit());
                                                dayDetails.setSessionID(item1.getSessionID());
                                                tempItemsList.set(i, dayDetails);
                                            }
                                        }
                                    }
                                }
//                        list.add(tempItemsList);
                                list.add(tempItemsList);
                            }
                            setTableAdapter(list);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (cpd.isShowing()) cpd.dismiss();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.table;
    }

    private void setTableAdapter(List<List<DayDetails>> list) {
        List<String> dayList = printDatesInMonth(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH));

//        SessionResponse sessionResponse = new SessionResponse();
//        List<SessionModel> sessionModelList = new ArrayList<>();
//        for (int i = 0; i < dayList.size(); i++) {
//            sessionModelList.add(new SessionModel("" + i));
//        }
//        List<SessionData> sessionDataList = new ArrayList<>();
//        for (int i = 0; i < 9; i++) {
//            sessionDataList.add(new SessionData("1PM - 2 PM", sessionModelList));
//        }
//        sessionResponse.setSessionDataList(sessionDataList);
        BaseTableAdapter baseTableAdapter = new FamilyNexusAdapter(this, list, dayList, timeList);
        tableFixHeaders.setAdapter(baseTableAdapter);
        txtDay.setText(theMonth(calendar.get(Calendar.MONTH)));
        txtYear.setText("" + calendar.get(Calendar.YEAR));
    }

    private class FamilyNexusAdapter extends BaseTableAdapter {
        private final LayoutInflater inflater;
        private String[] headers = null;
        private final float density;
        List<List<DayDetails>> list;
        List<String> timeList;
        List<String> dayList;
        Context context;

        FamilyNexusAdapter(Context context, List<List<DayDetails>> list, List<String> dayList, List<String> timeList) {
            this.list = list;
            this.dayList = dayList;
            this.timeList = timeList;
            this.context = context;
            inflater = ((Activity) context).getLayoutInflater();
            headers = new String[dayList.size()];
            density = context.getResources().getDisplayMetrics().density;
            for (int i = 0; i < dayList.size(); i++) {
                headers[i] = "" + i;
            }
        }

        @Override
        public int getRowCount() {
            return timeList.size();
        }

        @Override
        public int getColumnCount() {
            return dayList.size();
        }

        @Override
        public View getView(int row, int column, View convertView, ViewGroup parent) {
            final View view;
            switch (getItemViewType(row, column)) {
                case 1:
                    view = getHeader(row, column, convertView, parent);
                    break;
                case 2:
                    view = getBody(row, column, convertView, parent);
                    break;
                default:
                    throw new RuntimeException("wtf?");
            }
            return view;
        }

        private View getHeader(int row, int column, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_table_header, parent, false);
            }
            if (row == -1 && column == -1) {
                ((TextView) convertView.findViewById(text1)).setText("");
            } else {
                ((TextView) convertView.findViewById(text1)).setText(dayList.get(column));
            }
            return convertView;
        }

        private View getBody(int row, int column, View convertView, ViewGroup parent) {
            ViewHolder view;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_table, parent, false);
                view = new ViewHolder();
                view.text1 = convertView.findViewById(android.R.id.text1);
                view.ll_root = convertView.findViewById(R.id.ll_root);
                view.cardView = convertView.findViewById(R.id.cardView);
                view.tv_dayTime = convertView.findViewById(R.id.tv_dayTime);
                convertView.setTag(view);
            } else {
                view = (ViewHolder) convertView.getTag();
            }
            if (column == -1 && row != -1) {
                view.text1.setText(timeList.get(row));
                view.text1.setBackgroundResource(0);
                view.text1.setTextColor(Color.BLACK);
            } else if (column >= 0 && row != -1) {
                if (list != null && list.size() > 0) {
//                    String dayDate = dayList.get(column);
//                    String[] split = dayDate.split("\\n");
                    //if values set for alloted days
//                    list.get(row).stream().filter(dayDetails -> split[1].equalsIgnoreCase(dayDetails.getSessionDate()))
//                            .forEach(dayDetails -> {
//                                view.text1.setText(dayDetails.getAlloted() + "/" + dayDetails.getClientLimit());
//                            });
//                    for (DayDetails dayDetails : list.get(row)) {
//                        if (split[1].equalsIgnoreCase(dayDetails.getSessionDate())) {
//                            view.text1.setText(dayDetails.getAlloted() + "/" + dayDetails.getClientLimit());
//                        }
//                    }
                    //if values set for all days
                    if (list.get(row).get(column).getAlloted() > 0) {
                        long alloted = list.get(row).get(column).getAlloted();
                        long clientLimit = Long.parseLong(list.get(row).get(column).getClientLimit());
                        view.text1.setText(alloted + "/" + clientLimit);
                        if (alloted == clientLimit) {
                            view.text1.setBackgroundResource(R.drawable.rect_bg_red_fill);
//                            view.ll_root.setBackgroundColor(Color.RED);
                            view.text1.setTextColor(Color.WHITE);
                        } else {
                            view.text1.setBackgroundResource(R.drawable.rect_bg_green_fill);
//                            view.ll_root.setBackgroundColor(Color.GREEN);
                            view.text1.setTextColor(Color.WHITE);
                        }
                        view.text1.setTag(list.get(row).get(column));
                        convertView.setOnClickListener(view1 -> {
//                            DayDetails dayDetails = (DayDetails) view.text1.getTag();
                            TextView textView = view1.findViewById(android.R.id.text1);
                            if (textView != null && textView.getText() != null && !TextUtils.isEmpty(textView.getText())) {
                                DayDetails dayDetails = list.get(row).get(column);
                                String sessionDate = dayDetails.getSessionDate();
                                sessionDate = sessionDate + "-" + String.valueOf(calendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(calendar.get(Calendar.YEAR));
                                Log.e("sessionDate", sessionDate);
                                startActivity(new Intent(mContext, SessionDetailsActivity.class)
                                        .putExtra("sessionTime", timeList.get(row))
                                        .putExtra("sessionDate", sessionDate)
                                        .putExtra("dayDetails", dayDetails));
                            }
                        });
                    } else {
                        view.text1.setText("");
                        view.text1.setBackgroundResource(R.drawable.rect_bg_white);
                        view.text1.setTextColor(Color.BLACK);
                    }
                } else {
                    view.text1.setBackgroundResource(R.drawable.rect_bg_white);
                    view.text1.setTextColor(Color.BLACK);
                }
            }
            return convertView;
        }

        class ViewHolder {
            TextView text1, tv_dayTime;
            CardView cardView;
            LinearLayout ll_root;
        }

        @Override
        public int getWidth(int column) {
//            return Math.round(widths[column + 1] * density);
            return Math.round(60 * density);
        }

        @Override
        public int getHeight(int row) {
            final int height;
            if (row == -1) {
                height = 70;
            } else {
                height = 65;
            }
            return Math.round(height * density);
        }

        @Override
        public int getItemViewType(int row, int column) {
            final int itemViewType;
            if (row == -1) {
                itemViewType = 1;
            } else {
                itemViewType = 2;
            }
            return itemViewType;
        }

        @Override
        public int getViewTypeCount() {
            return 5;
        }
    }

    private static final int MAX_LOG_LENGTH = 4000;

    public void log(String message) {
        // Split by line, then ensure each line can fit into Log's maximum length.
        for (int i = 0, length = message.length(); i < length; i++) {
            int newline = message.indexOf('\n', i);
            newline = newline != -1 ? newline : length;
            do {
                int end = Math.min(newline, i + MAX_LOG_LENGTH);
                Log.e("OkHttp", message.substring(i, end));
                i = end;
            } while (i < newline);
        }
    }

    public List<String> printDatesInMonth(int year, int month) {
        List<String> dayList = new ArrayList<>();
        SimpleDateFormat fmt = new SimpleDateFormat("EE\ndd");
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.set(year, month, 1);
        int daysInMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysInMonth; i++) {
            String format = fmt.format(cal.getTime());
            dayList.add(format);
            cal.add(Calendar.DAY_OF_MONTH, 1);
        }
        Log.e("dayLoist", dayList.toString());
        return dayList;
    }

    public String theMonth(int month) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
