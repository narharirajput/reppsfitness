package com.reppsfitness.session_cal.json_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeList {
    @SerializedName("sessionFromTime")
    @Expose
    public String sessionFromTime;
    @SerializedName("sessionToTime")
    @Expose
    public String sessionToTime;

    public String getSessionFromTime() {
        return sessionFromTime;
    }

    public void setSessionFromTime(String sessionFromTime) {
        this.sessionFromTime = sessionFromTime;
    }

    public String getSessionToTime() {
        return sessionToTime;
    }

    public void setSessionToTime(String sessionToTime) {
        this.sessionToTime = sessionToTime;
    }
}