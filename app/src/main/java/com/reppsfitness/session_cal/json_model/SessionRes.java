package com.reppsfitness.session_cal.json_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mayur Gangurde on 12/11/17.
 */

public class SessionRes {
    @SerializedName("timeList")
    @Expose
    public List<String> timeList = new ArrayList<>();
    @SerializedName("sessionDateList")
    @Expose
    Map<String, List<DayDetails>> sessionDateList = new HashMap<>();

    public List<String> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<String> timeList) {
        this.timeList = timeList;
    }

    public Map<String, List<DayDetails>> getSessionDateList() {
        return sessionDateList;
    }

    public void setSessionDateList(Map<String, List<DayDetails>> sessionDateList) {
        this.sessionDateList = sessionDateList;
    }
}
