package com.reppsfitness.session_cal.json_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 12/11/17.
 */

public class DayDetails implements Parcelable {
    @SerializedName("sessionDate")
    @Expose
    public String sessionDate;
    @SerializedName("allotted")
    @Expose
    public long alloted;
    @SerializedName("clientLimit")
    @Expose
    public String clientLimit;
    @SerializedName("sessionID")
    @Expose
    public String sessionID;

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionDate() {
        return sessionDate;
    }

    public void setSessionDate(String sessionDate) {
        this.sessionDate = sessionDate;
    }

    public long getAlloted() {
        return alloted;
    }

    public void setAlloted(long alloted) {
        this.alloted = alloted;
    }

    public String getClientLimit() {
        return clientLimit;
    }

    public void setClientLimit(String clientLimit) {
        this.clientLimit = clientLimit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sessionDate);
        dest.writeLong(this.alloted);
        dest.writeString(this.clientLimit);
        dest.writeString(this.sessionID);
    }

    public DayDetails() {
    }

    protected DayDetails(Parcel in) {
        this.sessionDate = in.readString();
        this.alloted = in.readLong();
        this.clientLimit = in.readString();
        this.sessionID = in.readString();
    }

    public static final Parcelable.Creator<DayDetails> CREATOR = new Parcelable.Creator<DayDetails>() {
        @Override
        public DayDetails createFromParcel(Parcel source) {
            return new DayDetails(source);
        }

        @Override
        public DayDetails[] newArray(int size) {
            return new DayDetails[size];
        }
    };
}
