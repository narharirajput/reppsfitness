package com.reppsfitness;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.reppsfitness.fragment.AddPackageFragment;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;

import butterknife.BindView;

/**
 * Created by MXCPUU11 on 8/4/2017.
 */

public class AddClientPackageActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public User user;
    public static AddClientPackageActivity addClientPackageActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientPackageActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Packages");
        user = getIntent().getParcelableExtra("user");
        Utils.replaceFragment(AddClientPackageActivity.this, new AddPackageFragment(), true);
    }

    public void startAddClientWorkoutScheduleActivity() {
        startActivity(new Intent(mContext, AddClientActivity.class)
                .putExtra("isAddClient", false)
                .putExtra("user", user));
        finish();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_add_client_package;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
