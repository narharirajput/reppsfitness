package com.reppsfitness;
/**
 * Created by MXCPUU11 on 8/18/2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


public class CreateWorkoutSchedule extends BaseActivity {
    @BindView(R.id.et_from_date)
    EditText etFromDate;
    @BindView(R.id.et_to_date)
    EditText etToDate;
    String fromDate, toDate;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public static CreateWorkoutSchedule createWorkoutSchedule;
    User user;
    @BindView(R.id.rr_form)
    RelativeLayout rrForm;
    private Date pkgEndDate;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Workout Schedule");
        createWorkoutSchedule = this;
        user = getIntent().getParcelableExtra("user");
        getClientProfile();
    }

    @OnClick({R.id.btn_cancel, R.id.btn_next, R.id.et_from_date, R.id.et_to_date})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                finish();
                break;
            case R.id.btn_next:
                final String str_fromDate = etFromDate.getText().toString().trim();
                final String str_toDate = etToDate.getText().toString().trim();
                if (TextUtils.isEmpty(str_fromDate)) {
                    etFromDate.setError("Enter From Date");
                    etFromDate.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(str_toDate)) {
                    etToDate.setError("Enter To Date");
                    etToDate.requestFocus();
                    return;
                }
                Map<String, String> params = new HashMap<>();
                params.put("xAction", "saveWorkoutScheduleDayList");
                params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
                params.put("clientID", user.getClientID());
                params.put("scheduleFromDate", fromDate);
                params.put("scheduleToDate", toDate);
                createWorkoutScheduleList(params);
                break;
            case R.id.et_from_date:
                displayDatePickerDialog(true);
                break;
            case R.id.et_to_date:
                displayDatePickerDialog(false);
                break;
        }
    }

    private void createWorkoutScheduleList(Map<String, String> params) {
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().saveWorkoutScheduleDayList(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    WorkoutScheduleResponse response = (WorkoutScheduleResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0 && response.getDayList() != null
                                && response.getDayList().size() > 0) {
                            startActivity(new Intent(mContext, WorkoutScheduleActivity.class)
                                    .putExtra("fromDate", fromDate)
                                    .putExtra("toDate", toDate)
                                    .putParcelableArrayListExtra("dayList", response.getDayList())
                                    .putExtra("user", user)
                            );
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getClientProfile");
        map.put("clientID", user.getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getClientProfile(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    ClientProfileData clientProfileData = (ClientProfileData) object;
                    if (clientProfileData != null && clientProfileData.getCount() > 0) {
                        ClientProfile clientProfile = clientProfileData.getClientProfile();
                        String str_pkgEndDate = clientProfile.getPkgEndDate();
                        String str_pkgStartDate = clientProfile.getPkgStartDate();
                        if (str_pkgEndDate != null && !str_pkgEndDate.equals("0000-00-00")) {
                            try {
                                pkgEndDate = formatter.parse(str_pkgEndDate);
                                rrForm.setVisibility(View.VISIBLE);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            setPkgStartEndDateValidations();
                        }
                    } else {
                        setPkgStartEndDateValidations();
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    setPkgStartEndDateValidations();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void setPkgStartEndDateValidations() {
        if (user.getPkgEndDate() != null && !user.getPkgEndDate().equals("0000-00-00")) {
            String str_pkgEndDate = user.getPkgEndDate();
            try {
                this.pkgEndDate = formatter.parse(str_pkgEndDate);
                rrForm.setVisibility(View.VISIBLE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    private Date date1, date2;

    private void displayDatePickerDialog(boolean isFrom) {
        final Calendar minCalendar = Calendar.getInstance();
        minCalendar.setTime(new Date());
        Calendar now = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        DatePickerDialog dpd = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
                    if (isFrom) {
                        Log.d("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
                        try {
                            date1 = formatter.parse(String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                            if (date2 != null) {
                                if (date1.compareTo(date2) < 0 || date1.compareTo(date2) == 0) {
                                    System.out.println("date2 is Greater than my date1");
                                    fromDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    etFromDate.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
                                } else {
                                    Utils.showShortToast(mContext, "From Date must be before To Date");
                                    date1 = null;
                                    return;
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        fromDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        String date = String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        etFromDate.setText(date);
                    } else {
                        Log.d("to date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
                        try {
                            date2 = formatter.parse(String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                            if (date1 != null) {
                                if (date1.compareTo(date2) < 0 || date1.compareTo(date2) == 0) {
                                    System.out.println("date2 is Greater than my date1");
                                    toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    etToDate.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
                                } else {
                                    Utils.showShortToast(mContext, "To Date must be after From Date");
                                    date2 = null;
                                    return;
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        String date = String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        etToDate.setText(date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(minCalendar);
        if (pkgEndDate != null) {
            final Calendar maxCalendar = Calendar.getInstance();
            maxCalendar.setTime(pkgEndDate);
            dpd.setMaxDate(maxCalendar);
        }
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_create_workout_schedule;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                break;
        }
        return true;
    }
}


