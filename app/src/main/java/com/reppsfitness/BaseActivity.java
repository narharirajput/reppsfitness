package com.reppsfitness;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.reppsfitness.utils.ConnectionDetector;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

//public abstract class BaseActivity extends AppCompatActivity implements LifecycleRegistryOwner {
public abstract class BaseActivity extends AppCompatActivity {
    public App app;
    //    private LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private Unbinder unbinder;
    public Context mContext;
    public CompositeDisposable mCompositeDisposable;
    public ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getActivityLayout());
        unbinder = ButterKnife.bind(this);
        mContext = BaseActivity.this;
        app = (App) getApplication();
        mCompositeDisposable = new CompositeDisposable();
        cd = new ConnectionDetector(mContext);
    }

//    @Override
//    public LifecycleRegistry getLifecycle() {
//        return lifecycleRegistry;
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        mCompositeDisposable.clear();
    }

    protected abstract int getActivityLayout();
}