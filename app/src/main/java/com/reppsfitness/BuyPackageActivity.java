package com.reppsfitness;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.PaymentMode;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.utils.ConnectionDetector;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.reppsfitness.utils.Utils.addDays;


public class BuyPackageActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public static BuyPackageActivity buyPackageActivity;
    String transactionDate, paymentMode, paymentModeID;
    @BindView(R.id.et_transaction_date)
    EditText et_transaction_date;
    @BindView(R.id.et_from_date)
    EditText et_from_date;
    @BindView(R.id.et_to_date)
    EditText et_to_date;
    @BindView(R.id.et_paymentMode)
    EditText etPaymentMode;
    @BindView(R.id.et_transactionID)
    EditText et_transactionID;
    @BindView(R.id.et_comment)
    EditText et_comment;
    @BindView(R.id.tvPkgInfo)
    TextView tvPkgInfo;
    @BindView(R.id.btn_reject)
    Button btn_reject;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.ll_top)
    LinearLayout ll_top;
    private List<PaymentMode> paymentModeList;
    RegularItem regularItem;
    TermItem termItem;
    private String fromDate, toDate;
    SimpleDateFormat ymdSdf = new SimpleDateFormat ("yyyy-MM-dd");
    SimpleDateFormat dmySdfDisplay = new SimpleDateFormat ("dd/MM/yyyy");
    String approveRejectFlg = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        buyPackageActivity = this;
        termItem = getIntent ().getParcelableExtra ("termItem");
        regularItem = getIntent ().getParcelableExtra ("regularItem");
        approveRejectFlg = getIntent ().getStringExtra ("approveRejectFlg");
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        toolbarTitle.setText ("Buy Package");
        getPaymentModes ();

        //btn_reject.setVisibility (View.GONE);
        if (app.getPreferences ().isClient ()) {
            ll_top.setVisibility (View.GONE);
            btn_reject.setVisibility (View.GONE);
            btn_submit.setText ("Request");
        } else {
            if (approveRejectFlg !=null && approveRejectFlg.equals ("true")) {
                btn_submit.setText ("Approve");
            } else {
                btn_submit.setText ("Submit");
                btn_reject.setVisibility (View.GONE);
            }
        }
//        if (!app.getPreferences().isClient()) {
//            fromDate = ymdSdf.format(new Date());
//            String currDate = dmySdfDisplay.format(new Date());
//            et_from_date.setText(currDate);
//        }
        if (termItem != null) {
            tvPkgInfo.setText (termItem.getPackageFeatureDesc ());
            if (termItem.getIsRegular () != null && termItem.getIsTermApply () != null) {
                if (termItem.getIsRegular ().equals ("0") && termItem.getIsTermApply ().equals ("0")) {
                    et_from_date.setVisibility (View.VISIBLE);
                    et_to_date.setVisibility (View.GONE);
                }
            }
        }
        if (regularItem != null) {
            tvPkgInfo.setText (regularItem.getPackageFeatureDesc ());

            if (regularItem.getIsRegular () != null && regularItem.getIsTermApply () != null) {
                if (regularItem.getIsRegular ().equals ("0") && regularItem.getIsTermApply ().equals ("0")) {
                    et_from_date.setVisibility (View.VISIBLE);
                    et_to_date.setVisibility (View.GONE);
                }
            }
        }
    }

    @OnClick({R.id.et_transaction_date, R.id.et_from_date, R.id.et_paymentMode, R.id.btn_submit, R.id.btn_reject})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.et_transaction_date:
                displayDatePickerDialog (et_transaction_date);
                break;
            case R.id.et_from_date:
                displayDatePickerDialog ();
                break;
            case R.id.et_paymentMode:
                if (paymentModeList != null && paymentModeList.size () > 0) {
                    List<String> packageList = new ArrayList<> ();
                    for (int i = 0; i < paymentModeList.size (); i++) {
                        packageList.add (paymentModeList.get (i).getPaymentMode ());
                    }
                    new MaterialDialog.Builder (mContext).items (packageList).itemsCallback ((dialog, view1, which, text) -> {
                        paymentMode = text.toString ();
                        paymentModeID = paymentModeList.get (which).getPaymentModeID ();
                        etPaymentMode.setText (paymentMode);
                    }).show ();
                }
                break;
            case R.id.btn_submit:
                final String transactionID = et_transactionID.getText ().toString ().trim ();
                final String comment = et_comment.getText ().toString ().trim ();
                String approvedStatus = "0";
/*
                if (TextUtils.isEmpty(transactionID)) {
                    et_transactionID.setError("Enter Transaction ID");
                    et_transactionID.requestFocus();
                    return;
                }
*/
                if (TextUtils.isEmpty (paymentMode)) {
                    etPaymentMode.setError ("Select Payment Mode");
                    etPaymentMode.requestFocus ();
                    return;
                }
                if (TextUtils.isEmpty (transactionDate)) {
                    et_transaction_date.setError ("Select Transaction Date");
                    et_transaction_date.requestFocus ();
                    return;
                }
                if (!app.getPreferences ().isClient ()) {
                    String fromDate = et_from_date.getText ().toString ().trim ();
                    if (TextUtils.isEmpty (fromDate)) {
                        et_from_date.setError ("Select Start Date");
                        et_from_date.requestFocus ();
                        return;
                    }
                    approvedStatus = "1";
                }
                clientPackageRequest (transactionID, comment, approvedStatus);
                break;

            case R.id.btn_reject:
                clientPackageRequest ("", "", "2");

                break;
        }
    }

    private void clientPackageRequest(String transactionID, String comment, String approvedStatus) {
        Map<String, String> params = new HashMap<> ();
        params.put ("xAction", "clientPackageRequest");
        if (regularItem != null) params.put ("packageID", regularItem.getPackageID ());
        else if (termItem != null) params.put ("packageID", termItem.getPackageID ());
        params.put ("userType", !app.getPreferences ().isClient () ? "1" : "2");
        params.put ("paymentMode", paymentMode);
        params.put ("paymentModeID", paymentModeID);
        params.put (app.getPreferences ().isClient () ? "clientTransactionID" : "adminTransactionID", transactionID);
        params.put ("comment", comment);
        ///new parameters
        params.put ("pkgDetailsID", ClientHomeActivity.clientHomeActivity.user==null ? "0" : ClientHomeActivity.clientHomeActivity.user.pkgDetailsID);
        params.put ("approvedStatus", approvedStatus);
        params.put ("approveRejectFlg", approveRejectFlg);

        if (!app.getPreferences ().isClient ()) {
            params.put ("pkgStartDate", fromDate);
            //params.put("approvedStatus", approval);
            if (regularItem != null) {
                if (regularItem.getIsRegular () != null && regularItem.getIsTermApply () != null) {
                    if (regularItem.getIsRegular ().equals ("0") && regularItem.getIsTermApply ().equals ("0")) {
                    } else {
                        params.put ("pkgEndDate", toDate);
                    }
                }
            } else if (termItem != null) {
                if (termItem.getIsRegular () != null && termItem.getIsTermApply () != null) {
                    if (termItem.getIsRegular ().equals ("0") && termItem.getIsTermApply ().equals ("0")) {
                    } else {
                        params.put ("pkgEndDate", toDate);
                    }
                }
            }
        }
        if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing ())
            params.put ("clientID", ClientHomeActivity.clientHomeActivity.user.getClientID ());
        else if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing ())
            params.put ("clientID", AddClientPackageActivity.addClientPackageActivity.user.getClientID ());
        Log.e ("pkgDetailsID ", "clientPackageRequest: " + params);

        ConnectionDetector cd = new ConnectionDetector (mContext);
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().clientPackageRequest (params, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount () > 0) {
                            if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showLongToast (mContext, response.getMsg ());
                            }
                            if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing ()) {
                            } else {
                                if (regularItem != null) {
                                    if (regularItem.getIsRegular () != null && regularItem.getIsTermApply () != null) {
                                        if (regularItem.getIsRegular ().equals ("0") && regularItem.getIsTermApply ().equals ("0")) {
                                        } else {
                                            startAllotSessionActivity ();
                                        }
                                    }
                                } else if (termItem != null) {
                                    if (termItem.getIsRegular () != null && termItem.getIsTermApply () != null) {
                                        if (termItem.getIsRegular ().equals ("0") && termItem.getIsTermApply ().equals ("0")) {
                                        } else {
                                            startAllotSessionActivity ();
                                        }
                                    }
                                }
                            }
                            if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing ()) {
                                AddClientPackageActivity.addClientPackageActivity.finish ();
                            }
                            finish ();
                        } else {
                            if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showLongToast (mContext, response.getMsg ());
                            }
                        }
                    } else {
                        Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Utils.showLongToast (mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void startAllotSessionActivity() {
        Intent intent = new Intent (mContext, ChangeSessionActivity.class);
        if (regularItem != null) {
            intent.putExtra ("regularItem", regularItem);
            intent.putExtra ("packageID", regularItem.getPackageID ());
        } else if (termItem != null) {
            intent.putExtra ("termItem", termItem);
            intent.putExtra ("packageID", termItem.getPackageID ());
        }
        if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing ())
            intent.putExtra ("user", ClientHomeActivity.clientHomeActivity.user);
        else if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing ())
            intent.putExtra ("user", AddClientPackageActivity.addClientPackageActivity.user);
        startActivity (intent);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_buy_package;
    }

    private void getPaymentModes() {
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            mCompositeDisposable.add (app.getApiService ().getPaymentModes ("getPaymentModes").observeOn (AndroidSchedulers.mainThread ()).subscribeOn (Schedulers.io ()).subscribe (paymentModeData -> {
                if (pd.isShowing ()) pd.dismiss ();
                if (paymentModeData != null && paymentModeData.getCount () > 0) {
                    paymentModeList = paymentModeData.getData ();
                }
            }, throwable -> {
                if (pd.isShowing ()) pd.dismiss ();
                Log.e ("in", "error " + throwable.getMessage ());
            }));
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void displayDatePickerDialog(EditText editText) {
        // set minimum date
//        final Calendar minCalendar = Calendar.getInstance();
//        minCalendar.setTime(new Date());
        Calendar now = Calendar.getInstance ();
        DatePickerDialog dpd = DatePickerDialog.newInstance ((view12, year, monthOfYear, dayOfMonth) -> {
            Log.d ("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
            transactionDate = String.valueOf (year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            editText.setText (String.valueOf (dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
        }, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
//        dpd.setMinDate(minCalendar);
        dpd.show (getFragmentManager (), "Datepickerdialog");
    }

    private void displayDatePickerDialog() {
//        Date minDate = null;
//        if (regularItem != null) {
//            minDate = substractDays(new Date(), Integer.parseInt(regularItem.getPackageDuration()));
//        } else if (termItem != null) {
//            minDate = substractDays(new Date(), Integer.parseInt(termItem.getPackageDuration()));
//        }
        final Calendar minCalendar = Calendar.getInstance ();
        minCalendar.setTime (new Date ());
        Calendar now = Calendar.getInstance ();
        DatePickerDialog dpd = DatePickerDialog.newInstance ((view12, year, monthOfYear, dayOfMonth) -> {
            Log.d ("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
            fromDate = String.valueOf (year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            et_from_date.setText (String.valueOf (dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
            Date date;
            try {
                date = ymdSdf.parse (fromDate);
                Date resultDate = null;
                if (regularItem != null)
                    resultDate = addDays (date, Integer.parseInt (regularItem.getLimitDays ()));
                else if (termItem != null)
                    resultDate = addDays (date, Integer.parseInt (termItem.getLimitDays ()));
                String toDateToDisplay = dmySdfDisplay.format (resultDate);
                toDate = ymdSdf.format (resultDate);
                et_to_date.setText (toDateToDisplay);
            } catch (ParseException e) {
                e.printStackTrace ();
            }
//                    } else {
//                        Log.d("to date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
//                        toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                        et_to_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
//                    }
        }, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
        dpd.setMinDate (minCalendar);
//        final Calendar maxCalendar = Calendar.getInstance();
//        maxCalendar.setTime(new Date());
//        dpd.setMaxDate(maxCalendar);
        dpd.show (getFragmentManager (), "Datepickerdialog");
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case android.R.id.home:
                finish ();
                break;
//            case R.id.action_home:
//                Intent intent = new Intent(this, AdminMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(intent);
//                finish();
//                break;
        }
        return true;
    }
}
