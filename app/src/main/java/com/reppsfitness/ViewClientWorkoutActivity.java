package com.reppsfitness;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.adapter.ViewClientWorkoutAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.AddCommentActivity;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ClientExercise;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;
import static com.reppsfitness.R.id.iv_done;
import static com.reppsfitness.R.id.iv_editExercise;
import static com.reppsfitness.R.id.iv_edit_comment;
import static com.reppsfitness.R.id.rv_exercises;
import static com.reppsfitness.R.id.tv_add_comment;
import static com.reppsfitness.R.id.tv_done;

/**
 * Created by MXCPUU11 on 8/4/2017.
 */

public class ViewClientWorkoutActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public static ViewClientWorkoutActivity addClientPackageActivity;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.iv_view_comment)
    ImageView ivViewComment;
    @BindView(iv_editExercise)
    ImageView ivEditExercise;
    @BindView(iv_done)
    ImageView ivDone;
    @BindView(tv_add_comment)
    TextView tvAddComment;
    @BindView(tv_done)
    TextView tvDone;
    @BindView(R.id.rr_header)
    RelativeLayout rrHeader;
    @BindView(rv_exercises)
    RecyclerView rvExercises;
    //    @BindView(iv_edit_comment)
//    ImageView ivEditComment;
//    @BindView(R.id.tv_comment)
//    TextView tvComment;
//    @BindView(rr_comment)
//    RelativeLayout rrComment;
    @BindView(R.id.tv_error)
    TextView tv_error;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    boolean isFromAdmin;
    Day day;
    String dayText;
    User user;
    String comment = "", exDate;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.btn_notAttended)
    Button btnNotAttended;
    @BindView(R.id.btn_attended)
    Button btnAttended;
    @BindView(R.id.ll_options)
    LinearLayout llOptions;
    @BindView(R.id.rr_bottom)
    RelativeLayout rrBottom;
    @BindColor(R.color.btn_bg_color)
    int btn_bg_color;
    Date exDateObj = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientPackageActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Workout Schedule");
        isFromAdmin = getIntent().getBooleanExtra("isFromAdmin", false);
        day = getIntent().getParcelableExtra("day");
        user = getIntent().getParcelableExtra("user");
        dayText = getIntent().getStringExtra("dayText");

        SimpleDateFormat spf = new SimpleDateFormat ("yyyy-MM-dd");
        try {
            Date newDate = spf.parse (day.getDate ());
            spf = new SimpleDateFormat ("dd/MM/yyyy");
            exDate = spf.format (newDate);
            exDateObj = Utils.curFormater.parse (exDate);
            if (new Date ().before (exDateObj)) {
                llOptions.setVisibility (View.GONE);
            }

        } catch (ParseException e) {
            e.printStackTrace ();
        }

        rvExercises.setHasFixedSize(true);
        rvExercises.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rvExercises.setNestedScrollingEnabled(false);
        tvDay.setText(dayText);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_client_day_workout;
    }

    @Override
    protected void onResume() {
        super.onResume();
        ivViewComment.setVisibility(GONE);
        ivEditExercise.setVisibility(GONE);
        ivDone.setVisibility(GONE);
        tvAddComment.setVisibility(GONE);
        tvDone.setVisibility(GONE);
//        rrComment.setVisibility(GONE);
//        ivEditComment.setVisibility(GONE);
        getScheduleDetailsForClient();
    }

    private void getScheduleDetailsForClient() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getScheduleDetailsForClient");
        params.put("scheduleDayID", day.getScheduleDayID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getScheduleDetailsForClient(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null && response.getCount() > 0 && response.getData() != null) {
                            ClientExercise data = response.getData();
                            if (data.getWorkout() != null && data.getWorkout().size() > 0) {
                                if (data.getDoneStatus() != null && !TextUtils.isEmpty(data.getDoneStatus())) {
                                    if (data.getDoneStatus().equals("1")) {
                                        rrHeader.setBackgroundColor(Color.BLACK);
                                        ivDone.setVisibility(View.VISIBLE);
                                        tvDay.setTextColor(Color.WHITE);
                                        ivViewComment.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                                        if (!TextUtils.isEmpty(data.getComment())) {
                                            comment = data.getComment();
                                            ivViewComment.setVisibility(View.VISIBLE);
                                        } else {
                                            if (!isFromAdmin) {
                                                ivViewComment.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    } else {
                                        ivViewComment.getDrawable().setColorFilter(colorPrimary, PorterDuff.Mode.SRC_IN);
                                        rrHeader.setBackgroundColor(Color.WHITE);
                                        tvDay.setTextColor(Color.BLACK);
                                        if (isFromAdmin) {
                                            ivEditExercise.setVisibility(View.VISIBLE);
                                            if (!TextUtils.isEmpty(data.getComment())) {
                                                ivViewComment.setVisibility(View.VISIBLE);
                                                comment = data.getComment();
                                            }
                                        } else {
                                            tvDone.setVisibility(View.VISIBLE);
                                            if (TextUtils.isEmpty(data.getComment())) {
                                                tvAddComment.setVisibility(View.VISIBLE);
                                            } else {
                                                comment = data.getComment();
                                                ivViewComment.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                }
                                rvExercises.setAdapter(new ViewClientWorkoutAdapter(mContext, data.getWorkout()));
                                rvExercises.setVisibility(View.VISIBLE);
                                tv_error.setVisibility(GONE);
                                //Set Bottom Options
                                if (!app.getPreferences().isClient()) {
                                    if (data.getIsAttended() != null && !TextUtils.isEmpty(data.getIsAttended())) {
                                        switch (data.getIsAttended ()) {
                                            case "0":
                                                rrBottom.setVisibility (View.VISIBLE);
                                                if (new Date ().before (exDateObj)) {
                                                    llOptions.setVisibility (View.GONE);
                                                } else
                                                    llOptions.setVisibility (View.VISIBLE);
                                                tvStatus.setVisibility (GONE);

                                              /*  rrBottom.setVisibility (View.VISIBLE);
                                                llOptions.setVisibility (View.VISIBLE);
                                                tvStatus.setVisibility (GONE);*/
                                                break;
                                            case "1":
                                                rrBottom.setVisibility (View.VISIBLE);
                                                llOptions.setVisibility (View.GONE);
                                                tvStatus.setVisibility (View.VISIBLE);
                                                tvStatus.setText ("Attended");
                                                tvStatus.setTextColor (Color.GREEN);
                                                break;
                                            case "2":
                                                rrBottom.setVisibility (View.VISIBLE);
                                                llOptions.setVisibility (View.GONE);
                                                tvStatus.setVisibility (View.VISIBLE);
                                                tvStatus.setText ("Not Attended");
                                                tvStatus.setTextColor (btn_bg_color);
                                                break;
                                        }
                                    }
                                } else {
                                    rrBottom.setVisibility(View.GONE);
                                }
                            } else {
                                if (data.getDoneStatus() != null && !TextUtils.isEmpty(data.getDoneStatus())) {
                                    if (data.getDoneStatus().equals("0")) {
                                        ivEditExercise.setVisibility(View.VISIBLE);
                                    }
                                }
                                tv_error.setVisibility(View.VISIBLE);
                                rvExercises.setVisibility(GONE);
                            }
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @OnClick({R.id.iv_view_comment, iv_editExercise, iv_done, tv_add_comment, tv_done, iv_edit_comment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_view_comment:
                if (isFromAdmin) {
                    startActivity(new Intent(mContext, ViewCommentActivity.class)
                            .putExtra("comment", comment));
                } else {
                    if (TextUtils.isEmpty(comment)) {
                        startActivity(new Intent(mContext, AddCommentActivity.class).putExtra("isAddComment", true)
                                .putExtra("scheduleDayID", day.getScheduleDayID())
                                .putExtra("user", user));
                    } else {
                        startActivity(new Intent(mContext, AddCommentActivity.class).putExtra("isAddComment", false)
                                .putExtra("scheduleDayID", day.getScheduleDayID())
                                .putExtra("comment", comment)
                                .putExtra("user", user));
                    }
                }
                break;
            case iv_editExercise:
//                startActivity(new Intent(mContext, AddClientExerciseActivity.class)
//                        .putExtra("day", day)
//                        .putExtra("user", user)
//                        .putExtra("isFromAdmin", isFromAdmin));
                startActivity(new Intent(mContext, com.reppsfitness.exercise.AddExerciseActivity.class)
                        .putExtra("day", day)
                        .putExtra("user", user)
                        .putExtra("isFromAdmin", isFromAdmin));
                break;
            case iv_done:
                break;
            case tv_add_comment:
                startActivity(new Intent(mContext, AddCommentActivity.class).putExtra("isAddComment", true)
                        .putExtra("scheduleDayID", day.getScheduleDayID())
                        .putExtra("user", user));
                break;
            case tv_done:
                displayMaterialDialog ("Set workout as done.", (dialog, which) -> doneWorkoutSchedule ());
                break;
            case iv_edit_comment:
                startActivity(new Intent(mContext, AddCommentActivity.class).putExtra("isAddComment", false)
                        .putExtra("scheduleDayID", day.getScheduleDayID())
                        .putExtra("comment", comment)
                        .putExtra("user", user));
                break;
        }
    }

    @OnClick({R.id.btn_notAttended, R.id.btn_attended})
    public void onOptionsViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_notAttended:
                displayMaterialDialog("Set Workout not attended.", (dialog, which) -> workoutScheduleAttended("2"));
                break;
            case R.id.btn_attended:
                displayMaterialDialog("Set Workout attended.", (dialog, which) -> workoutScheduleAttended("1"));
                break;
        }
    }

    private void doneWorkoutSchedule() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "doneWorkoutSchedule");
        params.put("scheduleDayID", day.getScheduleDayID());
        params.put("clientID", user.getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().deleteScheduleExercise(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(userData -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (userData != null) {
                            if (userData.getCount() > 0) {
                                Utils.showLongToast(mContext, userData.getMsg());
                                finish();
                            } else {
                                if (userData.getMsg() != null && !TextUtils.isEmpty(userData.getMsg())) {
                                    Utils.showLongToast(mContext, userData.getMsg());
                                }
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void workoutScheduleAttended(String isAttended) {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "workoutScheduleAttended");
        map.put("scheduleDayID", day.getScheduleDayID());
        map.put("isAttended", isAttended);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().workoutScheduleAttended(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                        tvStatus.setText(isAttended.equals("1") ? "Attended" : "Not Attended");
                        tvStatus.setTextColor(isAttended.equals("1") ? Color.GREEN : btn_bg_color);
                        tvStatus.setVisibility(View.VISIBLE);
                        llOptions.setVisibility(GONE);
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        MenuItem menu_home = menu.findItem(R.id.action_home);
        menu_home.setVisible(!app.getPreferences().isClient());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void displayMaterialDialog(String message, MaterialDialog.SingleButtonCallback singleButtonCallback) {
        new MaterialDialog.Builder(mContext)
                .content(message)
                .positiveText("Ok")
                .negativeText("Cancel")
                .onPositive(singleButtonCallback)
                .show();
    }
}
