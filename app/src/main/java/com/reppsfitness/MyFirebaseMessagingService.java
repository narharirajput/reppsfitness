package com.reppsfitness;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.reppsfitness.client.ClientHomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    App app;
    Context mContext;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        app = (App) getApplication();
        mContext = getBaseContext();

//        if (remoteMessage.getNotification() != null) {
//            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
//            String body = remoteMessage.getNotification().getBody();
//            try {
//                JSONObject jsonObject = new JSONObject(body);
//                String data = jsonObject.optString("message");
////                Log.e("body", data);
//                Random random = new Random();
//                int m = random.nextInt(9999 - 1000) + 1000;
//                sendNotification(data, m, "Repps Fitness");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;
            Map<String, String> data = remoteMessage.getData();
            String type = null, buyRate = null, sellRate = null, title = null, message = null;
            try {
                JSONObject jsonObject = new JSONObject(data.get("data"));
                type = jsonObject.getString("type");
                message = jsonObject.getString("message");
                if (type != null && !TextUtils.isEmpty(type) && type.equalsIgnoreCase("rate-change")) {
                    title = jsonObject.getString("title");
                    sendNotification(message, m, title);
                } else if (type != null && !TextUtils.isEmpty(type) && type.equalsIgnoreCase("normal")) {
                    title = jsonObject.getString("title");
                    sendNotification(message, m, title);
                } else {
                    sendNotification(message, m, "Repps Fitness");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("data", "" + remoteMessage.getData());
//            Log.e("values", "" + image + "||" + title + "||" + message);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Random random = new Random();
            int m = random.nextInt(9999 - 1000) + 1000;
            sendNotification(remoteMessage.getNotification().getBody(), m, "Repps Fitness");
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     * @param title
     */
    private void sendNotification(String messageBody, int randomNo, String title) {
        Intent intent = null;
        if (app.getPreferences().isLoggedInUser() && app.getPreferences().isClient())
            intent = new Intent(this, ClientHomeActivity.class).putExtra("isFromNotification", true);
        else if (app.getPreferences().isLoggedInUser() && !app.getPreferences().isClient())
            intent = new Intent(this, AdminMainActivity.class).putExtra("isFromNotification", true);
        else
            intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setContentIntent(pendingIntent);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setSmallIcon(R.drawable.ic_launcher_transparent);
        } else {
            notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.notify(randomNo /* ID of notification */, notificationBuilder.build());
        }
    }
}