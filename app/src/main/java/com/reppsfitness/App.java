package com.reppsfitness;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.facebook.stetho.Stetho;
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.api.AppService;
import com.reppsfitness.preferences.Preferences;
import com.tspoon.traceur.Traceur;


public class App extends Application {
    private Preferences preferences;
    private ApiRequestHelper apiRequestHelper;
    private AppService apiService;

    @Override
    public void onCreate() {
        super.onCreate();
        doInit();
        Traceur.enableLogging();
        RxPaparazzo.register(this).withFileProviderPath("GymAppPhotos");
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
//                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
    }
    private void doInit() {
        this.preferences = new Preferences(this);
        this.apiRequestHelper = ApiRequestHelper.init(this);
        this.apiService = apiRequestHelper.getApiService();
    }

    public synchronized ApiRequestHelper getApiRequestHelper() {
        return apiRequestHelper;
    }

    public synchronized AppService getApiService() {
        return apiService;
    }

    public synchronized Preferences getPreferences() {
        return preferences;
    }
}
