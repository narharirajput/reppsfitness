package com.reppsfitness;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.adapter.QueueMemberAdapter;
import com.reppsfitness.adapter.SessionMemberAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.AllotData;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.RequestData;
import com.reppsfitness.model.SessionDetailsData;
import com.reppsfitness.model.Slotcancelpojo;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class SessionDetailsActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rv_session_members)
    RecyclerView rvSessionMembers;
    @BindView(R.id.rv_queued_members)
    RecyclerView rvQueuedMembers;
    String sessionTime, sessionDate, allotted, clientLimit;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_capacity)
    TextView tvCapacity;
    @BindView(R.id.tv_error)
    TextView tv_error;
    @BindView(R.id.btn_allot)
    Button btnAllot;
    DayDetails dayDetails;
    boolean isDataChanged = false;
    String TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionTime = getIntent().getStringExtra("sessionTime");
        sessionDate = getIntent().getStringExtra("sessionDate");
        dayDetails = getIntent().getParcelableExtra("dayDetails");
        allotted = dayDetails.getAlloted() + "";
        clientLimit = dayDetails.getClientLimit();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Session Details");
        setRvSessionMembers();
        setRvQueuedMembers();
        getSessionDetailsList();
        tvDate.setText(sessionDate);
        tvTime.setText(sessionTime);
        tvCapacity.setText("Capacity: " + clientLimit + "  |  Allotted: " + allotted);
    }

    private void getSessionDetailsList() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getSessionDetailsList");
        map.put("sessionTime", sessionTime);
        map.put("sessionDate", sessionDate);
        map.put("sessionID", dayDetails.getSessionID() != null ? dayDetails.getSessionID() : "");
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getSessionDetailsList(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    SessionDetailsData response = (SessionDetailsData) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getAllottdata() != null && response.getAllottdata().size() > 0)
                            rvSessionMembers.setAdapter(new SessionMemberAdapter(mContext, response.getAllottdata(),dayDetails,app));
                        if (response.getRequestdata() != null && response.getRequestdata().size() > 0) {
                            rvQueuedMembers.setAdapter(new QueueMemberAdapter(mContext, response.getRequestdata()));
                            rvQueuedMembers.setVisibility(View.VISIBLE);
                            tv_error.setVisibility(View.GONE);
                            btnAllot.setVisibility(View.VISIBLE);
                        } else {
                            rvQueuedMembers.setVisibility(View.GONE);
                            tv_error.setVisibility(View.VISIBLE);
                            btnAllot.setVisibility(View.GONE);
                        }
                        allotted = "" + response.getCountAllotedSession();
                        clientLimit = "" + response.getClientLimit();
                        tvCapacity.setText("Capacity: " + clientLimit + "  |  Allotted: " + allotted);
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                        btnAllot.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    btnAllot.setVisibility(View.GONE);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_session_details;
    }

    public void adminAllotSlotClient(RequestData requestData, MaterialDialog dialog, String requestStatus) {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "adminAllotSlotClient");
        map.put("clientID", requestData.getClientID());
        map.put("sessionID", dayDetails.getSessionID());
        map.put("requestStatus", requestStatus);
        map.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        map.put("sessionTime", sessionTime);
        map.put("sessionDate", sessionDate);
        map.put("clientLimit", dayDetails.getClientLimit());
        map.put("alloted", "" + dayDetails.getAlloted());
        map.put("sessionCRID", "" + requestData.getSessionCRID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().adminAllotSlotClient(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    if (requestStatus.equals("1"))
                        isDataChanged = true;
                    LoginResponse response = (LoginResponse) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
//                            Utils.showLongToast(mContext, response.getMsg());
                            Utils.showSimpleialog(mContext, response.getMsg(), "Ok");
                        }
                        getSessionDetailsList();
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
//                            Utils.showLongToast(mContext, response.getMsg());
                            Utils.showSimpleialog(mContext, response.getMsg(), "Ok");
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            dialog.dismiss();
            Utils.alert_dialog(mContext);
        }
    }

    @OnClick(R.id.btn_allot)
    public void onViewClicked() {
        new MaterialDialog.Builder(mContext)
                .content("Are you sure you don't want to allot members to Session?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .onPositive((dialog, which) -> sessionIDontWantToAllow(dialog))
                .show();
    }

    public void sessionIDontWantToAllow(MaterialDialog dialog) {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "sessionIDontWantToAllow");
        map.put("sessionID", dayDetails.getSessionID());
        map.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        map.put("sessionTime", sessionTime);
        map.put("sessionDate", sessionDate);
        map.put("clientLimit", dayDetails.getClientLimit());
        map.put("alloted", "" + dayDetails.getAlloted());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().adminAllotSlotClient(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    isDataChanged = true;
                    LoginResponse response = (LoginResponse) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
//                            Utils.showLongToast(mContext, response.getMsg());
                            Utils.showSimpleialog(mContext, response.getMsg(), "Ok");
                        }
                        getSessionDetailsList();
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
//                            Utils.showLongToast(mContext, response.getMsg());
                            Utils.showSimpleialog(mContext, response.getMsg(), "Ok");
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            dialog.dismiss();
            Utils.alert_dialog(mContext);
        }
    }

    private void setRvSessionMembers() {
        rvSessionMembers.setHasFixedSize(true);
        rvSessionMembers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        rvSessionMembers.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.size_5));
    }

    private void setRvQueuedMembers() {
        rvQueuedMembers.setHasFixedSize(true);
        rvQueuedMembers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        rvQueuedMembers.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.size_5));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (isDataChanged) {
                    setResult(RESULT_OK);
                }
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isDataChanged) {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
        finish();
    }

    public void Slotcancel(AllotData allotData, DayDetails details, MaterialDialog dialog, String userType) {
        Map<String,String> slotcancel=new HashMap<>();
        slotcancel.put("xAction","slotCancel");
        slotcancel.put("clientID",allotData.getClientID());
        slotcancel.put("sessionID",details.getSessionID());
        slotcancel.put("userType",userType);

        if(cd.isConnectingToInternet()){
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().slotCancel(slotcancel, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) throws JSONException {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    Slotcancelpojo slotcancel=(Slotcancelpojo)object;
                    Log.e(TAG, "onSuccess: "+slotcancel);

                    if(slotcancel != null && slotcancel.getCount()>0){
                        getSessionDetailsList();
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    Toast.makeText(mContext, "response Unsuccessfull", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }
}
