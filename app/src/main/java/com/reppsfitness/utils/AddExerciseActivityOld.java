package com.reppsfitness.utils;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reppsfitness.AddAMRAPExercise;
import com.reppsfitness.AddOTMExercise;
import com.reppsfitness.AddRFTExercise;
import com.reppsfitness.AddStandardExercise;
import com.reppsfitness.BaseActivity;
import com.reppsfitness.CreateWorkoutSchedule;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.Element;
import com.reppsfitness.model.Exercise;
import com.reppsfitness.model.ExerciseCat;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WorkoutElement;
import com.reppsfitness.model.WorkoutExerciseData;
import com.reppsfitness.model.WorkoutType;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.reppsfitness.widget.searchable_spinner.SpinnerDialog;
import com.reppsfitness.widget.swipelayout.SwipeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 8/4/2017.
 */

public class AddExerciseActivityOld extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    View rowView;
    public List<ExerciseCat> exerciseCatList;
    public List<WorkoutType> workoutTypeList;
    public String date;
    com.reppsfitness.model.Day day;
    int dayPosition;
    @BindView(R.id.rv_exercises)
    RecyclerView rvExercises;
    List<Exercise> exerciseList = new ArrayList<>();
    public ArrayList<String> workoutTypeNameList = new ArrayList<>();
    public ArrayList<String> workoutCatNameList = new ArrayList<>();
    User user;
    ArrayList<String> timeList = new ArrayList<>();
    ArrayList<String> roundList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbarTitle.setText("Workout Schedule");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        date = getIntent().getStringExtra("date");
        day = getIntent().getParcelableExtra("day");
        user = getIntent().getParcelableExtra("user");
        dayPosition = getIntent().getIntExtra("dayPosition", 0);
        getWorkoutExerciseElements();
        rvExercises.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        for (int i = 1; i <= 30; i++) {
            if (i <= 15)
                roundList.add(i + " Round");
            timeList.add(i + " Min");
        }

        fab.setVisibility(app.getPreferences().isClient() ? View.GONE : View.VISIBLE);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_standard_workout;
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        addExerciseView();
    }

    private void addExerciseView() {
        rowView = getLayoutInflater().inflate(R.layout.exercise_sample, null);
        int id = new Random().nextInt(10000);
        final SwipeLayout swipeLayout = rowView.findViewById(R.id.swipeLayout);
        swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
        final EditText et_type = rowView.findViewById(R.id.et_type);
        final EditText et_time = rowView.findViewById(R.id.et_time);
        final EditText et_rounds = rowView.findViewById(R.id.et_rounds);
        final FlexboxLayout fl_elements = rowView.findViewById(R.id.fl_elements);
        final RelativeLayout rr_next = rowView.findViewById(R.id.rr_next);
        final RelativeLayout rr_time = rowView.findViewById(R.id.rr_time);
        final RelativeLayout rr_rounds = rowView.findViewById(R.id.rr_rounds);
        swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
//            repeatExerciseView(id);
            swipeLayout.close();
        });
        swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
            deleteExerciseView(id);
            swipeLayout.close();
        });
        if (workoutTypeList != null && workoutTypeList.size() > 0) {
            ArrayList<String> workoutTypeNameList = new ArrayList<>();
            for (int i = 0; i < workoutTypeList.size(); i++) {
                workoutTypeNameList.add(workoutTypeList.get(i).getWorkoutTypeName());
            }
            et_type.setOnClickListener(view -> {
                SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutTypeNameList,
                        "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
                    et_type.setText(item);
                    et_type.setTag(workoutTypeList.get(position).getWorkoutTypeID());
                    String workoutTypeName = workoutTypeList.get(position).getWorkoutTypeName();
                    if (workoutTypeName.equalsIgnoreCase("STANDARD")) {
                        rr_rounds.setVisibility(View.GONE);
                        rr_time.setVisibility(View.GONE);
                    } else if (workoutTypeName.equalsIgnoreCase("OTM")) {
                        rr_rounds.setVisibility(View.GONE);
                        rr_time.setVisibility(View.VISIBLE);
                        et_time.setText("");
                        et_rounds.setText("");
                    } else if (workoutTypeName.equalsIgnoreCase("RFT")) {
                        rr_rounds.setVisibility(View.VISIBLE);
                        rr_time.setVisibility(View.GONE);
                        et_time.setText("");
                        et_rounds.setText("");
                    } else if (workoutTypeName.equalsIgnoreCase("AMRAP")) {
                        rr_rounds.setVisibility(View.GONE);
                        rr_time.setVisibility(View.VISIBLE);
                        et_time.setText("");
                        et_rounds.setText("");
                    }
//                    List<Element> elements = workoutTypeList.get(position).getElements();
//                    fl_elements.removeAllViews();
//                    for (int i = 0; i < elements.size(); i++) {
//                        Element element = elements.get(i);
//                        RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
//                        rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
//                                Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
//                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                                RelativeLayout.LayoutParams.WRAP_CONTENT);
//                        layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
//                        rr_element_view.setLayoutParams(layoutParams);
//                        EditText et_element = rr_element_view.findViewById(R.id.et_element);
//                        et_element.setHint(element.getWorkoutElementName());
//                        et_element.setTag(element.getWorkoutElementID());
//                        ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
//                        et_element.setOnClickListener(view1 -> {
//                            SpinnerDialog elementDialog = new SpinnerDialog(AddExerciseActivity.this, workoutElementValue,
//                                    "Select " + element.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
//                            elementDialog.showSpinerDialog();
//                            elementDialog.bindOnSpinerListener((item1, position1) -> {
//                                et_element.setText(item1);
//                            });
//                        });
//                        fl_elements.addView(rr_element_view, fl_elements.getChildCount());
//                    }
                });
            });
            rr_next.setOnClickListener(view -> {
                String type = et_type.getText().toString();
                String workoutTypeID = (String) et_type.getTag();
                if (!TextUtils.isEmpty(type)) {
                    if (type.equalsIgnoreCase("STANDARD")) {
                        startActivity(new Intent(mContext, AddStandardExercise.class)
                                .putExtra("workoutName", "STANDARD")
                                .putExtra("workoutTypeID", workoutTypeID)
                                .putExtra("user", user)
                                .putExtra("day", day));
                    } else if (type.equalsIgnoreCase("OTM")) {
                        String time = et_time.getText().toString();
                        if (!TextUtils.isEmpty(time)) {
                            time = time.split(" ")[0];
                            startActivity(new Intent(mContext, AddOTMExercise.class).putExtra("time", time)
                                    .putExtra("workoutName", "OTM")
                                    .putExtra("workoutTypeID", workoutTypeID)
                                    .putExtra("user", user)
                                    .putExtra("day", day));
                            //workoutName, workoutTypeID
                        } else {
                            Utils.showShortToast(mContext, "Select Time");
                        }
                    } else if (type.equalsIgnoreCase("RFT")) {
                        String round = et_rounds.getText().toString();
                        if (!TextUtils.isEmpty(round)) {
                            round = round.split(" ")[0];
                            startActivity(new Intent(mContext, AddRFTExercise.class).putExtra("round", round)
                                    .putExtra("workoutName", "RFT")
                                    .putExtra("workoutTypeID", workoutTypeID)
                                    .putExtra("user", user)
                                    .putExtra("day", day));
                        } else {
                            Utils.showShortToast(mContext, "Select Round");
                        }
                    } else if (type.equalsIgnoreCase("AMRAP")) {
                        String time = et_time.getText().toString();
                        if (!TextUtils.isEmpty(time)) {
                            time = time.split(" ")[0];
                            startActivity(new Intent(mContext, AddAMRAPExercise.class).putExtra("time", time)
                                    .putExtra("workoutName", "AMRAP")
                                    .putExtra("workoutTypeID", workoutTypeID)
                                    .putExtra("user", user)
                                    .putExtra("day", day));
                        } else {
                            Utils.showShortToast(mContext, "Select Time");
                        }
                    }
                }
            });
        }
        et_time.setOnClickListener(view -> {
            if (!app.getPreferences().isClient()) {
                SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, timeList,
                        "Select Minutes", R.style.DialogAnimations_SmileWindow, false);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    et_time.setText(item);
                });
            }
        });
        et_rounds.setOnClickListener(view -> {
            if (!app.getPreferences().isClient()) {
                SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, roundList,
                        "Select Rounds", R.style.DialogAnimations_SmileWindow, false);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    et_rounds.setText(item);
                });
            }
        });
//        if (exerciseCatList != null && exerciseCatList.size() > 0) {
//            ArrayList<String> workoutCatNameList = new ArrayList<>();
//            for (int i = 0; i < exerciseCatList.size(); i++) {
//                workoutCatNameList.add(exerciseCatList.get(i).getExerciseCatName());
//            }
//            ArrayList<String> exerciseNameList = new ArrayList<>();
//            ArrayList<Exercise> exerciseList = new ArrayList<>();
//            et_category.setOnClickListener(view -> {
//                SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivity.this, workoutCatNameList,
//                        "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
//                spinnerDialog.showSpinerDialog();
//                spinnerDialog.bindOnSpinerListener((item, position) -> {
//                    et_category.setText(item);
//                    etSelectExercise.setText("");
//                    et_category.setTag(exerciseCatList.get(position).getExerciseCatID());
//                    rr_selectExercise.setVisibility(View.VISIBLE);
//                    exerciseList.clear();
//                    exerciseNameList.clear();
//                    if (exerciseCatList.get(position).getExercises().size() > 0) {
//                        ExerciseCat exerciseCat = exerciseCatList.get(position);
//                        for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
//                            exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
//                            exerciseList.addAll(exerciseCat.getExercises());
//                        }
//                    }
//                });
//            });
//            etSelectExercise.setOnClickListener(view -> {
//                SpinnerDialog exerciseDialog = new SpinnerDialog(AddExerciseActivity.this, exerciseNameList,
//                        "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
//                exerciseDialog.showSpinerDialog();
//                exerciseDialog.bindOnSpinerListener((item, position) -> {
//                    etSelectExercise.setText(item);
//                    etSelectExercise.setTag(exerciseList.get(position).getExerciseID());
//                });
//            });
//        }
        rowView.setTag(id);
        llContent.addView(rowView, llContent.getChildCount());
    }

    private void repeatExerciseView(Object repeatPositionID) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = getLayoutInflater().inflate(R.layout.row_exercise, null);
        int id = new Random().nextInt(10000);
        final SwipeLayout swipeLayout = rowView.findViewById(R.id.swipeLayout);
        swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
        final EditText etSelectExercise = rowView.findViewById(R.id.et_selectExercise);
        final EditText et_type = rowView.findViewById(R.id.et_type);
        final EditText et_category = rowView.findViewById(R.id.et_category);
        final FlexboxLayout fl_elements = rowView.findViewById(R.id.fl_elements);
        final RelativeLayout rr_selectExercise = rowView.findViewById(R.id.rr_selectExercise);
        swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
//            repeatExerciseView(id);
            swipeLayout.close();
        });
        swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
            deleteExerciseView(id);
            swipeLayout.close();
        });
        View lastChild = null;
        if (repeatPositionID instanceof String) {
            String scheduleDetailsID = (String) repeatPositionID;
            for (int i = 0; i < llContent.getChildCount(); i++) {
                if (llContent.getChildAt(i).getTag() instanceof String
                        && llContent.getChildAt(i).getTag().equals(scheduleDetailsID)) {
                    lastChild = llContent.getChildAt(i);
                    break;
                }
            }
        } else if (repeatPositionID instanceof Integer) {
            for (int i = 0; i < llContent.getChildCount(); i++) {
                if (llContent.getChildAt(i).getTag() instanceof Integer
                        && (int) llContent.getChildAt(i).getTag() == (int) repeatPositionID) {
                    lastChild = llContent.getChildAt(i);
                    break;
                }
            }
        }
        final EditText etSelectExerciseLast = lastChild.findViewById(R.id.et_selectExercise);
        final EditText et_typeLast = lastChild.findViewById(R.id.et_type);
        final EditText et_categoryLast = lastChild.findViewById(R.id.et_category);
        et_type.setText(et_typeLast.getText() != null ? et_typeLast.getText().toString() : "");
        if (workoutTypeList != null && workoutTypeList.size() > 0) {
            ArrayList<String> workoutTypeNameList = new ArrayList<>();
            for (int i = 0; i < workoutTypeList.size(); i++) {
                workoutTypeNameList.add(workoutTypeList.get(i).getWorkoutTypeName());
            }
            et_type.setOnClickListener(view -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutTypeNameList,
                            "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                    spinnerDialog.showSpinerDialog();
                    spinnerDialog.bindOnSpinerListener((item, position) -> {
//                    swipeLayout.setSwipeEnabled(true);
                        et_type.setText(item);
                        et_type.setTag(workoutTypeList.get(position).getWorkoutTypeID());
                        List<Element> elements = workoutTypeList.get(position).getElements();
                        fl_elements.removeAllViews();
                        for (int i = 0; i < elements.size(); i++) {
                            Element element = elements.get(i);
//                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                            rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                    Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                            rr_element_view.setLayoutParams(layoutParams);
                            EditText et_element = rr_element_view.findViewById(R.id.et_element);
                            et_element.setHint(element.getWorkoutElementName());
                            et_element.setTag(element.getWorkoutElementID());
                            ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
                            et_element.setOnClickListener(view1 -> {
                                SpinnerDialog elementDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutElementValue,
                                        "Select " + element.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                                elementDialog.showSpinerDialog();
                                elementDialog.bindOnSpinerListener((item1, position1) -> {
                                    et_element.setText(item1);
                                });
                            });
                            fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                        }
                    });
                }
            });
        }
        if (et_typeLast.getTag() != null) {
            String workoutTypeIDLast = (String) et_typeLast.getTag();
            et_type.setTag(workoutTypeIDLast);
            FlexboxLayout fl_elementsLast = lastChild.findViewById(R.id.fl_elements);
            if (fl_elementsLast.getChildCount() > 0) {
//                swipeLayout.setSwipeEnabled(true);
                List<Element> elements = null;
                for (int i = 0; i < workoutTypeList.size(); i++) {
                    if (workoutTypeList.get(i).getWorkoutTypeID().matches(workoutTypeIDLast)) {
                        elements = workoutTypeList.get(i).getElements();
                        break;
                    }
                }
                if (elements != null && elements.size() > 0) {
                    for (int i = 0; i < fl_elementsLast.getChildCount(); i++) {
                        View elementView = fl_elementsLast.getChildAt(i);
//                        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                        rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                        rr_element_view.setLayoutParams(layoutParams);
                        EditText et_element = rr_element_view.findViewById(R.id.et_element);
                        EditText et_elementLast = elementView.findViewById(R.id.et_element);
                        if (et_elementLast != null && et_elementLast.getText() != null)
                            et_element.setText(et_elementLast.getText());
                        if (et_elementLast != null && et_elementLast.getHint() != null)
                            et_element.setHint(et_elementLast.getHint());
                        if (et_elementLast != null && et_elementLast.getTag() != null) {
                            String workoutElementIDLast = (String) et_elementLast.getTag();
                            et_element.setTag(workoutElementIDLast);
                            Element element = null;
                            for (int i1 = 0; i1 < elements.size(); i1++) {
                                if (elements.get(i1).getWorkoutElementID().matches(workoutElementIDLast)) {
                                    element = elements.get(i1);
                                    break;
                                }
                            }
                            if (element != null && element.getWorkoutElementValue() != null && element.getWorkoutElementValue().size() > 0) {
                                ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
                                Element finalElement = element;
                                et_element.setOnClickListener(view1 -> {
                                    SpinnerDialog elementDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutElementValue,
                                            "Select " + finalElement.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                                    elementDialog.showSpinerDialog();
                                    elementDialog.bindOnSpinerListener((item1, position1) -> {
                                        et_element.setText(item1);
                                    });
                                });
                            }
                        }
                        fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                    }
                }
            }
        }
        if (exerciseCatList != null && exerciseCatList.size() > 0) {
            ArrayList<String> workoutCatNameList = new ArrayList<>();
            for (int i = 0; i < exerciseCatList.size(); i++) {
                workoutCatNameList.add(exerciseCatList.get(i).getExerciseCatName());
            }
            ArrayList<String> exerciseNameList = new ArrayList<>();
            ArrayList<Exercise> exerciseList = new ArrayList<>();
            et_category.setOnClickListener(view -> {
                SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutCatNameList,
                        "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    et_category.setText(item);
                    etSelectExercise.setText("");
                    et_category.setTag(exerciseCatList.get(position).getExerciseCatID());
                    rr_selectExercise.setVisibility(View.VISIBLE);
                    exerciseList.clear();
                    exerciseNameList.clear();
                    if (exerciseCatList.get(position).getExercises().size() > 0) {
                        ExerciseCat exerciseCat = exerciseCatList.get(position);
                        for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                            exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                            exerciseList.addAll(exerciseCat.getExercises());
                        }
                    }
                });
            });
            etSelectExercise.setText(etSelectExerciseLast.getText() != null ? etSelectExerciseLast.getText().toString() : "");
            et_category.setText(et_categoryLast.getText() != null ? et_categoryLast.getText().toString() : "");
            et_category.setTag(et_categoryLast.getTag() != null ? et_categoryLast.getTag().toString() : "");
            String exerciseCatIDLast = "";
            if (etSelectExerciseLast.getTag() != null) {
                exerciseCatIDLast = (String) etSelectExerciseLast.getTag();
                etSelectExercise.setTag(etSelectExerciseLast.getTag() != null ? etSelectExerciseLast.getTag().toString() : "");
                if (etSelectExerciseLast.getVisibility() == View.VISIBLE)
                    rr_selectExercise.setVisibility(View.VISIBLE);
                ExerciseCat exerciseCat = null;
                for (int i = 0; i < exerciseCatList.size(); i++) {
                    if (exerciseCatList.get(i).getExerciseCatID().matches(exerciseCatIDLast)) {
                        exerciseCat = exerciseCatList.get(i);
                        break;
                    }
                }
                if (exerciseCat != null && exerciseCat.getExercises().size() > 0) {
                    for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                        exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                        exerciseList.addAll(exerciseCat.getExercises());
                    }
                }
            }
            etSelectExercise.setOnClickListener(view -> {
                SpinnerDialog exerciseDialog = new SpinnerDialog(AddExerciseActivityOld.this, exerciseNameList,
                        "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                exerciseDialog.showSpinerDialog();
                exerciseDialog.bindOnSpinerListener((item, position) -> {
                    etSelectExercise.setText(item);
                    etSelectExercise.setTag(exerciseList.get(position).getExerciseID());
                });
            });
        }
        rowView.setTag(id);
        llContent.addView(rowView, llContent.getChildCount());
    }

    private void displayExcerciseList() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> {
            for (int j = 0; j < exerciseList.size(); j++) {
                Exercise exercise = exerciseList.get(j);
                View rowView1 = getLayoutInflater().inflate(R.layout.row_exercise, null);
                final SwipeLayout swipeLayout = rowView1.findViewById(R.id.swipeLayout);
                swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
                final EditText etSelectExercise = rowView1.findViewById(R.id.et_selectExercise);
                final EditText et_type = rowView1.findViewById(R.id.et_type);
                final EditText et_category = rowView1.findViewById(R.id.et_category);
                final FlexboxLayout fl_elements = rowView1.findViewById(R.id.fl_elements);
                final RelativeLayout rr_selectExercise = rowView1.findViewById(R.id.rr_selectExercise);

                et_type.setText(exercise.getWorkoutName());
                et_type.setTag(exercise.getWorkoutTypeID());
                if (workoutTypeList != null && workoutTypeList.size() > 0) {
                    et_type.setOnClickListener(view -> {
                        SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutTypeNameList,
                                "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                        spinnerDialog.showSpinerDialog();
                        spinnerDialog.bindOnSpinerListener((item, position) -> {
                            swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
                            et_type.setText(item);
                            et_type.setTag(workoutTypeList.get(position).getWorkoutTypeID());
                            List<Element> elements = workoutTypeList.get(position).getElements();
                            fl_elements.removeAllViews();
                            for (int i = 0; i < elements.size(); i++) {
                                Element element = elements.get(i);
                                RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                                rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                        Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                                rr_element_view.setLayoutParams(layoutParams);
                                EditText et_element = rr_element_view.findViewById(R.id.et_element);
                                et_element.setHint(element.getWorkoutElementName());
                                et_element.setTag(element.getWorkoutElementID());
                                ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
                                et_element.setOnClickListener(view1 -> {
                                    SpinnerDialog elementDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutElementValue,
                                            "Select " + element.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                                    elementDialog.showSpinerDialog();
                                    elementDialog.bindOnSpinerListener((item1, position1) ->
                                            et_element.setText(item1));
                                });
                                fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                            }
                        });
                    });
                }
                List<WorkoutElement> workoutElements = exercise.getWorkoutElements();
                List<Element> elementList = new ArrayList<>();
                String workoutTypeID = exercise.getWorkoutTypeID();
                if (workoutTypeID != null) {
                    for (int i = 0; i < ((AddExerciseActivityOld) mContext).workoutTypeList.size(); i++) {
                        if (((AddExerciseActivityOld) mContext).workoutTypeList.get(i).getWorkoutTypeID().matches(workoutTypeID)) {
                            elementList = ((AddExerciseActivityOld) mContext).workoutTypeList.get(i).getElements();
                            break;
                        }
                    }
                }
                if (workoutElements != null && workoutElements.size() > 0) {
                    for (int i = 0; i < workoutElements.size(); i++) {
                        WorkoutElement workoutElement = workoutElements.get(i);
                        RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                        rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                        rr_element_view.setLayoutParams(layoutParams);
                        EditText et_element = rr_element_view.findViewById(R.id.et_element);
                        et_element.setHint(workoutElement.getWorkoutElementName());
                        et_element.setTag(workoutElement.getWorkoutElementID());
                        et_element.setText(workoutElement.getWorkoutElementValue());
                        ArrayList<String> workoutElementValue = null;
                        for (int i1 = 0; i1 < elementList.size(); i1++) {
                            if (elementList.get(i1).getWorkoutElementID().matches(workoutElement.getWorkoutElementID())) {
                                workoutElementValue = elementList.get(i1).getWorkoutElementValue();
                                break;
                            }
                        }
                        ArrayList<String> finalWorkoutElementValue = workoutElementValue;
                        et_element.setOnClickListener(view1 -> {
                            SpinnerDialog elementDialog = new SpinnerDialog(AddExerciseActivityOld.this, finalWorkoutElementValue,
                                    "Select " + workoutElement.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                            elementDialog.showSpinerDialog();
                            elementDialog.bindOnSpinerListener((item1, position1) -> et_element.setText(item1));
                        });
                        fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                    }
                }
                if (exerciseCatList != null && exerciseCatList.size() > 0) {
                    ArrayList<String> exerciseNameList = new ArrayList<>();
                    ArrayList<Exercise> exerciseList1 = new ArrayList<>();
                    et_category.setOnClickListener(view -> {
                        SpinnerDialog spinnerDialog = new SpinnerDialog(AddExerciseActivityOld.this, workoutCatNameList,
                                "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                        spinnerDialog.showSpinerDialog();
                        spinnerDialog.bindOnSpinerListener((item, position) -> {
                            et_category.setText(item);
                            etSelectExercise.setText("");
                            et_category.setTag(exerciseCatList.get(position).getExerciseCatID());
                            rr_selectExercise.setVisibility(View.VISIBLE);
                            exerciseList1.clear();
                            exerciseNameList.clear();
                            if (exerciseCatList.get(position).getExercises().size() > 0) {
                                ExerciseCat exerciseCat = exerciseCatList.get(position);
                                for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                                    exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                                    exerciseList1.addAll(exerciseCat.getExercises());
                                }
                            }
                        });
                    });
                    etSelectExercise.setTag(exercise.getExerciseID());
                    etSelectExercise.setText(exercise.getExerciseName());
                    rr_selectExercise.setVisibility(View.VISIBLE);
                    et_category.setText(exercise.getExerciseCatName());
                    et_category.setTag(exercise.getExerciseCatID());
                    String exerciseCatID = exercise.getExerciseCatID();
                    ExerciseCat exerciseCat = null;
                    for (int i = 0; i < exerciseCatList.size(); i++) {
                        if (exerciseCatList.get(i).getExerciseCatID().matches(exerciseCatID)) {
                            exerciseCat = exerciseCatList.get(i);
                            break;
                        }
                    }
                    if (exerciseCat != null && exerciseCat.getExercises().size() > 0) {
                        for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                            exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                            exerciseList1.addAll(exerciseCat.getExercises());
                        }
                    }
                    etSelectExercise.setOnClickListener(view -> {
                        SpinnerDialog exerciseDialog = new SpinnerDialog(AddExerciseActivityOld.this, exerciseNameList,
                                "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                        exerciseDialog.showSpinerDialog();
                        exerciseDialog.bindOnSpinerListener((item, position) -> {
                            etSelectExercise.setText(item);
                            etSelectExercise.setTag(exerciseList1.get(position).getExerciseID());
                        });
                    });
                }
                rowView1.setTag(exercise.getScheduleDetailsID());
                swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
//                    repeatExerciseView(exercise.getScheduleDetailsID());
                    swipeLayout.close();
                });
                swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
                    deleteExerciseView(exercise.getScheduleDetailsID());
                    swipeLayout.close();
                });
                llContent.addView(rowView1, llContent.getChildCount());
            }
        });
    }

    private void getAddedExerciseData() {
        List<com.reppsfitness.model.schedule.Exercise> exerciseList = new ArrayList<>();
        for (int i = 0; i < llContent.getChildCount(); i++) {
            com.reppsfitness.model.schedule.Exercise exercise = new com.reppsfitness.model.schedule.Exercise();
            View childAt = llContent.getChildAt(i);
            final EditText etSelectExercise = childAt.findViewById(R.id.et_selectExercise);
            final EditText et_type = childAt.findViewById(R.id.et_type);
            final EditText et_category = childAt.findViewById(R.id.et_category);
            final FlexboxLayout fl_elements = childAt.findViewById(R.id.fl_elements);
            String selectedExercise = etSelectExercise.getText() != null ? etSelectExercise.getText().toString() : "";
            String selectedExerciseID = etSelectExercise.getTag() != null ? (String) etSelectExercise.getTag() : "";
            String selectedType = et_type.getText() != null ? et_type.getText().toString() : "";
            String selectedTypeID = et_type.getTag() != null ? (String) et_type.getTag() : "";
            String selectedCategory = et_category.getText() != null ? et_category.getText().toString() : "";
            String selectedCategoryID = et_category.getTag() != null ? (String) et_category.getTag() : "";
            exercise.setExerciseName(selectedExercise);
            exercise.setExerciseID(selectedExerciseID);
            exercise.setWorkoutName(selectedType);
            exercise.setWorkoutTypeID(selectedTypeID);
            exercise.setExerciseCatName(selectedCategory);
            exercise.setExerciseCatID(selectedCategoryID);
            List<com.reppsfitness.model.schedule.Element> elementList = new ArrayList<>();
            for (int i1 = 0; i1 < fl_elements.getChildCount(); i1++) {
                View childAt1 = fl_elements.getChildAt(i1);
                EditText et_element = childAt1.findViewById(R.id.et_element);
                String selectedElementValue = et_element.getText() != null ? et_element.getText().toString() : "";
                String selectedElementID = et_element.getTag() != null ? (String) et_element.getTag() : "";
                String elementName = et_element.getHint() != null ? et_element.getHint().toString() : "";
                com.reppsfitness.model.schedule.Element element = new com.reppsfitness.model.schedule.Element();
                element.setWorkoutElementID(selectedElementID);
                element.setWorkoutElementName(elementName);
                element.setWorkoutElementValue(selectedElementValue);
                elementList.add(element);
            }
            exercise.setWorkoutElements(elementList);
            if (!TextUtils.isEmpty(selectedExercise))
                exerciseList.add(exercise);
        }
        if (exerciseList.size() > 0) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(exerciseList);
            Log.e("json", json);
            saveDayExercise(json);
        } else {
            new MaterialDialog.Builder(mContext)
                    .content("Please add exercise first")
                    .positiveText("Ok")
                    .show();
        }
    }

    private void saveDayExercise(String json) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "updateScheduleDayWorkout");
        params.put("scheduleDayID", day.getScheduleDayID());
        params.put("clientID", user.getClientID());
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("workoutDetails", json);
        params.put("workoutTypeID", json);
        params.put("workoutTypeValue", json);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().updateScheduleDayWorkout(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "success");
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                                setResult(RESULT_OK, null);
                                if (CreateWorkoutSchedule.createWorkoutSchedule != null && !CreateWorkoutSchedule.createWorkoutSchedule.isFinishing()) {
                                    CreateWorkoutSchedule.createWorkoutSchedule.finish();
                                }
                                finish();
                            }
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void getWorkoutExerciseElements() {
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getWorkoutExerciseEle("getWorkoutExerciseElements", new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    WorkoutExerciseData workoutExerciseData = (WorkoutExerciseData) object;
                    Log.e("in", "success");
                    if (workoutExerciseData != null) {
                        if (workoutExerciseData.getCount() > 0) {
                            exerciseCatList = workoutExerciseData.getExerciseCategories();
                            workoutTypeList = workoutExerciseData.getWorkoutTypeList();
                            for (int i = 0; i < ((AddExerciseActivityOld) mContext).workoutTypeList.size(); i++) {
                                workoutTypeNameList.add(((AddExerciseActivityOld) mContext).workoutTypeList.get(i).getWorkoutTypeName());
                            }
                            for (int i = 0; i < ((AddExerciseActivityOld) mContext).exerciseCatList.size(); i++) {
                                workoutCatNameList.add(((AddExerciseActivityOld) mContext).exerciseCatList.get(i).getExerciseCatName());
                            }
                            addExerciseView();
                        } else {
                            if (workoutExerciseData.getMsg() != null && !TextUtils.isEmpty(workoutExerciseData.getMsg())) {
                                Utils.showLongToast(mContext, workoutExerciseData.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void getScheduleDetails() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getScheduleDetails");
        params.put("scheduleDayID", day.getScheduleDayID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getScheduleDetails(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null) {
                            if (response.getCount() > 0 && response.getExerciseList() != null
                                    && response.getExerciseList().size() > 0) {
                                exerciseList.addAll(response.getExerciseList());
                                displayExcerciseList();
                            } else {
                                addExerciseView();
                            }
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void deleteExerciseView(Object deletePositionID) {
        if (deletePositionID != null) {
            Log.e("deletePositionID", "" + deletePositionID);
            if (deletePositionID instanceof String) {
                String scheduleDetailsID = (String) deletePositionID;
                deleteScheduleExercise(scheduleDetailsID);
            } else if (deletePositionID instanceof Integer) {
                for (int i = 0; i < llContent.getChildCount(); i++) {
                    if (llContent.getChildAt(i).getTag() instanceof Integer
                            && (int) llContent.getChildAt(i).getTag() == (int) deletePositionID) {
                        llContent.removeViewAt(i);
                        break;
                    }
                }
            }
        }
    }

    private void deleteScheduleExercise(String scheduleDetailsID) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "deleteScheduleExercise");
        params.put("scheduleDetailsID", scheduleDetailsID);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().deleteScheduleExercise(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null && response.getCount() > 0) {
                            if (llContent.getChildCount() > 0) {
                                for (int i = 0; i < llContent.getChildCount(); i++) {
                                    if (llContent.getChildAt(i).getTag() instanceof String
                                            && llContent.getChildAt(i).getTag().equals(scheduleDetailsID)) {
                                        llContent.removeViewAt(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_save, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.action_save:
//                getAddedExerciseData();
//                break;
//            case R.id.action_home:
//                Intent intent = new Intent(this, AdminMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(intent);
//                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
