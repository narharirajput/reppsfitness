package com.reppsfitness.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.reppsfitness.model.SaveTemplateResponse;
import com.reppsfitness.model.Template;

import java.lang.reflect.Type;

public class SaveTemplateResponseDeserializer implements JsonDeserializer<SaveTemplateResponse> {

    @Override
    public SaveTemplateResponse deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {
        SaveTemplateResponse userData = null;
        try {
            JsonObject jsonObject = json.getAsJsonObject();
            JsonElement jsonType = jsonObject.get("data");
            if (jsonType != null && jsonType.isJsonObject()) {
                JsonObject type = jsonType.getAsJsonObject();
                userData = new SaveTemplateResponse();
                userData.setCount(jsonObject.get("count").getAsInt());
                if (jsonObject.has("msg"))
                    userData.setMsg(jsonObject.get("msg").getAsString());
                Gson gson = new GsonBuilder().setLenient().create();
                userData.setTemplate(gson.fromJson(type, Template.class));
            } else {
                userData = new SaveTemplateResponse();
                userData.setCount(jsonObject.get("count").getAsInt());
                if (jsonObject.has("msg"))
                    userData.setMsg(jsonObject.get("msg").getAsString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userData;
    }
}