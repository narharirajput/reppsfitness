package com.reppsfitness.utils;

import java.util.Comparator;

public final class MyComparator implements Comparator<String> {
    public MyComparator(String[] items) {
        this.items = items;
    }

    private String[] items = {};

    @Override
    public int compare(String a, String b) {
        int ai = items.length, bi = items.length;
        for (int i = 0; i < items.length; i++) {
            if (items[i].equalsIgnoreCase(a))
                ai = i;
            if (items[i].equalsIgnoreCase(b))
                bi = i;
        }
        return ai - bi;
    }
}