package com.reppsfitness;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class AddClientActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_mobile)
    EditText etMobile;
    @BindView(R.id.btn_save)
    Button btnSave;
    @BindView(R.id.btn_selectTemplate)
    Button btnSelectTemplate;
    @BindView(R.id.btn_addNew)
    Button btnAddNew;
    @BindView(R.id.et_dob)
    EditText etDob;
    @BindView(R.id.rb_male)
    RadioButton rbMale;
    @BindView(R.id.rb_female)
    RadioButton rbFemale;
    @BindView(R.id.rg_gender)
    RadioGroup rgGender;
    @BindView(R.id.et_joinDate)
    EditText etJoinDate;
    @BindView(R.id.et_joinPurpose)
    EditText etJoinPurpose;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.ll_options)
    LinearLayout llOptions;
    @BindView(R.id.tv_error)
    TextView tvError;
    private String dob, joinDate;
    User user;
    boolean isAddClient;
    public static AddClientActivity addClientActivity;
    private Date pkgEndDate;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        isAddClient = getIntent().getBooleanExtra("isAddClient", false);
        if (isAddClient) {
            toolbarTitle.setText("Add Client");
            scrollView.setVisibility(View.VISIBLE);
            llOptions.setVisibility(View.GONE);
            tvError.setVisibility(View.GONE);
        } else {
            toolbarTitle.setText("Workout Schedule");
            user = getIntent().getParcelableExtra("user");
            getClientProfile();
        }

        //editor.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
    }

    @OnClick({R.id.btn_save, R.id.btn_selectTemplate, R.id.btn_addNew, R.id.et_dob, R.id.et_joinDate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_save:
                addClient();
                break;
            case R.id.btn_selectTemplate:
                launchActivityWithUserDataCheck(SelectTemplateActivity.class);
                break;
            case R.id.btn_addNew:
                launchActivityWithUserDataCheck(CreateWorkoutSchedule.class);
                break;
            case R.id.et_dob:
                Calendar dobCal = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
                            String date = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            etDob.setText(date);
                        },
                        dobCal.get(Calendar.YEAR), dobCal.get(Calendar.MONTH), dobCal.get(Calendar.DAY_OF_MONTH)
                );
               /* Calendar mMaxDate = Calendar.getInstance();
                int mMaxYear = mMaxDate.get(Calendar.YEAR);
                mMaxYear = mMaxYear - 18;
                mMaxDate.set(Calendar.YEAR, mMaxYear);
                dpd.setMaxDate(mMaxDate);*/
               dpd.setMaxDate (dobCal);
                dpd.show(getFragmentManager(), "Datepickerdialog");
                break;
            case R.id.et_joinDate:
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpdJoinDate = DatePickerDialog.newInstance((view1, year, monthOfYear, dayOfMonth) -> {
                            String date = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                            joinDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                            etJoinDate.setText(date);
                        },
                        now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH)
                );
                final Calendar minCalendar = Calendar.getInstance();
                minCalendar.setTime(new Date());
                dpdJoinDate.setMinDate(minCalendar);
                dpdJoinDate.show(getFragmentManager(), "Datepickerdialog");
                break;
        }
    }

    private void launchActivityWithUserDataCheck(Class<?> cls) {
        if (user != null) {
            startActivity(new Intent(AddClientActivity.this, cls).putExtra("user", user));
        } else {
            toolbarTitle.setText("Add Client");
            scrollView.setVisibility(View.GONE);
            llOptions.setVisibility(View.VISIBLE);
        }
    }

    private void addClient() {
        String first_name = etFirstName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();
        String str_dob = etDob.getText().toString().trim();
        String str_joinDate = etJoinDate.getText().toString().trim();
        String str_joinPurpose = etJoinPurpose.getText().toString().trim();
        if (TextUtils.isEmpty(first_name)) {
            etFirstName.setError("Enter first name");
            etFirstName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(last_name)) {
            etLastName.setError("Enter last lame");
            etLastName.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            etEmail.setError("Enter email");
            etEmail.requestFocus();
            return;
        }
        if (!Utils.isValidEmail(email)) {
            etEmail.setError("Enter valid email");
            etEmail.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            etMobile.setError("Enter mobile number");
            etMobile.requestFocus();
            return;
        }
        if (mobile.length() != 10) {
            etMobile.setError("Mobile number must be 10 digit number");
            etMobile.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(str_dob)) {
            etDob.setError("Select date of birth");
            etDob.requestFocus();
            return;
        }
        if (rgGender.getCheckedRadioButtonId() == -1) {
            Utils.showLongToast(mContext, "Select gender");
            return;
        }
        if (TextUtils.isEmpty(str_joinDate)) {
            etJoinDate.setError("Select joining date");
            etJoinDate.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(str_joinPurpose)) {
            etJoinPurpose.setError("Enter joining purpose");
            etJoinPurpose.requestFocus();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "createClient");
        params.put("clientFName", first_name);
        params.put("clientLName", last_name);
        params.put("clientMobile", mobile);
        params.put("clientEmail", email);
        params.put("clientDOB", dob);
        params.put("clientGender", rbMale.isChecked() ? "1" : "2");
        params.put("clientJoinDate", joinDate);
        params.put("clientJoinPurpose", str_joinPurpose);
        params.put("userID", "" + app.getPreferences().getLoggedInUser().getData().getUserID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().createClient(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg()))
                                Utils.showLongToast(mContext, response.getMsg());
                            user = response.getData();
                            startActivity(new Intent(mContext, AddClientPackageActivity.class).putExtra("user", user));
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            } else {
                                Utils.showLongToast(mContext, "Unable to create client. Please try again later.");
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_create_client;
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getClientProfile");
        map.put("clientID", user.getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getClientProfile(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    ClientProfileData clientProfileData = (ClientProfileData) object;
                    if (clientProfileData != null && clientProfileData.getCount() > 0) {
                        ClientProfile clientProfile = clientProfileData.getClientProfile();
                        String str_pkgEndDate = clientProfile.getPkgEndDate();
                        String str_pkgStartDate = clientProfile.getPkgStartDate();
                        if (str_pkgEndDate != null && !str_pkgEndDate.equals("0000-00-00")) {
                            try {
                                pkgEndDate = formatter.parse(str_pkgEndDate);
                                if (new Date().after(pkgEndDate)) {
                                    scrollView.setVisibility(View.GONE);
                                    llOptions.setVisibility(View.VISIBLE);
                                    tvError.setVisibility(View.VISIBLE);
                                } else {
                                    scrollView.setVisibility(View.GONE);
                                    llOptions.setVisibility(View.VISIBLE);
                                    tvError.setVisibility(View.GONE);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            setPkgStartEndDateValidations();
                        }
//                        if (str_pkgStartDate != null && !str_pkgStartDate.equals("0000-00-00")) {
//                            try {
//                                pkgStartDate = formatter.parse(str_pkgStartDate);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                        }
                    } else {
//                        if (clientProfileData != null && clientProfileData.getMsg() != null && !TextUtils.isEmpty(clientProfileData.getMsg())) {
//                            Utils.showLongToast(mContext, clientProfileData.getMsg());
//                        }
                        setPkgStartEndDateValidations();
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    setPkgStartEndDateValidations();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void setPkgStartEndDateValidations() {
        if (user.getPkgEndDate() != null && !user.getPkgEndDate().equals("0000-00-00")) {
            String str_pkgEndDate = user.getPkgEndDate();
            try {
                this.pkgEndDate = formatter.parse(str_pkgEndDate);
                if (new Date().after(this.pkgEndDate)) {
                    scrollView.setVisibility(View.GONE);
                    llOptions.setVisibility(View.VISIBLE);
                    tvError.setVisibility(View.VISIBLE);
                } else {
                    scrollView.setVisibility(View.GONE);
                    llOptions.setVisibility(View.VISIBLE);
                    tvError.setVisibility(View.GONE);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            if (user.getPkgEndDate() != null && user.getPkgEndDate().equals("0000-00-00")) {
                tvError.setText("Sorry the client has not purchased any package. Buy Package to create workout schedule.");
                tvError.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_home:
//                if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing()) {
//                    AddClientPackageActivity.addClientPackageActivity.finish();
//                }
//                if (ClientHomeActivity.clientHomeActivity != null && !ClientHomeActivity.clientHomeActivity.isFinishing()) {
//                    ClientHomeActivity.clientHomeActivity.finish();
//                }
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
        }
        return true;
    }
}
