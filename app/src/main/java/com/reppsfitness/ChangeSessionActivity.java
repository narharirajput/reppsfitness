package com.reppsfitness;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.adapter.WeekDaysListAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.SlotData;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WeekDay;
import com.reppsfitness.model.WeekDayDatum;
import com.reppsfitness.utils.MyComparator;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;

public class ChangeSessionActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public static ChangeSessionActivity addClientActivity;
    String fromDate;
    @BindView(R.id.et_time)
    EditText etTime;
    @BindView(R.id.rv_days)
    RecyclerView rv_days;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.font_black_0)
    int font_black_0;
    RegularItem regularItem;
    TermItem termItem;
    User user;
    private List<String> slotTimings;
    private String selectedTime;
    SimpleDateFormat ymdSdf = new SimpleDateFormat("yyyy-MM-dd");
    private int maxPerWeekSession, minPerWeekSession;
    private ArrayList<WeekDay> weekDayList;
    private ArrayList<Integer> checkedList;
    private ArrayList<String> checkedDayList;
    String packageID = "";
    //    String sessionDays;
    private MyComparator myComparator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addClientActivity = this;
        termItem = getIntent().getParcelableExtra("termItem");
        regularItem = getIntent().getParcelableExtra("regularItem");
        user = getIntent().getParcelableExtra("user");
        packageID = getIntent().getStringExtra("packageID");
//        sessionDays = getIntent().getStringExtra("sessionDays");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        fromDate = ymdSdf.format(new Date());
        String sessionTime = getIntent().getStringExtra("sessionTime");
        if (sessionTime != null)
            etTime.setText(sessionTime);
        toolbarTitle.setText(sessionTime != null ? "Change session" : "Allot session");
//        rv_days.setLayoutManager(new GridLayoutManager(mContext, 2));
        rv_days.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        rv_days.setHasFixedSize(true);
        weekDayList = new ArrayList<>();
        checkedList = new ArrayList<>();
        checkedDayList = new ArrayList<>();
        getSessionClientRequestTime();
    }

    @OnClick({R.id.et_time, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.et_time:
                if (slotTimings != null && slotTimings.size() > 0) {
                    new MaterialDialog.Builder(mContext)
                            .items(slotTimings)
                            .itemsCallback((dialog, view1, which, text) -> {
                                selectedTime = text.toString();
                                etTime.setText(selectedTime);
                            })
                            .show();
                }
                break;
            case R.id.btn_submit:
                allot_session();
                break;
        }
    }

    private void allot_session() {
//        String time = etTime.getText().toString();
        String sessionDays = "";
//        HashMap<String, String> checkedDayValList = new HashMap<>();
        if (checkedDayList == null || checkedDayList.size() == 0) {
            new MaterialDialog.Builder(mContext).content("Please select session days").positiveText("Ok").show();
            return;
        }
        if (checkedDayList != null && checkedDayList.size() < minPerWeekSession) {
            new MaterialDialog.Builder(mContext).content("Please select at least " + minPerWeekSession + " session days").positiveText("Ok").show();
            return;
        }
        JSONObject checkedDayValList = new JSONObject();
        if (checkedDayList != null && checkedDayList.size() > 0) {
            try {
                Collections.sort(checkedDayList, myComparator);
            } catch (Exception e) {
                e.printStackTrace();
            }
            sessionDays = checkedDayList.toString();
            sessionDays = sessionDays.replace("]", "");
            sessionDays = sessionDays.replace("[", "");
            sessionDays = sessionDays.replace(" ", "");
        }
        if (weekDayList != null && weekDayList.size() > 0) {
            for (int i = 0; i < weekDayList.size(); i++) {
                WeekDaysListAdapter.WeekDayViewHolder holder = (WeekDaysListAdapter.WeekDayViewHolder) rv_days.findViewHolderForAdapterPosition(i);
                for (int j = 0; j < checkedDayList.size(); j++) {
                    if (holder.checkbox.getText().toString().toLowerCase().matches(checkedDayList.get(j).toLowerCase())) {
                        try {
                            if (holder.etTime.getText() == null || TextUtils.isEmpty(holder.etTime.getText().toString())) {
                                Utils.showShortToast(mContext, "Select Time");
                                return;
                            }
                            checkedDayValList.put(holder.checkbox.getText().toString(), holder.etTime.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        Log.e("checkedDayValList", sessionDays + "||" + checkedDayValList.toString());
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "allottSessionToClient");
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("packageID", packageID);
        params.put("sessionDays", checkedDayValList.toString().toLowerCase());
        params.put("clientID", user.getClientID());
        for (String key : params.keySet()) {
            System.out.println(key + "=" + params.get(key));
        }
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().allottSessionToClient(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing()) {
                                AddClientPackageActivity.addClientPackageActivity.finish();
                            }
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showSimpleialog(mContext, response.getMsg(),"Ok");
                            } else {
                                Utils.showLongToast(mContext, "Unable to allot session. Please try again later.");
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_change_session;
    }

    private void getSessionClientRequestTime() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getSessionClientRequestTime");
        map.put("clientID", user.getClientID());
        map.put("packageID", packageID);
        map.put("requestDate", fromDate);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getSessionClientRequestTime(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    SlotData slotData = (SlotData) object;
                    if (slotData != null && slotData.getCount() > 0) {
                        if (slotData.getData() != null) {
                            slotTimings = slotData.getData().getSlotTime();
                            if (slotData.getData().getMaxPerWeekSession() != null && !TextUtils.isEmpty(slotData.getData().getMaxPerWeekSession()))
                                maxPerWeekSession = Integer.parseInt(slotData.getData().getMaxPerWeekSession());
                            if (slotData.getData().getMinPerWeekSession() != null && !TextUtils.isEmpty(slotData.getData().getMinPerWeekSession()))
                                minPerWeekSession = Integer.parseInt(slotData.getData().getMinPerWeekSession());
                            if (slotData.getData().getWeekDayData().size() > 0) {
                                List<String> dayStrList = new ArrayList<>();
                                for (int i = 0; i < slotData.getData().getWeekDayData().size(); i++) {
                                    WeekDayDatum weekDayDatum = slotData.getData().getWeekDayData().get(i);
                                    weekDayList.add(new WeekDay(weekDayDatum.getDay(), weekDayDatum.getIsWeekDayOff() == 0, weekDayDatum.getSlotTimeVal()));
                                    dayStrList.add(weekDayDatum.getDay());
                                }
                                try {
                                    myComparator = new MyComparator(dayStrList.toArray(new String[dayStrList.size()]));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                if (slotData.getData().getClientSessionDays() != null && slotData.getData().getClientSessionDays().size() > 0) {
                                    List<String> list = slotData.getData().getClientSessionDays();
//                                    List<String> list = new ArrayList<String>(Arrays.asList(sessionDays.split(",")));
                                    for (int i = 0; i < weekDayList.size(); i++) {
                                        Log.e("i", "" + i);
                                        for (int j = 0; j < list.size(); j++) {
                                            if (list.get(j).toUpperCase().trim().matches(weekDayList.get(i).getDay().toUpperCase().trim())) {
                                                checkedDayList.add(weekDayList.get(i).getDay());
                                                checkedList.add(i);
                                                weekDayList.get(i).setChecked(true);
                                            }
                                        }
                                    }
                                }
                                Log.e("e", checkedDayList + "||" + checkedList + "||" + weekDayList.toString());
                                rv_days.setAdapter(new WeekDaysListAdapter(mContext, weekDayList, slotTimings, new CheckboxClickListener() {
                                    @Override
                                    public void onCheckboxClick(CheckBox checkBox, int position) {
                                        if (checkBox.isEnabled()) {
                                            if (checkBox.isChecked()) {
                                                if (checkedList.size() < maxPerWeekSession) {
                                                    checkedList.add(position);
                                                    checkedDayList.add(weekDayList.get(position).getDay());
                                                    checkBox.setTextColor(checkBox.isChecked() ? colorPrimary : font_black_0);
                                                } else {
                                                    Utils.showShortToast(mContext, "Max" + maxPerWeekSession + "allowed");
                                                    checkBox.setChecked(false);
                                                }
                                            } else {
                                                if (checkedList.size() > minPerWeekSession) {
                                                    for (int i = 0; i < checkedList.size(); i++) {
                                                        if (checkedList.get(i) == position) {
                                                            checkedList.remove(i);
                                                            checkedDayList.remove(i);
                                                            break;
                                                        }
                                                    }
                                                    checkBox.setChecked(false);
                                                } else {
                                                    checkBox.setChecked(!checkBox.isChecked());
                                                    Utils.showShortToast(mContext, "Min " + minPerWeekSession + " allowed");
                                                }
                                                checkBox.setTextColor(checkBox.isChecked() ? colorPrimary : font_black_0);
                                            }
                                        } else {
                                            Utils.showShortToast(mContext, "Off Day");
                                        }
                                        Log.e("checkedlist", checkedList.toString() + "||" + checkedDayList.toString());
                                    }
                                }));
                            }
                        }
                    } else {
                        if (slotData != null && slotData.getMsg() != null && !TextUtils.isEmpty(slotData.getMsg())) {
                            Utils.showLongToast(mContext, slotData.getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

//    private void getClientProfile() {
//        Map<String, String> map = new HashMap<>();
//        map.put("xAction", "getClientProfile");
//        map.put("clientID", user.getClientID());
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().getClientProfile(map, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    ClientProfileData clientProfileData = (ClientProfileData) object;
//                    if (clientProfileData != null && clientProfileData.getCount() > 0) {
//                        ClientProfile clientProfile = clientProfileData.getClientProfile();
//                        String str_pkgEndDate = clientProfile.getPkgEndDate();
//                        String str_pkgStartDate = clientProfile.getPkgStartDate();
//                        user.setPkgEndDate(str_pkgEndDate);
//                        user.setPkgStartDate(str_pkgStartDate);
//                        if (str_pkgEndDate != null && !str_pkgEndDate.equals("0000-00-00")) {
//                            try {
//                                pkgEndDate = ymdSdf.parse(str_pkgEndDate);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
//    }

//    private void displayTimePickerDialog(boolean isFrom) {
//        Calendar now = Calendar.getInstance();
//        TimePickerDialog tpd = TimePickerDialog.newInstance(
//                (view, hourOfDay, minute, second) -> {
//                    Log.e("tpd", hourOfDay + "||" + minute + "||" + second);
//                    if (isFrom) {
//                        fromTime = hourOfDay + ":" + minute;
//                        etFromTime.setText(fromTime);
//                    } else {
//                        toTime = hourOfDay + ":" + minute;
//                        etToTime.setText(toTime);
//                    }
//                },
//                now.get(Calendar.HOUR_OF_DAY),
//                now.get(Calendar.MINUTE),
//                true
//        );
//        tpd.show(getFragmentManager(), "Datepickerdialog");
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.action_home:
//                Intent intent = new Intent(this, AdminMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(intent);
//                finish();
//                break;
        }
        return true;
    }
}
