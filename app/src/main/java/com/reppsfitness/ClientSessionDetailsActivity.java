package com.reppsfitness;

import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.adapter.SessionMemberAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.AllotData;
import com.reppsfitness.model.SessionDetailsData;
import com.reppsfitness.model.Slotcancelpojo;
import com.reppsfitness.session_cal.ClientSessionsFragment;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.utils.ItemOffsetDecoration;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import org.json.JSONException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class ClientSessionDetailsActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.rv_session_members)
    RecyclerView rvSessionMembers;
    @BindView(R.id.rv_queued_members)
    RecyclerView rvQueuedMembers;
    String sessionTime, sessionDate, allotted, clientLimit,sessionDateNew;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.tv_capacity)
    TextView tvCapacity;
    @BindView(R.id.btn_allot)
    Button btnAllot;
    @BindView(R.id.btn_request)
    Button btnRequest;
    @BindView(R.id.cv_queuedMembers)
    CardView cvQueuedMembers;
    DayDetails dayDetails;
    String clientID;
    boolean isClientPresent = false;
    private Date dateObj = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionTime = getIntent().getStringExtra("sessionTime");
        sessionDate = getIntent().getStringExtra("sessionDate");
        dayDetails = getIntent().getParcelableExtra("dayDetails");
        allotted = dayDetails.getAlloted() + "";
        clientLimit = dayDetails.getClientLimit();
        clientID = app.getPreferences().getLoggedInUser().getData().getClientID();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Session Details");
        cvQueuedMembers.setVisibility(View.GONE);

        try {
            SimpleDateFormat spf=new SimpleDateFormat("dd-MM-yyyy");
            Date newDate=spf.parse(sessionDate);
            spf= new SimpleDateFormat("dd/MM/yyyy");
            sessionDateNew = spf.format(newDate);
            //System.out.println(date);

            dateObj  = Utils.curFormater.parse(sessionDateNew);

        } catch (ParseException e) {
            e.printStackTrace ();
        }
        setRvSessionMembers();
        getSessionDetailsList();
        tvDate.setText(sessionDate);
        tvTime.setText(sessionTime);
        tvCapacity.setText("Capacity: " + clientLimit + "  |  Allotted: " + allotted);
    }

    private void getSessionDetailsList() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getSessionDetailsList");
        map.put("sessionTime", sessionTime);
        map.put("sessionDate", sessionDate);
        map.put("sessionID", dayDetails.getSessionID() != null ? dayDetails.getSessionID() : "");
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getSessionDetailsList(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    SessionDetailsData response = (SessionDetailsData) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getAllottdata() != null && response.getAllottdata().size() > 0) {
                            List<AllotData> allottdata = response.getAllottdata();
                            rvSessionMembers.setAdapter(new SessionMemberAdapter(mContext, allottdata, dayDetails, app));
                            for (int i = 0; i < allottdata.size(); i++) {
                                if (allottdata.get(i).getClientID().equals(clientID)) {
                                    if (dateObj!=null && !new Date ().after (dateObj)) {
                                        isClientPresent = true;
                                    }
                                    break;
                                }
                            }
                            if (isClientPresent) {
                                btnRequest.setVisibility(View.VISIBLE);
                            }
                            btnRequest.setOnClickListener(view -> new MaterialDialog.Builder(mContext).
                                    content("Do you want to cancel this session").
                                    positiveText("Yes").
                                    negativeText("No").
                                    onPositive((dialog, which) -> Slotclientcancel(allottdata, dialog, app)).
                                    show());

                        }
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_session_details;
    }

    @OnClick(R.id.btn_request)
    public void onViewClicked() {


    }

    private void setRvSessionMembers() {
        rvSessionMembers.setHasFixedSize(true);
        rvSessionMembers.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvSessionMembers.addItemDecoration(new ItemOffsetDecoration(mContext, R.dimen.size_5));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void Slotclientcancel(List<AllotData> data, MaterialDialog dialog, App user) {
        Map<String, String> slotcancel = new HashMap<>();
        slotcancel.put("xAction", "slotCancel");
        for (AllotData allotData : data) {
            if (allotData.getClientID().equals(user.getPreferences().getLoggedInUser().getData().getClientID())) {
                slotcancel.put("clientID", allotData.getClientID());
                slotcancel.put("sessionID", allotData.getSessionID());
                break;
            }
        }
        slotcancel.put("userType", user.getPreferences().getLoggedInUser().getData().getUserType());
        Log.e("slotCAncel", slotcancel.toString());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().slotCancel(slotcancel, new ApiRequestHelper.OnRequestComplete() {
                public static final String TAG = "";

                @Override
                public void onSuccess(Object object) throws JSONException {
                    if (pd.isShowing()) pd.dismiss();
                    dialog.dismiss();
                    Slotcancelpojo slotcancel = (Slotcancelpojo) object;
                    Log.e(TAG, "onSuccess: " + slotcancel);

                    if (slotcancel != null && slotcancel.getCount() > 0) {
                            isClientPresent=false;
                            btnRequest.setVisibility(View.GONE);
                            getSessionDetailsList();
                            ClientSessionsFragment.reloadSchedule=true;
                       // finish();
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    Toast.makeText(mContext, "response Unsuccessfull", Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

}
