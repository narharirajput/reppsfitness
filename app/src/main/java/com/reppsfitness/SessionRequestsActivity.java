package com.reppsfitness;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.reppsfitness.adapter.SessionRequestAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.SessionRequest;
import com.reppsfitness.model.SessionRequestData;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

public class SessionRequestsActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView tv_error;
   // String formattedDate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Session Requests");
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
      /*  Date c = Calendar.getInstance ().getTime ();
        //System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat ("yyyy-MM-dd");
        String formattedDate = df.format (c);*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAdminSessionRequest();
    }

    private void getAdminSessionRequest() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getAdminSessionRequest");
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getAdminSessionRequest(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    SessionRequestData sessionRequestData = (SessionRequestData) object;
                    if (sessionRequestData != null && sessionRequestData.getCount() > 0) {
                        if (sessionRequestData.getData().size() > 0) {
                            List<SessionRequest> sessionRequestList = sessionRequestData.getData();
                            recyclerView.setAdapter(new SessionRequestAdapter(mContext, sessionRequestList));
                            manageVisibility(View.GONE, View.VISIBLE);
                        } else {
                            if (sessionRequestData.getMsg() != null && !TextUtils.isEmpty(sessionRequestData.getMsg())) {
                                Utils.showLongToast(mContext, sessionRequestData.getMsg());
                            }
                            manageVisibility(View.VISIBLE, View.GONE);
                        }
                    } else {
                        if (sessionRequestData != null && sessionRequestData.getMsg() != null && !TextUtils.isEmpty(sessionRequestData.getMsg())) {
                            Utils.showLongToast(mContext, sessionRequestData.getMsg());
                        }
                        manageVisibility(View.VISIBLE, View.GONE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    manageVisibility(View.VISIBLE, View.GONE);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    public void updateSessionRequestStatus(String sessionCRID,String requestStatus) {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "updateSessionRequestStatus");
        map.put("sessionCRID", sessionCRID);
        map.put("requestStatus", requestStatus);
        map.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().updateSessionRequestStatus(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null && response.getCount() > 0) {
                        if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                        getAdminSessionRequest();
                    } else {
                        if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                            Utils.showLongToast(mContext, response.getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_session_requests;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void manageVisibility(int gone, int visible) {
        tv_error.setVisibility(gone);
        recyclerView.setVisibility(visible);
    }
}
