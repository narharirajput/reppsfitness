package com.reppsfitness.api;

import com.reppsfitness.exercise.model.WorkoutResponse;
import com.reppsfitness.model.AddBmi;
import com.reppsfitness.model.BmiData;
import com.reppsfitness.model.ClientExerciseRes;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.ClientsData;
import com.reppsfitness.model.DayExerciseData;
import com.reppsfitness.model.ExerciseData;
import com.reppsfitness.model.ForgotPassword;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.NotificationData;
import com.reppsfitness.model.PackageData;
import com.reppsfitness.model.PaymentModeData;
import com.reppsfitness.model.PlansData;
import com.reppsfitness.model.SaveTemplateResponse;
import com.reppsfitness.model.SentRequestData;
import com.reppsfitness.model.SessionDetailsData;
import com.reppsfitness.model.SessionRequestData;
import com.reppsfitness.model.ShiftSessionData;
import com.reppsfitness.model.SlotData;
import com.reppsfitness.model.Slotcancelpojo;
import com.reppsfitness.model.TemplateData;
import com.reppsfitness.model.UserData;
import com.reppsfitness.model.WorkoutExerciseData;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.model.WorkoutTypeData;

import org.json.JSONObject;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface AppService {
    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<UserData> call_api(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> login(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<ForgotPassword> forgotPassword(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
//    Call<PackageData> packages(@FieldMap Map<String, String> params);
    Call<PlansData> packages(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<ClientsData> getClients(@Field("userID") String userID, @Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<WorkoutExerciseData> getWorkoutExerciseElements(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<WorkoutTypeData> getWorkoutElements(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<WorkoutScheduleResponse> getTemplateDayList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<WorkoutScheduleResponse> saveWorkoutScheduleDayList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<WorkoutScheduleResponse> getClientScheduleDayList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<DayExerciseData> getScheduleDetails(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> updateScheduleDayWorkout(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<TemplateData> getTemplateList(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> createClient(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> updateAdminProfile(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> updateClientProfile(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> changeClientPassword(@FieldMap Map<String, String> params);

    @POST("/repps-fitness/1.0/services/index.php")
    Observable<LoginResponse> uploadProfilePictureRx(@Body RequestBody requestBody);

    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> uploadProfilePicture(@Body RequestBody requestBody);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> saveAsTemplate(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> clientPackageRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<PaymentModeData> getPaymentModes(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<SaveTemplateResponse> saveTemplate(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> assignTemplateToSchedule(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<LoginResponse> deleteScheduleExercise(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Observable<ClientExerciseRes> getScheduleDetailsForClient(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/services/index.php")
    Observable<BmiData> bmiDetails(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/services/index.php")
    Observable<AddBmi> addBmi(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/services/index.php")
    Observable<SentRequestData> getClientSessionRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/services/index.php")
    Call<LoginResponse> updateWorkoutScheduleComment(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<SlotData> getSessionClientRequestTime(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> requestSession(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<ClientProfileData> getClientProfile(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<SessionRequestData> getAdminSessionRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> updateSessionRequestStatus(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> allottSessionToClient(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<SessionDetailsData> getSessionDetailsList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> isPackageValidateCheck(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> workoutScheduleAttended(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> adminAllotSlotClient(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<NotificationData> getNotificationList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<ExerciseData> getWorkoutExerciseElementsNew(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<WorkoutExerciseData> getWorkoutExerciseEle(@Field("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<WorkoutResponse> getScheduleDetailsForClientNew(@FieldMap Map<String, String> params);

    @GET("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> getOtherSettings(@Query("xAction") String xAction);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<LoginResponse> clearNotifications(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<ShiftSessionData> getShiftSessionDateList(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("/repps-fitness/1.0/services/index.php")
    Call<Slotcancelpojo> getSlotCancel(@FieldMap Map<String, String> params);
}
