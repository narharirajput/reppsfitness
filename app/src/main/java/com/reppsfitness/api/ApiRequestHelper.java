package com.reppsfitness.api;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.reppsfitness.App;
import com.reppsfitness.exercise.model.WorkoutResponse;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.ClientsData;
import com.reppsfitness.model.ExerciseData;
import com.reppsfitness.model.ForgotPassword;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.NotificationData;
import com.reppsfitness.model.PlansData;
import com.reppsfitness.model.SaveTemplateResponse;
import com.reppsfitness.model.SessionDetailsData;
import com.reppsfitness.model.SessionRequestData;
import com.reppsfitness.model.ShiftSessionData;
import com.reppsfitness.model.SlotData;
import com.reppsfitness.model.Slotcancelpojo;
import com.reppsfitness.model.WorkoutExerciseData;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.utils.ClientProfileDataDeserializer;
import com.reppsfitness.utils.ForgetPassResponseModelDeserializer;
import com.reppsfitness.utils.LoginResponseModelDeserializer;
import com.reppsfitness.utils.SaveTemplateResponseDeserializer;
import com.reppsfitness.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.ContentValues.TAG;

public class
ApiRequestHelper {

    public static interface OnRequestComplete {
        public void onSuccess(Object object) throws JSONException;

        public void onFailure(String apiResponse);
    }

    private static ApiRequestHelper instance;
    private App app;
    private AppService appService;
    private AppService appRxService;
    static Gson gson;

    public static synchronized ApiRequestHelper init(App app) {
        if (null == instance) {
            instance = new ApiRequestHelper();
            instance.setApplication(app);
            gson = new GsonBuilder()
                    .setLenient()
                    .registerTypeAdapterFactory(new NullStringToEmptyAdapterFactory())
                    .registerTypeAdapter(LoginResponse.class, new LoginResponseModelDeserializer())
                    .registerTypeAdapter(ForgotPassword.class, new ForgetPassResponseModelDeserializer ())
                    .registerTypeAdapter(ClientProfileData.class, new ClientProfileDataDeserializer())
                    .registerTypeAdapter(SaveTemplateResponse.class, new SaveTemplateResponseDeserializer())
                    .create();
            Log.e(TAG, "init: "+gson );
            instance.createRestAdapter();
        }
        return instance;
    }

    public synchronized AppService getApiService() {
        return instance.createRetrofitBuilder();
    }

    public static class NullStringToEmptyAdapterFactory<T> implements TypeAdapterFactory {
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {

            Class<T> rawType = (Class<T>) type.getRawType();
            if (rawType != String.class) {
                return null;
            }
            return (TypeAdapter<T>) new StringAdapter();
        }
    }

    public static class StringAdapter extends TypeAdapter<String> {
        public String read(JsonReader reader) throws IOException {
            if (reader.peek() == JsonToken.NULL) {
                reader.nextNull();
                return "";
            }
            return reader.nextString();
        }

        public void write(JsonWriter writer, String value) throws IOException {
            if (value == null) {
                writer.nullValue();
                return;
            }
            writer.value(value);
        }
    }

    /**
     * API Calls
     */
    private void handle_fail_response(Throwable t, OnRequestComplete onRequestComplete) {
        if (t != null && t.getMessage() != null)
            Log.e("server msg", Html.fromHtml(t.getMessage()) + "");
        if (t != null && t.getMessage() != null) {
            if (t.getMessage().contains("Unable to resolve host")) {
                onRequestComplete.onFailure(Utils.NO_INTERNET_MSG);
            } else {
                onRequestComplete.onFailure(Html.fromHtml(t.getMessage()) + "");
            }
        } else {
            onRequestComplete.onFailure(Utils.UNPROPER_RESPONSE);
        }
    }

//    public void login(Map<String, String> params, final OnRequestComplete onRequestComplete) {
//        Call<LoginResponse> call = appService.login(params);
//        call.enqueue(new Callback<LoginResponse>() {
//            @Override
//            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//                if (response.isSuccessful()) {
//                    onRequestComplete.onSuccess(response.body());
//                } else {
//                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<LoginResponse> call, Throwable t) {
//                handle_fail_response(t, onRequestComplete);
//            }
//        });
//    }

//    public void getAdminClients(String user_id, final OnRequestComplete onRequestComplete) {
//        Call<ClientsData> call = appService.getAdminClients(user_id, "getAdminClients");
//        call.enqueue(new Callback<ClientsData>() {
//            @Override
//            public void onResponse(Call<ClientsData> call, Response<ClientsData> response) {
//                if (response.isSuccessful()) {
//                    onRequestComplete.onSuccess(response.body());
//                } else {
//                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ClientsData> call, Throwable t) {
//                handle_fail_response(t, onRequestComplete);
//            }
//        });
//    }

//    public void createClient(Map<String, String> params, final OnRequestComplete onRequestComplete) {
//        Call<UserData> call = appService.call_api(params);
//        call.enqueue(new Callback<UserData>() {
//            @Override
//            public void onResponse(Call<UserData> call, Response<UserData> response) {
//                if (response.isSuccessful()) {
//                    onRequestComplete.onSuccess(response.body());
//                } else {
//                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<UserData> call, Throwable t) {
//                handle_fail_response(t, onRequestComplete);
//            }
//        });
//    }

//    public void getPackages(Map<String, String> params, final OnRequestComplete onRequestComplete) {
//        Call<PlansData> call = appService.packages(params);
//        call.enqueue(new Callback<PlansData>() {
//            @Override
//            public void onResponse(Call<PlansData> call, Response<PlansData> response) {
//                if (response.isSuccessful()) {
//                    onRequestComplete.onSuccess(response.body());
//                } else {
//                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<PlansData> call, Throwable t) {
//                handle_fail_response(t, onRequestComplete);
//            }
//        });
//    }

//    public void getClients(String user_id, final OnRequestComplete onRequestComplete) {
//        Observable<ClientsData> getAdminClients = appRxService.getClients(user_id, "getAdminClients");
//
//
//    }

    public void createClient(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.createClient(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void updateAdminProfile(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.updateAdminProfile(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void updateClientProfile(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.updateClientProfile(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void changeClientPassword(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.changeClientPassword(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void login(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.login(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void forgotPassword(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<ForgotPassword> call = appService.forgotPassword(params);
        call.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(Call<ForgotPassword> call, Response<ForgotPassword> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<ForgotPassword> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void packages(Map<String, String> params, final OnRequestComplete onRequestComplete) {
//        Call<PackageData> call = appService.packages(params);
//        call.enqueue(new Callback<PackageData>() {
//            @Override
//            public void onResponse(Call<PackageData> call, Response<PackageData> response) {
//                if (response.isSuccessful()) {
//                    onRequestComplete.onSuccess(response.body());
//                } else {
//                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<PackageData> call, Throwable t) {
//                handle_fail_response(t, onRequestComplete);
//            }
//        });
        Call<PlansData> call = appService.packages(params);
        call.enqueue(new Callback<PlansData>() {
            @Override
            public void onResponse(Call<PlansData> call, Response<PlansData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<PlansData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getClients(String userID, final OnRequestComplete onRequestComplete) {
        Call<ClientsData> call = appService.getClients(userID, "getAdminClients");
        call.enqueue(new Callback<ClientsData>() {
            @Override
            public void onResponse(Call<ClientsData> call, Response<ClientsData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<ClientsData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getTemplateDayList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<WorkoutScheduleResponse> call = appService.getTemplateDayList(params);
        call.enqueue(new Callback<WorkoutScheduleResponse>() {
            @Override
            public void onResponse(Call<WorkoutScheduleResponse> call, Response<WorkoutScheduleResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<WorkoutScheduleResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void saveWorkoutScheduleDayList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<WorkoutScheduleResponse> call = appService.saveWorkoutScheduleDayList(params);
        call.enqueue(new Callback<WorkoutScheduleResponse>() {
            @Override
            public void onResponse(Call<WorkoutScheduleResponse> call, Response<WorkoutScheduleResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<WorkoutScheduleResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getClientScheduleDayList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<WorkoutScheduleResponse> call = appService.getClientScheduleDayList(params);
        call.enqueue(new Callback<WorkoutScheduleResponse>() {
            @Override
            public void onResponse(Call<WorkoutScheduleResponse> call, Response<WorkoutScheduleResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<WorkoutScheduleResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void updateScheduleDayWorkout(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.updateScheduleDayWorkout(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void uploadProfilePicture(RequestBody requestBody, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.uploadProfilePicture(requestBody);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void saveAsTemplate(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.saveAsTemplate(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void clientPackageRequest(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.clientPackageRequest(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void assignTemplateToSchedule(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.assignTemplateToSchedule(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void updateWorkoutScheduleComment(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.updateWorkoutScheduleComment(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getSessionClientRequestTime(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<SlotData> call = appService.getSessionClientRequestTime(params);
        call.enqueue(new Callback<SlotData>() {
            @Override
            public void onResponse(Call<SlotData> call, Response<SlotData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<SlotData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void requestSession(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.requestSession(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void slotCancel(Map<String, String> params, OnRequestComplete onRequestComplete) {
        Call<Slotcancelpojo> call = appService.getSlotCancel(params);
        call.enqueue(new Callback<Slotcancelpojo>() {
            @Override
            public void onResponse(Call<Slotcancelpojo > call, Response<Slotcancelpojo > response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<Slotcancelpojo > call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }
    public void getClientProfile(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<ClientProfileData> call = appService.getClientProfile(params);
        call.enqueue(new Callback<ClientProfileData>() {
            @Override
            public void onResponse(Call<ClientProfileData> call, Response<ClientProfileData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<ClientProfileData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getAdminSessionRequest(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<SessionRequestData> call = appService.getAdminSessionRequest(params);
        call.enqueue(new Callback<SessionRequestData>() {
            @Override
            public void onResponse(Call<SessionRequestData> call, Response<SessionRequestData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<SessionRequestData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void updateSessionRequestStatus(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.updateSessionRequestStatus(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void allottSessionToClient(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.allottSessionToClient(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getSessionDetailsList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<SessionDetailsData> call = appService.getSessionDetailsList(params);
        call.enqueue(new Callback<SessionDetailsData>() {
            @Override
            public void onResponse(Call<SessionDetailsData> call, Response<SessionDetailsData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<SessionDetailsData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void isPackageValidateCheck(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.isPackageValidateCheck(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void workoutScheduleAttended(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.workoutScheduleAttended(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void adminAllotSlotClient(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.adminAllotSlotClient(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getNotificationList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<NotificationData> call = appService.getNotificationList(params);
        call.enqueue(new Callback<NotificationData>() {
            @Override
            public void onResponse(Call<NotificationData> call, Response<NotificationData> response) {
                Log.e(TAG, "onResponse: "+call );
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<NotificationData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getWorkoutExerciseElementsNew(String params, final OnRequestComplete onRequestComplete) {
        Call<ExerciseData> call = appService.getWorkoutExerciseElementsNew(params);
        call.enqueue(new Callback<ExerciseData>() {
            @Override
            public void onResponse(Call<ExerciseData> call, Response<ExerciseData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<ExerciseData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getWorkoutExerciseEle(String params, final OnRequestComplete onRequestComplete) {
        Call<WorkoutExerciseData> call = appService.getWorkoutExerciseEle(params);
        call.enqueue(new Callback<WorkoutExerciseData>() {
            @Override
            public void onResponse(Call<WorkoutExerciseData> call, Response<WorkoutExerciseData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<WorkoutExerciseData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getScheduleDetailsForClientNew(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<WorkoutResponse> call = appService.getScheduleDetailsForClientNew(params);
        call.enqueue(new Callback<WorkoutResponse>() {
            @Override
            public void onResponse(Call<WorkoutResponse> call, Response<WorkoutResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<WorkoutResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getOtherSettings(String xAction, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.getOtherSettings(xAction);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void clearNotifications(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<LoginResponse> call = appService.clearNotifications(params);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }

    public void getShiftSessionDateList(Map<String, String> params, final OnRequestComplete onRequestComplete) {
        Call<ShiftSessionData> call = appService.getShiftSessionDateList(params);
        call.enqueue(new Callback<ShiftSessionData>() {
            @Override
            public void onResponse(Call<ShiftSessionData> call, Response<ShiftSessionData> response) {
                if (response.isSuccessful()) {
                    try {
                        onRequestComplete.onSuccess(response.body());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    onRequestComplete.onFailure(Html.fromHtml(response.message()) + "");
                }
            }

            @Override
            public void onFailure(Call<ShiftSessionData> call, Throwable t) {
                handle_fail_response(t, onRequestComplete);
            }
        });
    }
    /**
     * End API Calls
     */

    /**
     * REST Adapter Configuration
     */
    private void createRestAdapter() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson));
        Retrofit retrofit = builder.client(getClient().build()).build();
        appService = retrofit.create(AppService.class);
    }

    private AppService createRetrofitBuilder() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(Utils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        Retrofit retrofit = builder.build();
        appRxService = retrofit.create(AppService.class);
        return appRxService;
    }

    @NonNull
    private OkHttpClient.Builder getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
// add your other interceptors …

// add logging as last interceptor
        httpClient.interceptors().add(logging);
        return httpClient;
    }

    /**
     * End REST Adapter Configuration
     */

    public App getApplication() {
        return app;
    }

    public void setApplication(App app) {
        this.app = app;
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MultipartBody.FORM, descriptionString);
    }
}
