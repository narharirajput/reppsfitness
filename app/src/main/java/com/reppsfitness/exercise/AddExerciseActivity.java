package com.reppsfitness.exercise;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.AddAMRAPExercise;
import com.reppsfitness.AddOTMExercise;
import com.reppsfitness.AddRFTExercise;
import com.reppsfitness.AddStandardExercise;
import com.reppsfitness.AdminMainActivity;
import com.reppsfitness.BaseActivity;
import com.reppsfitness.R;
import com.reppsfitness.ViewCommentActivity;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.AddCommentActivity;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.exercise.model.ElementsValueArr;
import com.reppsfitness.exercise.model.Workout;
import com.reppsfitness.exercise.model.WorkoutData;
import com.reppsfitness.exercise.model.WorkoutResponse;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.ExerciseCat;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WorkoutExerciseData;
import com.reppsfitness.model.WorkoutType;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.reppsfitness.widget.searchable_spinner.SpinnerDialog;
import com.reppsfitness.widget.swipelayout.SwipeLayout;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.view.View.GONE;

/**
 * Created by MXCPUU11 on 8/4/2017.
 */

public class AddExerciseActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.tv_day)
    TextView tvDay;
    @BindView(R.id.rv_exercises)
    RecyclerView rvExercises;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.btn_notAttended)
    Button btnNotAttended;
    @BindView(R.id.btn_attended)
    Button btnAttended;
    @BindView(R.id.ll_options)
    LinearLayout llOptions;
    @BindView(R.id.rr_bottom)
    RelativeLayout rrBottom;
    @BindView(R.id.iv_view_comment)
    ImageView ivViewComment;
    @BindView(R.id.iv_done)
    ImageView ivDone;
    @BindView(R.id.tv_add_comment)
    TextView tvAddComment;
    @BindView(R.id.tv_done)
    TextView tvDone;
    @BindView(R.id.rr_header)
    RelativeLayout rrHeader;
    @BindColor(R.color.btn_bg_color)
    int btn_bg_color;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    View rowView;
    public List<ExerciseCat> exerciseCatList = new ArrayList<> ();
    public List<WorkoutType> workoutTypeList = new ArrayList<> ();
    Day day;
    int dayPosition;
    public ArrayList<String> workoutTypeNameList = new ArrayList<> ();
    public ArrayList<String> workoutCatNameList = new ArrayList<> ();
    User user;
    ArrayList<String> timeListOTM = new ArrayList<> ();
    ArrayList<String> timeListAMRAP = new ArrayList<> ();
    ArrayList<String> roundListRFT = new ArrayList<> ();
    boolean isTemplate;
    String date, templateID, dayText, comment = "", exDate;
    private List<ElementsValueArr> elementsValueArr = new ArrayList<> ();
    private final int iv_view_comment = R.id.iv_view_comment;
    Date exDateObj = null;

    /*     SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd");
                Date newDate=spf.parse(sessionDate);
                spf= new SimpleDateFormat("dd/MM/yyyy");
                sessionDateNew = spf.format(newDate);

                dateObj  = Utils.curFormater.parse(sessionDateNew);
                if (dateObj!=null && !new Date ().after (dateObj)) {

                }*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        toolbarTitle.setText ("Workout Schedule");
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        date = getIntent ().getStringExtra ("date");
        day = getIntent ().getParcelableExtra ("day");

        SimpleDateFormat spf = new SimpleDateFormat ("yyyy-MM-dd");
        try {
            if (day.getDate ()!=null) {
                Date newDate = spf.parse (day.getDate ());
                spf = new SimpleDateFormat ("dd/MM/yyyy");
                exDate = spf.format (newDate);
                exDateObj = Utils.curFormater.parse (exDate);
                if (new Date ().before (exDateObj)) {
                    llOptions.setVisibility (View.GONE);
                }
            }
        } catch (ParseException e) {
            e.printStackTrace ();
        }

        user = getIntent ().getParcelableExtra ("user");
        dayPosition = getIntent ().getIntExtra ("dayPosition", 0);
        isTemplate = getIntent ().getBooleanExtra ("isTemplate", false);
        templateID = getIntent ().getStringExtra ("templateID");
        dayText = getIntent ().getStringExtra ("dayText");
//        Log.e("val", isTemplate + "||" + templateID + "||" + dayText);
        rvExercises.setLayoutManager (new LinearLayoutManager (mContext, LinearLayoutManager.VERTICAL, false));
        fab.setVisibility (app.getPreferences ().isClient () ? GONE : View.VISIBLE);
        if (dayText != null) tvDay.setText (dayText);
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_standard_workout;
    }

    @Override
    protected void onResume() {
        super.onResume ();
        //if (!app.getPreferences().isClient() && !isTemplate) {
        if (!isTemplate) {
            ivViewComment.setVisibility (app.getPreferences ().isClient () ? GONE : View.VISIBLE);
            ivDone.setVisibility (GONE);
            tvAddComment.setVisibility (GONE);
            tvDone.setVisibility (GONE);
        }
        if (exerciseCatList.size () > 0) exerciseCatList.clear ();
        if (workoutTypeList.size () > 0) workoutTypeList.clear ();
        if (workoutTypeNameList.size () > 0) workoutTypeNameList.clear ();
        if (workoutCatNameList.size () > 0) workoutCatNameList.clear ();
        if (elementsValueArr.size () > 0) elementsValueArr.clear ();
        if (timeListOTM.size () > 0) timeListOTM.clear ();
        if (timeListAMRAP.size () > 0) timeListAMRAP.clear ();
        if (roundListRFT.size () > 0) roundListRFT.clear ();

        if (llContent != null && llContent.getChildCount () > 0) {
            llContent.removeAllViews ();
        }
        getWorkoutExerciseElements ();
    }

    @OnClick(R.id.fab)
    public void onFabClicked() {
        addExerciseView ();
    }

    private void addExerciseView() {
        rowView = getLayoutInflater ().inflate (R.layout.exercise_sample, null);
        int id = new Random ().nextInt (10000);
        final SwipeLayout swipeLayout = rowView.findViewById (R.id.swipeLayout);
        swipeLayout.setSwipeEnabled (false);
        final EditText et_type = rowView.findViewById (R.id.et_type);
        final EditText et_time = rowView.findViewById (R.id.et_time);
        final EditText et_rounds = rowView.findViewById (R.id.et_rounds);
        final EditText et_category = rowView.findViewById (R.id.et_category);
        final RelativeLayout rr_next = rowView.findViewById (R.id.rr_next);
        final RelativeLayout rr_time = rowView.findViewById (R.id.rr_time);
        final RelativeLayout rr_rounds = rowView.findViewById (R.id.rr_rounds);
        final RelativeLayout rr_category = rowView.findViewById (R.id.rr_category);
        swipeLayout.findViewById (R.id.tv_repeat).setOnClickListener (view -> {
//            repeatExerciseView(id);
            swipeLayout.close ();
        });
        swipeLayout.findViewById (R.id.tv_delete).setOnClickListener (view -> {
            deleteExerciseView (id);
            swipeLayout.close ();
        });
        if (workoutTypeList != null && workoutTypeList.size () > 0) {
            et_type.setOnClickListener (view -> {
                if (!app.getPreferences ().isClient ()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, workoutTypeNameList, "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                    spinnerDialog.showSpinerDialog ();
                    spinnerDialog.bindOnSpinerListener ((item, position) -> {
                        swipeLayout.setSwipeEnabled (false);
                        et_type.setText (item);
                        et_type.setTag (workoutTypeList.get (position).getWorkoutTypeID ());
                        String workoutTypeName = workoutTypeList.get (position).getWorkoutTypeName ();
                        manageTypesVisibility (et_time, et_rounds, et_category, rr_time, rr_rounds, rr_category, workoutTypeName);
                    });
                }
            });
            rr_next.setOnClickListener (view -> {
                String type = et_type.getText ().toString ();
                String workoutTypeID = (String) et_type.getTag ();
                if (!TextUtils.isEmpty (type)) {
                    if (type.equalsIgnoreCase ("STANDARD")) {
                        String category = et_category.getText ().toString ();
                        if (!TextUtils.isEmpty (category)) {
                            String categoryID = (String) et_category.getTag ();
                            startActivity (new Intent (mContext, AddStandardExercise.class).putExtra ("workoutName", "STANDARD").putExtra ("selectedCategory", category).putExtra ("selectedCategoryID", categoryID).putExtra ("workoutTypeID", workoutTypeID).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("user", user).putExtra ("day", day));
                        } else {
                            if (!app.getPreferences ().isClient ())
                                new MaterialDialog.Builder (mContext).content ("Select Category").positiveText ("Ok").show ();
                        }
                    } else if (type.equalsIgnoreCase ("OTM")) {
                        String time = et_time.getText ().toString ();
                        if (!TextUtils.isEmpty (time)) {
                            startActivity (new Intent (mContext, AddOTMExercise.class).putExtra ("time", time).putExtra ("workoutName", "OTM").putExtra ("workoutTypeID", workoutTypeID).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("timeList", timeListOTM).putExtra ("user", user).putExtra ("day", day));
                        } else {
                            if (!app.getPreferences ().isClient ())
                                new MaterialDialog.Builder (mContext).content ("Select Time").positiveText ("Ok").show ();
                        }
                    } else if (type.equalsIgnoreCase ("RFT")) {
                        String round = et_rounds.getText ().toString ();
                        if (!TextUtils.isEmpty (round)) {
                            startActivity (new Intent (mContext, AddRFTExercise.class).putExtra ("round", round).putExtra ("workoutName", "RFT").putExtra ("workoutTypeID", workoutTypeID).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("roundList", roundListRFT).putExtra ("user", user).putExtra ("day", day));
                        } else {
                            if (!app.getPreferences ().isClient ())
                                new MaterialDialog.Builder (mContext).content ("Select Round").positiveText ("Ok").show ();
                        }
                    } else if (type.equalsIgnoreCase ("AMRAP")) {
                        String time = et_time.getText ().toString ();
                        if (!TextUtils.isEmpty (time)) {
                            startActivity (new Intent (mContext, AddAMRAPExercise.class).putExtra ("time", time).putExtra ("workoutName", "AMRAP").putExtra ("workoutTypeID", workoutTypeID).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("user", user).putExtra ("day", day));
                        } else {
                            if (!app.getPreferences ().isClient ())
                                new MaterialDialog.Builder (mContext).content ("Select Time").positiveText ("Ok").show ();
                        }
                    } else {
                        if (!app.getPreferences ().isClient ())
                            new MaterialDialog.Builder (mContext).content ("Select Workout Value").positiveText ("Ok").show ();
                    }
                } else {
                    if (!app.getPreferences ().isClient ())
                        new MaterialDialog.Builder (mContext).content ("Select Workout Type").positiveText ("Ok").show ();
                }
            });
        }
        et_time.setOnClickListener (view -> {
            if (!app.getPreferences ().isClient ()) {
                ArrayList<String> strings = new ArrayList<> ();
                if (et_time.getTag () != null) {
                    String type = (String) et_time.getTag ();
                    if (type.equalsIgnoreCase ("OTM")) {
                        strings = timeListOTM;
                    } else if (type.equalsIgnoreCase ("AMRAP")) {
                        strings = timeListAMRAP;
                    }
                }
                SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, strings, "Select Minutes", R.style.DialogAnimations_SmileWindow, false);
                spinnerDialog.showSpinerDialog ();
                spinnerDialog.bindOnSpinerListener ((item, position) -> {
                    et_time.setText (item);
                });
            }
        });
        et_rounds.setOnClickListener (view -> {
            if (!app.getPreferences ().isClient ()) {
                SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, roundListRFT, "Select Rounds", R.style.DialogAnimations_SmileWindow, false);
                spinnerDialog.showSpinerDialog ();
                spinnerDialog.bindOnSpinerListener ((item, position) -> {
                    et_rounds.setText (item);
                });
            }
        });
        if (exerciseCatList != null && exerciseCatList.size () > 0) {
            et_category.setOnClickListener (view -> {
                if (!app.getPreferences ().isClient ()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, workoutCatNameList, "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                    spinnerDialog.showSpinerDialog ();
                    spinnerDialog.bindOnSpinerListener ((item, position) -> {
                        et_category.setText (item);
                        et_category.setTag (exerciseCatList.get (position).getExerciseCatID ());
                    });
                }
            });
        }
        rowView.setTag (id);
        llContent.addView (rowView, llContent.getChildCount ());
    }

    private void getWorkoutExerciseElements() {
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getWorkoutExerciseEle ("getWorkoutExerciseElements", new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    WorkoutExerciseData workoutExerciseData = (WorkoutExerciseData) object;
                    Log.e ("in", "success");
                    if (workoutExerciseData != null) {
                        if (workoutExerciseData.getCount () > 0) {
                            exerciseCatList = workoutExerciseData.getExerciseCategories ();
                            workoutTypeList = workoutExerciseData.getWorkoutTypeList ();
                            elementsValueArr = workoutExerciseData.getElementsValueArr ();
                            for (int i = 0; i < ((AddExerciseActivity) mContext).workoutTypeList.size (); i++) {
                                workoutTypeNameList.add (((AddExerciseActivity) mContext).workoutTypeList.get (i).getWorkoutTypeName ());
                            }
                            for (int i = 0; i < ((AddExerciseActivity) mContext).exerciseCatList.size (); i++) {
                                workoutCatNameList.add (((AddExerciseActivity) mContext).exerciseCatList.get (i).getExerciseCatName ());
                            }
                            if (elementsValueArr.size () > 0) {
                                for (int i = 0; i < elementsValueArr.size (); i++) {
                                    if (elementsValueArr.get (i).getWorkoutTypeName ().equalsIgnoreCase ("OTM")) {
                                        timeListOTM = elementsValueArr.get (i).getElementsValue ();
                                    }
                                    if (elementsValueArr.get (i).getWorkoutTypeName ().equalsIgnoreCase ("AMRAP")) {
                                        timeListAMRAP = elementsValueArr.get (i).getElementsValue ();
                                    }
                                    if (elementsValueArr.get (i).getWorkoutTypeName ().equalsIgnoreCase ("RFT")) {
                                        roundListRFT = elementsValueArr.get (i).getElementsValue ();
                                    }
                                }
                            }
                            getScheduleDetailsForClientNew ();
                        } else {
                            if (workoutExerciseData.getMsg () != null && !TextUtils.isEmpty (workoutExerciseData.getMsg ())) {
                                Utils.showLongToast (mContext, workoutExerciseData.getMsg ());
                            }
                        }
                    } else {
                        Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Log.e ("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void deleteExerciseView(Object deletePositionID) {
        if (deletePositionID != null) {
            Log.e ("deletePositionID", "" + deletePositionID);
            if (deletePositionID instanceof String) {
                String scheduleDetailsID = (String) deletePositionID;
//                deleteScheduleExercise(scheduleDetailsID);
            } else if (deletePositionID instanceof Integer) {
                for (int i = 0; i < llContent.getChildCount (); i++) {
                    if (llContent.getChildAt (i).getTag () instanceof Integer && (int) llContent.getChildAt (i).getTag () == (int) deletePositionID) {
                        llContent.removeViewAt (i);
                        break;
                    }
                }
            }
        }
    }

    private void deleteScheduleExercise(String scheduleDetailsID) {
        Map<String, String> params = new HashMap<> ();
        params.put ("xAction", "deleteScheduleExercise");
        params.put ("scheduleDetailsID", scheduleDetailsID);
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            mCompositeDisposable.add (app.getApiService ().deleteScheduleExercise (params).observeOn (AndroidSchedulers.mainThread ()).subscribeOn (Schedulers.io ()).subscribe (response -> {
                if (pd.isShowing ()) pd.dismiss ();
                if (response != null && response.getCount () > 0) {
                    if (llContent.getChildCount () > 0) {
                        for (int i = 0; i < llContent.getChildCount (); i++) {
                            if (llContent.getChildAt (i).getTag () instanceof String && llContent.getChildAt (i).getTag ().equals (scheduleDetailsID)) {
                                llContent.removeViewAt (i);
                                break;
                            }
                        }
                    }
                }
            }, throwable -> {
                if (pd.isShowing ()) pd.dismiss ();
                Log.e ("in", "error " + throwable.getMessage ());
            }));
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void getScheduleDetailsForClientNew() {
        Map<String, String> map = new HashMap<> ();
        if (isTemplate) {
            map.put ("xAction", "getTemplateDetails");
            map.put ("templateDayID", day.getTemplateDayID ());
        } else {
            map.put ("xAction", "getScheduleDetailsForClientNew");
            map.put ("scheduleDayID", day.getScheduleDayID ());
        }
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getScheduleDetailsForClientNew (map, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    WorkoutResponse workoutResponse = (WorkoutResponse) object;
                    Log.e ("in", "success");
                    if (workoutResponse != null) {
                        if (workoutResponse.getCount () > 0) {
                            if (workoutResponse.getData () != null) {
                                WorkoutData data = workoutResponse.getData ();
                                if (data.getWorkout () != null && data.getWorkout ().size () > 0) {
                                    List<Workout> workout = data.getWorkout ();
                                    displayWorkoutList (workout);
                                    if (!isTemplate) {
                                        if (data.getDoneStatus () != null && !TextUtils.isEmpty (data.getDoneStatus ())) {
                                            if (data.getDoneStatus ().equals ("1")) {
                                                rrHeader.setBackgroundColor (Color.BLACK);
                                                ivDone.setVisibility (View.VISIBLE);
                                                tvDay.setTextColor (Color.WHITE);
                                                ivViewComment.getDrawable ().setColorFilter (Color.WHITE, PorterDuff.Mode.SRC_IN);
                                                if (!TextUtils.isEmpty (data.getComment ())) {
                                                    comment = data.getComment ();
                                                    ivViewComment.setVisibility (View.VISIBLE);
                                                } else {
                                                    if (app.getPreferences ().isClient ()) {
                                                        ivViewComment.setVisibility (View.VISIBLE);
                                                    }
                                                }
                                                tvAddComment.setVisibility (View.GONE);
                                            } else {
                                                ivViewComment.getDrawable ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                                                rrHeader.setBackgroundColor (Color.WHITE);
                                                tvDay.setTextColor (Color.BLACK);
                                                if (!app.getPreferences ().isClient ()) {
                                                    if (!TextUtils.isEmpty (data.getComment ())) {
                                                        ivViewComment.setVisibility (View.VISIBLE);
                                                        comment = data.getComment ();
                                                    }
                                                } else {
                                                    tvDone.setVisibility (View.VISIBLE);
                                                    if (TextUtils.isEmpty (data.getComment ())) {
                                                        tvAddComment.setVisibility (View.VISIBLE);
                                                    } else {
                                                        comment = data.getComment ();
                                                        ivViewComment.setVisibility (View.VISIBLE);
                                                        tvAddComment.setVisibility (View.GONE);
                                                    }
                                                }
                                            }
                                        }
                                        //Set Bottom Options
                                        if (!app.getPreferences ().isClient ()) {
                                            if (data.getIsAttended () != null && !TextUtils.isEmpty (data.getIsAttended ())) {

                                                switch (data.getIsAttended ()) {
                                                    case "0":
                                                        rrBottom.setVisibility (View.VISIBLE);
                                                        if (new Date ().before (exDateObj)) {
                                                            llOptions.setVisibility (View.GONE);
                                                        } else
                                                            llOptions.setVisibility (View.VISIBLE);
                                                        tvStatus.setVisibility (GONE);
                                                        break;
                                                    case "1":
                                                        rrBottom.setVisibility (View.VISIBLE);
                                                        llOptions.setVisibility (GONE);
                                                        tvStatus.setVisibility (View.VISIBLE);
                                                        tvStatus.setText ("Attended");
                                                        tvStatus.setTextColor (Color.GREEN);
                                                        break;
                                                    case "2":
                                                        rrBottom.setVisibility (View.VISIBLE);
                                                        llOptions.setVisibility (GONE);
                                                        tvStatus.setVisibility (View.VISIBLE);
                                                        tvStatus.setText ("Not Attended");
                                                        tvStatus.setTextColor (btn_bg_color);
                                                        break;
                                                }
                                            }
                                        } else {
                                            rrBottom.setVisibility (GONE);
                                        }
                                    }
                                } else {
                                    if (!app.getPreferences ().isClient ()) addExerciseView ();
                                }
                            } else {
                                if (!app.getPreferences ().isClient ()) addExerciseView ();
                            }
                        } else {
                            if (workoutResponse.getMsg () != null && !TextUtils.isEmpty (workoutResponse.getMsg ())) {
                                Utils.showLongToast (mContext, workoutResponse.getMsg ());
                            }
                        }
                    } else {
                        Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Log.e ("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void displayWorkoutList(List<Workout> workoutList) {
        for (int j = 0; j < workoutList.size (); j++) {
            rowView = getLayoutInflater ().inflate (R.layout.exercise_sample, null);
            int id = new Random ().nextInt (10000);
            final SwipeLayout swipeLayout = rowView.findViewById (R.id.swipeLayout);
            swipeLayout.setSwipeEnabled (false);
            final EditText et_type = rowView.findViewById (R.id.et_type);
            final EditText et_time = rowView.findViewById (R.id.et_time);
            final EditText et_rounds = rowView.findViewById (R.id.et_rounds);
            final EditText et_category = rowView.findViewById (R.id.et_category);
            final RelativeLayout rr_next = rowView.findViewById (R.id.rr_next);
            final RelativeLayout rr_time = rowView.findViewById (R.id.rr_time);
            final RelativeLayout rr_rounds = rowView.findViewById (R.id.rr_rounds);
            final RelativeLayout rr_category = rowView.findViewById (R.id.rr_category);

            Workout workout = workoutList.get (j);
            et_type.setText (workout.getWorkoutTypeName ());
            et_type.setTag (workout.getWorkoutTypeID ());
            if (workout.getWorkoutTypeName ().equalsIgnoreCase ("STANDARD")) {
                rr_category.setVisibility (View.VISIBLE);
                et_category.setText (workout.getExerciseCatName ());
                et_category.setTag (workout.getExerciseCatID ());
            } else if (workout.getWorkoutTypeName ().equalsIgnoreCase ("OTM")) {
                rr_time.setVisibility (View.VISIBLE);
                et_time.setText (workout.getWorkoutTypeValue ());
                et_time.setTag ("OTM");
            } else if (workout.getWorkoutTypeName ().equalsIgnoreCase ("RFT")) {
                rr_rounds.setVisibility (View.VISIBLE);
                et_rounds.setText (workout.getWorkoutTypeValue ());
            } else if (workout.getWorkoutTypeName ().equalsIgnoreCase ("AMRAP")) {
                rr_time.setVisibility (View.VISIBLE);
                et_time.setText (workout.getWorkoutTypeValue ());
                et_time.setTag ("AMRAP");
            }
//            swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
//            repeatExerciseView(id);
//                swipeLayout.close();
//            });
//            swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
//                deleteExerciseView(id);
//                swipeLayout.close();
//            });
            if (workoutTypeList != null && workoutTypeList.size () > 0) {
                et_type.setOnClickListener (view -> {
                    if (!app.getPreferences ().isClient ()) {
                        SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, workoutTypeNameList, "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                        spinnerDialog.showSpinerDialog ();
                        spinnerDialog.bindOnSpinerListener ((item, position) -> {
                            swipeLayout.setSwipeEnabled (false);
                            et_type.setText (item);
                            et_type.setTag (workoutTypeList.get (position).getWorkoutTypeID ());
                            workout.setWorkoutDetails (null);
                            String workoutTypeName = workoutTypeList.get (position).getWorkoutTypeName ();
                            manageTypesVisibility (et_time, et_rounds, et_category, rr_time, rr_rounds, rr_category, workoutTypeName);
                        });
                    }
                });
                rr_next.setOnClickListener (view -> {
                    String type = et_type.getText ().toString ();
                    String workoutTypeID = (String) et_type.getTag ();
                    if (!TextUtils.isEmpty (type)) {
                        if (type.equalsIgnoreCase ("STANDARD")) {
                            String category = et_category.getText ().toString ();
                            if (!TextUtils.isEmpty (category)) {
                                String categoryID = (String) et_category.getTag ();
                                startActivity (new Intent (mContext, AddStandardExercise.class).putExtra ("workoutName", "STANDARD").putExtra ("selectedCategory", category).putExtra ("selectedCategoryID", categoryID).putExtra ("workoutTypeID", workoutTypeID).putExtra ("workout", workout).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("user", user).putExtra ("day", day));
                            } else {
                                if (!app.getPreferences ().isClient ())
                                    new MaterialDialog.Builder (mContext).content ("Select Category").positiveText ("Ok").show ();
                            }
                        } else if (type.equalsIgnoreCase ("OTM")) {
                            String time = et_time.getText ().toString ();
                            if (!TextUtils.isEmpty (time)) {
//                                time = time.split(" ")[0];
                                startActivity (new Intent (mContext, AddOTMExercise.class).putExtra ("time", time).putExtra ("workoutName", "OTM").putExtra ("workoutTypeID", workoutTypeID).putExtra ("workout", workout).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("timeList", timeListOTM).putExtra ("user", user).putExtra ("day", day));
                            } else {
                                if (!app.getPreferences ().isClient ())
                                    new MaterialDialog.Builder (mContext).content ("Select Time").positiveText ("Ok").show ();
                            }
                        } else if (type.equalsIgnoreCase ("RFT")) {
                            String round = et_rounds.getText ().toString ();
                            if (!TextUtils.isEmpty (round)) {
//                                round = round.split(" ")[0];
                                startActivity (new Intent (mContext, AddRFTExercise.class).putExtra ("round", round).putExtra ("workoutName", "RFT").putExtra ("workoutTypeID", workoutTypeID).putExtra ("workout", workout).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("roundList", roundListRFT).putExtra ("user", user).putExtra ("day", day));
                            } else {
                                if (!app.getPreferences ().isClient ())
                                    new MaterialDialog.Builder (mContext).content ("Select Round").positiveText ("Ok").show ();
                            }
                        } else if (type.equalsIgnoreCase ("AMRAP")) {
                            String time = et_time.getText ().toString ();
                            if (!TextUtils.isEmpty (time)) {
//                                time = time.split(" ")[0];
                                startActivity (new Intent (mContext, AddAMRAPExercise.class).putExtra ("time", time).putExtra ("workoutName", "AMRAP").putExtra ("workoutTypeID", workoutTypeID).putExtra ("workout", workout).putExtra ("isTemplate", isTemplate).putExtra ("templateID", templateID).putExtra ("user", user).putExtra ("day", day));
                            } else {
                                if (!app.getPreferences ().isClient ())
                                    new MaterialDialog.Builder (mContext).content ("Select Time").positiveText ("Ok").show ();
                            }
                        } else {
                            if (!app.getPreferences ().isClient ())
                                new MaterialDialog.Builder (mContext).content ("Select Workout Value").positiveText ("Ok").show ();
                        }
                    } else {
                        if (!app.getPreferences ().isClient ())
                            new MaterialDialog.Builder (mContext).content ("Select Workout Type").positiveText ("Ok").show ();
                    }
                });
            }
            et_time.setOnClickListener (view -> {
                if (!app.getPreferences ().isClient ()) {
                    ArrayList<String> strings = new ArrayList<> ();
                    if (et_time.getTag () != null) {
                        String type = (String) et_time.getTag ();
                        if (type.equalsIgnoreCase ("OTM")) {
                            strings = timeListOTM;
                        } else if (type.equalsIgnoreCase ("AMRAP")) {
                            strings = timeListAMRAP;
                        }
                    }
                    SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, strings, "Select Minutes", R.style.DialogAnimations_SmileWindow, false);
                    spinnerDialog.showSpinerDialog ();
                    spinnerDialog.bindOnSpinerListener ((item, position) -> {
                        et_time.setText (item);
                        workout.setWorkoutDetails (null);
                    });
                }
            });
            et_rounds.setOnClickListener (view -> {
                if (!app.getPreferences ().isClient ()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, roundListRFT, "Select Rounds", R.style.DialogAnimations_SmileWindow, false);
                    spinnerDialog.showSpinerDialog ();
                    spinnerDialog.bindOnSpinerListener ((item, position) -> {
                        et_rounds.setText (item);
                        workout.setWorkoutDetails (null);
                    });
                }
            });
            if (exerciseCatList != null && exerciseCatList.size () > 0) {
                et_category.setOnClickListener (view -> {
                    if (!app.getPreferences ().isClient ()) {
                        SpinnerDialog spinnerDialog = new SpinnerDialog (AddExerciseActivity.this, workoutCatNameList, "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                        spinnerDialog.showSpinerDialog ();
                        spinnerDialog.bindOnSpinerListener ((item, position) -> {
                            et_category.setText (item);
                            et_category.setTag (exerciseCatList.get (position).getExerciseCatID ());
                            workout.setWorkoutDetails (null);
                        });
                    }
                });
            }
            rowView.setTag (id);
            llContent.addView (rowView, llContent.getChildCount ());
        }
    }

    private void manageTypesVisibility(EditText et_time, EditText et_rounds, EditText et_category, RelativeLayout rr_time, RelativeLayout rr_rounds, RelativeLayout rr_category, String workoutTypeName) {
        if (workoutTypeName.equalsIgnoreCase ("STANDARD")) {
            rr_rounds.setVisibility (GONE);
            rr_time.setVisibility (GONE);
            rr_category.setVisibility (View.VISIBLE);
        } else if (workoutTypeName.equalsIgnoreCase ("OTM")) {
            rr_rounds.setVisibility (GONE);
            rr_category.setVisibility (GONE);
            rr_time.setVisibility (View.VISIBLE);
            et_time.setText ("");
            et_time.setTag ("OTM");
            et_category.setText ("");
            et_rounds.setText ("");
        } else if (workoutTypeName.equalsIgnoreCase ("RFT")) {
            rr_rounds.setVisibility (View.VISIBLE);
            rr_time.setVisibility (GONE);
            rr_category.setVisibility (GONE);
            et_time.setText ("");
            et_rounds.setText ("");
            et_category.setText ("");
        } else if (workoutTypeName.equalsIgnoreCase ("AMRAP")) {
            rr_rounds.setVisibility (GONE);
            rr_category.setVisibility (GONE);
            rr_time.setVisibility (View.VISIBLE);
            et_time.setTag ("AMRAP");
            et_time.setText ("");
            et_rounds.setText ("");
            et_category.setText ("");
        }
    }

    @OnClick({iv_view_comment, R.id.tv_add_comment, R.id.tv_done})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.iv_view_comment:
               /* if (!app.getPreferences().isClient()) {
                    startActivity(new Intent(mContext, ViewCommentActivity.class).putExtra("comment", comment));
                } else {*/
                if (TextUtils.isEmpty (comment)) {
                    startActivity (new Intent (mContext, AddCommentActivity.class).putExtra ("isAddComment", true).putExtra ("scheduleDayID", day.getScheduleDayID ()).putExtra ("user", user));
                } else {
                    startActivity (new Intent (mContext, AddCommentActivity.class).putExtra ("isAddComment", false).putExtra ("scheduleDayID", day.getScheduleDayID ()).putExtra ("comment", comment).putExtra ("user", user));
                }
                //}
                break;
            case R.id.tv_add_comment:
                startActivity (new Intent (mContext, AddCommentActivity.class).putExtra ("isAddComment", true).putExtra ("scheduleDayID", day.getScheduleDayID ()).putExtra ("user", user));
                break;
            case R.id.tv_done:
                displayMaterialDialog ("Set workout as done.", (dialog, which) -> doneWorkoutSchedule ());


                break;
        }
    }

    @OnClick({R.id.btn_notAttended, R.id.btn_attended})
    public void onOptionsViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.btn_notAttended:
                displayMaterialDialog ("Set workout not attended.", (dialog, which) -> workoutScheduleAttended ("2"));
                break;
            case R.id.btn_attended:
                displayMaterialDialog ("Set workout attended.", (dialog, which) -> workoutScheduleAttended ("1"));
                break;
        }
    }

    private void doneWorkoutSchedule() {
        Map<String, String> params = new HashMap<> ();
        params.put ("xAction", "doneWorkoutSchedule");
        params.put ("scheduleDayID", day.getScheduleDayID ());
        params.put ("clientID", user.getClientID ());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            mCompositeDisposable.add (app.getApiService ().deleteScheduleExercise (params).observeOn (AndroidSchedulers.mainThread ()).subscribeOn (Schedulers.io ()).subscribe (userData -> {
                if (pd.isShowing ()) pd.dismiss ();
                if (userData != null) {
                    if (userData.getCount () > 0) {
                        if (userData.getMsg () != null && !TextUtils.isEmpty (userData.getMsg ())) {
                            Utils.showLongToast (mContext, userData.getMsg ());
                        }
                        Utils.showLongToast (mContext, userData.getMsg ());
                        finish ();
                    } else {
                        if (userData.getMsg () != null && !TextUtils.isEmpty (userData.getMsg ())) {
                            Utils.showLongToast (mContext, userData.getMsg ());
                        }
                    }
                } else {
                    Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                }
            }, throwable -> {
                if (pd.isShowing ()) pd.dismiss ();
                Log.e ("in", "error " + throwable.getMessage ());
            }));
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void workoutScheduleAttended(String isAttended) {
        Map<String, String> map = new HashMap<> ();
        map.put ("xAction", "workoutScheduleAttended");
        map.put ("scheduleDayID", day.getScheduleDayID ());
        map.put ("isAttended", isAttended);
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().workoutScheduleAttended (map, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null && response.getCount () > 0) {
                        if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                            Utils.showLongToast (mContext, response.getMsg ());
                        }
                        tvStatus.setText (isAttended.equals ("1") ? "Attended" : "Not Attended");
                        tvStatus.setTextColor (isAttended.equals ("1") ? Color.GREEN : btn_bg_color);
                        tvStatus.setVisibility (View.VISIBLE);
                        llOptions.setVisibility (GONE);
                    } else {
                        if (response != null && response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                            Utils.showLongToast (mContext, response.getMsg ());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Utils.showLongToast (mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater ().inflate (R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case android.R.id.home:
                finish ();
                break;
//            case R.id.action_save:
//                getAddedExerciseData();
//                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences ().isClient ()) {
                    intent = new Intent (this, ClientHomeActivity.class);
                } else {
                    intent = new Intent (this, AdminMainActivity.class);
                }
                intent.addFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity (intent);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed ();
        finish ();
    }

    private void displayMaterialDialog(String message, MaterialDialog.SingleButtonCallback singleButtonCallback) {
        new MaterialDialog.Builder (mContext).content (message).positiveText ("Ok").negativeText ("Cancel").onPositive (singleButtonCallback).show ();
    }
}
