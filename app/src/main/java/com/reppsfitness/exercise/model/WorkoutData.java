package com.reppsfitness.exercise.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur Gangurde on 25/11/17.
 */

public class WorkoutData implements Parcelable {
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("doneStatus")
    @Expose
    public String doneStatus;
    @SerializedName("isAttended")
    @Expose
    public String isAttended;
    @SerializedName("workout")
    @Expose
    public List<Workout> workout = null;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDoneStatus() {
        return doneStatus;
    }

    public void setDoneStatus(String doneStatus) {
        this.doneStatus = doneStatus;
    }

    public String getIsAttended() {
        return isAttended;
    }

    public void setIsAttended(String isAttended) {
        this.isAttended = isAttended;
    }

    public List<Workout> getWorkout() {
        return workout;
    }

    public void setWorkout(List<Workout> workout) {
        this.workout = workout;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comment);
        dest.writeString(this.doneStatus);
        dest.writeString(this.isAttended);
        dest.writeTypedList(this.workout);
    }

    public WorkoutData() {
    }

    protected WorkoutData(Parcel in) {
        this.comment = in.readString();
        this.doneStatus = in.readString();
        this.isAttended = in.readString();
        this.workout = in.createTypedArrayList(Workout.CREATOR);
    }

    public static final Parcelable.Creator<WorkoutData> CREATOR = new Parcelable.Creator<WorkoutData>() {
        @Override
        public WorkoutData createFromParcel(Parcel source) {
            return new WorkoutData(source);
        }

        @Override
        public WorkoutData[] newArray(int size) {
            return new WorkoutData[size];
        }
    };
}
