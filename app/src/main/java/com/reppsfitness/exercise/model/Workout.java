package com.reppsfitness.exercise.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Workout implements Parcelable {
    @SerializedName("templateDetailsID")
    @Expose
    public String templateDetailsID;
    @SerializedName("workoutTypeID")
    @Expose
    public String workoutTypeID;
    @SerializedName("workoutTypeName")
    @Expose
    public String workoutTypeName;
    @SerializedName("workoutTypeValue")
    @Expose
    public String workoutTypeValue;
    @SerializedName("scheduleDetailsID")
    @Expose
    public String scheduleDetailsID;
    @SerializedName("workoutDetails")
    @Expose
    public List<Exercise> workoutDetails = new ArrayList<>();
    @SerializedName("exerciseCatID")
    @Expose
    public String exerciseCatID;
    @SerializedName("exerciseCatName")
    @Expose
    public String exerciseCatName;

    public String getTemplateDetailsID() {
        return templateDetailsID;
    }

    public void setTemplateDetailsID(String templateDetailsID) {
        this.templateDetailsID = templateDetailsID;
    }

    public String getExerciseCatID() {
        return exerciseCatID;
    }

    public void setExerciseCatID(String exerciseCatID) {
        this.exerciseCatID = exerciseCatID;
    }

    public String getExerciseCatName() {
        return exerciseCatName;
    }

    public void setExerciseCatName(String exerciseCatName) {
        this.exerciseCatName = exerciseCatName;
    }

    public String getWorkoutTypeID() {
        return workoutTypeID;
    }

    public void setWorkoutTypeID(String workoutTypeID) {
        this.workoutTypeID = workoutTypeID;
    }

    public String getWorkoutTypeName() {
        return workoutTypeName;
    }

    public void setWorkoutTypeName(String workoutTypeName) {
        this.workoutTypeName = workoutTypeName;
    }

    public String getWorkoutTypeValue() {
        return workoutTypeValue;
    }

    public void setWorkoutTypeValue(String workoutTypeValue) {
        this.workoutTypeValue = workoutTypeValue;
    }

    public String getScheduleDetailsID() {
        return scheduleDetailsID;
    }

    public void setScheduleDetailsID(String scheduleDetailsID) {
        this.scheduleDetailsID = scheduleDetailsID;
    }

    public List<Exercise> getWorkoutDetails() {
        return workoutDetails;
    }

    public void setWorkoutDetails(List<Exercise> workoutDetails) {
        this.workoutDetails = workoutDetails;
    }

    public Workout() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.templateDetailsID);
        dest.writeString(this.workoutTypeID);
        dest.writeString(this.workoutTypeName);
        dest.writeString(this.workoutTypeValue);
        dest.writeString(this.scheduleDetailsID);
        dest.writeTypedList(this.workoutDetails);
        dest.writeString(this.exerciseCatID);
        dest.writeString(this.exerciseCatName);
    }

    protected Workout(Parcel in) {
        this.templateDetailsID = in.readString();
        this.workoutTypeID = in.readString();
        this.workoutTypeName = in.readString();
        this.workoutTypeValue = in.readString();
        this.scheduleDetailsID = in.readString();
        this.workoutDetails = in.createTypedArrayList(Exercise.CREATOR);
        this.exerciseCatID = in.readString();
        this.exerciseCatName = in.readString();
    }

    public static final Creator<Workout> CREATOR = new Creator<Workout>() {
        @Override
        public Workout createFromParcel(Parcel source) {
            return new Workout(source);
        }

        @Override
        public Workout[] newArray(int size) {
            return new Workout[size];
        }
    };
}