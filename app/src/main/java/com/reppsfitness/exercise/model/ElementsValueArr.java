package com.reppsfitness.exercise.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ElementsValueArr implements Parcelable {
    @SerializedName("workoutTypeID")
    @Expose
    public String workoutTypeID;
    @SerializedName("workoutTypeName")
    @Expose
    public String workoutTypeName;
    @SerializedName("elementsValue")
    @Expose
    public ArrayList<String> elementsValue = new ArrayList<>();

    public String getWorkoutTypeID() {
        return workoutTypeID;
    }

    public void setWorkoutTypeID(String workoutTypeID) {
        this.workoutTypeID = workoutTypeID;
    }

    public String getWorkoutTypeName() {
        return workoutTypeName;
    }

    public void setWorkoutTypeName(String workoutTypeName) {
        this.workoutTypeName = workoutTypeName;
    }

    public ArrayList<String> getElementsValue() {
        return elementsValue;
    }

    public void setElementsValue(ArrayList<String> elementsValue) {
        this.elementsValue = elementsValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutTypeID);
        dest.writeString(this.workoutTypeName);
        dest.writeStringList(this.elementsValue);
    }

    public ElementsValueArr() {
    }

    protected ElementsValueArr(Parcel in) {
        this.workoutTypeID = in.readString();
        this.workoutTypeName = in.readString();
        this.elementsValue = in.createStringArrayList();
    }

    public static final Parcelable.Creator<ElementsValueArr> CREATOR = new Parcelable.Creator<ElementsValueArr>() {
        @Override
        public ElementsValueArr createFromParcel(Parcel source) {
            return new ElementsValueArr(source);
        }

        @Override
        public ElementsValueArr[] newArray(int size) {
            return new ElementsValueArr[size];
        }
    };
}