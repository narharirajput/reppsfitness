package com.reppsfitness.exercise.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.reppsfitness.model.schedule.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class Exercise implements Parcelable {
    @SerializedName("templateEID")
    @Expose
    public String templateEID;
    @SerializedName("scheduleEID")
    @Expose
    public String scheduleEID;
    @SerializedName("workoutName")
    @Expose
    private String workoutName;
    @SerializedName("workoutTypeID")
    @Expose
    private String workoutTypeID;
    @SerializedName("workoutTypeName")
    @Expose
    private String workoutTypeName;
    @SerializedName("workoutTypeValue")
    @Expose
    private String workoutTypeValue;
    @SerializedName("exerciseCatName")
    @Expose
    private String exerciseCatName;
    @SerializedName("exerciseCatID")
    @Expose
    private String exerciseCatID;
    @SerializedName("exerciseName")
    @Expose
    private String exerciseName;
    @SerializedName("exerciseID")
    @Expose
    private String exerciseID;
    @SerializedName("exerciseValue")
    @Expose
    private String exerciseValue;
    @SerializedName("workoutElements")
    @Expose
    private List<Element> workoutElements = new ArrayList<>();

    public String getTemplateEID() {
        return templateEID;
    }

    public void setTemplateEID(String templateEID) {
        this.templateEID = templateEID;
    }

    public String getWorkoutTypeName() {
        return workoutTypeName;
    }

    public void setWorkoutTypeName(String workoutTypeName) {
        this.workoutTypeName = workoutTypeName;
    }

    public String getScheduleEID() {
        return scheduleEID;
    }

    public void setScheduleEID(String scheduleEID) {
        this.scheduleEID = scheduleEID;
    }

    public String getExerciseValue() {
        return exerciseValue;
    }

    public void setExerciseValue(String exerciseValue) {
        this.exerciseValue = exerciseValue;
    }

    public String getWorkoutTypeValue() {
        return workoutTypeValue;
    }

    public void setWorkoutTypeValue(String workoutTypeValue) {
        this.workoutTypeValue = workoutTypeValue;
    }

    public String getExerciseCatName() {
        return exerciseCatName;
    }

    public void setExerciseCatName(String exerciseCatName) {
        this.exerciseCatName = exerciseCatName;
    }

    public String getExerciseCatID() {
        return exerciseCatID;
    }

    public void setExerciseCatID(String exerciseCatID) {
        this.exerciseCatID = exerciseCatID;
    }

    public List<Element> getWorkoutElements() {
        return workoutElements;
    }

    public void setWorkoutElements(List<Element> workoutElements) {
        this.workoutElements = workoutElements;
    }

    public String getWorkoutName() {
        return workoutName;
    }

    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }

    public String getWorkoutTypeID() {
        return workoutTypeID;
    }

    public void setWorkoutTypeID(String workoutTypeID) {
        this.workoutTypeID = workoutTypeID;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public String getExerciseID() {
        return exerciseID;
    }

    public void setExerciseID(String exerciseID) {
        this.exerciseID = exerciseID;
    }

    public Exercise() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.templateEID);
        dest.writeString(this.scheduleEID);
        dest.writeString(this.workoutName);
        dest.writeString(this.workoutTypeID);
        dest.writeString(this.workoutTypeName);
        dest.writeString(this.workoutTypeValue);
        dest.writeString(this.exerciseCatName);
        dest.writeString(this.exerciseCatID);
        dest.writeString(this.exerciseName);
        dest.writeString(this.exerciseID);
        dest.writeString(this.exerciseValue);
        dest.writeTypedList(this.workoutElements);
    }

    protected Exercise(Parcel in) {
        this.templateEID = in.readString();
        this.scheduleEID = in.readString();
        this.workoutName = in.readString();
        this.workoutTypeID = in.readString();
        this.workoutTypeName = in.readString();
        this.workoutTypeValue = in.readString();
        this.exerciseCatName = in.readString();
        this.exerciseCatID = in.readString();
        this.exerciseName = in.readString();
        this.exerciseID = in.readString();
        this.exerciseValue = in.readString();
        this.workoutElements = in.createTypedArrayList(Element.CREATOR);
    }

    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel source) {
            return new Exercise(source);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };
}
