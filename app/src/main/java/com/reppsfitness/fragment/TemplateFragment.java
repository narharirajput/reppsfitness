package com.reppsfitness.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.reppsfitness.AdminMainActivity;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.TemplateDayListActivity;
import com.reppsfitness.adapter.TemplateAdapter;
import com.reppsfitness.model.Template;
import com.reppsfitness.utils.DialogUtils;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.reppsfitness.R.id.recyclerView;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class TemplateFragment extends BaseFragment {
    @BindView(recyclerView)
    RecyclerView categoryRecyclerView;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.linear_toolbar)
    LinearLayout linearToolbar;
    @BindView(R.id.tv_error)
    public TextView tv_error;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        linearToolbar.setVisibility(View.GONE);
        categoryRecyclerView.setHasFixedSize(true);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_template;
    }

    private void getTemplateList() {
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getTemplateList("getTemplateList")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(templateData -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "success");
                        if (templateData != null) {
                            if (templateData.getMsg() != null && !TextUtils.isEmpty(templateData.getMsg())) {
                                tv_error.setText(templateData.getMsg());
                            }
                            if (templateData.getCount() > 0) {
                                if (templateData.getTemplateList() != null && templateData.getTemplateList().size() > 0) {
                                    List<Template> templateList = new ArrayList<>();
                                    templateList.addAll(templateData.getTemplateList());
                                    TemplateAdapter templateAdapter = new TemplateAdapter(mContext, templateList);
                                    categoryRecyclerView.setAdapter(templateAdapter);
                                    setSearchFilter(templateAdapter);
                                    categoryRecyclerView.setVisibility(View.VISIBLE);
                                    tv_error.setVisibility(View.GONE);
                                } else {
                                    categoryRecyclerView.setVisibility(View.GONE);
                                    tv_error.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (templateData.getMsg() != null && !TextUtils.isEmpty(templateData.getMsg())) {
                                    Utils.showLongToast(mContext, templateData.getMsg());
                                }
                                categoryRecyclerView.setVisibility(View.GONE);
                                tv_error.setVisibility(View.VISIBLE);
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                            categoryRecyclerView.setVisibility(View.GONE);
                            tv_error.setVisibility(View.GONE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
//                            Utils.showLongToast(mContext, throwable.getMessage());
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        String title = "Add Template";
        String negativeButton = "CANCEL";
        String positiveButton = "SUBMIT";

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_create_template, null);
        EditText et_templateName = customView.findViewById(R.id.et_templateName);
        EditText et_noOfDays = customView.findViewById(R.id.et_noOfDays);

        final Dialog myDialog = DialogUtils.createCustomDialog(mContext, title, customView,
                negativeButton, positiveButton, false, new DialogUtils.DialogListener() {
                    @Override
                    public void onPositiveButton(Dialog dialog) {
                        String templateName, noOfDays;
                        templateName = et_templateName.getText().toString().trim();
                        noOfDays = et_noOfDays.getText().toString().trim();
                        if (TextUtils.isEmpty(templateName)) {
                            et_templateName.setError("Enter Template Name");
                            et_templateName.requestFocus();
                            return;
                        }
                        if (TextUtils.isEmpty(noOfDays)) {
                            et_noOfDays.setError("Enter Number of Days");
                            et_noOfDays.requestFocus();
                            return;
                        }
                        saveTemplate(templateName, noOfDays, dialog);
                    }

                    @Override
                    public void onNegativeButton() {
                        Utils.hideSoftKeyboard((AdminMainActivity)mContext);
                    }
                });

        if (myDialog != null && !myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void saveTemplate(String templateName, String noOfDays, Dialog dialog) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "saveTemplate");
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("noOfDays", noOfDays);
        params.put("templateName", templateName);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            CompositeDisposable mCompositeDisposable = new CompositeDisposable();
            mCompositeDisposable.add(app.getApiService().saveTemplate(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (dialog.isShowing()) dialog.dismiss();
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null) {
                            if (response.getCount() > 0) {
                                if (response.getMsg() != null)
                                    Utils.showShortToast(mContext, response.getMsg());
                                if (response.getTemplate() != null) {
                                    Intent intent = new Intent(mContext, TemplateDayListActivity.class);
                                    intent.putExtra("template", response.getTemplate());
                                    mContext.startActivity(intent);
                                }
                            }
                        }
                    }, throwable -> {
                        if (dialog.isShowing()) dialog.dismiss();
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void setSearchFilter(final TemplateAdapter templateAdapter) {
        ((AdminMainActivity) mContext).searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Utils.hideSoftKeyboard(getActivity());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText)) {
                    templateAdapter.filter(newText);
                } else {
                    templateAdapter.filter("");
                }
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getTemplateList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
        }
        return true;
    }
}

