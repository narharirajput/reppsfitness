package com.reppsfitness.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.reppsfitness.AddClientActivity;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.RequestSessionActivity;
import com.reppsfitness.adapter.ShiftTimeAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.ShiftSession;
import com.reppsfitness.model.ShiftSessionData;
import com.reppsfitness.session_cal.apiclient.SessionClient;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.session_cal.json_model.SessionRes;
import com.reppsfitness.utils.GridSpacingItemDecoration;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.BreakIterator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class NewRequestFragment extends BaseFragment {
    //    @BindView(R.id.et_date)
//    EditText et_date;
//    @BindView(R.id.et_slotTimings)
//    EditText etSlotTimings;
    @BindView(R.id.et_selectSession)
    EditText et_selectSession;
    @BindView(R.id.et_selectDate)
    EditText et_selectDate;
    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.ll_form)
    LinearLayout llForm;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView tvError;
    private List<String> slotTimings;
    //    private String selectedSlotTime, selectedDate;
    public String shiftFromSessionDate = "", shiftFromSessionTime = "", shiftToSessionDate = "", shiftToSessionTime = "", shiftFromSessionID = "", shiftToSessionID = "";
    public String showSessionDates = "";
    private Date pkgEndDate, pkgStartDate;
    private String packageID;
    private List<String> sessionDateList = new ArrayList<> ();
    private List<String> timeList;
    private Date selectedSessionDate;
    public DayDetails dayDetails;
    private List<ShiftSession> sessionList;
    private Boolean isEnterOnce = false;
    ClientProfile clientProfile;
    String foldername;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_shift_session;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);
        getClientProfile ();
        recyclerView.setLayoutManager (new GridLayoutManager (mContext, 4));
        int spanCount = 4; // 3 columns
        int spacing = 25; // 50px
        recyclerView.addItemDecoration (new GridSpacingItemDecoration (spanCount, spacing, false));
    }

    //    @OnClick({R.id.et_date, R.id.et_slotTimings, R.id.btn_submit})
    @OnClick({R.id.et_selectSession, R.id.et_selectDate, R.id.btn_submit, R.id.tv_error})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.et_selectSession:
                if (sessionList != null && sessionList.size () > 0) {
                    if (!isEnterOnce) {                                                              // code by ajit
                        for (int i = 0; i < sessionList.size (); i++) {
                            sessionDateList.add (sessionList.get (i).getShiftFromSessionDateString ());
                            isEnterOnce = true;
                        }
                    }
                    new MaterialDialog.Builder (mContext).items (sessionDateList).itemsCallback ((dialog, view1, which, text) -> {
                        shiftFromSessionDate = sessionList.get (which).getShiftFromSessionDate ();
                        shiftFromSessionTime = sessionList.get (which).getShiftFromSessionTime ();
                        shiftFromSessionID = sessionList.get (which).getShiftFromSessionID ();
                        et_selectSession.setText (text.toString ());
                        Log.e ("val", shiftFromSessionDate + "||" + shiftFromSessionTime + "||" + shiftFromSessionID + "||" + which);
                        shiftToSessionDate = "";
                        et_selectDate.setText ("");
                        recyclerView.setAdapter (null);
                    }).show ();
                } else Toast.makeText (mContext, "Session Not Alloted", Toast.LENGTH_SHORT).show ();
                break;

            case R.id.et_selectDate:
                if (sessionList != null && sessionList.size () > 0) {
                    if (shiftFromSessionDate != null && !TextUtils.isEmpty (shiftFromSessionDate)) {
                        SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
                        try {
                            selectedSessionDate = formatter.parse (shiftFromSessionDate);
                        } catch (ParseException e) {
                            e.printStackTrace ();
                        }
                        displayDatePickerDialog ();
                    } else {
                        et_selectSession.setError ("Select session to shift");
                        et_selectSession.requestFocus ();
                    }
                    if (clientProfile.getIsRegular ().equals ("0")) {                                      //code by ajit
                        Log.e (TAG, "onViewClicked: client profile is regular clicked");
                        displayTermDatePickerDialog ();
                    }
                } else Toast.makeText (mContext, "Session Not Alloted", Toast.LENGTH_SHORT).show ();

                break;
//            case R.id.et_date:
//                displayDatePickerDialog();
//                break;
//            case R.id.et_slotTimings:
//                if (slotTimings != null && slotTimings.size() > 0) {
//                    new MaterialDialog.Builder(mContext)
//                            .items(slotTimings)
//                            .itemsCallback((dialog, view1, which, text) -> {
//                                selectedSlotTime = text.toString();
//                                etSlotTimings.setText(selectedSlotTime);
//                            })
//                            .show();
//                }
//                break;
            case R.id.btn_submit:
                if (sessionList != null && sessionList.size () > 0) {

                if (clientProfile.getIsRegular ().equals ("1")) {                                      //code by ajit
                    if (TextUtils.isEmpty (shiftFromSessionDate)) {
                        et_selectSession.setError ("Select session to shift");
                        et_selectSession.requestFocus ();
                        Log.e (TAG, "onViewClicked: 1 Select session to shift");
                        return;
                    }
                    if (TextUtils.isEmpty (shiftToSessionDate)) {
                        et_selectDate.setError ("Select date to shift");
                        et_selectDate.requestFocus ();
                        Log.e (TAG, "onViewClicked: 2 Select date to shift");
                        return;
                    }
                    if (TextUtils.isEmpty (shiftToSessionTime)) {
                        Utils.showLongToast (mContext, "Select session slot");
                        Log.e (TAG, "onViewClicked: 3 Select session slot");
                        return;
                    }
                } else {                                                                              //code by ajit
                    if (TextUtils.isEmpty (showSessionDates)) {
                        et_selectDate.setError ("Select date to shift");
                        et_selectDate.requestFocus ();
                        Log.e (TAG, "onViewClicked: 4 Select date to shift");
                        return;
                    }
                    if (TextUtils.isEmpty (shiftToSessionTime)) {
                        Utils.showLongToast (mContext, "Select session slot");
                        Log.e (TAG, "onViewClicked: 5 Select session slot");
                        return;
                    }
                }
                if (dayDetails != null)
                    if (String.valueOf (dayDetails.getAlloted ()).equals (dayDetails.getClientLimit ())) {
                        new MaterialDialog.Builder (mContext).content ("Session can not be allotted as Session's max capacity is " + dayDetails.getClientLimit ()).positiveText ("Ok").show ();
                        //Toast.makeText (mContext, "Session can not be allotted", Toast.LENGTH_SHORT).show ();
                        return;
                    }
                alertDialog ();

                } else Toast.makeText (mContext, "Session Not Alloted", Toast.LENGTH_SHORT).show ();

                break;

            case R.id.tv_error:
                startActivity (new Intent (getActivity (), ClientHomeActivity.class).putExtra ("isFromSession", true));
                break;
//            case R.id.et_date:
//                displayDatePickerDialog();
//                break;
//            case R.id.et_slotTimings:
//                if (slotTimings != null && slotTimings.size() > 0) {
//                    new MaterialDialog.Builder(mContext)
//                            .items(slotTimings)
//                            .itemsCallback((dialog, view1, which, text) -> {
//                                selectedSlotTime = text.toString();
//                                etSlotTimings.setText(selectedSlotTime);
//                            })
//                            .show();
//                }
//                break;
        }
    }

    public void alertDialog() {                                                                      //code by ajit
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder (mContext);
        alertBuilder.setTitle ("ShiftSessionAlert");
        alertBuilder.setMessage ("Do you want to send session request");
        alertBuilder.setPositiveButton ("Yes", (dialog, which) -> requestSession ());
        alertBuilder.setNegativeButton ("Cancel", (dialog, which) -> dialog.dismiss ());
        alertBuilder.show ();
    }


    private void displayTermDatePickerDialog() {                                                     // code by ajit
        // Get Current Date
        final Calendar minCalendar = Calendar.getInstance ();
        minCalendar.setTime (pkgStartDate);
        minCalendar.add (Calendar.DAY_OF_MONTH, 1);
        Calendar now = Calendar.getInstance ();
        DatePickerDialog dpd = DatePickerDialog.newInstance ((view12, year, monthOfYear, dayOfMonth) -> {
            Log.d ("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
            showSessionDates = String.valueOf (year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            et_selectDate.setText (String.valueOf (dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
            getTermSessionCalendarList ();
        }, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
        dpd.setMinDate (minCalendar);
        if (pkgEndDate != null) {
            final Calendar maxCalendar = Calendar.getInstance ();
            maxCalendar.setTime (pkgEndDate);
            dpd.setMaxDate (maxCalendar);
        }
        dpd.show (((AppCompatActivity) mContext).getFragmentManager (), "Datepickerdialog");
    }


    private void getTermSessionCalendarList() {                                                      //code by ajit
        CustomProgressDialog cpd = new CustomProgressDialog (mContext);
        cpd.show ();
        if (cd.isConnectingToInternet ()) {
            SessionClient sessionClient = new SessionClient ();
            Map<String, String> map = new HashMap<> ();
            map.put ("xAction", "getSessionCalendarList");
            map.put ("shiftSessionDate", showSessionDates);
            Call<String> call = sessionClient.sessionService.call_api (map);
            call.enqueue (new Callback<String> () {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (cpd.isShowing ()) cpd.dismiss ();
                    try {
                        if (response.body () != null) {
                            com.reppsfitness.session_cal.json_model.SessionResponse sessionResponse = new Gson ().fromJson (response.body (), com.reppsfitness.session_cal.json_model.SessionResponse.class);
                            SessionRes data1 = sessionResponse.getData ();
                            timeList = data1.getTimeList ();
                            //If selected position set initially to 0th position
//                            if (timeList.size() > 0) {
//                                shiftToSessionTime = timeList.get(0);
//                                if (sessionResponse.getData().getSessionDateList().containsKey(timeList.get(0))) {
//                                    shiftToSessionID = ((DayDetails) sessionResponse.getData().getSessionDateList().get(timeList.get(0))).getSessionID();
//                                }
//                            }
                            recyclerView.setAdapter (new ShiftTimeAdapter (mContext, data1, NewRequestFragment.this));
                            btn_submit.setVisibility (View.VISIBLE);
                            Log.e ("timeList", timeList.toString ());
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (cpd.isShowing ()) cpd.dismiss ();
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void displayDatePickerDialog() {
        // set minimum date
        final Calendar minCalendar = Calendar.getInstance ();
        minCalendar.setTime (selectedSessionDate);
        minCalendar.add (Calendar.DAY_OF_MONTH, 1);
        Calendar now = Calendar.getInstance ();
        DatePickerDialog dpd = DatePickerDialog.newInstance ((view12, year, monthOfYear, dayOfMonth) -> {
            Log.d ("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
            shiftToSessionDate = String.valueOf (year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
            et_selectDate.setText (String.valueOf (dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
            getSessionCalendarList ();
        }, now.get (Calendar.YEAR), now.get (Calendar.MONTH), now.get (Calendar.DAY_OF_MONTH));
        dpd.setMinDate (minCalendar);
        if (pkgEndDate != null) {
            final Calendar maxCalendar = Calendar.getInstance ();
            maxCalendar.setTime (pkgEndDate);
            dpd.setMaxDate (maxCalendar);
        }
        dpd.show (((AppCompatActivity) mContext).getFragmentManager (), "Datepickerdialog");
    }


    private void getSessionCalendarList() {
        CustomProgressDialog cpd = new CustomProgressDialog (mContext);
        cpd.show ();
        if (cd.isConnectingToInternet ()) {
            SessionClient sessionClient = new SessionClient ();
            Map<String, String> map = new HashMap<> ();
            map.put ("xAction", "getSessionCalendarList");
            map.put ("shiftSessionDate", shiftToSessionDate);
            Call<String> call = sessionClient.sessionService.call_api (map);
            call.enqueue (new Callback<String> () {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (cpd.isShowing ()) cpd.dismiss ();
                    try {
                        if (response.body () != null) {
                            com.reppsfitness.session_cal.json_model.SessionResponse sessionResponse = new Gson ().fromJson (response.body (), com.reppsfitness.session_cal.json_model.SessionResponse.class);
                            SessionRes data1 = sessionResponse.getData ();
                            timeList = data1.getTimeList ();
                            //If selected position set initially to 0th position
//                            if (timeList.size() > 0) {
//                                shiftToSessionTime = timeList.get(0);
//                                if (sessionResponse.getData().getSessionDateList().containsKey(timeList.get(0))) {
//                                    shiftToSessionID = ((DayDetails) sessionResponse.getData().getSessionDateList().get(timeList.get(0))).getSessionID();
//                                }
//                            }
                            recyclerView.setAdapter (new ShiftTimeAdapter (mContext, data1, NewRequestFragment.this));
                            btn_submit.setVisibility (View.VISIBLE);
                            Log.e ("timeList", timeList.toString ());
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    if (cpd.isShowing ()) cpd.dismiss ();
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    private void requestSession() {

        if (clientProfile.getIsRegular ().equals ("1")) {
            Log.e (TAG, "onViewClicked: 96");
            Map<String, String> map = new HashMap<> ();
            map.put ("xAction", "requestSession");
            map.put ("clientID", app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
//        map.put("requestDate", selectedDate);
//        map.put("sessionTime", selectedSlotTime);
            map.put ("shiftFromSessionDate", shiftFromSessionDate);
            map.put ("shiftFromSessionTime", shiftFromSessionTime);
            map.put ("shiftFromSessionID", shiftFromSessionID);
            map.put ("shiftToSessionDate", shiftToSessionDate);
            map.put ("shiftToSessionTime", shiftToSessionTime);
            map.put ("shiftToSessionID", shiftToSessionID);
            if (cd.isConnectingToInternet ()) {
                CustomProgressDialog pd = new CustomProgressDialog (mContext);
                pd.show ();
                app.getApiRequestHelper ().requestSession (map, new ApiRequestHelper.OnRequestComplete () {
                    @Override
                    public void onSuccess(Object object) {
                        if (pd.isShowing ()) pd.dismiss ();
                        LoginResponse response = (LoginResponse) object;
                        if (response != null && response.getCount () > 0) {
                            if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showLongToast (mContext, response.getMsg ());
                            }
//                        et_date.setText("");
//                        etSlotTimings.setText("");
                            ((RequestSessionActivity) mContext).displaySentRequestFrag ();
                        } else {
                            if (response != null && response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showSimpleialog (mContext, response.getMsg (), "Ok");
                            } else {
                                Utils.showSimpleialog (mContext, Utils.UNPROPER_RESPONSE, "Ok");
                            }
                        }
                    }

                    @Override
                    public void onFailure(String apiResponse) {
                        if (pd.isShowing ()) pd.dismiss ();
                        Utils.showLongToast (mContext, apiResponse);
                    }
                });
            } else {
                Utils.alert_dialog (mContext);
            }
        } else {
            Log.e (TAG, "onViewClicked: 97");                                                         //code by ajit
            Map<String, String> map = new HashMap<> ();
            map.put ("xAction", "requestSession");
            map.put ("clientID", app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
//          map.put("requestDate", selectedDate);
//          map.put("sessionTime", selectedSlotTime);
            map.put ("shiftFromSessionDate", "");
            map.put ("shiftFromSessionTime", "");
            map.put ("shiftFromSessionID", "");
            map.put ("shiftToSessionDate", showSessionDates);
            map.put ("shiftToSessionTime", shiftToSessionTime);
            map.put ("shiftToSessionID", shiftToSessionID);
            if (cd.isConnectingToInternet ()) {
                CustomProgressDialog pd = new CustomProgressDialog (mContext);
                pd.show ();
                app.getApiRequestHelper ().requestSession (map, new ApiRequestHelper.OnRequestComplete () {
                    @Override
                    public void onSuccess(Object object) {
                        if (pd.isShowing ()) pd.dismiss ();
                        LoginResponse response = (LoginResponse) object;
                        if (response != null && response.getCount () > 0) {
                            if (response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showLongToast (mContext, response.getMsg ());
                            }
//                        et_date.setText("");
//                        etSlotTimings.setText("");
                            ((RequestSessionActivity) mContext).displaySentRequestFrag ();
                        } else {
                            if (response != null && response.getMsg () != null && !TextUtils.isEmpty (response.getMsg ())) {
                                Utils.showSimpleialog (mContext, response.getMsg (), "Ok");
                            } else {
                                Utils.showSimpleialog (mContext, Utils.UNPROPER_RESPONSE, "Ok");
                            }
                        }
                    }

                    @Override
                    public void onFailure(String apiResponse) {
                        if (pd.isShowing ()) pd.dismiss ();
                        Utils.showLongToast (mContext, apiResponse);
                    }
                });
            } else {
                Utils.alert_dialog (mContext);
            }

        }
    }

    @Override
    public void onResume() {
        super.onResume ();
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<> ();
        map.put ("xAction", "getClientProfile");
        map.put ("clientID", app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
        Log.d (TAG, "getClientProfile: " + map);
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getClientProfile (map, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    try {
                        if (pd.isShowing ()) pd.dismiss ();
                        ClientProfileData clientProfileData = (ClientProfileData) object;
                        Log.e (TAG, "onSuccess: " + clientProfileData);
                        if (clientProfileData != null && clientProfileData.getCount () > 0) {
                            clientProfile = clientProfileData.getClientProfile ();                   //code by ajit
                            Log.e (TAG, "onSuccess: " + clientProfile);
                            String str_pkgEndDate = clientProfile.getPkgEndDate ();
                            String str_pkgstartdate = clientProfile.getPkgStartDate ();                //code by ajit
                            if (clientProfile.getPackageID () != null && !TextUtils.isEmpty (clientProfile.getPackageID ())) {
                                packageID = clientProfile.getPackageID ();
                            }
                            SimpleDateFormat formatter = new SimpleDateFormat ("yyyy-MM-dd");
                            try {
                                pkgEndDate = formatter.parse (str_pkgEndDate);
                                pkgStartDate = formatter.parse (str_pkgstartdate);                     //code by ajit
                                if (new Date ().after (pkgEndDate)) {
                                    llForm.setVisibility (View.GONE);
                                    tvError.setVisibility (View.VISIBLE);
                                } else {
                                    getShiftSessionDateList ();
                                    if (clientProfile.getIsRegular ().equals ("0")) {                   //code by ajit
                                        et_selectSession.setVisibility (View.GONE);
                                    }
                                    llForm.setVisibility (View.VISIBLE);
                                    tvError.setVisibility (View.GONE);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace ();
                            }
                        } else {
                            if (clientProfileData != null && clientProfileData.getMsg () != null && !TextUtils.isEmpty (clientProfileData.getMsg ())) {
                                Utils.showLongToast (mContext, clientProfileData.getMsg ());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

//    private void getSessionClientRequestTime() {
//        Map<String, String> map = new HashMap<>();
//        map.put("xAction", "getSessionClientRequestTime");
//        map.put("packageID", packageID);
//        map.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID());
//        map.put("requestDate", selectedDate);
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().getSessionClientRequestTime(map, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    SlotData slotData = (SlotData) object;
//                    if (slotData != null && slotData.getCount() > 0) {
//                        if (slotData.getData() != null && slotData.getData().getSlotTime() != null && slotData.getData().getSlotTime().size() > 0)
//                            slotTimings = slotData.getData().getSlotTime();
//                    } else {
//                        if (slotData != null && slotData.getMsg() != null && !TextUtils.isEmpty(slotData.getMsg())) {
//                            Utils.showLongToast(mContext, slotData.getMsg());
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                    Utils.showLongToast(mContext, apiResponse);
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
//    }

    private void getShiftSessionDateList() {
        Map<String, String> params = new HashMap<> ();
        params.put ("xAction", "getShiftSessionDateList");
        params.put ("clientID", app.getPreferences ().getLoggedInUser ().getData ().getClientID ());
        if (cd.isConnectingToInternet ()) {
            CustomProgressDialog pd = new CustomProgressDialog (mContext);
            pd.show ();
            app.getApiRequestHelper ().getShiftSessionDateList (params, new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Log.e ("in", "success");
                    ShiftSessionData response = (ShiftSessionData) object;
                    if (response != null) {
                        if (response.getCount () > 0 && response.getData () != null && response.getData () != null && response.getData ().size () > 0) {
                            sessionList = response.getData ();
                            Log.e (TAG, "onSuccess: " + sessionList);
                        } else {

                        }
                    } else {

                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing ()) pd.dismiss ();
                    Utils.showLongToast (mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }


//    private void displayDatePickerDialog() {
//        // set minimum date
//        final Calendar minCalendar = Calendar.getInstance();
//        minCalendar.setTime(new Date());
//
//        Calendar now = Calendar.getInstance();
//        DatePickerDialog dpd = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
//                    Log.d("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
//                    selectedDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                    et_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
//                    getSessionClientRequestTime();
//                },
//                now.get(Calendar.YEAR),
//                now.get(Calendar.MONTH),
//                now.get(Calendar.DAY_OF_MONTH)
//        );
//        dpd.setMinDate(minCalendar);
//        if (pkgEndDate != null) {
//            final Calendar maxCalendar = Calendar.getInstance();
//            maxCalendar.setTime(pkgEndDate);
//            dpd.setMaxDate(maxCalendar);
//        }
//        dpd.show(((AppCompatActivity) mContext).getFragmentManager(), "Datepickerdialog");
//    }
}
