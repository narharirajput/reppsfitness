package com.reppsfitness.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.client.EditBMIActivity;
import com.reppsfitness.client.adapter.BMIAdapter;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class BMIFragment extends BaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.tv_error)
    TextView tv_error;

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fab.setVisibility(View.VISIBLE);
    }

    private void getBmi() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getClientBMIDetails");
        if (app.getPreferences().isClient()) {
            params.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID()); //app.getPreferences().getLoggedInUser().getData().getClientID()
        } else {
            params.put("clientID", ((ClientHomeActivity) mContext).user.getClientID());
        }
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().bmiDetails(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(bmiData -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "success");
                        if (bmiData != null) {
                            if (bmiData.getCount() > 0 && bmiData.getData().size() > 0) {
                                recyclerView.setHasFixedSize(true);
                                recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
                                recyclerView.setAdapter(new BMIAdapter(mContext, bmiData.getData()));
                                tv_error.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                            } else {
                                tv_error.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                if (bmiData.getMsg() != null && !TextUtils.isEmpty(bmiData.getMsg())) {
                                    tv_error.setText(bmiData.getMsg());
                                }
                            }
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivity(new Intent(mContext, EditBMIActivity.class).putExtra("isAddBMI", true)
                .putExtra("user", ((ClientHomeActivity) mContext).user));
    }

    @Override
    public void onResume() {
        super.onResume();
        getBmi();
    }
}
