package com.reppsfitness.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.expandable_list.ExpandableRegularPackage;
import com.reppsfitness.expandable_list.ExpandableTermPackage;
import com.reppsfitness.expandable_list.RegularAdapter;
import com.reppsfitness.expandable_list.TermAdapter;
import com.reppsfitness.model.Plans;
import com.reppsfitness.model.PlansData;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.utils.ItemOffsetDecoration;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by MXCPUU11 on 8/8/2017.
 */

public class PackagePlansFragment extends BaseFragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.rv_term)
    RecyclerView rvTerm;
    @BindView(R.id.tv_regular)
    TextView tvRegular;
    @BindView(R.id.tv_term)
    TextView tvTerm;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerViewProperties(recyclerView);
        setRecyclerViewProperties(rvTerm);
        getPackagePlans();
    }

    private void setRecyclerViewProperties(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_package_plans;
    }

    private void getPackagePlans() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getPackagePlans");
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().packages(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    tvRegular.setText("Regular");
                    tvRegular.setVisibility(View.VISIBLE);
                    PlansData plansData = (PlansData) object;
                    if (plansData != null) {
                        if (plansData.getCount() > 0) {
                            if (plansData.getData() != null) {
                                Plans data = plansData.getData();
                                List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
                                List<ExpandableTermPackage> expandableTermPackages = new ArrayList<>();
                                List<RegularItem> listPackages = data.getRegular();
                                List<TermItem> listPackagesterm = data.getTerm();
                                if (listPackages != null && listPackages.size() > 0) {
                                    for (int i = 0; i < listPackages.size(); i++) {
                                        RegularItem regularItem = listPackages.get(i);
                                        List<RegularItem> regularItemList = new ArrayList<RegularItem>();
                                        regularItemList.add(regularItem);
                                        regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList, regularItem.getPackageImage (), regularItem.getPackageID ()));
                                    }
                                }
                                if (listPackagesterm != null && listPackagesterm.size() > 0) {
                                    tvTerm.setText("Term");
                                    tvTerm.setVisibility(View.VISIBLE);
                                    for (int j = 0; j < listPackagesterm.size(); j++) {
                                        TermItem termItem = listPackagesterm.get(j);
                                        List<TermItem> termItemList = new ArrayList<TermItem>();
                                        termItemList.add(termItem);
                                        expandableTermPackages.add(new ExpandableTermPackage(termItem.getPackageName(), termItem.getPackageAmount(), termItemList,termItem.getPackageImage (), termItem.getPackageID ()));
                                    }
                                    if (expandableTermPackages.size() > 0) {
                                        rvTerm.setNestedScrollingEnabled(false);
                                        rvTerm.setVisibility(View.VISIBLE);
                                        tvTerm.setText("Term");
                                    } else {
                                        rvTerm.setVisibility(View.GONE);
                                        tvTerm.setText("");
                                    }
                                }
                                if (listPackages != null && listPackages.size() > 0) {
                                    recyclerView.setAdapter(new RegularAdapter(PackagePlansFragment.this, regularPackageArrayList,
                                            false, "", null, "", false, -1, false, false));
                                }
                                if (expandableTermPackages.size() > 0) {
                                    rvTerm.setAdapter(new TermAdapter(PackagePlansFragment.this, expandableTermPackages,
                                            false, "", null, "", false, -1, false, false));
                                }
                            } else {
                                if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
                                    Utils.showLongToast(mContext, plansData.getMsg());
                                }
                            }
                        } else {
                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
                                Utils.showLongToast(mContext, plansData.getMsg());
                            } else {
                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again.");
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
//        Map<String, String> params = new HashMap<>();
//        params.put("xAction", "getPackagePlans");
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().packages(params, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    PackageData plansData = (PackageData) object;
//                    if (plansData != null) {
//                        if (plansData.getCount() > 0) {
//                            List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
//                            List<RegularItem> listPackages = plansData.getData();
//                            for (int i = 0; i < listPackages.size(); i++) {
//                                RegularItem regularItem = listPackages.get(i);
//                                List<RegularItem> regularItemList = new ArrayList<RegularItem>();
//                                regularItemList.add(regularItem);
//                                regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList));
//                            }
//                            recyclerView.setAdapter(new RegularAdapter(PackagePlansFragment.this, regularPackageArrayList, false, "", null, "", false, -1));
//                        } else {
//                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
//                                Utils.showLongToast(mContext, plansData.getMsg());
//                            } else {
//                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again.");
//                            }
//                        }
//                    } else {
//                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                    Log.e("in", "error " + apiResponse);
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
    }

//    private void getPackagePlans() {
//        Map<String, String> params = new HashMap<>();
//        params.put("xAction", "getPackagePlans");
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().packages(params, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    tvRegular.setText("Regular");
//                    PlansData plansData = (PlansData) object;
//                    if (plansData != null) {
//                        if (plansData.getCount() > 0) {
//                            List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
//                            List<ExpandableTermPackage> expandableTermPackages = new ArrayList<>();
//                            List<RegularItem> listPackages = plansData.getData().getRegular();
//                            List<TermItem> listPackagesterm = plansData.getData().getTerm();
//                            for (int i = 0; i < listPackages.size(); i++) {
//                                RegularItem regularItem = listPackages.get(i);
//                                List<RegularItem> regularItemList = new ArrayList<RegularItem>();
//                                regularItemList.add(regularItem);
//                                regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList));
//                            }
//                            recyclerView.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
//                            recyclerView.setAdapter(new RegularAdapter(PackagePlansFragment.this, regularPackageArrayList, true, false));
//                            tvTerm.setText("Term");
//                            if (listPackagesterm != null && listPackagesterm.size() > 0) {
//                                for (int j = 0; j < listPackagesterm.size(); j++) {
//                                    TermItem termItem = listPackagesterm.get(j);
//                                    List<TermItem> termItemList = new ArrayList<TermItem>();
//                                    termItemList.add(termItem);
//                                    expandableTermPackages.add(new ExpandableTermPackage(termItem.getPackageName(), termItem.getPackageAmount(), termItemList));
//                                }
//                                rvTerm.setAdapter(new TermAdapter(PackagePlansFragment.this, expandableTermPackages, true, false));
//                                rvTerm.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
//                                rvTerm.setNestedScrollingEnabled(false);
//                                rvTerm.setVisibility(View.VISIBLE);
//                            } else {
//                                rvTerm.setVisibility(View.GONE);
//                                tvTerm.setText("");
//                            }
//                        } else {
//                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
//                                Utils.showLongToast(mContext, plansData.getMsg());
//                            } else {
//                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again.");
//                            }
//                        }
//                    } else {
//                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                    Log.e("in", "error " + apiResponse);
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
//    }
}
