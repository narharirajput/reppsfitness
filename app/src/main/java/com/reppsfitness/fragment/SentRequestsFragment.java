package com.reppsfitness.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.adapter.SentRequestAdapter;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class SentRequestsFragment extends BaseFragment {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView tv_error;

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_message;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onResume() {
        super.onResume();
        getSentRequests();
    }

    private void getSentRequests() {
        tv_error.setText("No sent requests found.");
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getClientSessionRequest");
        map.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getClientSessionRequest(map)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(sentRequestData -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (sentRequestData != null) {
                            if (sentRequestData.getMsg() != null && !TextUtils.isEmpty(sentRequestData.getMsg())) {
                                tv_error.setText(sentRequestData.getMsg());
                            }
                            if (sentRequestData.getCount() > 0) {
                                if (sentRequestData.getData() != null && sentRequestData.getData().size() > 0) {
                                    recyclerView.setAdapter(new SentRequestAdapter(mContext, sentRequestData.getData()));
                                    manageVisibility(View.GONE, View.VISIBLE);
                                } else {
                                    manageVisibility(View.VISIBLE, View.GONE);
                                }
                            } else {
                                manageVisibility(View.VISIBLE, View.GONE);
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                            manageVisibility(View.GONE, View.GONE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
//                        Utils.showLongToast(mContext, throwable.getMessage());
                        manageVisibility(View.GONE, View.GONE);
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void manageVisibility(int gone, int visible) {
        tv_error.setVisibility(gone);
        recyclerView.setVisibility(visible);
    }
}
