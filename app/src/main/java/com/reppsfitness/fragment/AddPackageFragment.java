package com.reppsfitness.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reppsfitness.AddClientPackageActivity;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.SettingsActivity;
import com.reppsfitness.expandable_list.ExpandableRegularPackage;
import com.reppsfitness.expandable_list.ExpandableTermPackage;
import com.reppsfitness.expandable_list.RegularAdapter;
import com.reppsfitness.expandable_list.TermAdapter;
import com.reppsfitness.model.Plans;
import com.reppsfitness.model.PlansData;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.utils.ItemOffsetDecoration;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class AddPackageFragment extends BaseFragment {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.rv_term)
    RecyclerView rvTerm;
    @BindView(R.id.tv_regular)
    TextView tvRegular;
    @BindView(R.id.tv_term)
    TextView tvTerm;
 /*   @BindView(R.id.ll_currentPackage)
    LinearLayout llCurrentPackage;*/
    @BindView(R.id.tv_currentPlanTitle)
    TextView tvCurrentPlanTitle;
    @BindView(R.id.tv_currentPlanPrice)
    TextView tvCurrentPlanPrice;
    @BindView(R.id.rr_noImage)
    RelativeLayout rrNoImage;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_name)
    TextView tvName;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setRecyclerViewProperties(recyclerView);
        setRecyclerViewProperties(rvTerm);
        recyclerView.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
        rvTerm.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
       /* if(((AddClientPackageActivity) mContext).user.getPackageID ().equals ("0"))
            llCurrentPackage.setVisibility(View.GONE);*/
        getPackagePlans();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_add_package;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        if (((AddClientPackageActivity) mContext).user != null) {
            tvName.setText(((AddClientPackageActivity) mContext).user.getClientFName() + " " +
                    ((AddClientPackageActivity) mContext).user.getClientLName());
            if (((AddClientPackageActivity) mContext).user.getClientProfilePic() != null) {
                Glide.with(mContext).load(((AddClientPackageActivity) mContext).user.getClientProfilePic())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(ivProfile);
                rrNoImage.setVisibility(View.GONE);
                ivProfile.setVisibility(View.VISIBLE);
            } else {
                rrNoImage.setVisibility(View.VISIBLE);
                ivProfile.setVisibility(View.GONE);
            }
        }
    }

    //@OnClick({R.id.rr_noImage, R.id.iv_profile, R.id.tv_name})
    @OnClick({R.id.btn_edit_profile})
    public void onViewClicked(View view) {
        if (!app.getPreferences().isClient()) {
            startActivity(new Intent(mContext, SettingsActivity.class)
                    .putExtra("user", ((AddClientPackageActivity) mContext).user));
        }
    }

    private void getPackagePlans() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getPackagePlans");
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().packages(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    PlansData plansData = (PlansData) object;
                    if (plansData != null) {
                        if (plansData.getCount() > 0) {
                            if (plansData.getData() != null) {
                                Plans data = plansData.getData();
                                List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
                                List<ExpandableTermPackage> expandableTermPackages = new ArrayList<>();
                                List<RegularItem> listPackages = data.getRegular();
                                List<TermItem> listPackagesterm = data.getTerm();
                                if (listPackages != null && listPackages.size() > 0) {
                                    for (int i = 0; i < listPackages.size(); i++) {
                                        RegularItem regularItem = listPackages.get(i);
                                        List<RegularItem> regularItemList = new ArrayList<RegularItem>();
                                        regularItemList.add(regularItem);
                                        //setCurrentPackage(regularItem);
                                        regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList, regularItem.getPackageImage (), regularItem.getPackageID ()));
                                    }
                                }
                                if (listPackagesterm != null && listPackagesterm.size() > 0) {
                                    tvTerm.setText("Terms packages");
                                    tvTerm.setVisibility(View.VISIBLE);
                                    for (int j = 0; j < listPackagesterm.size(); j++) {
                                        TermItem termItem = listPackagesterm.get(j);
                                        List<TermItem> termItemList = new ArrayList<TermItem>();
                                        termItemList.add(termItem);
                                        expandableTermPackages.add(new ExpandableTermPackage(termItem.getPackageName(), termItem.getPackageAmount(), termItemList,termItem.getPackageImage (), termItem.getPackageID ()));
                                    }
                                    if (expandableTermPackages.size() > 0) {
                                        rvTerm.setNestedScrollingEnabled(false);
                                        rvTerm.setVisibility(View.VISIBLE);
                                        tvTerm.setText("Terms packages");
                                    } else {
                                        rvTerm.setVisibility(View.GONE);
                                        tvTerm.setText("");
                                    }
                                }
                                if (listPackages != null && listPackages.size() > 0) {
                                    recyclerView.setAdapter(new RegularAdapter(AddPackageFragment.this, regularPackageArrayList,
                                            false, "", null, "", false, -1, false, false));
                                }
                                if (expandableTermPackages.size() > 0) {
                                    rvTerm.setAdapter(new TermAdapter(AddPackageFragment.this, expandableTermPackages,
                                            false, "", null, "", false, -1, false, false));
                                }
                            } else {
                                if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
                                    Utils.showLongToast(mContext, plansData.getMsg());
                                }
                            }
                        } else {
                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
                                Utils.showLongToast(mContext, plansData.getMsg());
                            } else {
                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again.");
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
//                    PackageData plansData = (PackageData) object;
//                    if (plansData != null) {
//                        if (plansData.getCount() > 0) {
//                            List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
//                            List<RegularItem> listPackages = plansData.getData();
//                            for (int i = 0; i < listPackages.size(); i++) {
//                                RegularItem regularItem = listPackages.get(i);
//                                List<RegularItem> regularItemList = new ArrayList<RegularItem>();
//                                regularItemList.add(regularItem);
//                                regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList));
//                            }
//                            recyclerView.addItemDecoration(new ItemOffsetDecoration(getActivity(), R.dimen.size_3));
//                            recyclerView.setAdapter(new RegularAdapter(AddPackageFragment.this, regularPackageArrayList,
//                                    false, "", null, "", false, -1));
//                        } else {
//                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
//                                Utils.showLongToast(mContext, plansData.getMsg());
//                            } else {
//                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again.");
//                            }
//                        }
//                    } else {
//                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

//    public void getClientPackagePlans() {
//        Map<String, String> params = new HashMap<>();
//        params.put("xAction", "getClientPackagePlans");
//        params.put("clientID", ((AddClientPackageActivity) mContext).user.getClientID());
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().packages(params, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    tvRegular.setText("Regular");
//                    PlansData plansData = (PlansData) object;
//                    if (plansData != null) {
//                        if (plansData.getCount() > 0) {
//                            List<ExpandableRegularPackage> regularPackageArrayList = new ArrayList<>();
//                            List<ExpandableTermPackage> expandableTermPackages = new ArrayList<>();
//                            List<RegularItem> listPackages = plansData.getData().getRegular();
//                            List<TermItem> listPackagesterm = plansData.getData().getTerm();
//                            for (int i = 0; i < listPackages.size(); i++) {
//                                RegularItem regularItem = listPackages.get(i);
//                                List<RegularItem> regularItemList = new ArrayList<RegularItem>();
//                                regularItemList.add(regularItem);
//                                regularPackageArrayList.add(new ExpandableRegularPackage(regularItem.getPackageName(), regularItem.getPackageAmount(), regularItemList));
//                            }
//                            boolean isAnyPackageBuy = false;
//                            for (int i = 0; i < listPackages.size(); i++) {
//                                RegularItem regularItem = listPackages.get(i);
//                                if (regularItem.getIsCurrentPackage() == 1
//                                        || (regularItem.getIsCurrentPackage() == 0 && regularItem.getIsRequestPackage() == 1)) {
//                                    isAnyPackageBuy = true;
//                                    break;
//                                }
//                            }
//                            recyclerView.setAdapter(new RegularAdapter(AddPackageFragment.this, regularPackageArrayList, true, isAnyPackageBuy, ""));
//                            tvTerm.setText("Term");
//                            for (int j = 0; j < listPackagesterm.size(); j++) {
//                                TermItem termItem = listPackagesterm.get(j);
//                                List<TermItem> termItemList = new ArrayList<TermItem>();
//                                termItemList.add(termItem);
//                                expandableTermPackages.add(new ExpandableTermPackage(termItem.getPackageName(), termItem.getPackageAmount(), termItemList));
//                            }
//                            boolean isAnyTermBuy = false;
//                            for (int i = 0; i < listPackagesterm.size(); i++) {
//                                TermItem termItem = listPackagesterm.get(i);
//                                if (termItem.getIsCurrentPackage() == 1
//                                        || (termItem.getIsCurrentPackage() == 0 && termItem.getIsRequestPackage() == 1)) {
//                                    isAnyTermBuy = true;
//                                    break;
//                                }
//                            }
//                            rvTerm.setAdapter(new TermAdapter(AddPackageFragment.this, expandableTermPackages, true, isAnyTermBuy));
//                            rvTerm.setNestedScrollingEnabled(false);
//                        } else {
//                            if (plansData.getMsg() != null && !TextUtils.isEmpty(plansData.getMsg())) {
//                                Utils.showLongToast(mContext, plansData.getMsg());
//                            } else {
//                                Utils.showLongToast(mContext, "Unable to get Packages/Plans. Please try again later.");
//                            }
//                        }
//                    } else {
//                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                    Log.e("in", "error " + apiResponse);
//                    Utils.showLongToast(mContext, apiResponse);
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
//    }

    /*private void setCurrentPackage(Object currentPackage) {
        if (currentPackage != null) {
            if (currentPackage instanceof RegularItem) {
                RegularItem regularItem = (RegularItem) currentPackage;
                llCurrentPackage.setVisibility(View.VISIBLE);
                tvCurrentPlanTitle.setText(regularItem.getPackageName());
                tvCurrentPlanPrice.setText("₹ " + regularItem.getPackageAmount());
            } else if (currentPackage instanceof TermItem) {
                TermItem termItem = (TermItem) currentPackage;
                llCurrentPackage.setVisibility(View.VISIBLE);
                tvCurrentPlanTitle.setText(termItem.getPackageName());
                tvCurrentPlanPrice.setText("₹ " + termItem.getPackageAmount());
            }
        } else {
            llCurrentPackage.setVisibility(View.GONE);
        }
    }*/

    private void setRecyclerViewProperties(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);
    }
}
