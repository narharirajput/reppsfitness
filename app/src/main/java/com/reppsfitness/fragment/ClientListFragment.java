package com.reppsfitness.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.reppsfitness.AddClientActivity;
import com.reppsfitness.AdminMainActivity;
import com.reppsfitness.BaseFragment;
import com.reppsfitness.R;
import com.reppsfitness.adapter.ClientListAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.ClientsData;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/5/2017.
 */

public class ClientListFragment extends BaseFragment {
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    public TextView tv_error;
    @BindView(R.id.swiperefreshLayout)
    SwipeRefreshLayout swiperefreshLayout;
    CustomProgressDialog pd;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pd = new CustomProgressDialog(mContext);
        fab.setVisibility(View.VISIBLE);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        swiperefreshLayout.setOnRefreshListener(this::getClientListInRxFashion);
        tv_error.setText("No Clients found.");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
        getClientListInRxFashion();
    }

    private void getClientListInRxFashion() {
        if (cd.isConnectingToInternet()) {
            if (pd == null) {
                pd.hide();
            }else{
                pd.setTitle("Loading...");
                pd.show();
            }
            app.getApiRequestHelper().getClients(app.getPreferences().getLoggedInUser().getData().getUserID(), new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (swiperefreshLayout != null && swiperefreshLayout.isRefreshing()) {
                        swiperefreshLayout.setRefreshing(false);
                    }
                    if (pd != null && pd.isShowing()) pd.dismiss();
                    ClientsData clientsData = (ClientsData) object;
                    Log.e("in", "success");
                    if (clientsData != null) {
                        if (clientsData.getCount() > 0 && clientsData.getData().size() > 0) {
                            final ClientListAdapter clientListAdapter = new ClientListAdapter(mContext, clientsData.getData(),ClientListFragment.this);
                            recyclerView.setAdapter(clientListAdapter);
                            recyclerView.getAdapter ().notifyDataSetChanged ();
                            setSearchFilter(clientListAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            tv_error.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            tv_error.setVisibility(View.VISIBLE);
                            if (clientsData.getMsg() != null && !TextUtils.isEmpty(clientsData.getMsg())) {
                                tv_error.setText(clientsData.getMsg());
                            }
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        tv_error.setVisibility(View.VISIBLE);
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (swiperefreshLayout != null && swiperefreshLayout.isRefreshing()) {
                        swiperefreshLayout.setRefreshing(false);
                    }
                    if (pd != null && pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            if (swiperefreshLayout != null && swiperefreshLayout.isRefreshing()) {
                swiperefreshLayout.setRefreshing(false);
            }
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.fragment_home;
    }

    private void setSearchFilter(final ClientListAdapter clientListAdapter) {
        ((AdminMainActivity) mContext).searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Utils.hideSoftKeyboard(getActivity());
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText)) {
                    clientListAdapter.filter(newText);
                } else {
                    clientListAdapter.filter("");
                }
                return true;
            }
        });
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), AddClientActivity.class)
                .putExtra("isAddClient", true));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }
}
