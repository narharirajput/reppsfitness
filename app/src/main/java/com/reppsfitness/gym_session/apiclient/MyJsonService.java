package com.reppsfitness.gym_session.apiclient;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Raquib-ul-Alam Kanak on 1/3/16.
 * Website: http://alamkanak.github.io
 */
public interface MyJsonService {

    @GET("/bins/1kpjf")
    Call<List<Event>> listEvents();

}
