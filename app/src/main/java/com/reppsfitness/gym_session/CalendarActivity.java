package com.reppsfitness.gym_session;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.GsonBuilder;
import com.reppsfitness.R;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.gym_session.apiclient.Event;
import com.reppsfitness.gym_session.apiclient.MyJsonService;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.SaveTemplateResponse;
import com.reppsfitness.utils.LoginResponseModelDeserializer;
import com.reppsfitness.utils.SaveTemplateResponseDeserializer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * An example of how events can be fetched from network and be displayed on the week view.
 * Created by Raquib-ul-Alam Kanak on 1/3/2014.
 * Website: http://alamkanak.github.io
 */
public class CalendarActivity extends BaseActivity implements Callback<List<Event>> {

    private List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
    boolean calledNetwork = false;

    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        Log.e("mnthYear", newMonth + "||" + newYear);
        // Download events from network if it hasn't been done already. To understand how events are
        // downloaded using retrofit, visit http://square.github.io/retrofit
        if (!calledNetwork) {
//            RestAdapter retrofit = new RestAdapter.Builder()
//                    .setEndpoint("https://api.myjson.com/bins")
//                    .build();
//            MyJsonService service = retrofit.create(MyJsonService.class);
//            service.listEvents(this);
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl("https://api.myjson.com/")
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                            .setLenient()
                            .registerTypeAdapterFactory(new ApiRequestHelper.NullStringToEmptyAdapterFactory())
                            .registerTypeAdapter(LoginResponse.class, new LoginResponseModelDeserializer())
                            .registerTypeAdapter(SaveTemplateResponse.class, new SaveTemplateResponseDeserializer())
                            .create()));
            Retrofit retrofit = builder.client(getClient().build()).build();
            MyJsonService appService = retrofit.create(MyJsonService.class);
            Call<List<Event>> listCall = appService.listEvents();
            listCall.enqueue(this);
            calledNetwork = true;
        }

        // Return only the events that matches newYear and newMonth.
        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event : events) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event);
            }
        }
        return matchedEvents;
    }

    /**
     * Checks if an event falls into a specific year and month.
     *
     * @param event The event to check for.
     * @param year  The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month - 1) || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }

//    @Override
//    public void success(List<Event> events, Response response) {
//        this.events.clear();
//        for (Event event : events) {
//            this.events.add(event.toWeekViewEvent());
//        }
//        getWeekView().notifyDatasetChanged();
//    }

//    @Override
//    public void failure(RetrofitError error) {
//        error.printStackTrace();
//        Toast.makeText(this, R.string.async_error, Toast.LENGTH_SHORT).show();
//    }

    @Override
    public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
        this.events.clear();
        for (Event event : response.body()) {
            this.events.add(event.toWeekViewEvent());
        }
        getWeekView().notifyDatasetChanged();
    }

    @Override
    public void onFailure(Call<List<Event>> call, Throwable t) {
        t.printStackTrace();
        Toast.makeText(this, R.string.async_error, Toast.LENGTH_SHORT).show();
    }

    @NonNull
    private OkHttpClient.Builder getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
// add your other interceptors …

// add logging as last interceptor
        httpClient.interceptors().add(logging);
        return httpClient;
    }
}
