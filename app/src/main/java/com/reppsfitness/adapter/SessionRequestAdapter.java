package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.SessionRequestsActivity;
import com.reppsfitness.model.SessionRequest;
import com.reppsfitness.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class SessionRequestAdapter extends RecyclerView.Adapter<SessionRequestAdapter.RecyclerViewHolder> {
    Context mContext;
    private List<SessionRequest> sessionRequestList;
    private Date dateObj = null;

    public SessionRequestAdapter(Context mContext, List<SessionRequest> sessionRequestList) {
        this.mContext = mContext;
        this.sessionRequestList = sessionRequestList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_session_requests, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        SessionRequest sessionRequest = sessionRequestList.get(position);
        String date = Utils.ymdTodmy(sessionRequest.getRequestDate());
        try {
            dateObj  = Utils.curFormater.parse(date);
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        if (date != null && !TextUtils.isEmpty(date) && holder.tvDate != null) {
            holder.tvDate.setText ("Date: " + date);
        }
        holder.tvTime.setText("Time: " + sessionRequest.getRequestFromTime() + " - " + sessionRequest.getRequestToTime());
        holder.tvClientFName.setText("Client Name: " + sessionRequest.getClientFName());
        holder.tvClientEmail.setText("Client Email: " + sessionRequest.getClientEmail());
        holder.llButtons.setVisibility (View.VISIBLE);
        if (sessionRequest.getRequestStatus() != null && !TextUtils.isEmpty(sessionRequest.getRequestDate())) {
            switch (sessionRequest.getRequestStatus()) {
                case "0":
                    holder.btnApprove.setVisibility(View.VISIBLE);
                    holder.btnCancel.setVisibility(View.VISIBLE);
                    holder.tvStatus.setText("");
                    holder.tvStatus.setTextColor(holder.yellow);
                    if (new Date ().after (dateObj)) {
                        holder.llButtons.setVisibility (View.GONE);
                    }
                    break;
                case "1":
                    holder.btnApprove.setVisibility(View.GONE);
                    holder.btnCancel.setVisibility(View.VISIBLE);
                    holder.tvStatus.setText("APPROVED");
                    holder.tvStatus.setTextColor(holder.app_green_color);
                    if (new Date ().after (dateObj)) {
                        holder.llButtons.setVisibility (View.GONE);
                    }
                    break;
                case "2":
                    holder.btnApprove.setVisibility(View.GONE);
                    holder.btnCancel.setVisibility(View.GONE);
                    holder.tvStatus.setText("CANCELLED");
                    holder.tvStatus.setTextColor(holder.red);
                    if (new Date ().after (dateObj)) {
                        holder.llButtons.setVisibility (View.GONE);
                    }
                    break;
            }
        }
        holder.btnCancel.setOnClickListener(view -> {

            alertDialog (sessionRequest.getSessionCRID(),"2","Do you want to Cancel this session request");

            //((SessionRequestsActivity) mContext).updateSessionRequestStatus(sessionRequest.getSessionCRID(),"2");
        });
        holder.btnApprove.setOnClickListener(view -> {
            alertDialog (sessionRequest.getSessionCRID(),"1","Do you want to Approve this session request");

            //((SessionRequestsActivity) mContext).updateSessionRequestStatus(sessionRequest.getSessionCRID(),"1");
        });
    }

    private void alertDialog(String sessionCRID, String s, String msg) {                                                                      //code by hari
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder (mContext);
        alertBuilder.setTitle ("Session request action");
        alertBuilder.setMessage (msg);
        alertBuilder.setPositiveButton ("Yes", (dialog, which) ->
                ((SessionRequestsActivity) mContext).updateSessionRequestStatus(sessionCRID,s));
        alertBuilder.setNegativeButton ("Cancel", (dialog, which) -> dialog.dismiss ());
        alertBuilder.show ();
    }
    @Override
    public int getItemCount() {
        return sessionRequestList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llButtons)
        LinearLayout llButtons;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_clientFName)
        TextView tvClientFName;
        @BindView(R.id.tv_clientEmail)
        TextView tvClientEmail;
        @BindView(R.id.btn_approve)
        Button btnApprove;
        @BindView(R.id.btn_cancel)
        Button btnCancel;
        @BindView(R.id.ll_left)
        LinearLayout llLeft;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindColor(R.color.event_color_04)
        int yellow;
        @BindColor(R.color.red)
        int red;
        @BindColor(R.color.app_green_color)
        int app_green_color;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
