package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.reppsfitness.AddExerciseActivity;
import com.reppsfitness.R;
import com.reppsfitness.model.Element;
import com.reppsfitness.model.Exercise;
import com.reppsfitness.model.ExerciseCat;
import com.reppsfitness.model.WorkoutElement;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.searchable_spinner.SpinnerDialog;
import com.reppsfitness.widget.swipelayout.SwipeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ExerciseListAdapter extends RecyclerView.Adapter<ExerciseListAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<Exercise> exerciseList;
    private final List<FlexboxLayout> flexboxLayoutPool = new ArrayList<>();

    public ExerciseListAdapter(Context mContext, List<Exercise> exerciseList) {
        this.mContext = mContext;
        this.exerciseList = exerciseList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_exercise, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int pos) {
        Exercise exercise = exerciseList.get(pos);
        holder.swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
//            repeatExerciseView();
        });

        //Setting data
//        if (exercise.getWorkoutElements() != null && exercise.getWorkoutElements().size() > 0) {
//            holder.swipeLayout.setSwipeEnabled(true);
////            String workoutTypeID = exercise.getWorkoutTypeID();
////            List<Element> elementList = new ArrayList<>();
////            if (workoutTypeID != null) {
////                for (int i = 0; i < ((AddExerciseActivity) mContext).workoutTypeList.size(); i++) {
////                    if (((AddExerciseActivity) mContext).workoutTypeList.get(i).getWorkoutTypeID().matches(workoutTypeID)) {
////                        elementList = ((AddExerciseActivity) mContext).workoutTypeList.get(i).getElements();
////                        break;
////                    }
////                }
////            }
//            List<WorkoutElement> elements = exercise.getWorkoutElements();
//            if (elements != null && elements.size() > 0) {
//                holder.fl_elements.removeAllViews();
//                for (int i = 0; i < elements.size(); i++) {
//                    WorkoutElement workoutElement1 = elements.get(i);
//                    LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                    RelativeLayout rr_element_view = (RelativeLayout) layoutInflater.inflate(R.layout.rr_element_view, null);
//                    rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
//                            Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
//                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                            RelativeLayout.LayoutParams.WRAP_CONTENT);
//                    layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
//                    rr_element_view.setLayoutParams(layoutParams);
//                    EditText et_element = rr_element_view.findViewById(R.id.et_element);
//                    et_element.setHint(workoutElement1.getWorkoutElementName());
//                    et_element.setTag(workoutElement1.getWorkoutElementID());
//                    //                    for (int i1 = 0; i1 < elementList.size(); i1++) {
////                        if (elementList.get(i1).getWorkoutElementID().matches(workoutElement1.getWorkoutElementID())) {
////                            workoutElementValue = elementList.get(i1).getWorkoutElementValue();
////                            break;
////                        }
////                    }
//                    ArrayList<String> finalWorkoutElementValue = workoutElement1.getWorkoutElementValueList();
//                    et_element.setOnClickListener(view1 -> {
//                        SpinnerDialog elementDialog = new SpinnerDialog(((AppCompatActivity) mContext), finalWorkoutElementValue,
//                                "Select " + workoutElement1.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
//                        elementDialog.showSpinerDialog();
//                        elementDialog.bindOnSpinerListener((item1, position1) -> {
//                            et_element.setText(item1);
//                            WorkoutElement workoutElement = new WorkoutElement();
//                            workoutElement.setWorkoutElementID(workoutElement1.getWorkoutElementID());
//                            workoutElement.setWorkoutElementName(workoutElement1.getWorkoutElementName());
//                            workoutElement.setWorkoutElementValue(finalWorkoutElementValue.get(position1));
//                            workoutElement.setWorkoutElementValueList(finalWorkoutElementValue);
//                            exercise.getWorkoutElements().add(workoutElement);
//                        });
//                    });
//                    holder.fl_elements.addView(rr_element_view, holder.fl_elements.getChildCount());
//                }
//            }
//        }
        if (((AddExerciseActivity) mContext).workoutTypeList != null && ((AddExerciseActivity) mContext).workoutTypeList.size() > 0) {
            if (exercise.getWorkoutName() != null)
                holder.et_type.setText(exercise.getWorkoutName());
            holder.et_type.setOnClickListener(view -> {
                SpinnerDialog spinnerDialog = new SpinnerDialog(((AppCompatActivity) mContext), ((AddExerciseActivity) mContext).workoutTypeNameList,
                        "Select Workout Type", R.style.DialogAnimations_SmileWindow, true);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    holder.swipeLayout.setSwipeEnabled(true);
                    holder.et_type.setText(item);
                    exercise.setExerciseName(item);
                    exercise.setExerciseID(((AddExerciseActivity) mContext).workoutTypeList.get(position).getWorkoutTypeID());
//                    holder.et_type.setTag(((AddExerciseActivity) mContext).workoutTypeList.get(position).getWorkoutTypeID());
                    List<Element> elements = ((AddExerciseActivity) mContext).workoutTypeList.get(position).getElements();
//                    holder.fl_elements.removeAllViews();
                    for (int i = 0; i < elements.size(); i++) {
                        Element element = elements.get(i);
                        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        RelativeLayout rr_element_view = (RelativeLayout) layoutInflater.inflate(R.layout.rr_element_view, null);
                        rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                        rr_element_view.setLayoutParams(layoutParams);
                        EditText et_element = rr_element_view.findViewById(R.id.et_element);
                        et_element.setHint(element.getWorkoutElementName());
                        et_element.setTag(element.getWorkoutElementID());
                        ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
                        et_element.setOnClickListener(view1 -> {
                            SpinnerDialog elementDialog = new SpinnerDialog(((AppCompatActivity) mContext), workoutElementValue,
                                    "Select " + element.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                            elementDialog.showSpinerDialog();
                            elementDialog.bindOnSpinerListener((item1, position1) -> {
                                et_element.setText(item1);
                                WorkoutElement workoutElement = new WorkoutElement();
                                workoutElement.setWorkoutElementID(element.getWorkoutElementID());
                                workoutElement.setWorkoutElementName(element.getWorkoutElementName());
                                workoutElement.setWorkoutElementValue(workoutElementValue.get(position1));
                                workoutElement.setWorkoutElementValueList(workoutElementValue);
                                exercise.getWorkoutElements().add(workoutElement);
                            });
                        });
                        holder.fl_elements.addView(rr_element_view, holder.fl_elements.getChildCount());
                    }
                    holder.getFlexboxLayoutList().add(pos, holder.fl_elements);
                });
            });
        }
        if (((AddExerciseActivity) mContext).exerciseCatList != null && ((AddExerciseActivity) mContext).exerciseCatList.size() > 0) {
            if (exercise.getWorkoutName() != null)
                holder.et_category.setText(exercise.getExerciseCatName());
            if (exercise.getExerciseName() != null) {
                holder.etSelectExercise.setVisibility(View.VISIBLE);
                holder.etSelectExercise.setText(exercise.getExerciseName());
                ArrayList<String> exerciseNameList = exercise.getExerciseNameList();
                ArrayList<Exercise> exerciseList = exercise.getExerciseList();
                holder.etSelectExercise.setOnClickListener(v -> {
                    SpinnerDialog exerciseDialog = new SpinnerDialog(((AppCompatActivity) mContext), exerciseNameList,
                            "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                    exerciseDialog.showSpinerDialog();
                    exerciseDialog.bindOnSpinerListener((item1, position1) -> {
                        holder.etSelectExercise.setText(item1);
                        holder.etSelectExercise.setTag(this.exerciseList.get(position1).getExerciseID());
                        exercise.setExerciseName(item1);
                        exercise.setExerciseID(exerciseList.get(position1).getExerciseID());
                        exercise.setExerciseList(exerciseList);
                        exercise.setExerciseNameList(exerciseNameList);
                    });
                });
            }
            holder.et_category.setOnClickListener(view -> {
                SpinnerDialog spinnerDialog = new SpinnerDialog(((AppCompatActivity) mContext), ((AddExerciseActivity) mContext).workoutCatNameList,
                        "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                spinnerDialog.showSpinerDialog();
                spinnerDialog.bindOnSpinerListener((item, position) -> {
                    holder.et_category.setText(item);
                    holder.etSelectExercise.setText("");
                    exercise.setExerciseCatName(item);
                    exercise.setExerciseCatID(((AddExerciseActivity) mContext).exerciseCatList.get(position).getExerciseCatID());
                    holder.et_category.setTag(((AddExerciseActivity) mContext).exerciseCatList.get(position).getExerciseCatID());
                    holder.rr_selectExercise.setVisibility(View.VISIBLE);
                    ArrayList<String> exerciseNameList = new ArrayList<>();
                    ArrayList<Exercise> exerciseList = new ArrayList<>();
                    if (((AddExerciseActivity) mContext).exerciseCatList.get(position).getExercises().size() > 0) {
                        ExerciseCat exerciseCat = ((AddExerciseActivity) mContext).exerciseCatList.get(position);
                        for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                            exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                            exerciseList.addAll(exerciseCat.getExercises());
                        }
                    }
                    holder.etSelectExercise.setOnClickListener(v -> {
                        SpinnerDialog exerciseDialog = new SpinnerDialog(((AppCompatActivity) mContext), exerciseNameList,
                                "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                        exerciseDialog.showSpinerDialog();
                        exerciseDialog.bindOnSpinerListener((item1, position1) -> {
                            holder.etSelectExercise.setText(item1);
                            holder.etSelectExercise.setTag(exerciseList.get(position1).getExerciseID());
                            exercise.setExerciseName(item1);
                            exercise.setExerciseID(exerciseList.get(position1).getExerciseID());
                            exercise.setExerciseList(exerciseList);
                            exercise.setExerciseNameList(exerciseNameList);
                        });
                    });
                });
            });
        }
    }

//    @Override
//    public void onViewRecycled(RecyclerViewHolder personViewHolder) {
//        super.onViewRecycled(personViewHolder);
//        personViewHolder.ll_root.removeAllViews();
//    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_repeat)
        TextView tvRepeat;
        @BindView(R.id.tv_delete)
        TextView tvDelete;
        @BindView(R.id.bottom_wrapper)
        LinearLayout bottomWrapper;
        @BindView(R.id.et_type)
        EditText et_type;
        @BindView(R.id.et_category)
        EditText et_category;
        @BindView(R.id.et_selectExercise)
        EditText etSelectExercise;
        @BindView(R.id.rr_selectExercise)
        RelativeLayout rr_selectExercise;
        @BindView(R.id.fl_elements)
        FlexboxLayout fl_elements;
        @BindView(R.id.swipeLayout)
        SwipeLayout swipeLayout;
        @BindView(R.id.ll_root)
        LinearLayout ll_root;
        private final List<FlexboxLayout> flexboxLayoutList = new ArrayList<>();
        private final ViewGroup container;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            container = (ViewGroup) itemView;
            Log.e("fl list size", "" + flexboxLayoutList.size());
            flexboxLayoutList.add(fl_elements);
        }

        public List<FlexboxLayout> getFlexboxLayoutList() {
            return flexboxLayoutList;
        }
//        public void bind(Exercise exercise) {
//            recycleImageViews();
//            for (int i = 0; i < exercise.getWorkoutElements().size(); ++i) {
//                final FlexboxLayout imageView = getRecycledImageViewOrCreate();
//                flexboxLayoutList.add(imageView);
//            }
//        }
//
//        private FlexboxLayout getRecycledImageViewOrCreate() {
//            if (flexboxLayoutList.isEmpty()) {
//                return fl_elements;
//            }
//            return flexboxLayoutList.remove(0);
//        }
//
//        public void recycleImageViews() {
//            flexboxLayoutPool.addAll(flexboxLayoutList);
//            flexboxLayoutList.clear();
//        }
    }

//    @Override
//    public void onViewRecycled(RecyclerViewHolder holder) {
//        super.onViewRecycled(holder);
//        holder.recycleImageViews();
//    }

}