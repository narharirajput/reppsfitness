package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.model.Notification;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class AdminMessageAdapter extends RecyclerView.Adapter<AdminMessageAdapter.RecyclerViewHolder> {
    Context mContext;
    public ArrayList<Notification> notificationList = new ArrayList<>();
    public ArrayList<Notification> selected_notificationList = new ArrayList<>();

    public AdminMessageAdapter(Context mContext, ArrayList<Notification> notificationList) {
        this.mContext = mContext;
        this.notificationList = notificationList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_message, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Notification notification = notificationList.get(position);
        holder.tvTitle.setText(notification.getNotifyType());
        holder.tvContent.setText(notification.getMessage());
        if (selected_notificationList.size() > 0) {
            if (selected_notificationList.contains(notificationList.get(position)))
                holder.ivChecked.setVisibility(View.VISIBLE);
            else
                holder.ivChecked.setVisibility(View.GONE);
        } else {
            holder.ivChecked.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.tv_content)
        TextView tvContent;
        @BindView(R.id.iv_checked)
        ImageView ivChecked;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
