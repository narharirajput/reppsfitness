package com.reppsfitness.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.reppsfitness.R;
import com.reppsfitness.fragment.NewRequestFragment;
import com.reppsfitness.model.SessionRequest;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.session_cal.json_model.SessionRes;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class ShiftTimeAdapter extends RecyclerView.Adapter<ShiftTimeAdapter.RecyclerViewHolder> {
    Context mContext;
    public List<String> list = new ArrayList<>();
    private int selected_pos = -1;
    private SessionRes sessionRes;
    private NewRequestFragment newRequestFragment;

    public ShiftTimeAdapter(Context mContext, SessionRes sessionRes, NewRequestFragment newRequestFragment) {
        this.mContext = mContext;
        this.newRequestFragment = newRequestFragment;
        this.sessionRes = sessionRes;
        this.list = sessionRes.getTimeList();
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_shift_time, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
//        Notification notification = notificationList.get(position);
//        holder.tvTime.setText(notification.getNotifyType());
//        holder.tvContent.setText(notification.getMessage());
        holder.tvTime.setText(list.get(position));


        if (sessionRes.getSessionDateList().containsKey(list.get(position))) {
            DayDetails dayDetails = sessionRes.getSessionDateList().get(list.get(position)).get(0);
            Log.e(TAG, "onBindViewHolder: " + new Gson().toJson(dayDetails));
            long alloted = dayDetails.getAlloted();
            long clientLimit = Long.parseLong(dayDetails.getClientLimit());
            holder.tvCount.setText(alloted + "/" + clientLimit);
            if (alloted > 0) {
                Log.e(TAG, "onBindViewHolder: Z" + alloted);
                if (alloted == clientLimit) {
                    holder.rrRoot.setBackground(holder.rect_bg_colorprimary);
                    holder.tvTime.setTextColor(holder.white);
                    holder.tvCount.setTextColor(holder.white);
                } else {
                    Log.e(TAG, "onBindViewHolder:green " );
                        holder.rrRoot.setBackground(holder.rect_bg_green_fill);
                        holder.tvTime.setTextColor(holder.white);
                        holder.tvCount.setTextColor(holder.white);
                }
            } else {
                Log.e(TAG, "onBindViewHolder: ZZ" + alloted);
                holder.tvTime.setTextColor(holder.black);
                holder.tvCount.setTextColor(holder.black);
                holder.rrBorder.setBackground(holder.rect_bg_white);
            }
        } else {
             {
                Log.e(TAG, "onBindViewHolder: ZZZ");
                holder.tvTime.setTextColor(holder.black);
                holder.tvCount.setTextColor(holder.black);
                holder.rrRoot.setBackground(holder.rect_bg_white);                                   //code by ajit
                holder.tvCount.setText("");
            }
        }
        if (selected_pos != -1 && selected_pos == position) {
            holder.rrBorder.setBackground(holder.rect_bg_light_grey);
        } else{
            holder.rrBorder.setBackground(holder.rect_bg_white);
        }
        holder.rrRoot.setOnClickListener(view -> {
            selected_pos = holder.getAdapterPosition();
            if (sessionRes.getSessionDateList().containsKey(list.get(holder.getAdapterPosition()))) {
                DayDetails dayDetails = sessionRes.getSessionDateList().get(list.get(holder.getAdapterPosition())).get(0);
                Log.e("dayDetails", new Gson().toJson(dayDetails));
                newRequestFragment.dayDetails = dayDetails;
                newRequestFragment.shiftToSessionID = dayDetails.getSessionID();
            } else {
                newRequestFragment.dayDetails=null;
                newRequestFragment.shiftToSessionID = "";
            }
            newRequestFragment.shiftToSessionTime = list.get(holder.getAdapterPosition());
            notifyItemChanged(holder.getAdapterPosition());
            notifyDataSetChanged();

        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_count)
        TextView tvCount;
        @BindView(R.id.rr_root)
        LinearLayout rrRoot;
        @BindView(R.id.rr_border)
        RelativeLayout rrBorder;
        @BindDrawable(R.drawable.rect_bg_light_grey)
        Drawable rect_bg_light_grey;
        @BindDrawable(R.drawable.rect_bg_colorprimary)
        Drawable rect_bg_colorprimary;
        @BindDrawable(R.drawable.rect_bg_green_fill)
        Drawable rect_bg_green_fill;
        @BindDrawable(R.drawable.rect_bg_green)
        Drawable rect_bg_green;
        @BindDrawable(R.drawable.rect_bg_white)
        Drawable rect_bg_white;
        @BindDrawable(R.drawable.rect_bg_blue)
        Drawable rect_bg_blue;
        @BindColor(R.color.white)
        int white;
        @BindColor(R.color.black)
        int black;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
