package com.reppsfitness.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.reppsfitness.App;
import com.reppsfitness.R;
import com.reppsfitness.SessionDetailsActivity;
import com.reppsfitness.model.AllotData;
import com.reppsfitness.session_cal.json_model.DayDetails;
import com.reppsfitness.utils.ConnectionDetector;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class SessionMemberAdapter extends RecyclerView.Adapter<SessionMemberAdapter.RecyclerViewHolder> {
    Context context;
    private List<AllotData> allottdata;
    private DayDetails details;
     private App app;
    public ConnectionDetector cs;

    String clientid="",sessionid="",usertype="";

    public SessionMemberAdapter(Context context, List<AllotData> allottdata, DayDetails dayDetails, App app) {
        this.context = context;
        this.allottdata = allottdata;
        this.details = dayDetails;
        this.app=app;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_session, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        AllotData allotData = allottdata.get(position);
        if (allotData.getClientFName() != null)
            holder.tvTitle.setText(allotData.getClientFName());
        if (allotData.getClientProfilePic() != null) {
            Glide.with(context).load(allotData.getClientProfilePic()).into(holder.ivUser);
        }
        if (allotData.getIsSessionShifted() != null && allotData.getIsSessionShifted().equals("2")) {
            holder.ll_root.setBackgroundColor(Color.parseColor("#FF6600"));
        } else{
            holder.ll_root.setBackgroundColor(0);
            //                                                  Admin
        } if (allotData.getIsSessionShifted()!=null && allotData.getIsSessionShifted().equals("0")&& app.getPreferences().getLoggedInUser().getData().getUserType().equals("1")){

            holder.ll_root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "onClick:1 "+details );
                    Log.e(TAG, "onClick:2 "+ app.getPreferences().getLoggedInUser().getData().getUserType());
                    Log.e(TAG, "onClick: 3"+allotData );
                    new MaterialDialog.Builder(context)
                            .content("Cancel this session for"+" "+allotData.getClientFName())
                            .positiveText("Yes")
                            .negativeText("No")
                            .onPositive((dialog, which) -> ((SessionDetailsActivity)context).Slotcancel(allotData,details,dialog,app.getPreferences().getLoggedInUser().getData().getUserType()))
                            .onNegative((dialog, which) -> dialog.dismiss())
                            .show();
                }
            });
                //                                       Client
        }/*else if(allotData.getIsSessionShifted()!=null && allotData.getIsSessionShifted().equals("0")&& app.getPreferences().getLoggedInUser().getData().getUserType().equals("2")){
            holder.ll_root.setOnClickListener(view -> new MaterialDialog.Builder(context)
                    .content("Cancel this session for"+" "+allotData.getClientFName())
                    .positiveText("Cancel")
                    .onPositive(((dialog, which) -> ((ClientSessionDetailsActivity)context).Slotclientcancel(allotData,details,dialog,app.getPreferences().getLoggedInUser().getData().getUserType())))
                    //.onPositive((dialog, which) -> ((SessionDetailsActivity)context).Slotcancel(allotData,details,dialog,app.getPreferences().getLoggedInUser().getData().getUserType()))
                   // .onNegative((dialog, which) -> dialog.dismiss())
                    .show());*/



    }

    @Override
    public int getItemCount() {
        return allottdata.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_user)
        ImageView ivUser;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.ll_root)
        LinearLayout ll_root;

        public RecyclerViewHolder(View itemView) {

                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(view -> {
                    Log.e(TAG, "RecyclerViewHolder: uu");


                        /*.onPositive((dialog, which) -> {
                            ((SessionDetailsActivity) context).adminAllotSlotClient(, dialog, "1");
                        })
                        .onNegative((dialog, which) -> {
                            ((SessionDetailsActivity) context).adminAllotSlotClient(requestData, dialog, "2");
                        })*/


                });


            }
        }
    }


