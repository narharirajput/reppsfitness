package com.reppsfitness.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.reppsfitness.R;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.fragment.ClientListFragment;
import com.reppsfitness.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientListAdapter extends RecyclerView.Adapter<ClientListAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<User> userList = new ArrayList<>();
    private List<User> tempUserList;
    ClientListFragment clientListFragment;

    public ClientListAdapter(Context mContext, List<User> userList, ClientListFragment clientListFragment) {
        this.mContext = mContext;
        this.userList.addAll(userList);
        this.tempUserList = userList;
        this.clientListFragment = clientListFragment;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_client, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        User user = tempUserList.get(position);
        String name = "";
        if (user.getClientFName() != null) {
            name = user.getClientFName() + " ";
        }
        if (user.getClientLName() != null) {
            name = name + user.getClientLName();
        }
        holder.tvClientName.setText(name);
        holder.tvClientMobile.setText(user.getClientMobile());
        holder.itemView.setOnClickListener(view ->
                mContext.startActivity(new Intent(mContext, ClientHomeActivity.class)
                        .putExtra("isFromAdmin", true)
                        .putExtra("user", user)));
        Glide.with(mContext).load(user.getClientProfilePic())
                .error(holder.no_image).into(holder.ivClientPic);
    }

    @Override
    public int getItemCount() {
        return tempUserList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_client_pic)
        ImageView ivClientPic;
        @BindView(R.id.tv_clientName)
        TextView tvClientName;
        @BindView(R.id.tv_clientMobile)
        TextView tvClientMobile;
        @BindDrawable(R.drawable.no_image)
        Drawable no_image;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        tempUserList.clear();
        clientListFragment.tv_error.setVisibility (View.GONE);
        if (charText.length() == 0) {
            tempUserList.addAll(userList);
        } else {
            for (int i = 0; i < userList.size(); i++) {
                if (userList.get(i).getClientFName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    tempUserList.add(userList.get(i));
                }
            }
            if (tempUserList.isEmpty ()){
                clientListFragment.tv_error.setVisibility (View.VISIBLE);
                clientListFragment.tv_error.setText ("No Clients found.");
                //Toast.makeText (mContext, "No records found", Toast.LENGTH_SHORT).show ();
            }
//            for (Client client : userList) {
//                if (client.getClientFName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    tempUserList.add(client);
//                }
//            }
        }
        notifyDataSetChanged();
    }
}