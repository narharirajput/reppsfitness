package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.reppsfitness.R;
import com.reppsfitness.ViewClientWorkoutActivity;
import com.reppsfitness.model.Workout;
import com.reppsfitness.model.WorkoutDetail;
import com.reppsfitness.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewClientWorkoutAdapter extends RecyclerView.Adapter<ViewClientWorkoutAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<Workout> workoutList;
    LinearLayout ll_element;

    public ViewClientWorkoutAdapter(Context mContext, List<Workout> workoutList) {
        this.mContext = mContext;
        this.workoutList = workoutList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_view_exercise, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Workout workout = workoutList.get(position);
        holder.tvExerciseTitle.setText(workout.getExerciseName());
        if (workout.getWorkoutDetails() != null && workout.getWorkoutDetails().size() > 0) {
            List<WorkoutDetail> workoutDetails = workout.getWorkoutDetails();
            for (int i = 0; i < workoutDetails.size(); i++) {
                WorkoutDetail workoutDetail = workoutDetails.get(i);
                ll_element = (LinearLayout) ((ViewClientWorkoutActivity) mContext).getLayoutInflater().inflate(R.layout.row_view_exercise_element, null);
                ll_element.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                        Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                ll_element.setLayoutParams(layoutParams);
                TextView tv_element = ll_element.findViewById(R.id.tv_element);
                TextView tv_elementVal = ll_element.findViewById(R.id.tv_elementVal);
                tv_element.setText(workoutDetail.getWorkoutElementName());
                tv_elementVal.setText(workoutDetail.getWorkoutElementValue());
                holder.fl_elements.addView(ll_element, holder.fl_elements.getChildCount());
            }
        }
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_exerciseTitle)
        TextView tvExerciseTitle;
        @BindView(R.id.fl_elements)
        FlexboxLayout fl_elements;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}