package com.reppsfitness.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.exercise.AddExerciseActivity;
import com.reppsfitness.model.Day;
import com.reppsfitness.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientScheduleDayListAdapter extends RecyclerView.Adapter<ClientScheduleDayListAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<Day> dayList;
    private String currDate, tomorrowDate;

    public ClientScheduleDayListAdapter(Context mContext, List<Day> dayList) {
        this.mContext = mContext;
        this.dayList = dayList;
        SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        Calendar c = Calendar.getInstance();
        currDate = simpleDateFormatDate.format(c.getTime());
        c.add(Calendar.DAY_OF_YEAR, 1);
        tomorrowDate = simpleDateFormatDate.format(c.getTime());
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.client_schedule_day_row, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bindData(dayList.get(position), position, holder);
//        Day day = dayList.get(position);
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.ll_day)
        LinearLayout llDay;
        @BindView(R.id.ll_triangle)
        LinearLayout llTriangle;
        @BindView(R.id.tv_exercises)
        TextView tvExercises;
        @BindView(R.id.ll_bg)
        LinearLayout ll_bg;
        @BindDrawable(R.drawable.rect_bg_green)
        Drawable rect_bg_green;
        @BindColor(R.color.toolbar_color)
        int toolbar_color;
        @BindColor(R.color.day_grey)
        int day_grey;
        @BindDrawable(R.drawable.triangular_red)
        Drawable triangular_red;
        @BindDrawable(R.drawable.triangular_grey)
        Drawable triangular_grey;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        void bindData(Day day, int position, RecyclerViewHolder holder) {
            String date = Utils.ymdTodmy(day.getDate());
            holder.tvDate.setText(date);
            holder.tvDate.setTag(position);
            if (date.matches(currDate)) {
                holder.ll_bg.setBackground(holder.rect_bg_green);
                holder.tvDay.setText("Today");
            } else if (date.matches(tomorrowDate)) {
                holder.tvDay.setText("Tomorrow");
                holder.ll_bg.setBackground(null);
            } else {
                holder.ll_bg.setBackground(null);
                holder.tvDay.setText("Day " + String.valueOf(position + 1));
            }
            if (day.getIsWeekDay() == 1) {
                if (day.getIsSessionAllotedDay() == 1) {
                    if (day.getExercise() == null || TextUtils.isEmpty(day.getExercise())) {
                        holder.tvExercises.setText("Select Exercise");
                        holder.tvExercises.setTextColor(Color.GRAY);
                        holder.llTriangle.setBackground(holder.triangular_grey);
                        holder.llDay.setBackground(new ColorDrawable(holder.day_grey));
                    } else {
                        holder.tvExercises.setText(day.getExercise());
                        holder.tvExercises.setTextColor(Color.BLACK);
                        holder.llTriangle.setBackground(holder.triangular_red);
                        holder.llDay.setBackground(new ColorDrawable(holder.toolbar_color));
                    }
                    holder.ll_bg.setOnClickListener(view -> {
                        int clickPos = position;
                        if (holder.tvDate.getTag() != null)
                            clickPos = Integer.parseInt(holder.tvDate.getTag().toString());
                        Day day1 = dayList.get(clickPos);
//                        Log.e("data day", holder.tvDate.getTag().toString() + "||" + new Gson().toJson(day1));
                        if (day1.getIsWeekDay() == 1 && day1.getIsSessionAllotedDay() == 1) {
                            mContext.startActivity(new Intent(mContext, AddExerciseActivity.class)
                                    .putExtra("day", day1)
                                    .putExtra("dayText", holder.tvDay.getText().toString())
                                    .putExtra("user", ((ClientHomeActivity) mContext).user)
                                    .putExtra("dayText", holder.tvDay.getText().toString())
                                    .putExtra("isFromAdmin", ((ClientHomeActivity) mContext).isFromAdmin)
                            );
                        }
                    });
                } else {
                    holder.tvExercises.setText("Not a Session Day");
                    holder.tvExercises.setTextColor(Color.GRAY);
                    holder.llTriangle.setBackground(holder.triangular_grey);
                    holder.llDay.setBackground(new ColorDrawable(holder.day_grey));
                }
            } else {
                holder.tvExercises.setText("Off Day");
                holder.tvExercises.setTextColor(Color.GRAY);
                holder.llTriangle.setBackground(holder.triangular_grey);
                holder.llDay.setBackground(new ColorDrawable(holder.day_grey));
            }
        }
    }
}