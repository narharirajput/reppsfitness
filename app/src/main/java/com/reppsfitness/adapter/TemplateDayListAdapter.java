package com.reppsfitness.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.TemplateDayListActivity;
import com.reppsfitness.exercise.AddExerciseActivity;
import com.reppsfitness.model.Day;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.swipelayout.SwipeLayout;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TemplateDayListAdapter extends RecyclerView.Adapter<TemplateDayListAdapter.RecyclerViewHolder> {
    private Context mContext;
    private List<Day> dayList;
    private boolean isSelectTemplate;

    public TemplateDayListAdapter(Context mContext, List<Day> dayList, boolean isSelectTemplate) {
        this.mContext = mContext;
        this.dayList = dayList;
        this.isSelectTemplate = isSelectTemplate;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workout_schedule_day_row, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        Day day = dayList.get(position);
        holder.swipeLayout.setSwipeEnabled(isSelectTemplate);
        holder.tvDay.setText("Day " + day.getDay());
        holder.tvDate.setVisibility(View.GONE);
        if (day.getExercise() == null || TextUtils.isEmpty(day.getExercise())) {
            holder.tvExercises.setText("Select Exercise");
            holder.tvExercises.setTextColor(Color.GRAY);
            holder.llTriangle.setBackground(holder.triangular_grey);
            holder.llDay.setBackground(new ColorDrawable(holder.day_grey));
        } else {
            holder.tvExercises.setText(day.getExercise());
            holder.tvExercises.setTextColor(Color.BLACK);
            holder.llTriangle.setBackground(holder.triangular_red);
            holder.llDay.setBackground(new ColorDrawable(holder.toolbar_color));
        }
        holder.llDayView.setOnClickListener(view -> {
//            Intent intent = new Intent(mContext, AddTemplateExerciseActivity.class);
//            intent.putExtra("day", day);
//            intent.putExtra("templateID", ((TemplateDayListActivity) mContext).template.getTemplateID());
//            ((TemplateDayListActivity) mContext).startActivityForResult(intent, Utils.ADD_EXERCISE);
            Intent intent = new Intent(mContext, AddExerciseActivity.class);
            intent.putExtra("day", day);
            intent.putExtra("dayText", holder.tvDay.getText().toString());
            intent.putExtra("isTemplate", true);
            intent.putExtra("templateID", ((TemplateDayListActivity) mContext).template.getTemplateID());
            ((TemplateDayListActivity) mContext).startActivityForResult(intent, Utils.ADD_EXERCISE);
        });
        holder.tvDelete.setOnClickListener(view -> {
            holder.swipeLayout.close();
            dayList.remove(position);
            notifyDataSetChanged();
            if (dayList.size() == 0 && mContext instanceof TemplateDayListActivity) {
                ((TemplateDayListActivity) mContext).item.setVisible(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dayList.size();
    }

    public List<Day> getDayList() {
        return dayList;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_day)
        TextView tvDay;
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_exercises)
        TextView tvExercises;
        @BindView(R.id.ll_day)
        LinearLayout llDay;
        @BindView(R.id.ll_triangle)
        LinearLayout llTriangle;
        @BindView(R.id.swipeLayout)
        SwipeLayout swipeLayout;
        @BindView(R.id.tv_delete)
        TextView tvDelete;
        @BindView(R.id.ll_dayView)
        LinearLayout llDayView;
        @BindColor(R.color.toolbar_color)
        int toolbar_color;
        @BindColor(R.color.day_grey)
        int day_grey;
        @BindDrawable(R.drawable.triangular_red)
        Drawable triangular_red;
        @BindDrawable(R.drawable.triangular_grey)
        Drawable triangular_grey;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}