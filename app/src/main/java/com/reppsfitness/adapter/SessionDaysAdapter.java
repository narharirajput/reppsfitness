package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reppsfitness.R;


import butterknife.BindView;
import butterknife.ButterKnife;

public class SessionDaysAdapter extends RecyclerView.Adapter<SessionDaysAdapter.RecyclerViewHolder> {
    private Context mContext;
    private String[] dayList ;

    public SessionDaysAdapter(Context mContext, String[] dayList) {
        this.mContext = mContext;
        this.dayList=dayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_session_day, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        String day = dayList[position];
        if (day== null) {
            day =" ";
        }

        holder.sessionDayTime.setText(day.trim ());
    }

    @Override
    public int getItemCount() {
        return dayList.length;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_sessionDay)
        TextView sessionDayTime;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}