package com.reppsfitness.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.reppsfitness.AdminMainActivity;
import com.reppsfitness.R;
import com.reppsfitness.SelectTemplateActivity;
import com.reppsfitness.TemplateDayListActivity;
import com.reppsfitness.model.Template;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 8/4/2017.
 */

public class TemplateAdapter extends RecyclerView.Adapter<TemplateAdapter.ViewHolder> {
    private Context mContext;
    private List<Template> templateList = new ArrayList<>();
    private List<Template> tempTemplateList;

    public TemplateAdapter(Context mContext, List<Template> templateList) {
        this.mContext = mContext;
        this.templateList.addAll(templateList);
        this.tempTemplateList = templateList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_template, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Template template = tempTemplateList.get(position);
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(mContext, TemplateDayListActivity.class);
            intent.putExtra("template", template);
            if (mContext instanceof SelectTemplateActivity) {
                intent.putExtra("user", ((SelectTemplateActivity) mContext).user);
            }
            mContext.startActivity(intent);
        });
        holder.tvWorkoutName.setText(template.getTemplateName());
        holder.tvSrNo.setText(String.format("%02d", position + 1));
//        holder.tvDelete.setOnClickListener(view -> Utils.showLongToast(mContext, template.getTemplateID()));
    }

    @Override
    public int getItemCount() {
        return tempTemplateList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_workout_name)
        TextView tvWorkoutName;
        @BindView(R.id.tv_srNo)
        TextView tvSrNo;
        @BindView(R.id.iv_next)
        ImageView ivNext;
//        @BindView(R.id.swipeLayout)
//        SwipeLayout swipeLayout;
//        @BindView(R.id.tv_delete)
//        TextView tvDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        tempTemplateList.clear();

        if (mContext instanceof AdminMainActivity){
            ((AdminMainActivity)mContext).templateFragment.tv_error.setVisibility (View.GONE);
        }else if (mContext instanceof SelectTemplateActivity) {
            ((SelectTemplateActivity) mContext).tv_error.setVisibility (View.GONE);
        }

        if (charText.length() == 0) {
            tempTemplateList.addAll(templateList);
        } else {
            for (int i = 0; i < templateList.size(); i++) {
                if (templateList.get(i).getTemplateName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    tempTemplateList.add(templateList.get(i));
                }
            }
            if (tempTemplateList.isEmpty ()){
                if (mContext instanceof AdminMainActivity){
                    ((AdminMainActivity)mContext).templateFragment.tv_error.setVisibility (View.VISIBLE);
                    ((AdminMainActivity)mContext).templateFragment.tv_error.setText ("No Templates found.");
                }else if (mContext instanceof SelectTemplateActivity) {
                    ((SelectTemplateActivity) mContext).tv_error.setVisibility (View.VISIBLE);
                    ((SelectTemplateActivity) mContext).tv_error.setText ("No Templates found.");
                }
                //Toast.makeText (mContext, "No records found", Toast.LENGTH_SHORT).show ();
            }
//            for (Client client : userList) {
//                if (client.getClientFName().toLowerCase(Locale.getDefault()).contains(charText)) {
//                    tempTemplateList.add(client);
//                }
//            }
        }
        notifyDataSetChanged();
    }
}
