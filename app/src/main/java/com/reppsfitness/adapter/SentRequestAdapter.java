package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reppsfitness.R;
import com.reppsfitness.model.SentRequest;
import com.reppsfitness.utils.Utils;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 11/1/2017.
 */

public class SentRequestAdapter extends RecyclerView.Adapter<SentRequestAdapter.RecyclerViewHolder> {
    Context mContext;
    List<SentRequest> sentRequestList;

    public SentRequestAdapter(Context mContext, List<SentRequest> sentRequestList) {
        this.mContext = mContext;
        this.sentRequestList = sentRequestList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_sent_requests, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        SentRequest sentRequest = sentRequestList.get(position);
        holder.tvDate.setText("Date: " + Utils.ymdTodmy(sentRequest.getRequestDate()));
        holder.tvTime.setText("Time: " + sentRequest.getRequestFromTime() + " - " + sentRequest.getRequestToTime());
        if (sentRequest.getRequestStatus() != null && !TextUtils.isEmpty(sentRequest.getRequestDate())) {
            switch (sentRequest.getRequestStatus()) {
                case "0":
                    holder.tvStatus.setText("PENDING");
                    holder.tvStatus.setTextColor(holder.yellow);
                    break;
                case "1":
                    holder.tvStatus.setText("APPROVED");
                    holder.tvStatus.setTextColor(holder.app_green_color);
                    break;
                case "2":
                    holder.tvStatus.setText("CANCELLED");
                    holder.tvStatus.setTextColor(holder.red);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return sentRequestList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_date)
        TextView tvDate;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindColor(R.color.event_color_04)
        int yellow;
        @BindColor(R.color.red)
        int red;
        @BindColor(R.color.app_green_color)
        int app_green_color;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
