package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.reppsfitness.R;
import com.reppsfitness.SessionDetailsActivity;
import com.reppsfitness.model.RequestData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 8/10/2017.
 */

public class QueueMemberAdapter extends RecyclerView.Adapter<QueueMemberAdapter.RecyclerViewHolder> {
    Context context;
    List<RequestData> requestdata;

    public QueueMemberAdapter(Context context, List<RequestData> requestdata) {
        this.context = context;
        this.requestdata = requestdata;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_session, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        RequestData requestData = requestdata.get(position);
        if (requestData.getClientFName() != null)
            holder.tvTitle.setText(requestData.getClientFName());
        if (requestData.getClientProfilePic() != null) {
            Glide.with(context).load(requestData.getClientProfilePic()).into(holder.ivUser);
        }
        holder.itemView.setOnClickListener(view -> {
            String content = "Allot this session ";
            if (requestData.getClientFName() != null) {
                content = content + "to " + requestData.getClientFName();
            }
            new MaterialDialog.Builder(context)
                    .content(content)
                    .positiveText("ALLOT")
                    .negativeText("DON'T ALLOT")
                    .neutralText("CANCEL")
                    .onPositive((dialog, which) -> {
                        ((SessionDetailsActivity) context).adminAllotSlotClient(requestData, dialog, "1");
                    })
                    .onNegative((dialog, which) -> {
                        ((SessionDetailsActivity) context).adminAllotSlotClient(requestData, dialog, "2");
                    })
                    .onNeutral((dialog, which) -> dialog.dismiss())
                    .show();
        });
    }

    @Override
    public int getItemCount() {
        return requestdata.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_user)
        ImageView ivUser;
        @BindView(R.id.tv_title)
        TextView tvTitle;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
