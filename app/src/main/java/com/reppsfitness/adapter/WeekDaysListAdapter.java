package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.CheckboxClickListener;
import com.reppsfitness.R;
import com.reppsfitness.model.WeekDay;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WeekDaysListAdapter extends RecyclerView.Adapter<WeekDaysListAdapter.WeekDayViewHolder> {
    private Context mContext;
    private List<WeekDay> weekDayList = new ArrayList<>();
    private CheckboxClickListener listener;
    private List<String> slotTimings = new ArrayList<>();

    public WeekDaysListAdapter(Context mContext, List<WeekDay> weekDayList, List<String> slotTimings, CheckboxClickListener listener) {
        this.mContext = mContext;
        this.weekDayList = weekDayList;
        this.slotTimings = slotTimings;
        this.listener = listener;
    }

    @Override
    public WeekDayViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_checkbox, parent, false);
        return new WeekDayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(WeekDayViewHolder holder, int position) {
        WeekDay weekDay = weekDayList.get(position);
        holder.checkbox.setText(weekDay.getDay());
        holder.checkbox.setChecked(weekDay.isChecked());
        holder.checkbox.setEnabled(weekDay.isEnabled());
        holder.checkbox.setTextColor(weekDay.isChecked() ? holder.colorPrimary : holder.font_black_0);
        holder.checkbox.setOnCheckedChangeListener((compoundButton, b) -> {
            listener.onCheckboxClick(holder.checkbox, position);
            holder.etTime.setText("");
        });
//                holder.checkbox.setTextColor(b ? holder.colorPrimary : holder.font_black_0));
        holder.etTime.setText(weekDay.getSlotTimeVal().toUpperCase());
        holder.etTime.setOnClickListener(view -> {
            if (holder.checkbox.isChecked() && holder.checkbox.isEnabled() && slotTimings != null && slotTimings.size() > 0) {
                new MaterialDialog.Builder(mContext)
                        .items(slotTimings)
                        .itemsCallback((dialog, view1, which, text) -> {
                            String selectedTime = text.toString();
                            holder.etTime.setText(selectedTime);
                        })
                        .show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return weekDayList.size();
    }

    public class WeekDayViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.checkbox)
        public CheckBox checkbox;
        @BindView(R.id.et_time)
        public EditText etTime;
        @BindView(R.id.rr_root)
        LinearLayout rr_root;
        @BindColor(R.color.colorPrimary)
        int colorPrimary;
        @BindColor(R.color.font_black_0)
        int font_black_0;

        WeekDayViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}