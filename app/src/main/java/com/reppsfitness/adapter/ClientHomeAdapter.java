package com.reppsfitness.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reppsfitness.R;
import com.reppsfitness.widget.CustomTextViewRegular;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class ClientHomeAdapter extends RecyclerView.Adapter<ClientHomeAdapter.RecyclerViewHolder> {
    Context context;
    ArrayList<String> list;

    public ClientHomeAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_client_home, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvWorkout.setText(list.get(position));
        if (position == 0) {
            holder.tvTrackProgress.setText("Due in 4 days");
        } else {
            holder.tvTrackProgress.setText("");
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_workout)
        CustomTextViewRegular tvWorkout;
        @BindView(R.id.tv_track_progress)
        CustomTextViewRegular tvTrackProgress;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
