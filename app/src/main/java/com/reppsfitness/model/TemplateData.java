package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 17/10/17.
 */

public class TemplateData implements Parcelable {
    @SerializedName("data")
    @Expose
    public List<Template> templateList = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<Template> getTemplateList() {
        return templateList;
    }

    public void setTemplateList(List<Template> templateList) {
        this.templateList = templateList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.templateList);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    public TemplateData() {
    }

    protected TemplateData(Parcel in) {
        this.templateList = in.createTypedArrayList(Template.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Parcelable.Creator<TemplateData> CREATOR = new Parcelable.Creator<TemplateData>() {
        @Override
        public TemplateData createFromParcel(Parcel source) {
            return new TemplateData(source);
        }

        @Override
        public TemplateData[] newArray(int size) {
            return new TemplateData[size];
        }
    };
}
