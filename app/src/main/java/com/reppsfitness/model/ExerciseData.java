package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur Gangurde on 24/11/17.
 */

public class ExerciseData {
    @SerializedName("exerciseCategories")
    @Expose
    public List<ExerciseCategory> exerciseCategories = null;
    @SerializedName("workoutElements")
    @Expose
    public List<Element> elementList = null;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<ExerciseCategory> getExerciseCategories() {
        return exerciseCategories;
    }

    public void setExerciseCategories(List<ExerciseCategory> exerciseCategories) {
        this.exerciseCategories = exerciseCategories;
    }

    public List<Element> getElementList() {
        return elementList;
    }

    public void setElementList(List<Element> elementList) {
        this.elementList = elementList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
