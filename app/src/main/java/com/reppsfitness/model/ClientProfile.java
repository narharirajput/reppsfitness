package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 13/11/17.
 */

public class ClientProfile implements Parcelable {
    @SerializedName("clientID")
    @Expose
    public String clientID;
    @SerializedName("userID")
    @Expose
    public String userID;
    @SerializedName("packageID")
    @Expose
    public String packageID;
    @SerializedName("pkgDetailsID")
    @Expose
    public String pkgDetailsID;
    @SerializedName("reqPackageID")
    @Expose
    public String reqPackageID;
    @SerializedName("pkgStartDate")
    @Expose
    public String pkgStartDate;
    @SerializedName("pkgEndDate")
    @Expose
    public String pkgEndDate;
    @SerializedName("sessionAllottedTime")
    @Expose
    public String sessionAllottedTime;
    @SerializedName("clientPassword")
    @Expose
    public String clientPassword;
    @SerializedName("clientPasswordText")
    @Expose
    public String clientPasswordText;
    @SerializedName("clientFName")
    @Expose
    public String clientFName;
    @SerializedName("clientLName")
    @Expose
    public String clientLName;
    @SerializedName("clientMobile")
    @Expose
    public String clientMobile;
    @SerializedName("clientProfilePic")
    @Expose
    public String clientProfilePic;
    @SerializedName("clientEmail")
    @Expose
    public String clientEmail;
    @SerializedName("clientJoinDate")
    @Expose
    public String clientJoinDate;
    @SerializedName("clientGender")
    @Expose
    public String clientGender;
    @SerializedName("clientDOB")
    @Expose
    public String clientDOB;
    @SerializedName("dateAdded")
    @Expose
    public String dateAdded;
    @SerializedName("dateModified")
    @Expose
    public String dateModified;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("isSessionAllotted")
    @Expose
    public String isSessionAllotted;
    @SerializedName("sessionDays")
    @Expose
    public String sessionDays;
    @SerializedName("clientSessionDays")
    @Expose
    public String clientSessionDays;
    @SerializedName("isTermApply")
    @Expose
    public String isTermApply;
    @SerializedName("isRegular")
    @Expose
    public String isRegular;
    @SerializedName("totalSessionAllotted")
    @Expose
    public String totalSessionAllotted;
    @SerializedName("totalSessionRemainning")
    @Expose
    public String totalSessionRemainning;
    @SerializedName("totalSessionAttended")
    @Expose
    public String totalSessionAttended;
    @SerializedName("noOfSessionAllotted")
    @Expose
    public String noOfSessionAllotted;
    @SerializedName("packageType")
    @Expose
    public String packageType;
//    @SerializedName("clientSessionDays")
//    @Expose
//    public List<String> clientSessionDays;
//    @SerializedName("clientSessionTimes")
//    @Expose
//    public List<String> clientSessionTimes;

    public String getIsTermApply() {
        return isTermApply;
    }

    public void setIsTermApply(String isTermApply) {
        this.isTermApply = isTermApply;
    }

    public String getIsRegular() {
        return isRegular;
    }

    public void setIsRegular(String isRegular) {
        this.isRegular = isRegular;
    }

    public String getTotalSessionAllotted() {
        return totalSessionAllotted;
    }

    public void setTotalSessionAllotted(String totalSessionAllotted) {
        this.totalSessionAllotted = totalSessionAllotted;
    }

    public String getTotalSessionRemainning() {
        return totalSessionRemainning;
    }

    public void setTotalSessionRemainning(String totalSessionRemainning) {
        this.totalSessionRemainning = totalSessionRemainning;
    }

    public String getNoOfSessionAllotted() {
        return noOfSessionAllotted;
    }

    public void setNoOfSessionAllotted(String noOfSessionAllotted) {
        this.noOfSessionAllotted = noOfSessionAllotted;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getClientSessionDays() {
        return clientSessionDays;
    }

    public void setClientSessionDays(String clientSessionDays) {
        this.clientSessionDays = clientSessionDays;
    }

    public String getReqPackageID() {
        return reqPackageID;
    }

    public void setReqPackageID(String reqPackageID) {
        this.reqPackageID = reqPackageID;
    }

    public String getSessionDays() {
        return sessionDays;
    }

    public void setSessionDays(String sessionDays) {
        this.sessionDays = sessionDays;
    }

    public String getIsSessionAllotted() {
        return isSessionAllotted;
    }

    public void setIsSessionAllotted(String isSessionAllotted) {
        this.isSessionAllotted = isSessionAllotted;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public String getPkgDetailsID() {
        return pkgDetailsID;
    }

    public void setPkgDetailsID(String pkgDetailsID) {
        this.pkgDetailsID = pkgDetailsID;
    }

    public String getTotalSessionAttended() {
        return totalSessionAttended;
    }

    public void setTotalSessionAttended(String totalSessionAttended) {
        this.totalSessionAttended = totalSessionAttended;
    }

    public String getPkgStartDate() {
        return pkgStartDate;
    }

    public void setPkgStartDate(String pkgStartDate) {
        this.pkgStartDate = pkgStartDate;
    }

    public String getPkgEndDate() {
        return pkgEndDate;
    }

    public void setPkgEndDate(String pkgEndDate) {
        this.pkgEndDate = pkgEndDate;
    }

    public String getClientPassword() {
        return clientPassword;
    }

    public String getSessionAllottedTime() {
        return sessionAllottedTime;
    }

    public void setSessionAllottedTime(String sessionAllottedTime) {
        this.sessionAllottedTime = sessionAllottedTime;
    }

    public void setClientPassword(String clientPassword) {
        this.clientPassword = clientPassword;
    }

    public String getClientPasswordText() {
        return clientPasswordText;
    }

    public void setClientPasswordText(String clientPasswordText) {
        this.clientPasswordText = clientPasswordText;
    }

    public String getClientFName() {
        return clientFName;
    }

    public void setClientFName(String clientFName) {
        this.clientFName = clientFName;
    }

    public String getClientLName() {
        return clientLName;
    }

    public void setClientLName(String clientLName) {
        this.clientLName = clientLName;
    }

    public String getClientMobile() {
        return clientMobile;
    }

    public void setClientMobile(String clientMobile) {
        this.clientMobile = clientMobile;
    }

    public String getClientProfilePic() {
        return clientProfilePic;
    }

    public void setClientProfilePic(String clientProfilePic) {
        this.clientProfilePic = clientProfilePic;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientJoinDate() {
        return clientJoinDate;
    }

    public void setClientJoinDate(String clientJoinDate) {
        this.clientJoinDate = clientJoinDate;
    }

    public String getClientGender() {
        return clientGender;
    }

    public void setClientGender(String clientGender) {
        this.clientGender = clientGender;
    }

    public String getClientDOB() {
        return clientDOB;
    }

    public void setClientDOB(String clientDOB) {
        this.clientDOB = clientDOB;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ClientProfile() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.clientID);
        dest.writeString(this.userID);
        dest.writeString(this.packageID);
        dest.writeString(this.pkgDetailsID);
        dest.writeString(this.reqPackageID);
        dest.writeString(this.pkgStartDate);
        dest.writeString(this.pkgEndDate);
        dest.writeString(this.sessionAllottedTime);
        dest.writeString(this.clientPassword);
        dest.writeString(this.clientPasswordText);
        dest.writeString(this.clientFName);
        dest.writeString(this.clientLName);
        dest.writeString(this.clientMobile);
        dest.writeString(this.clientProfilePic);
        dest.writeString(this.clientEmail);
        dest.writeString(this.clientJoinDate);
        dest.writeString(this.clientGender);
        dest.writeString(this.clientDOB);
        dest.writeString(this.dateAdded);
        dest.writeString(this.dateModified);
        dest.writeString(this.status);
        dest.writeString(this.isSessionAllotted);
        dest.writeString(this.sessionDays);
        dest.writeString(this.clientSessionDays);
        dest.writeString(this.isTermApply);
        dest.writeString(this.isRegular);
        dest.writeString(this.totalSessionAllotted);
        dest.writeString(this.totalSessionAttended);
        dest.writeString(this.totalSessionRemainning);
        dest.writeString(this.noOfSessionAllotted);
        dest.writeString(this.packageType);
    }

    protected ClientProfile(Parcel in) {
        this.clientID = in.readString();
        this.userID = in.readString();
        this.packageID = in.readString();
        this.pkgDetailsID = in.readString();
        this.reqPackageID = in.readString();
        this.pkgStartDate = in.readString();
        this.pkgEndDate = in.readString();
        this.sessionAllottedTime = in.readString();
        this.clientPassword = in.readString();
        this.clientPasswordText = in.readString();
        this.clientFName = in.readString();
        this.clientLName = in.readString();
        this.clientMobile = in.readString();
        this.clientProfilePic = in.readString();
        this.clientEmail = in.readString();
        this.clientJoinDate = in.readString();
        this.clientGender = in.readString();
        this.clientDOB = in.readString();
        this.dateAdded = in.readString();
        this.dateModified = in.readString();
        this.status = in.readString();
        this.isSessionAllotted = in.readString();
        this.sessionDays = in.readString();
        this.clientSessionDays = in.readString();
        this.isTermApply = in.readString();
        this.isRegular = in.readString();
        this.totalSessionAllotted = in.readString();
        this.totalSessionRemainning = in.readString();
        this.totalSessionAttended = in.readString();
        this.noOfSessionAllotted = in.readString();
        this.packageType = in.readString();
    }

    public static final Creator<ClientProfile> CREATOR = new Creator<ClientProfile>() {
        @Override
        public ClientProfile createFromParcel(Parcel source) {
            return new ClientProfile(source);
        }

        @Override
        public ClientProfile[] newArray(int size) {
            return new ClientProfile[size];
        }
    };
}
