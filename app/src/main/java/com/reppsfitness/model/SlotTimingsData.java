package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 13/11/17.
 */

public class SlotTimingsData {
    @SerializedName("slotTime")
    @Expose
    public List<String> slotTime = new ArrayList<>();
    @SerializedName("weekDayData")
    @Expose
    public List<WeekDayDatum> weekDayData = new ArrayList<>();
    @SerializedName("minPerWeekSession")
    @Expose
    public String minPerWeekSession;
    @SerializedName("maxPerWeekSession")
    @Expose
    public String maxPerWeekSession;
    @SerializedName("clientSessionDays")
    @Expose
    public List<String> clientSessionDays = new ArrayList<>();

    public List<String> getClientSessionDays() {
        return clientSessionDays;
    }

    public void setClientSessionDays(List<String> clientSessionDays) {
        this.clientSessionDays = clientSessionDays;
    }

    public List<String> getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(List<String> slotTime) {
        this.slotTime = slotTime;
    }

    public List<WeekDayDatum> getWeekDayData() {
        return weekDayData;
    }

    public void setWeekDayData(List<WeekDayDatum> weekDayData) {
        this.weekDayData = weekDayData;
    }

    public String getMinPerWeekSession() {
        return minPerWeekSession;
    }

    public void setMinPerWeekSession(String minPerWeekSession) {
        this.minPerWeekSession = minPerWeekSession;
    }

    public String getMaxPerWeekSession() {
        return maxPerWeekSession;
    }

    public void setMaxPerWeekSession(String maxPerWeekSession) {
        this.maxPerWeekSession = maxPerWeekSession;
    }
}
