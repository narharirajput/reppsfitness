package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by MXCPUU11 on 11/3/2017.
 */

public class Bmi implements Serializable {
    @SerializedName("clientBMIID")
    @Expose
    private String clientBMIID;
    @SerializedName("BMIDate")
    @Expose
    private String bMIDate;
    @SerializedName("clientID")
    @Expose
    private String clientID;
    @SerializedName("userID")
    @Expose
    private String userID;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("bodyFlat")
    @Expose
    private String bodyFlat;
    @SerializedName("chest")
    @Expose
    private String chest;
    @SerializedName("abdominal")
    @Expose
    private String abdominal;
    @SerializedName("thigh")
    @Expose
    private String thigh;
    @SerializedName("tricep")
    @Expose
    private String tricep;
    @SerializedName("head")
    @Expose
    private String head;
    @SerializedName("neck")
    @Expose
    private String neck;
    @SerializedName("armRelaxed")
    @Expose
    private String armRelaxed;
    @SerializedName("armFlexed")
    @Expose
    private String armFlexed;
    @SerializedName("dateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("dateModified")
    @Expose
    private String dateModified;
    @SerializedName("status")
    @Expose
    private String status;
    private final static long serialVersionUID = 2145224879828314829L;

    public String getClientBMIID() {
        return clientBMIID;
    }

    public void setClientBMIID(String clientBMIID) {
        this.clientBMIID = clientBMIID;
    }

    public String getBMIDate() {
        return bMIDate;
    }

    public void setBMIDate(String bMIDate) {
        this.bMIDate = bMIDate;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBodyFlat() {
        return bodyFlat;
    }

    public void setBodyFlat(String bodyFlat) {
        this.bodyFlat = bodyFlat;
    }

    public String getChest() {
        return chest;
    }

    public void setChest(String chest) {
        this.chest = chest;
    }

    public String getAbdominal() {
        return abdominal;
    }

    public void setAbdominal(String abdominal) {
        this.abdominal = abdominal;
    }

    public String getThigh() {
        return thigh;
    }

    public void setThigh(String thigh) {
        this.thigh = thigh;
    }

    public String getTricep() {
        return tricep;
    }

    public void setTricep(String tricep) {
        this.tricep = tricep;
    }

    public String getHead() {
        return head;
    }

    public void setHead(String head) {
        this.head = head;
    }

    public String getNeck() {
        return neck;
    }

    public void setNeck(String neck) {
        this.neck = neck;
    }

    public String getArmRelaxed() {
        return armRelaxed;
    }

    public void setArmRelaxed(String armRelaxed) {
        this.armRelaxed = armRelaxed;
    }

    public String getArmFlexed() {
        return armFlexed;
    }

    public void setArmFlexed(String armFlexed) {
        this.armFlexed = armFlexed;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
