package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class TermItem implements Parcelable {
	@SerializedName("packageID")
	private String packageID;
	@SerializedName("isRegular")
	private String isRegular;

	@SerializedName("packageFeatureTitle")
	private String packageFeatureTitle;

	@SerializedName("packageImage")
	private String packageImage;

	@SerializedName("packageName")
	private String packageName;

	@SerializedName("packageDuration")
	private String packageDuration;

	@SerializedName("isTermApply")
	private String isTermApply;

	@SerializedName("packageFeatureDesc")
	private String packageFeatureDesc;

	@SerializedName("packageAmount")
	private String packageAmount;
	@SerializedName("isCurrentPackage")
	@Expose
	public long isCurrentPackage;
	@SerializedName("isRequestPackage")
	@Expose
	public long isRequestPackage;
	@SerializedName("limitDays")
	@Expose
	public String limitDays;

	public String getLimitDays() {
		return limitDays;
	}

	public void setLimitDays(String limitDays) {
		this.limitDays = limitDays;
	}

	public String getPackageID() {
		return packageID;
	}

	public void setPackageID(String packageID) {
		this.packageID = packageID;
	}

	public long getIsCurrentPackage() {
		return isCurrentPackage;
	}

	public void setIsCurrentPackage(long isCurrentPackage) {
		this.isCurrentPackage = isCurrentPackage;
	}

	public long getIsRequestPackage() {
		return isRequestPackage;
	}

	public void setIsRequestPackage(long isRequestPackage) {
		this.isRequestPackage = isRequestPackage;
	}

	public void setIsRegular(String isRegular){
		this.isRegular = isRegular;
	}

	public String getIsRegular(){
		return isRegular;
	}

	public void setPackageFeatureTitle(String packageFeatureTitle){
		this.packageFeatureTitle = packageFeatureTitle;
	}

	public String getPackageFeatureTitle(){
		return packageFeatureTitle;
	}

	public void setPackageName(String packageName){
		this.packageName = packageName;
	}

	public String getPackageName(){
		return packageName;
	}

	public void setPackageDuration(String packageDuration){
		this.packageDuration = packageDuration;
	}

	public String getPackageDuration(){
		return packageDuration;
	}

	public void setIsTermApply(String isTermApply){
		this.isTermApply = isTermApply;
	}

	public String getIsTermApply(){
		return isTermApply;
	}

	public void setPackageFeatureDesc(String packageFeatureDesc){
		this.packageFeatureDesc = packageFeatureDesc;
	}

	public String getPackageImage() {
		return packageImage;
	}

	public void setPackageImage(String packageImage) {
		this.packageImage = packageImage;
	}

	public String getPackageFeatureDesc(){
		return packageFeatureDesc;
	}

	public void setPackageAmount(String packageAmount){
		this.packageAmount = packageAmount;
	}

	public String getPackageAmount(){
		return packageAmount;
	}

	@Override
 	public String toString(){
		return 
			"TermItem{" + 
			"isRegular = '" + isRegular + '\'' + 
			",packageFeatureTitle = '" + packageFeatureTitle + '\'' + 
			",packageName = '" + packageName + '\'' + 
			",packageDuration = '" + packageDuration + '\'' + 
			",packageImage = '" + packageImage + '\'' +
			",isTermApply = '" + isTermApply + '\'' +
			",packageFeatureDesc = '" + packageFeatureDesc + '\'' + 
			",packageAmount = '" + packageAmount + '\'' + 
			"}";
		}

	public TermItem() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.packageID);
		dest.writeString(this.isRegular);
		dest.writeString(this.packageFeatureTitle);
		dest.writeString(this.packageName);
		dest.writeString(this.packageImage);
		dest.writeString(this.packageDuration);
		dest.writeString(this.isTermApply);
		dest.writeString(this.packageFeatureDesc);
		dest.writeString(this.packageAmount);
		dest.writeLong(this.isCurrentPackage);
		dest.writeLong(this.isRequestPackage);
		dest.writeString(this.limitDays);
	}

	protected TermItem(Parcel in) {
		this.packageID = in.readString();
		this.isRegular = in.readString();
		this.packageFeatureTitle = in.readString();
		this.packageName = in.readString();
		this.packageDuration = in.readString();
		this.isTermApply = in.readString();
		this.packageImage = in.readString();
		this.packageFeatureDesc = in.readString();
		this.packageAmount = in.readString();
		this.isCurrentPackage = in.readLong();
		this.isRequestPackage = in.readLong();
		this.limitDays = in.readString();
	}

	public static final Creator<TermItem> CREATOR = new Creator<TermItem>() {
		@Override
		public TermItem createFromParcel(Parcel source) {
			return new TermItem(source);
		}

		@Override
		public TermItem[] newArray(int size) {
			return new TermItem[size];
		}
	};
}