package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 06/11/17.
 */

public class PaymentModeData implements Parcelable {
    @SerializedName("data")
    @Expose
    public List<PaymentMode> data = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<PaymentMode> getData() {
        return data;
    }

    public void setData(List<PaymentMode> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    public PaymentModeData() {
    }

    protected PaymentModeData(Parcel in) {
        this.data = in.createTypedArrayList(PaymentMode.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Parcelable.Creator<PaymentModeData> CREATOR = new Parcelable.Creator<PaymentModeData>() {
        @Override
        public PaymentModeData createFromParcel(Parcel source) {
            return new PaymentModeData(source);
        }

        @Override
        public PaymentModeData[] newArray(int size) {
            return new PaymentModeData[size];
        }
    };
}
