package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeekDayDatum implements Parcelable {
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("isWeekDayOff")
    @Expose
    public long isWeekDayOff;
    @SerializedName("isAllottedDay")
    @Expose
    public long isAllottedDay;
    @SerializedName("slotTimeVal")
    @Expose
    public String slotTimeVal;

    public String getSlotTimeVal() {
        return slotTimeVal;
    }

    public void setSlotTimeVal(String slotTimeVal) {
        this.slotTimeVal = slotTimeVal;
    }

    public long getIsWeekDayOff() {
        return isWeekDayOff;
    }

    public void setIsWeekDayOff(long isWeekDayOff) {
        this.isWeekDayOff = isWeekDayOff;
    }

    public long getIsAllottedDay() {
        return isAllottedDay;
    }

    public void setIsAllottedDay(long isAllottedDay) {
        this.isAllottedDay = isAllottedDay;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public WeekDayDatum() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.day);
        dest.writeLong(this.isWeekDayOff);
        dest.writeLong(this.isAllottedDay);
        dest.writeString(this.slotTimeVal);
    }

    protected WeekDayDatum(Parcel in) {
        this.day = in.readString();
        this.isWeekDayOff = in.readLong();
        this.isAllottedDay = in.readLong();
        this.slotTimeVal = in.readString();
    }

    public static final Creator<WeekDayDatum> CREATOR = new Creator<WeekDayDatum>() {
        @Override
        public WeekDayDatum createFromParcel(Parcel source) {
            return new WeekDayDatum(source);
        }

        @Override
        public WeekDayDatum[] newArray(int size) {
            return new WeekDayDatum[size];
        }
    };
}