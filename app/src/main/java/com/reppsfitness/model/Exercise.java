package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 28/09/17.
 */

public class Exercise implements Parcelable {
    @SerializedName("templateDetailsID")
    @Expose
    public String templateDetailsID;
    @SerializedName("scheduleDetailsID")
    @Expose
    public String scheduleDetailsID;
    @SerializedName("workoutTypeID")
    @Expose
    public String workoutTypeID;
    @SerializedName("workoutName")
    @Expose
    public String workoutName;
    @SerializedName("exerciseCatID")
    @Expose
    public String exerciseCatID;
    @SerializedName("exerciseCatName")
    @Expose
    public String exerciseCatName;
    @SerializedName("exerciseID")
    @Expose
    public String exerciseID;
    @SerializedName("exerciseName")
    @Expose
    public String exerciseName;
    @SerializedName("workoutElements")
    @Expose
    public List<WorkoutElement> workoutElements = new ArrayList<>();
    ArrayList<Exercise> exerciseList = new ArrayList<>();
    ArrayList<String> exerciseNameList = new ArrayList<>();

    public String getTemplateDetailsID() {
        return templateDetailsID;
    }

    public void setTemplateDetailsID(String templateDetailsID) {
        this.templateDetailsID = templateDetailsID;
    }

    public String getScheduleDetailsID() {
        return scheduleDetailsID;
    }

    public void setScheduleDetailsID(String scheduleDetailsID) {
        this.scheduleDetailsID = scheduleDetailsID;
    }

    public ArrayList<Exercise> getExerciseList() {
        return exerciseList;
    }

    public void setExerciseList(ArrayList<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }

    public ArrayList<String> getExerciseNameList() {
        return exerciseNameList;
    }

    public void setExerciseNameList(ArrayList<String> exerciseNameList) {
        this.exerciseNameList = exerciseNameList;
    }

    public List<WorkoutElement> getWorkoutElements() {
        return workoutElements;
    }

    public void setWorkoutElements(List<WorkoutElement> workoutElements) {
        this.workoutElements = workoutElements;
    }

    public String getWorkoutTypeID() {
        return workoutTypeID;
    }

    public void setWorkoutTypeID(String workoutTypeID) {
        this.workoutTypeID = workoutTypeID;
    }

    public String getWorkoutName() {
        return workoutName;
    }

    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }

    public String getExerciseCatID() {
        return exerciseCatID;
    }

    public void setExerciseCatID(String exerciseCatID) {
        this.exerciseCatID = exerciseCatID;
    }

    public String getExerciseCatName() {
        return exerciseCatName;
    }

    public void setExerciseCatName(String exerciseCatName) {
        this.exerciseCatName = exerciseCatName;
    }

    public String getExerciseID() {
        return exerciseID;
    }

    public void setExerciseID(String exerciseID) {
        this.exerciseID = exerciseID;
    }

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public Exercise() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.templateDetailsID);
        dest.writeString(this.scheduleDetailsID);
        dest.writeString(this.workoutTypeID);
        dest.writeString(this.workoutName);
        dest.writeString(this.exerciseCatID);
        dest.writeString(this.exerciseCatName);
        dest.writeString(this.exerciseID);
        dest.writeString(this.exerciseName);
        dest.writeTypedList(this.workoutElements);
        dest.writeTypedList(this.exerciseList);
        dest.writeStringList(this.exerciseNameList);
    }

    protected Exercise(Parcel in) {
        this.templateDetailsID = in.readString();
        this.scheduleDetailsID = in.readString();
        this.workoutTypeID = in.readString();
        this.workoutName = in.readString();
        this.exerciseCatID = in.readString();
        this.exerciseCatName = in.readString();
        this.exerciseID = in.readString();
        this.exerciseName = in.readString();
        this.workoutElements = in.createTypedArrayList(WorkoutElement.CREATOR);
        this.exerciseList = in.createTypedArrayList(Exercise.CREATOR);
        this.exerciseNameList = in.createStringArrayList();
    }

    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel source) {
            return new Exercise(source);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };
}
