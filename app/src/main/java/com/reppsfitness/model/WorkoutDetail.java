package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkoutDetail implements Parcelable {
    @SerializedName("workoutElementName")
    @Expose
    public String workoutElementName;
    @SerializedName("workoutElementValue")
    @Expose
    public String workoutElementValue;

    public String getWorkoutElementName() {
        return workoutElementName;
    }

    public void setWorkoutElementName(String workoutElementName) {
        this.workoutElementName = workoutElementName;
    }

    public String getWorkoutElementValue() {
        return workoutElementValue;
    }

    public void setWorkoutElementValue(String workoutElementValue) {
        this.workoutElementValue = workoutElementValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutElementName);
        dest.writeString(this.workoutElementValue);
    }

    public WorkoutDetail() {
    }

    protected WorkoutDetail(Parcel in) {
        this.workoutElementName = in.readString();
        this.workoutElementValue = in.readString();
    }

    public static final Parcelable.Creator<WorkoutDetail> CREATOR = new Parcelable.Creator<WorkoutDetail>() {
        @Override
        public WorkoutDetail createFromParcel(Parcel source) {
            return new WorkoutDetail(source);
        }

        @Override
        public WorkoutDetail[] newArray(int size) {
            return new WorkoutDetail[size];
        }
    };
}