package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 22/11/17.
 */

public class Notification implements Parcelable {
    @SerializedName("notificationID")
    @Expose
    public String notificationID;
    @SerializedName("senderID")
    @Expose
    public String senderID;
    @SerializedName("deviceID")
    @Expose
    public String deviceID;
    @SerializedName("receiverID")
    @Expose
    public String receiverID;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("isViewed")
    @Expose
    public String isViewed;
    @SerializedName("notifyType")
    @Expose
    public String notifyType;

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIsViewed() {
        return isViewed;
    }

    public void setIsViewed(String isViewed) {
        this.isViewed = isViewed;
    }

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.notificationID);
        dest.writeString(this.senderID);
        dest.writeString(this.deviceID);
        dest.writeString(this.receiverID);
        dest.writeString(this.message);
        dest.writeString(this.isViewed);
        dest.writeString(this.notifyType);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.notificationID = in.readString();
        this.senderID = in.readString();
        this.deviceID = in.readString();
        this.receiverID = in.readString();
        this.message = in.readString();
        this.isViewed = in.readString();
        this.notifyType = in.readString();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
