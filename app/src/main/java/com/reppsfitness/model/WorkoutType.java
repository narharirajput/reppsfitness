package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class WorkoutType implements Parcelable {
    @SerializedName("workoutTypeID")
    @Expose
    public String workoutTypeID;
    @SerializedName("workoutTypeName")
    @Expose
    public String workoutTypeName;
    @SerializedName("elements")
    @Expose
    public List<Element> elements = new ArrayList<>();

    public String getWorkoutTypeID() {
        return workoutTypeID;
    }

    public void setWorkoutTypeID(String workoutTypeID) {
        this.workoutTypeID = workoutTypeID;
    }

    public String getWorkoutTypeName() {
        return workoutTypeName;
    }

    public void setWorkoutTypeName(String workoutTypeName) {
        this.workoutTypeName = workoutTypeName;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutTypeID);
        dest.writeString(this.workoutTypeName);
        dest.writeTypedList(this.elements);
    }

    public WorkoutType() {
    }

    protected WorkoutType(Parcel in) {
        this.workoutTypeID = in.readString();
        this.workoutTypeName = in.readString();
        this.elements = in.createTypedArrayList(Element.CREATOR);
    }

    public static final Parcelable.Creator<WorkoutType> CREATOR = new Parcelable.Creator<WorkoutType>() {
        @Override
        public WorkoutType createFromParcel(Parcel source) {
            return new WorkoutType(source);
        }

        @Override
        public WorkoutType[] newArray(int size) {
            return new WorkoutType[size];
        }
    };
}
