package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur Gangurde on 15/11/17.
 */

public class SessionDetailsData implements Parcelable {
    @SerializedName("allottedClients")
    @Expose
    public List<AllotData> allottdata = null;
    @SerializedName("requestedClients")
    @Expose
    public List<RequestData> requestdata = null;
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;
    @SerializedName("clientLimit")
    @Expose
    public String clientLimit;
    @SerializedName("countAllotedSession")
    @Expose
    public String countAllotedSession;

    public String getClientLimit() {
        return clientLimit;
    }

    public void setClientLimit(String clientLimit) {
        this.clientLimit = clientLimit;
    }

    public String getCountAllotedSession() {
        return countAllotedSession;
    }

    public void setCountAllotedSession(String countAllotedSession) {
        this.countAllotedSession = countAllotedSession;
    }

    public List<AllotData> getAllottdata() {
        return allottdata;
    }

    public void setAllottdata(List<AllotData> allottdata) {
        this.allottdata = allottdata;
    }

    public List<RequestData> getRequestdata() {
        return requestdata;
    }

    public void setRequestdata(List<RequestData> requestdata) {
        this.requestdata = requestdata;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public SessionDetailsData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.allottdata);
        dest.writeTypedList(this.requestdata);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
        dest.writeString(this.clientLimit);
        dest.writeString(this.countAllotedSession);
    }

    protected SessionDetailsData(Parcel in) {
        this.allottdata = in.createTypedArrayList(AllotData.CREATOR);
        this.requestdata = in.createTypedArrayList(RequestData.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
        this.clientLimit = in.readString();
        this.countAllotedSession = in.readString();
    }

    public static final Creator<SessionDetailsData> CREATOR = new Creator<SessionDetailsData>() {
        @Override
        public SessionDetailsData createFromParcel(Parcel source) {
            return new SessionDetailsData(source);
        }

        @Override
        public SessionDetailsData[] newArray(int size) {
            return new SessionDetailsData[size];
        }
    };
}
