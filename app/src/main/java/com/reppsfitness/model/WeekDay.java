package com.reppsfitness.model;

/**
 * Created by Mayur Gangurde on 29/11/17.
 */

public class WeekDay {
    private String day, slotTimeVal;
    private boolean isChecked, isEnabled = true;

    public WeekDay(String day, boolean isEnabled, String slotTimeVal) {
        this.day = day;
        this.isEnabled = isEnabled;
        this.slotTimeVal = slotTimeVal;
    }

    public String getSlotTimeVal() {
        return slotTimeVal;
    }

    public void setSlotTimeVal(String slotTimeVal) {
        this.slotTimeVal = slotTimeVal;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
