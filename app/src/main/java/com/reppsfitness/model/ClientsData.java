package com.reppsfitness.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ClientsData {

    @SerializedName("msg")
    private String msg;

    @SerializedName("data")
    private List<User> data = new ArrayList<>();

    @SerializedName("count")
    private int count;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setData(List<User> data) {
        this.data = data;
    }

    public List<User> getData() {
        return data;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return
                "ClientsData{" +
                        "msg = '" + msg + '\'' +
                        ",data = '" + data + '\'' +
                        ",count = '" + count + '\'' +
                        "}";
    }
}