package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Mayur Gangurde on 16/10/17.
 */

public class WorkoutScheduleResponse implements Parcelable {
    @SerializedName("data")
    @Expose
    public ArrayList<Day> dayList = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public ArrayList<Day> getDayList() {
        return dayList;
    }

    public void setDayList(ArrayList<Day> dayList) {
        this.dayList = dayList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.dayList);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    public WorkoutScheduleResponse() {
    }

    protected WorkoutScheduleResponse(Parcel in) {
        this.dayList = in.createTypedArrayList(Day.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Parcelable.Creator<WorkoutScheduleResponse> CREATOR = new Parcelable.Creator<WorkoutScheduleResponse>() {
        @Override
        public WorkoutScheduleResponse createFromParcel(Parcel source) {
            return new WorkoutScheduleResponse(source);
        }

        @Override
        public WorkoutScheduleResponse[] newArray(int size) {
            return new WorkoutScheduleResponse[size];
        }
    };
}
