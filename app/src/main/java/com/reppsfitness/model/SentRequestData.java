package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 13/11/17.
 */

public class SentRequestData {
    @SerializedName("data")
    @Expose
    public List<SentRequest> data = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<SentRequest> getData() {
        return data;
    }

    public void setData(List<SentRequest> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
