package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class ExerciseCat implements Parcelable {
    @SerializedName("exerciseCatID")
    @Expose
    public String exerciseCatID;
    @SerializedName("exerciseCatName")
    @Expose
    public String exerciseCatName;
    @SerializedName("exercises")
    @Expose
    public List<Exercise> exercises = new ArrayList<>();

    public String getExerciseCatID() {
        return exerciseCatID;
    }

    public void setExerciseCatID(String exerciseCatID) {
        this.exerciseCatID = exerciseCatID;
    }

    public String getExerciseCatName() {
        return exerciseCatName;
    }

    public void setExerciseCatName(String exerciseCatName) {
        this.exerciseCatName = exerciseCatName;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.exerciseCatID);
        dest.writeString(this.exerciseCatName);
        dest.writeTypedList(this.exercises);
    }

    public ExerciseCat() {
    }

    protected ExerciseCat(Parcel in) {
        this.exerciseCatID = in.readString();
        this.exerciseCatName = in.readString();
        this.exercises = in.createTypedArrayList(Exercise.CREATOR);
    }

    public static final Parcelable.Creator<ExerciseCat> CREATOR = new Parcelable.Creator<ExerciseCat>() {
        @Override
        public ExerciseCat createFromParcel(Parcel source) {
            return new ExerciseCat(source);
        }

        @Override
        public ExerciseCat[] newArray(int size) {
            return new ExerciseCat[size];
        }
    };
}
