package com.reppsfitness.model.schedule;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class Day implements Parcelable {
    String date, day;
    List<Exercise> exerciseList = new ArrayList<>();

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Exercise> getExerciseList() {
        return exerciseList;
    }

    public void setExerciseList(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }

    public String getExcerciseString() {
        List<String> stringList = new ArrayList<>();
        if (exerciseList.size() > 0) {
            for (int i = 0; i < exerciseList.size(); i++) {
                stringList.add(exerciseList.get(i).getExerciseName());
            }
            return stringList.toString();
        }
        return "";
    }

    public Day() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.date);
        dest.writeString(this.day);
        dest.writeTypedList(this.exerciseList);
    }

    protected Day(Parcel in) {
        this.date = in.readString();
        this.day = in.readString();
        this.exerciseList = in.createTypedArrayList(Exercise.CREATOR);
    }

    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
