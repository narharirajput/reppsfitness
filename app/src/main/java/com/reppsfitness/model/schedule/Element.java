package com.reppsfitness.model.schedule;

import android.os.Parcel;
import android.os.Parcelable;

public class Element implements Parcelable {
    public String workoutElementID, workoutElementName, workoutElementValue;

    public String getWorkoutElementValue() {
        return workoutElementValue;
    }

    public void setWorkoutElementValue(String workoutElementValue) {
        this.workoutElementValue = workoutElementValue;
    }

    public String getWorkoutElementID() {
        return workoutElementID;
    }

    public void setWorkoutElementID(String workoutElementID) {
        this.workoutElementID = workoutElementID;
    }

    public String getWorkoutElementName() {
        return workoutElementName;
    }

    public void setWorkoutElementName(String workoutElementName) {
        this.workoutElementName = workoutElementName;
    }

    public Element() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutElementID);
        dest.writeString(this.workoutElementName);
        dest.writeString(this.workoutElementValue);
    }

    protected Element(Parcel in) {
        this.workoutElementID = in.readString();
        this.workoutElementName = in.readString();
        this.workoutElementValue = in.readString();
    }

    public static final Creator<Element> CREATOR = new Creator<Element>() {
        @Override
        public Element createFromParcel(Parcel source) {
            return new Element(source);
        }

        @Override
        public Element[] newArray(int size) {
            return new Element[size];
        }
    };
}