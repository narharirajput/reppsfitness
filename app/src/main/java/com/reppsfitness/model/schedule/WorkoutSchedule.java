package com.reppsfitness.model.schedule;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class WorkoutSchedule implements Parcelable {
    String fromDate, toDate, clienID, userID;
    List<Day> dayList = new ArrayList<>();

    public String getClienID() {
        return clienID;
    }

    public void setClienID(String clienID) {
        this.clienID = clienID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public List<Day> getDayList() {
        return dayList;
    }

    public void setDayList(List<Day> dayList) {
        this.dayList = dayList;
    }

    public WorkoutSchedule() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.fromDate);
        dest.writeString(this.toDate);
        dest.writeString(this.clienID);
        dest.writeString(this.userID);
        dest.writeTypedList(this.dayList);
    }

    protected WorkoutSchedule(Parcel in) {
        this.fromDate = in.readString();
        this.toDate = in.readString();
        this.clienID = in.readString();
        this.userID = in.readString();
        this.dayList = in.createTypedArrayList(Day.CREATOR);
    }

    public static final Creator<WorkoutSchedule> CREATOR = new Creator<WorkoutSchedule>() {
        @Override
        public WorkoutSchedule createFromParcel(Parcel source) {
            return new WorkoutSchedule(source);
        }

        @Override
        public WorkoutSchedule[] newArray(int size) {
            return new WorkoutSchedule[size];
        }
    };
}
