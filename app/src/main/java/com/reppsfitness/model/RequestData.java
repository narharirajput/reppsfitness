package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 15/11/17.
 */

public class RequestData implements Parcelable {
    @SerializedName("clientID")
    @Expose
    public String clientID;
    @SerializedName("clientFName")
    @Expose
    public String clientFName;
    @SerializedName("clientProfilePic")
    @Expose
    public String clientProfilePic;
    @SerializedName("sessionCRID")
    @Expose
    public String sessionCRID;

    public String getSessionCRID() {
        return sessionCRID;
    }

    public void setSessionCRID(String sessionCRID) {
        this.sessionCRID = sessionCRID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientFName() {
        return clientFName;
    }

    public void setClientFName(String clientFName) {
        this.clientFName = clientFName;
    }

    public String getClientProfilePic() {
        return clientProfilePic;
    }

    public void setClientProfilePic(String clientProfilePic) {
        this.clientProfilePic = clientProfilePic;
    }

    public RequestData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.clientID);
        dest.writeString(this.clientFName);
        dest.writeString(this.clientProfilePic);
        dest.writeString(this.sessionCRID);
    }

    protected RequestData(Parcel in) {
        this.clientID = in.readString();
        this.clientFName = in.readString();
        this.clientProfilePic = in.readString();
        this.sessionCRID = in.readString();
    }

    public static final Creator<RequestData> CREATOR = new Creator<RequestData>() {
        @Override
        public RequestData createFromParcel(Parcel source) {
            return new RequestData(source);
        }

        @Override
        public RequestData[] newArray(int size) {
            return new RequestData[size];
        }
    };
}
