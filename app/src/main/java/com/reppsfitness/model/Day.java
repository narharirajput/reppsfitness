package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 16/10/17.
 */

public class Day implements Parcelable {
    @SerializedName("templateDayID")
    @Expose
    public String templateDayID;
    @SerializedName("scheduleDayID")
    @Expose
    public String scheduleDayID;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("day")
    @Expose
    public String day;
    @SerializedName("exercise")
    @Expose
    public String exercise;
    @SerializedName("isWeekDay")
    @Expose
    public long isWeekDay;
    @SerializedName("isSessionAllotedDay")
    @Expose
    public long isSessionAllotedDay;

    public long getIsSessionAllotedDay() {
        return isSessionAllotedDay;
    }

    public void setIsSessionAllotedDay(long isSessionAllotedDay) {
        this.isSessionAllotedDay = isSessionAllotedDay;
    }

    public long getIsWeekDay() {
        return isWeekDay;
    }

    public void setIsWeekDay(long isWeekDay) {
        this.isWeekDay = isWeekDay;
    }

    public String getTemplateDayID() {
        return templateDayID;
    }

    public void setTemplateDayID(String templateDayID) {
        this.templateDayID = templateDayID;
    }

    public String getScheduleDayID() {
        return scheduleDayID;
    }

    public void setScheduleDayID(String scheduleDayID) {
        this.scheduleDayID = scheduleDayID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Day() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.templateDayID);
        dest.writeString(this.scheduleDayID);
        dest.writeString(this.date);
        dest.writeString(this.day);
        dest.writeString(this.exercise);
        dest.writeLong(this.isWeekDay);
        dest.writeLong(this.isSessionAllotedDay);
    }

    protected Day(Parcel in) {
        this.templateDayID = in.readString();
        this.scheduleDayID = in.readString();
        this.date = in.readString();
        this.day = in.readString();
        this.exercise = in.readString();
        this.isWeekDay = in.readLong();
        this.isSessionAllotedDay = in.readLong();
    }

    public static final Creator<Day> CREATOR = new Creator<Day>() {
        @Override
        public Day createFromParcel(Parcel source) {
            return new Day(source);
        }

        @Override
        public Day[] newArray(int size) {
            return new Day[size];
        }
    };
}
