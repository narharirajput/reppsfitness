package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Element implements Parcelable {
    @SerializedName("workoutElementID")
    @Expose
    public String workoutElementID;
    @SerializedName("workoutElementName")
    @Expose
    public String workoutElementName;
    @SerializedName("workoutElementValue")
    @Expose
    public ArrayList<String> workoutElementValue = null;

    public String getWorkoutElementID() {
        return workoutElementID;
    }

    public void setWorkoutElementID(String workoutElementID) {
        this.workoutElementID = workoutElementID;
    }

    public String getWorkoutElementName() {
        return workoutElementName;
    }

    public void setWorkoutElementName(String workoutElementName) {
        this.workoutElementName = workoutElementName;
    }

    public ArrayList<String> getWorkoutElementValue() {
        return workoutElementValue;
    }

    public void setWorkoutElementValue(ArrayList<String> workoutElementValue) {
        this.workoutElementValue = workoutElementValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutElementID);
        dest.writeString(this.workoutElementName);
        dest.writeStringList(this.workoutElementValue);
    }

    public Element() {
    }

    protected Element(Parcel in) {
        this.workoutElementID = in.readString();
        this.workoutElementName = in.readString();
        this.workoutElementValue = in.createStringArrayList();
    }

    public static final Parcelable.Creator<Element> CREATOR = new Parcelable.Creator<Element>() {
        @Override
        public Element createFromParcel(Parcel source) {
            return new Element(source);
        }

        @Override
        public Element[] newArray(int size) {
            return new Element[size];
        }
    };
}