package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WorkoutElement implements Parcelable {
    @SerializedName("workoutElementID")
    @Expose
    public String workoutElementID;
    @SerializedName("workoutElementName")
    @Expose
    public String workoutElementName;
    @SerializedName("workoutElementValue")
    @Expose
    public String workoutElementValue;
    public ArrayList<String> workoutElementValueList = new ArrayList<>();

    public ArrayList<String> getWorkoutElementValueList() {
        return workoutElementValueList;
    }

    public void setWorkoutElementValueList(ArrayList<String> workoutElementValueList) {
        this.workoutElementValueList = workoutElementValueList;
    }

    public String getWorkoutElementID() {
        return workoutElementID;
    }

    public void setWorkoutElementID(String workoutElementID) {
        this.workoutElementID = workoutElementID;
    }

    public String getWorkoutElementName() {
        return workoutElementName;
    }

    public void setWorkoutElementName(String workoutElementName) {
        this.workoutElementName = workoutElementName;
    }

    public String getWorkoutElementValue() {
        return workoutElementValue;
    }

    public void setWorkoutElementValue(String workoutElementValue) {
        this.workoutElementValue = workoutElementValue;
    }

    public WorkoutElement() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.workoutElementID);
        dest.writeString(this.workoutElementName);
        dest.writeString(this.workoutElementValue);
        dest.writeStringList(this.workoutElementValueList);
    }

    protected WorkoutElement(Parcel in) {
        this.workoutElementID = in.readString();
        this.workoutElementName = in.readString();
        this.workoutElementValue = in.readString();
        this.workoutElementValueList = in.createStringArrayList();
    }

    public static final Creator<WorkoutElement> CREATOR = new Creator<WorkoutElement>() {
        @Override
        public WorkoutElement createFromParcel(Parcel source) {
            return new WorkoutElement(source);
        }

        @Override
        public WorkoutElement[] newArray(int size) {
            return new WorkoutElement[size];
        }
    };
}