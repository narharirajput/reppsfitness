package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 01/02/18.
 */

public class ShiftSession {
    @SerializedName("shiftFromSessionDate")
    @Expose
    public String shiftFromSessionDate;
    @SerializedName("shiftFromSessionTime")
    @Expose
    public String shiftFromSessionTime;
    @SerializedName("shiftFromSessionID")
    @Expose
    public String shiftFromSessionID;
    @SerializedName("shiftFromSessionDateString")
    @Expose
    public String shiftFromSessionDateString;

    public String getShiftFromSessionDate() {
        return shiftFromSessionDate;
    }

    public void setShiftFromSessionDate(String shiftFromSessionDate) {
        this.shiftFromSessionDate = shiftFromSessionDate;
    }

    public String getShiftFromSessionTime() {
        return shiftFromSessionTime;
    }

    public void setShiftFromSessionTime(String shiftFromSessionTime) {
        this.shiftFromSessionTime = shiftFromSessionTime;
    }

    public String getShiftFromSessionID() {
        return shiftFromSessionID;
    }

    public void setShiftFromSessionID(String shiftFromSessionID) {
        this.shiftFromSessionID = shiftFromSessionID;
    }

    public String getShiftFromSessionDateString() {
        return shiftFromSessionDateString;
    }

    public void setShiftFromSessionDateString(String shiftFromSessionDateString) {
        this.shiftFromSessionDateString = shiftFromSessionDateString;
    }
}
