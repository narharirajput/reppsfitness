package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 13/11/17.
 */

public class SessionRequest {
    @SerializedName("sessionCRID")
    @Expose
    public String sessionCRID;
    @SerializedName("packageID")
    @Expose
    public String packageID;
    @SerializedName("requestDate")
    @Expose
    public String requestDate;
    @SerializedName("requestFromTime")
    @Expose
    public String requestFromTime;
    @SerializedName("requestToTime")
    @Expose
    public String requestToTime;
    @SerializedName("requestStatus")
    @Expose
    public String requestStatus;
    @SerializedName("clientFName")
    @Expose
    public String clientFName;
    @SerializedName("clientEmail")
    @Expose
    public String clientEmail;
    @SerializedName("clientID")
    @Expose
    public String clientID;

    public String getSessionCRID() {
        return sessionCRID;
    }

    public void setSessionCRID(String sessionCRID) {
        this.sessionCRID = sessionCRID;
    }

    public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getRequestFromTime() {
        return requestFromTime;
    }

    public void setRequestFromTime(String requestFromTime) {
        this.requestFromTime = requestFromTime;
    }

    public String getRequestToTime() {
        return requestToTime;
    }

    public void setRequestToTime(String requestToTime) {
        this.requestToTime = requestToTime;
    }

    public String getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(String requestStatus) {
        this.requestStatus = requestStatus;
    }

    public String getClientFName() {
        return clientFName;
    }

    public void setClientFName(String clientFName) {
        this.clientFName = clientFName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }
}
