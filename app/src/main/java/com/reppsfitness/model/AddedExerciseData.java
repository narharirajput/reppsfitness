package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 06/10/17.
 */

public class AddedExerciseData implements Parcelable {
    @SerializedName("data")
    @Expose
    public List<Exercise> data = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<Exercise> getData() {
        return data;
    }

    public void setData(List<Exercise> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    public AddedExerciseData() {
    }

    protected AddedExerciseData(Parcel in) {
        this.data = in.createTypedArrayList(Exercise.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Parcelable.Creator<AddedExerciseData> CREATOR = new Parcelable.Creator<AddedExerciseData>() {
        @Override
        public AddedExerciseData createFromParcel(Parcel source) {
            return new AddedExerciseData(source);
        }

        @Override
        public AddedExerciseData[] newArray(int size) {
            return new AddedExerciseData[size];
        }
    };
}
