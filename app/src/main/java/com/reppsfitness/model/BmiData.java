package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by MXCPUU11 on 11/3/2017.
 */

public class BmiData implements Serializable {

    @SerializedName("data")
    @Expose
    private List<Bmi> data = null;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("count")
    @Expose
    private Integer count;
    private final static long serialVersionUID = 2504986024189554658L;

    public List<Bmi> getData() {
        return data;
    }

    public void setData(List<Bmi> data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
