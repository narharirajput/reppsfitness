package com.reppsfitness.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class PlansData{

	@SerializedName("msg")
	private String msg;

	@SerializedName("data")
	private Plans data;

	@SerializedName("count")
	private int count;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setData(Plans data){
		this.data = data;
	}

	public Plans getData(){
		return data;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@Override
 	public String toString(){
		return 
			"PlansData{" + 
			"msg = '" + msg + '\'' + 
			",data = '" + data + '\'' + 
			",count = '" + count + '\'' + 
			"}";
		}
}