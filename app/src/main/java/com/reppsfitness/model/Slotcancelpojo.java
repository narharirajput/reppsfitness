package com.reppsfitness.model;

import android.provider.ContactsContract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Slotcancelpojo {

    @SerializedName("data")
    @Expose
    private ContactsContract.Contacts.Data data;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("count")
    @Expose
    private Integer count;

    public ContactsContract.Contacts.Data getData() {
        return data;
    }

    public void setData(ContactsContract.Contacts.Data data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
