package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class RegularItem implements Parcelable {
    @SerializedName("packageID")
    private String packageID;
    @SerializedName("isRegular")
    private String isRegular;
    @SerializedName("packageFeatureTitle")
    private String packageFeatureTitle;
    @SerializedName("packageName")
    private String packageName;
    @SerializedName("packageImage")
    private String packageImage;
    @SerializedName("packageDuration")
    private String packageDuration;
    @SerializedName("isTermApply")
    private String isTermApply;
    @SerializedName("packageFeatureDesc")
    private String packageFeatureDesc;
    @SerializedName("packageAmount")
    private String packageAmount;
    @SerializedName("isCurrentPackage")
    @Expose
    public long isCurrentPackage;
    @SerializedName("isRequestPackage")
    @Expose
    public long isRequestPackage;
    @SerializedName("noOfSessions")
    @Expose
    public String noOfSessions;
    @SerializedName("minPerWeekSession")
    @Expose
    public String minPerWeekSession;
    @SerializedName("maxPerWeekSession")
    @Expose
    public String maxPerWeekSession;
    @SerializedName("perSessionPrice")
    @Expose
    public String perSessionPrice;
    @SerializedName("limitDays")
    @Expose
    public String limitDays;

    public String getPackageImage() {
        return packageImage;
    }

    public void setPackageImage(String packageImage) {
        this.packageImage = packageImage;
    }

    public String getNoOfSessions() {
        return noOfSessions;
    }

    public void setNoOfSessions(String noOfSessions) {
        this.noOfSessions = noOfSessions;
    }

    public String getMinPerWeekSession() {
        return minPerWeekSession;
    }

    public void setMinPerWeekSession(String minPerWeekSession) {
        this.minPerWeekSession = minPerWeekSession;
    }

    public String getMaxPerWeekSession() {
        return maxPerWeekSession;
    }

    public void setMaxPerWeekSession(String maxPerWeekSession) {
        this.maxPerWeekSession = maxPerWeekSession;
    }

    public String getPerSessionPrice() {
        return perSessionPrice;
    }

    public void setPerSessionPrice(String perSessionPrice) {
        this.perSessionPrice = perSessionPrice;
    }

    public String getLimitDays() {
        return limitDays;
    }

    public void setLimitDays(String limitDays) {
        this.limitDays = limitDays;
    }

    public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public long getIsCurrentPackage() {
        return isCurrentPackage;
    }

    public void setIsCurrentPackage(long isCurrentPackage) {
        this.isCurrentPackage = isCurrentPackage;
    }

    public long getIsRequestPackage() {
        return isRequestPackage;
    }

    public void setIsRequestPackage(long isRequestPackage) {
        this.isRequestPackage = isRequestPackage;
    }

    public void setIsRegular(String isRegular) {
        this.isRegular = isRegular;
    }

    public String getIsRegular() {
        return isRegular;
    }

    public void setPackageFeatureTitle(String packageFeatureTitle) {
        this.packageFeatureTitle = packageFeatureTitle;
    }

    public String getPackageFeatureTitle() {
        return packageFeatureTitle;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }

    public String getPackageDuration() {
        return packageDuration;
    }

    public void setIsTermApply(String isTermApply) {
        this.isTermApply = isTermApply;
    }

    public String getIsTermApply() {
        return isTermApply;
    }

    public void setPackageFeatureDesc(String packageFeatureDesc) {
        this.packageFeatureDesc = packageFeatureDesc;
    }

    public String getPackageFeatureDesc() {
        return packageFeatureDesc;
    }

    public void setPackageAmount(String packageAmount) {
        this.packageAmount = packageAmount;
    }

    public String getPackageAmount() {
        return packageAmount;
    }

    @Override
    public String toString() {
        return
                "RegularItem{" +
                        "isRegular = '" + isRegular + '\'' +
                        ",packageFeatureTitle = '" + packageFeatureTitle + '\'' +
                        ",packageImage = '" + packageImage + '\'' +
                        ",packageName = '" + packageName + '\'' +
                        ",packageDuration = '" + packageDuration + '\'' +
                        ",isTermApply = '" + isTermApply + '\'' +
                        ",packageFeatureDesc = '" + packageFeatureDesc + '\'' +
                        ",packageAmount = '" + packageAmount + '\'' +
                        "}";
    }

    public RegularItem() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageID);
        dest.writeString(this.isRegular);
        dest.writeString(this.packageFeatureTitle);
        dest.writeString(this.packageImage);
        dest.writeString(this.packageName);
        dest.writeString(this.packageDuration);
        dest.writeString(this.isTermApply);
        dest.writeString(this.packageFeatureDesc);
        dest.writeString(this.packageAmount);
        dest.writeLong(this.isCurrentPackage);
        dest.writeLong(this.isRequestPackage);
        dest.writeString(this.noOfSessions);
        dest.writeString(this.minPerWeekSession);
        dest.writeString(this.maxPerWeekSession);
        dest.writeString(this.perSessionPrice);
        dest.writeString(this.limitDays);
    }

    protected RegularItem(Parcel in) {
        this.packageID = in.readString();
        this.isRegular = in.readString();
        this.packageFeatureTitle = in.readString();
        this.packageName = in.readString();
        this.packageImage = in.readString();
        this.packageDuration = in.readString();
        this.isTermApply = in.readString();
        this.packageFeatureDesc = in.readString();
        this.packageAmount = in.readString();
        this.isCurrentPackage = in.readLong();
        this.isRequestPackage = in.readLong();
        this.noOfSessions = in.readString();
        this.minPerWeekSession = in.readString();
        this.maxPerWeekSession = in.readString();
        this.perSessionPrice = in.readString();
        this.limitDays = in.readString();
    }

    public static final Creator<RegularItem> CREATOR = new Creator<RegularItem>() {
        @Override
        public RegularItem createFromParcel(Parcel source) {
            return new RegularItem(source);
        }

        @Override
        public RegularItem[] newArray(int size) {
            return new RegularItem[size];
        }
    };
}