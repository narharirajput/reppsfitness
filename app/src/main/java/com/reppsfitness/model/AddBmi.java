package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by MXCPUU11 on 11/3/2017.
 */

public class AddBmi implements Serializable {
    @SerializedName("data")
    @Expose
    private AddBmiData data;
    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("count")
    @Expose
    private Integer count;
    private final static long serialVersionUID = -8989617453144789460L;

    public AddBmiData getData() {
        return data;
    }

    public void setData(AddBmiData data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}