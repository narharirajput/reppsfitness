package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 06/11/17.
 */

public class PaymentMode implements Parcelable {
    @SerializedName("paymentModeID")
    @Expose
    public String paymentModeID;
    @SerializedName("paymentMode")
    @Expose
    public String paymentMode;

    public String getPaymentModeID() {
        return paymentModeID;
    }

    public void setPaymentModeID(String paymentModeID) {
        this.paymentModeID = paymentModeID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.paymentModeID);
        dest.writeString(this.paymentMode);
    }

    public PaymentMode() {
    }

    protected PaymentMode(Parcel in) {
        this.paymentModeID = in.readString();
        this.paymentMode = in.readString();
    }

    public static final Parcelable.Creator<PaymentMode> CREATOR = new Parcelable.Creator<PaymentMode>() {
        @Override
        public PaymentMode createFromParcel(Parcel source) {
            return new PaymentMode(source);
        }

        @Override
        public PaymentMode[] newArray(int size) {
            return new PaymentMode[size];
        }
    };
}
