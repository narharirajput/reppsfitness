package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.reppsfitness.exercise.model.ElementsValueArr;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 02/10/17.
 */

public class WorkoutExerciseData implements Parcelable {
    @SerializedName("exerciseCategories")
    @Expose
    public List<ExerciseCat> exerciseCategories = new ArrayList<>();
    @SerializedName("workoutElements")
    @Expose
    public List<WorkoutType> workoutTypeList = new ArrayList<>();
    @SerializedName("elementsValueArr")
    @Expose
    public List<ElementsValueArr> elementsValueArr = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<ElementsValueArr> getElementsValueArr() {
        return elementsValueArr;
    }

    public void setElementsValueArr(List<ElementsValueArr> elementsValueArr) {
        this.elementsValueArr = elementsValueArr;
    }

    public List<ExerciseCat> getExerciseCategories() {
        return exerciseCategories;
    }

    public void setExerciseCategories(List<ExerciseCat> exerciseCategories) {
        this.exerciseCategories = exerciseCategories;
    }

    public List<WorkoutType> getWorkoutTypeList() {
        return workoutTypeList;
    }

    public void setWorkoutTypeList(List<WorkoutType> workoutTypeList) {
        this.workoutTypeList = workoutTypeList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.exerciseCategories);
        dest.writeTypedList(this.workoutTypeList);
        dest.writeTypedList(this.elementsValueArr);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    public WorkoutExerciseData() {
    }

    protected WorkoutExerciseData(Parcel in) {
        this.exerciseCategories = in.createTypedArrayList(ExerciseCat.CREATOR);
        this.workoutTypeList = in.createTypedArrayList(WorkoutType.CREATOR);
        this.elementsValueArr = in.createTypedArrayList(ElementsValueArr.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Parcelable.Creator<WorkoutExerciseData> CREATOR = new Parcelable.Creator<WorkoutExerciseData>() {
        @Override
        public WorkoutExerciseData createFromParcel(Parcel source) {
            return new WorkoutExerciseData(source);
        }

        @Override
        public WorkoutExerciseData[] newArray(int size) {
            return new WorkoutExerciseData[size];
        }
    };
}
