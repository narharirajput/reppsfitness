package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Client implements Parcelable {
	@SerializedName("clientFName")
	private String clientFName;
	@SerializedName("clientID")
	private String clientID;
	@SerializedName("clientLName")
	private String clientLName;
	@SerializedName("clientEmail")
	private String clientEmail;
	@SerializedName("clientMobile")
	private String clientMobile;
	@SerializedName("userID")
	private String userID;
	@SerializedName("dateAdded")
	private String dateAdded;
	@SerializedName("status")
	private String status;
	@SerializedName("clientDateOfBirth")
	private String clientDateOfBirth;
	@SerializedName("clientGender")
	private String clientGender;
	@SerializedName("clientJoinDate")
	private String clientJoinDate;
	@SerializedName("imageName")
	private String imageName;
	@SerializedName("clientProfilePic")
	private String clientProfilePic;

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getClientProfilePic() {
		return clientProfilePic;
	}

	public void setClientProfilePic(String clientProfilePic) {
		this.clientProfilePic = clientProfilePic;
	}

	public String getClientDateOfBirth() {
		return clientDateOfBirth;
	}

	public void setClientDateOfBirth(String clientDateOfBirth) {
		this.clientDateOfBirth = clientDateOfBirth;
	}

	public String getClientGender() {
		return clientGender;
	}

	public void setClientGender(String clientGender) {
		this.clientGender = clientGender;
	}

	public String getClientJoinDate() {
		return clientJoinDate;
	}

	public void setClientJoinDate(String clientJoinDate) {
		this.clientJoinDate = clientJoinDate;
	}

	public void setClientFName(String clientFName){
		this.clientFName = clientFName;
	}

	public String getClientFName(){
		return clientFName;
	}

	public void setClientID(String clientID){
		this.clientID = clientID;
	}

	public String getClientID(){
		return clientID;
	}

	public void setClientLName(String clientLName){
		this.clientLName = clientLName;
	}

	public String getClientLName(){
		return clientLName;
	}

	public void setClientEmail(String clientEmail){
		this.clientEmail = clientEmail;
	}

	public String getClientEmail(){
		return clientEmail;
	}

	public void setClientMobile(String clientMobile){
		this.clientMobile = clientMobile;
	}

	public String getClientMobile(){
		return clientMobile;
	}

	public void setUserID(String userID){
		this.userID = userID;
	}

	public String getUserID(){
		return userID;
	}

	public void setDateAdded(String dateAdded){
		this.dateAdded = dateAdded;
	}

	public String getDateAdded(){
		return dateAdded;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Client{" +
			"clientFName = '" + clientFName + '\'' + 
			",clientID = '" + clientID + '\'' + 
			",clientLName = '" + clientLName + '\'' + 
			",clientEmail = '" + clientEmail + '\'' + 
			",clientMobile = '" + clientMobile + '\'' + 
			",userID = '" + userID + '\'' + 
			",dateAdded = '" + dateAdded + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	public Client() {
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.clientFName);
		dest.writeString(this.clientID);
		dest.writeString(this.clientLName);
		dest.writeString(this.clientEmail);
		dest.writeString(this.clientMobile);
		dest.writeString(this.userID);
		dest.writeString(this.dateAdded);
		dest.writeString(this.status);
		dest.writeString(this.clientDateOfBirth);
		dest.writeString(this.clientGender);
		dest.writeString(this.clientJoinDate);
		dest.writeString(this.imageName);
		dest.writeString(this.clientProfilePic);
	}

	protected Client(Parcel in) {
		this.clientFName = in.readString();
		this.clientID = in.readString();
		this.clientLName = in.readString();
		this.clientEmail = in.readString();
		this.clientMobile = in.readString();
		this.userID = in.readString();
		this.dateAdded = in.readString();
		this.status = in.readString();
		this.clientDateOfBirth = in.readString();
		this.clientGender = in.readString();
		this.clientJoinDate = in.readString();
		this.imageName = in.readString();
		this.clientProfilePic = in.readString();
	}

	public static final Creator<Client> CREATOR = new Creator<Client>() {
		@Override
		public Client createFromParcel(Parcel source) {
			return new Client(source);
		}

		@Override
		public Client[] newArray(int size) {
			return new Client[size];
		}
	};
}