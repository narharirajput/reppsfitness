package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MXCPUU11 on 8/21/2017.
 */

public class Workout implements Parcelable {
    @SerializedName("exerciseName")
    @Expose
    public String exerciseName;
    @SerializedName("workoutDetails")
    @Expose
    public List<WorkoutDetail> workoutDetails = new ArrayList<>();

    public String getExerciseName() {
        return exerciseName;
    }

    public void setExerciseName(String exerciseName) {
        this.exerciseName = exerciseName;
    }

    public List<WorkoutDetail> getWorkoutDetails() {
        return workoutDetails;
    }

    public void setWorkoutDetails(List<WorkoutDetail> workoutDetails) {
        this.workoutDetails = workoutDetails;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.exerciseName);
        dest.writeTypedList(this.workoutDetails);
    }

    public Workout() {
    }

    protected Workout(Parcel in) {
        this.exerciseName = in.readString();
        this.workoutDetails = in.createTypedArrayList(WorkoutDetail.CREATOR);
    }

    public static final Parcelable.Creator<Workout> CREATOR = new Parcelable.Creator<Workout>() {
        @Override
        public Workout createFromParcel(Parcel source) {
            return new Workout(source);
        }

        @Override
        public Workout[] newArray(int size) {
            return new Workout[size];
        }
    };
}
