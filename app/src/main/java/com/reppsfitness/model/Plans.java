package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class Plans implements Parcelable {
    @SerializedName("term")
    private List<TermItem> term;
    @SerializedName("regular")
    private List<RegularItem> regular;
    @SerializedName("isSessionAlloted")
    @Expose
    public long isSessionAlloted;
    @SerializedName("sessionTime")
    @Expose
    public String sessionTime;

    public String getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public long getIsSessionAlloted() {
        return isSessionAlloted;
    }

    public void setIsSessionAlloted(long isSessionAlloted) {
        this.isSessionAlloted = isSessionAlloted;
    }

    public void setTerm(List<TermItem> term) {
        this.term = term;
    }

    public List<TermItem> getTerm() {
        return term;
    }

    public void setRegular(List<RegularItem> regular) {
        this.regular = regular;
    }

    public List<RegularItem> getRegular() {
        return regular;
    }

    @Override
    public String toString() {
        return
                "Plans{" +
                        "term = '" + term + '\'' +
                        ",regular = '" + regular + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.term);
        dest.writeTypedList(this.regular);
        dest.writeLong(this.isSessionAlloted);
        dest.writeString(this.sessionTime);
    }

    public Plans() {
    }

    protected Plans(Parcel in) {
        this.term = in.createTypedArrayList(TermItem.CREATOR);
        this.regular = in.createTypedArrayList(RegularItem.CREATOR);
        this.isSessionAlloted = in.readLong();
        this.sessionTime = in.readString();
    }

    public static final Parcelable.Creator<Plans> CREATOR = new Parcelable.Creator<Plans>() {
        @Override
        public Plans createFromParcel(Parcel source) {
            return new Plans(source);
        }

        @Override
        public Plans[] newArray(int size) {
            return new Plans[size];
        }
    };
}