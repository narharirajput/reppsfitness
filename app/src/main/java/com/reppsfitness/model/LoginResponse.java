package com.reppsfitness.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class LoginResponse {
    @SerializedName("msg")
    private String msg;

    @SerializedName("data")
    private User data;

    @SerializedName("count")
    private int count;

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setData(User data) {
        this.data = data;
    }

    public User getData() {
        return data;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return
                "LoginResponse{" +
                        "msg = '" + msg + '\'' +
                        ",data = '" + data + '\'' +
                        ",count = '" + count + '\'' +
                        "}";
    }
}