package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Mayur Gangurde on 15/11/17.
 */

public class AllotData implements Parcelable {
    @SerializedName("clientID")
    @Expose
    public String clientID;
    @SerializedName("clientFName")
    @Expose
    public String clientFName;
    @SerializedName("clientProfilePic")
    @Expose
    public String clientProfilePic;
    @SerializedName("sessionID")
    @Expose
    public String sessionID;
    @SerializedName("isSessionShifted")
    @Expose
    public String isSessionShifted;


    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }
    public String getIsSessionShifted() {
        return isSessionShifted;
    }

    public void setIsSessionShifted(String isSessionShifted) {
        this.isSessionShifted = isSessionShifted;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientFName() {
        return clientFName;
    }

    public void setClientFName(String clientFName) {
        this.clientFName = clientFName;
    }

    public String getClientProfilePic() {
        return clientProfilePic;
    }

    public void setClientProfilePic(String clientProfilePic) {
        this.clientProfilePic = clientProfilePic;
    }

    public AllotData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.clientID);
        dest.writeString(this.clientFName);
        dest.writeString(this.clientProfilePic);
        dest.writeString(this.isSessionShifted);
    }

    protected AllotData(Parcel in) {
        this.clientID = in.readString();
        this.clientFName = in.readString();
        this.clientProfilePic = in.readString();
        this.isSessionShifted = in.readString();
    }

    public static final Creator<AllotData> CREATOR = new Creator<AllotData>() {
        @Override
        public AllotData createFromParcel(Parcel source) {
            return new AllotData(source);
        }

        @Override
        public AllotData[] newArray(int size) {
            return new AllotData[size];
        }
    };
}
