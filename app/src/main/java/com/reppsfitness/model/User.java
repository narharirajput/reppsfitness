package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mayur Gangurde on 27/09/17.
 */

public class User implements Parcelable {
    @SerializedName("displayName")
    @Expose
    public String displayName;
    @SerializedName("pkgDetailsID")
    @Expose
    public String pkgDetailsID;
    @SerializedName("userEmail")
    @Expose
    public String userEmail;
    @SerializedName("userID")
    @Expose
    public String userID;
    @SerializedName("roleAID")
    @Expose
    public String roleAID;
    @SerializedName("imageName")
    @Expose
    public String imageName;
    @SerializedName("userType")
    @Expose
    public String userType;
    @SerializedName("clientFName")
    private String clientFName;
    @SerializedName("clientID")
    private String clientID;
    @SerializedName("clientLName")
    private String clientLName;
    @SerializedName("clientEmail")
    private String clientEmail;
    @SerializedName("clientMobile")
    private String clientMobile;
    @SerializedName("clientDOB")
    private String clientDOB;
    @SerializedName("clientGender")
    private String clientGender;
    @SerializedName("clientJoinDate")
    private String clientJoinDate;
    @SerializedName("clientProfilePic")
    private String clientProfilePic;
    @SerializedName("userImage")
    private String userImage;
    @SerializedName("userName")
    @Expose
    public String userName;
    @SerializedName("pkgStartDate")
    @Expose
    public String pkgStartDate;
    @SerializedName("pkgEndDate")
    @Expose
    public String pkgEndDate;
    @SerializedName("getSessionAllottedTime")
    @Expose
    public String getSessionAllottedTime;
    @SerializedName("packageID")
    @Expose
    public String packageID;
    @SerializedName("isSessionAllotted")
    @Expose
    public String isSessionAlloted;
    @SerializedName("termsConditionsUrl")
    @Expose
    public String termsConditionsUrl;
    @SerializedName("disclaimerUrl")
    @Expose
    public String disclaimerUrl;
    @SerializedName("shiftedSessionDateList")
    @Expose
    public List<String> shiftedSessionDateList = null;

    public List<String> getShiftedSessionDateList() {
        return shiftedSessionDateList;
    }

    public void setShiftedSessionDateList(List<String> shiftedSessionDateList) {
        this.shiftedSessionDateList = shiftedSessionDateList;
    }

    public String getTermsConditionsUrl() {
        return termsConditionsUrl;
    }

    public void setTermsConditionsUrl(String termsConditionsUrl) {
        this.termsConditionsUrl = termsConditionsUrl;
    }

    public String getDisclaimerUrl() {
        return disclaimerUrl;
    }

    public void setDisclaimerUrl(String disclaimerUrl) {
        this.disclaimerUrl = disclaimerUrl;
    }

    public String getIsSessionAlloted() {
        return isSessionAlloted;
    }

    public void setIsSessionAlloted(String isSessionAlloted) {
        this.isSessionAlloted = isSessionAlloted;
    }

    public String getPackageID() {
        return packageID;
    }

    public String getPkgDetailsID() {
        return pkgDetailsID;
    }

    public void setPkgDetailsID(String pkgDetailsID) {
        this.pkgDetailsID = pkgDetailsID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public String getPkgStartDate() {
        return pkgStartDate;
    }

    public void setPkgStartDate(String pkgStartDate) {
        this.pkgStartDate = pkgStartDate;
    }

    public String getPkgEndDate() {
        return pkgEndDate;
    }

    public void setPkgEndDate(String pkgEndDate) {
        this.pkgEndDate = pkgEndDate;
    }

    public String getGetSessionAllottedTime() {
        return getSessionAllottedTime;
    }

    public void setGetSessionAllottedTime(String getSessionAllottedTime) {
        this.getSessionAllottedTime = getSessionAllottedTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getClientFName() {
        return clientFName;
    }

    public void setClientFName(String clientFName) {
        this.clientFName = clientFName;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getClientLName() {
        return clientLName;
    }

    public void setClientLName(String clientLName) {
        this.clientLName = clientLName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientMobile() {
        return clientMobile;
    }

    public void setClientMobile(String clientMobile) {
        this.clientMobile = clientMobile;
    }

    public String getClientDOB() {
        return clientDOB;
    }

    public void setClientDOB(String clientDOB) {
        this.clientDOB = clientDOB;
    }

    public String getClientGender() {
        return clientGender;
    }

    public void setClientGender(String clientGender) {
        this.clientGender = clientGender;
    }

    public String getClientJoinDate() {
        return clientJoinDate;
    }

    public void setClientJoinDate(String clientJoinDate) {
        this.clientJoinDate = clientJoinDate;
    }

    public String getClientProfilePic() {
        return clientProfilePic;
    }

    public void setClientProfilePic(String clientProfilePic) {
        this.clientProfilePic = clientProfilePic;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getRoleAID() {
        return roleAID;
    }

    public void setRoleAID(String roleAID) {
        this.roleAID = roleAID;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.displayName);
        dest.writeString(this.userEmail);
        dest.writeString(this.userID);
        dest.writeString(this.roleAID);
        dest.writeString(this.imageName);
        dest.writeString(this.userType);
        dest.writeString(this.clientFName);
        dest.writeString(this.clientID);
        dest.writeString(this.clientLName);
        dest.writeString(this.clientEmail);
        dest.writeString(this.clientMobile);
        dest.writeString(this.clientDOB);
        dest.writeString(this.clientGender);
        dest.writeString(this.clientJoinDate);
        dest.writeString(this.clientProfilePic);
        dest.writeString(this.userImage);
        dest.writeString(this.userName);
        dest.writeString(this.pkgStartDate);
        dest.writeString(this.pkgEndDate);
        dest.writeString(this.getSessionAllottedTime);
        dest.writeString(this.packageID);
        dest.writeString(this.pkgDetailsID);
        dest.writeString(this.isSessionAlloted);
        dest.writeString(this.termsConditionsUrl);
        dest.writeString(this.disclaimerUrl);
        dest.writeStringList(this.shiftedSessionDateList);
    }

    protected User(Parcel in) {
        this.displayName = in.readString();
        this.userEmail = in.readString();
        this.userID = in.readString();
        this.roleAID = in.readString();
        this.imageName = in.readString();
        this.userType = in.readString();
        this.clientFName = in.readString();
        this.clientID = in.readString();
        this.clientLName = in.readString();
        this.clientEmail = in.readString();
        this.clientMobile = in.readString();
        this.clientDOB = in.readString();
        this.clientGender = in.readString();
        this.clientJoinDate = in.readString();
        this.clientProfilePic = in.readString();
        this.userImage = in.readString();
        this.userName = in.readString();
        this.pkgStartDate = in.readString();
        this.pkgEndDate = in.readString();
        this.getSessionAllottedTime = in.readString();
        this.packageID = in.readString();
        this.pkgDetailsID = in.readString();
        this.isSessionAlloted = in.readString();
        this.termsConditionsUrl = in.readString();
        this.disclaimerUrl = in.readString();
        this.shiftedSessionDateList = in.createStringArrayList();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
