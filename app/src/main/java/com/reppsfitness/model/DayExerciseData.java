package com.reppsfitness.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mayur Gangurde on 16/10/17.
 */

public class DayExerciseData implements Parcelable {
    @SerializedName("data")
    @Expose
    public List<Exercise> exerciseList = new ArrayList<>();
    @SerializedName("msg")
    @Expose
    public String msg;
    @SerializedName("count")
    @Expose
    public long count;

    public List<Exercise> getExerciseList() {
        return exerciseList;
    }

    public void setExerciseList(List<Exercise> exerciseList) {
        this.exerciseList = exerciseList;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public DayExerciseData() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.exerciseList);
        dest.writeString(this.msg);
        dest.writeLong(this.count);
    }

    protected DayExerciseData(Parcel in) {
        this.exerciseList = in.createTypedArrayList(Exercise.CREATOR);
        this.msg = in.readString();
        this.count = in.readLong();
    }

    public static final Creator<DayExerciseData> CREATOR = new Creator<DayExerciseData>() {
        @Override
        public DayExerciseData createFromParcel(Parcel source) {
            return new DayExerciseData(source);
        }

        @Override
        public DayExerciseData[] newArray(int size) {
            return new DayExerciseData[size];
        }
    };
}
