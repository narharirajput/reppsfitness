package com.reppsfitness.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by MXCPUU11 on 11/3/2017.
 */

public class AddBmiData implements Serializable {

    @SerializedName("clientBMIID")
    @Expose
    private Integer clientBMIID;
    @SerializedName("BMIDate")
    @Expose
    private String bMIDate;
    private final static long serialVersionUID = 1198631664139486885L;

    public Integer getClientBMIID() {
        return clientBMIID;
    }

    public void setClientBMIID(Integer clientBMIID) {
        this.clientBMIID = clientBMIID;
    }

    public String getBMIDate() {
        return bMIDate;
    }

    public void setBMIDate(String bMIDate) {
        this.bMIDate = bMIDate;
    }

}