package com.reppsfitness;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.reppsfitness.fragment.AdminMessageFragment;
import com.reppsfitness.fragment.ClientListFragment;
import com.reppsfitness.fragment.PackagePlansFragment;
import com.reppsfitness.fragment.TemplateFragment;
import com.reppsfitness.session_cal.SessionsFragment;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;

public class AdminMainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_view)
    public MaterialSearchView searchView;
    @BindView(R.id.fragment_container)
    FrameLayout fragmentContainer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.btn_templates)
    Button btnTemplates;
    @BindView(R.id.btn_packages)
    Button btnPackages;
    @BindDrawable(R.drawable.no_image)
    Drawable no_image;
    @BindView(R.id.ll_tabs)
    LinearLayout llTabs;
    @BindView(R.id.btn_clients)
    Button btnClients;
    @BindView(R.id.btn_notifications)
    Button btn_notifications;
    public MenuItem actionSearch, actionDelete;
    public int clientFlag, notificationFlag, templateFlag, packageFlag;
    public TemplateFragment templateFragment;
    public Menu menu;
    Fragment fragmentA;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setSupportActionBar (toolbar);
        getSupportActionBar ().setDisplayShowTitleEnabled (false);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (false);
        getSupportActionBar ().setHomeAsUpIndicator (R.drawable.ic_menu_white_24dp);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle (this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener (toggle);
        toggle.syncState ();
        navigationView.setNavigationItemSelectedListener (this);
        Utils.replaceFragment (AdminMainActivity.this, new ClientListFragment (), true);
        navigationView.setCheckedItem (R.id.nav_home);
        clientFlag = 1;
        btnBackground ();
        searchView.setVoiceSearch (false);
        searchView.setCursorDrawable (R.drawable.custom_cursor);
        searchView.setVisibility (View.VISIBLE);
        toolbarTitle.setText ("Clients");

        Log.e (TAG, "onCreate: prioritys ");
        tintViewDrawable (btnTemplates);
        tintViewDrawable (btnPackages);
        TextView tvName = navigationView.getHeaderView (0).findViewById (R.id.tv_name);
        ImageView ivProfile = navigationView.getHeaderView (0).findViewById (R.id.iv_profile);
        tvName.setText (app.getPreferences ().getLoggedInUser ().getData ().getDisplayName ());
        if (!TextUtils.isEmpty (app.getPreferences ().getLoggedInUser ().getData ().getImageName ()))
            Glide.with (mContext).load (app.getPreferences ().getLoggedInUser ().getData ().getImageName ()).into (ivProfile);
        if (getIntent ().getBooleanExtra ("isFromNotification", false)) {
            //this.actionSearch.setVisible(false);
            Utils.replaceFragment (AdminMainActivity.this, new AdminMessageFragment (), true);
            toolbarTitle.setText ("Notifications");
            llTabs.setVisibility (View.VISIBLE);
            navigationView.setCheckedItem (R.id.nav_notifications);
            notificationFlag=1;
            btnBackground ();
        }
    }

    @Override
    protected void onStart() {
        super.onStart ();
        Log.e (TAG, "onStart: prioritys");
    }

    @Override
    protected void onResume() {
        super.onResume ();
        Log.e (TAG, "onResume: opened prioritys");
        try {
            TextView tvName = navigationView.getHeaderView (0).findViewById (R.id.tv_name);
            ImageView ivProfile = navigationView.getHeaderView (0).findViewById (R.id.iv_profile);
            tvName.setText (app.getPreferences ().getLoggedInUser ().getData ().getDisplayName ());
            if (!TextUtils.isEmpty (app.getPreferences ().getLoggedInUser ().getData ().getImageName ()))
                Glide.with (mContext).load (app.getPreferences ().getLoggedInUser ().getData ().getImageName ()).into (ivProfile);
        } catch (Exception e) {
            e.printStackTrace ();
        }
    }

    private void tintViewDrawable(Button view) {
        Drawable[] drawables = view.getCompoundDrawables ();
        for (Drawable drawable : drawables) {
            if (drawable != null) {
                drawable.mutate ();
                drawable.setColorFilter (Color.WHITE, PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_admin_main;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d (TAG, "onCreateOptionsMenu: prioritys");
        getMenuInflater ().inflate (R.menu.menu_search, menu);
        this.menu = menu;
        checkMenu (this.menu);
        return true;
    }

    public void checkMenu(Menu menu) {
        if (menu != null) {
            actionSearch = menu.findItem (R.id.action_search);
            searchView.setMenuItem (actionSearch);
            actionDelete = menu.findItem (R.id.action_delete);

            Fragment fragment = getSupportFragmentManager ().findFragmentById (R.id.fragment_container);
            if (fragment instanceof AdminMessageFragment) {
                actionSearch.setVisible (false);
                actionDelete.setOnMenuItemClickListener (item -> {
                    List<String> notificationIds = new ArrayList<> ();
                    for (int i = 0; i < ((AdminMessageFragment) fragment).multiselect_list.size (); i++) {
                        notificationIds.add (((AdminMessageFragment) fragment).multiselect_list.get (i).getNotificationID ());
                    }
                    String str_notificationIDs = notificationIds.toString ();
                    str_notificationIDs = str_notificationIDs.replace ("[", "");
                    str_notificationIDs = str_notificationIDs.replace ("]", "");
                    ((AdminMessageFragment) fragment).clearNotifications (str_notificationIDs);
                    Log.e ("multiselect_list", new Gson ().toJson (((AdminMessageFragment) fragment).multiselect_list));
                    return super.onOptionsItemSelected (((AdminMainActivity) mContext).actionDelete);
                });
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen ()) {
            searchView.closeSearch ();
        } else {
            if (drawer.isDrawerOpen (GravityCompat.START)) {
                drawer.closeDrawer (GravityCompat.START);
            } else {
                Utils.closeApp (mContext);
            }
        }
    }

    @OnClick({R.id.btn_clients, R.id.btn_notifications, R.id.btn_templates, R.id.btn_packages})
    public void onViewClicked(View view) {
        fragmentA = getSupportFragmentManager ().findFragmentByTag("MyFragment");
        switch (view.getId ()) {
            case R.id.btn_clients:
                if (!(fragmentA instanceof  ClientListFragment)) {
                    navigationView.setCheckedItem (R.id.nav_home);
                    if (actionSearch != null) actionSearch.setVisible (true);
                    if (actionDelete != null) actionDelete.setVisible (false);

                    Utils.replaceFragment (AdminMainActivity.this, new ClientListFragment (), true);
                    clientFlag = 1;
                    btnBackground ();
                    toolbarTitle.setText ("Clients");
                }
                break;
            case R.id.btn_notifications:
                if (!(fragmentA instanceof  AdminMessageFragment)) {
                    navigationView.setCheckedItem (R.id.nav_notifications);
                    if (actionSearch != null) actionSearch.setVisible (false);
                    if (actionDelete != null) actionDelete.setVisible (false);
                    Utils.replaceFragment (AdminMainActivity.this, new AdminMessageFragment (), true);
                    notificationFlag = 1;
                    btnBackground ();
                    toolbarTitle.setText ("Notifications");
                }
                break;
            case R.id.btn_templates:
                if (!(fragmentA instanceof  TemplateFragment)) {
                    navigationView.setCheckedItem (R.id.nav_template);
                    if (actionSearch != null) actionSearch.setVisible (true);
                    if (actionDelete != null) actionDelete.setVisible (false);
                    templateFragment = new TemplateFragment ();
                    Utils.replaceFragment (AdminMainActivity.this, templateFragment, true);
                    templateFlag = 1;
                    btnBackground ();
                    toolbarTitle.setText ("Workout Templates");
                }
                break;
            case R.id.btn_packages:
                if (!(fragmentA instanceof  PackagePlansFragment)) {
                    navigationView.setCheckedItem (R.id.nav_packages);
                    if (actionSearch != null) actionSearch.setVisible (false);
                    if (actionDelete != null) actionDelete.setVisible (false);
                    Utils.replaceFragment (AdminMainActivity.this, new PackagePlansFragment (), true);
                    packageFlag = 1;
                    btnBackground ();
                    toolbarTitle.setText ("Packages");
                }
                break;
        }
        if (searchView.isSearchOpen ()) {
            searchView.closeSearch ();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        fragmentA = getSupportFragmentManager ().findFragmentByTag("MyFragment");
        int id = item.getItemId ();
        if (id == R.id.nav_home) {
            if (!(fragmentA instanceof  ClientListFragment)) {
                this.actionSearch.setVisible (true);
                this.actionDelete.setVisible (false);
                Utils.replaceFragment (AdminMainActivity.this, new ClientListFragment (), true);
                clientFlag = 1;
                btnBackground ();
                toolbarTitle.setText ("Clients");
                llTabs.setVisibility (View.VISIBLE);
            }
        } else if (id == R.id.nav_template) {
            if (!(fragmentA instanceof  TemplateFragment)) {
                this.actionSearch.setVisible (true);
                this.actionDelete.setVisible (false);
                Utils.replaceFragment (AdminMainActivity.this, new TemplateFragment (), true);
                templateFlag = 1;
                btnBackground ();
                toolbarTitle.setText ("Workout Templates");
                llTabs.setVisibility (View.VISIBLE);
            }
        } else if (id == R.id.nav_notifications) {
            if (!(fragmentA instanceof  AdminMessageFragment)) {
                this.actionSearch.setVisible (false);
                Utils.replaceFragment (AdminMainActivity.this, new AdminMessageFragment (), true);
                notificationFlag = 1;
                btnBackground ();
                toolbarTitle.setText ("Notifications");
                llTabs.setVisibility (View.VISIBLE);
            }
        } else if (id == R.id.nav_settings) {
            startActivity (new Intent (mContext, AdminSettingsActivity.class));
        } else if (id == R.id.nav_session_request) {
            startActivity (new Intent (mContext, SessionRequestsActivity.class));
        } else if (id == R.id.nav_schedule) {
            if (!(fragmentA instanceof  SessionsFragment)) {
                this.actionSearch.setVisible (false);
                this.actionDelete.setVisible (false);
                Utils.replaceFragment (AdminMainActivity.this, new SessionsFragment (), true);
                toolbarTitle.setText ("SCHEDULE");
                llTabs.setVisibility (View.GONE);
            }
        } else if (id == R.id.nav_packages) {
            if (!(fragmentA instanceof  PackagePlansFragment)) {
                this.actionSearch.setVisible (false);
                this.actionDelete.setVisible (false);
                Utils.replaceFragment (AdminMainActivity.this, new PackagePlansFragment (), true);
                packageFlag = 1;
                btnBackground ();
                toolbarTitle.setText ("Packages");
            }
        } else if (id == R.id.nav_logout) {
           logout ();
        }
        if (searchView.isSearchOpen ()) {
            searchView.closeSearch ();
        }
        drawer.closeDrawer (GravityCompat.START);
        return true;
    }

    public void logout() {
        final Dialog dialog = new Dialog(mContext, android.R.style.Theme_Dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_logout);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        dialog.findViewById(R.id.btn_ok).setOnClickListener(view -> {
            app.getPreferences ().logOutUser ();
            startActivity (new Intent (mContext, LoginActivity.class));
            NotificationManager notificationManager = (NotificationManager) getSystemService (Context.
                    NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.cancelAll ();
            }

            dialog.dismiss();
            ((Activity) mContext).finish();

        });
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    private void btnBackground() {
        if (clientFlag == 1) {
            clientFlag = 0;
            btnClients.setBackgroundResource (R.drawable.after_clicked_color);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btnTemplates.setBackgroundResource (R.drawable.btn_clicked_default);
            btnPackages.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (notificationFlag == 1) {
            notificationFlag = 0;
            btnClients.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.after_clicked_color);
            btnTemplates.setBackgroundResource (R.drawable.btn_clicked_default);
            btnPackages.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (templateFlag == 1) {
            templateFlag = 0;
            btnClients.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btnTemplates.setBackgroundResource (R.drawable.after_clicked_color);
            btnPackages.setBackgroundResource (R.drawable.btn_clicked_default);
        } else if (packageFlag == 1) {
            packageFlag = 0;
            btnClients.setBackgroundResource (R.drawable.btn_clicked_default);
            btn_notifications.setBackgroundResource (R.drawable.btn_clicked_default);
            btnTemplates.setBackgroundResource (R.drawable.btn_clicked_default);
            btnPackages.setBackgroundResource (R.drawable.after_clicked_color);
        }
    }
}
