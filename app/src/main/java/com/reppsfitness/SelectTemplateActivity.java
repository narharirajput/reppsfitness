package com.reppsfitness;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.reppsfitness.adapter.TemplateAdapter;
import com.reppsfitness.model.Template;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.DialogUtils;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 8/18/2017.
 */

public class SelectTemplateActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView categoryRecyclerView;
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.search_view)
    public MaterialSearchView searchView;
    @BindView(R.id.tv_error)
    public TextView tv_error;
    public User user;
    public static SelectTemplateActivity selectTemplateActivity;
    private MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        selectTemplateActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        user = getIntent().getParcelableExtra("user");
        toolbarTitle.setText("Templates");
        searchView.setVoiceSearch(false);
        searchView.setCursorDrawable(R.drawable.custom_cursor);
        searchView.setVisibility(View.VISIBLE);
        categoryRecyclerView.setHasFixedSize(true);
        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        getTemplateList();
    }

    private void getTemplateList() {
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getTemplateList("getTemplateList")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(templateData -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "success");
                        if (templateData != null) {
                            if (templateData.getCount() > 0) {
                                if (templateData.getTemplateList() != null && templateData.getTemplateList().size() > 0) {
                                    List<Template> templateList = new ArrayList<>();
                                    templateList.addAll(templateData.getTemplateList());
                                    TemplateAdapter templateAdapter = new TemplateAdapter(mContext, templateList);
                                    categoryRecyclerView.setAdapter(templateAdapter);
                                    setSearchFilter(templateAdapter);
                                    searchView.setVisibility(View.VISIBLE);
                                } else {
                                    searchView.setVisibility(View.GONE);
                                }
                            } else {
                                if (templateData.getMsg() != null && !TextUtils.isEmpty(templateData.getMsg())) {
                                    Utils.showLongToast(mContext, templateData.getMsg());
                                }
                                searchView.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                            searchView.setVisibility(View.GONE);
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                        searchView.setVisibility(View.GONE);
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_template;
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        String title = "Add Template";
        String negativeButton = "CANCEL";
        String positiveButton = "SUBMIT";

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_create_template, null);
        EditText et_templateName = customView.findViewById(R.id.et_templateName);
        EditText et_noOfDays = customView.findViewById(R.id.et_noOfDays);

        final Dialog myDialog = DialogUtils.createCustomDialog(mContext, title, customView,
                negativeButton, positiveButton, false, new DialogUtils.DialogListener() {
                    @Override
                    public void onPositiveButton(Dialog dialog) {
                        String templateName, noOfDays;
                        templateName = et_templateName.getText().toString().trim();
                        noOfDays = et_noOfDays.getText().toString().trim();
                        if (TextUtils.isEmpty(templateName)) {
                            et_templateName.setError("Enter Template Name");
                            et_templateName.requestFocus();
                            return;
                        }
                        if (TextUtils.isEmpty(noOfDays)) {
                            et_noOfDays.setError("Enter Number of Days");
                            et_noOfDays.requestFocus();
                            return;
                        }
                        saveTemplate(templateName, noOfDays, dialog);
                    }

                    @Override
                    public void onNegativeButton() {
                    }
                });

        if (myDialog != null && !myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void saveTemplate(String templateName, String noOfDays, Dialog dialog) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "saveTemplate");
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("noOfDays", noOfDays);
        params.put("templateName", templateName);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            CompositeDisposable mCompositeDisposable = new CompositeDisposable();
            mCompositeDisposable.add(app.getApiService().saveTemplate(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (dialog.isShowing()) dialog.dismiss();
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null) {
                            if (response.getCount() > 0) {
                                if (response.getMsg() != null)
                                    Utils.showShortToast(mContext, response.getMsg());
                                getTemplateList();
                            }
                        }
                    }, throwable -> {
                        if (dialog.isShowing()) dialog.dismiss();
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        }
        finish();
    }

    private void setSearchFilter(final TemplateAdapter templateAdapter) {
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Utils.hideSoftKeyboard(SelectTemplateActivity.this);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (!TextUtils.isEmpty(newText)) {
                    templateAdapter.filter(newText);
                } else {
                    templateAdapter.filter("");
                }
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
