package com.reppsfitness;


import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.reppsfitness.fragment.NewRequestFragment;
import com.reppsfitness.fragment.SentRequestsFragment;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RequestSessionActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Shift Session");
        Utils.replaceFragment(RequestSessionActivity.this, new NewRequestFragment(), true);
        getSentRequests();
        //getPhoneName();
    }

    public String getPhoneName() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        String deviceName = myDevice.getName();
        Log.e("TAG", "getPhoneName: "+deviceName);
        return deviceName;
    }

    @OnClick({R.id.btn_newRequest, R.id.btn_sentRequests})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_newRequest:
                displayNewRequestFrag();
                break;
            case R.id.btn_sentRequests:
                displaySentRequestFrag();
                break;
        }
    }

    private void displayNewRequestFrag() {
        Utils.replaceFragment(RequestSessionActivity.this, new NewRequestFragment(), true);
        toolbarTitle.setText("Shift Session");
    }

    public void displaySentRequestFrag() {
        Utils.replaceFragment(RequestSessionActivity.this, new SentRequestsFragment(), true);
        toolbarTitle.setText("Sent Requests");
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_request_session;
    }

    private void getSentRequests() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getClientSessionRequest");
        map.put("clientID", app.getPreferences().getLoggedInUser().getData().getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().getClientSessionRequest(map)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(sentRequestData -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (sentRequestData != null) {
                            if (sentRequestData.getCount() > 0) {
//                                Utils.showLongToast(mContext, sentRequestData.getMsg());
                                if (sentRequestData.getData() != null && sentRequestData.getData().size() > 0) {
                                    displaySentRequestFrag();
                                } else {
                                    displayNewRequestFrag();
                                }
                            } else {
                                displayNewRequestFrag();
                            }
                        } else {
                            displayNewRequestFrag();
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                        displayNewRequestFrag();
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
