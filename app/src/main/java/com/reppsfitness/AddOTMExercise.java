package com.reppsfitness;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.flexbox.FlexboxLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.exercise.model.Exercise;
import com.reppsfitness.exercise.model.Workout;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.Element;
import com.reppsfitness.model.ExerciseCategory;
import com.reppsfitness.model.ExerciseData;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.reppsfitness.widget.searchable_spinner.SpinnerDialog;
import com.reppsfitness.widget.swipelayout.SwipeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by MXCPUU11 on 8/18/2017.
 */

public class AddOTMExercise extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_time)
    TextView tvTime;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private List<ExerciseCategory> exerciseCategories;
    ArrayList<String> workoutCatNameList = new ArrayList<>();
    List<Element> elementList = new ArrayList<>();
    ArrayList<String> timeList = new ArrayList<>();
    String time, workoutName, workoutTypeID;
    Day day;
    User user;
    Workout workout;
    private ArrayList<Exercise> exerciseList = new ArrayList<>();
    boolean isTemplate;
    String templateID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("OTM Workout");
        time = getIntent().getStringExtra("time");
        workoutName = getIntent().getStringExtra("workoutName");
        workoutTypeID = getIntent().getStringExtra("workoutTypeID");
        day = getIntent().getParcelableExtra("day");
        user = getIntent().getParcelableExtra("user");
        workout = getIntent().getParcelableExtra("workout");
        isTemplate = getIntent().getBooleanExtra("isTemplate", false);
        templateID = getIntent().getStringExtra("templateID");
        ArrayList<String> timeArrayList = getIntent().getStringArrayListExtra("timeList");
        tvTime.setText(time);
//        Log.e("val", isTemplate + "||" + templateID);
//        for (int i = 1; i <= Integer.parseInt(time); i++) {
//            timeList.add(i + " Min");
//        }
        for (int i = 0; i < timeArrayList.size(); i++) {
            timeList.add(timeArrayList.get(i));
            if (timeArrayList.get(i).equalsIgnoreCase(time)) {
                break;
            }
        }
        fab.setVisibility(app.getPreferences().isClient() ? View.GONE : View.VISIBLE);
        if (workout != null && workout.getWorkoutDetails() != null && workout.getWorkoutDetails().size() > 0) {
            exerciseList.addAll(workout.getWorkoutDetails());
        }
        getWorkoutExerciseElements();
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.add_exercise_otm;
    }

    private void getWorkoutExerciseElements() {
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getWorkoutExerciseElementsNew("getWorkoutExerciseElementsNew", new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    ExerciseData exerciseData = (ExerciseData) object;
                    if (exerciseData != null) {
                        if (exerciseData.getCount() > 0) {
                            exerciseCategories = exerciseData.getExerciseCategories();
                            elementList = exerciseData.getElementList();
                            for (int i = 0; i < exerciseCategories.size(); i++) {
                                workoutCatNameList.add(exerciseCategories.get(i).getExerciseCatName());
                            }
                            displayExcerciseList();
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        addExerciseView();
    }

    private void addExerciseView() {
        View rowView = getLayoutInflater().inflate(R.layout.view_otm, null);
        int id = new Random().nextInt(10000);
        final SwipeLayout swipeLayout = rowView.findViewById(R.id.swipeLayout);
        swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
        final EditText etSelectExercise = rowView.findViewById(R.id.et_exercise);
        final EditText et_category = rowView.findViewById(R.id.et_category);
        final FlexboxLayout fl_elements = rowView.findViewById(R.id.fl_elements);
        final RelativeLayout rr_selectExercise = rowView.findViewById(R.id.rr_exercise);
        final EditText et_minute = rowView.findViewById(R.id.et_minute);
        et_minute.setOnClickListener(view1 -> {
            if (!app.getPreferences().isClient()) {
                SpinnerDialog elementDialog = new SpinnerDialog(AddOTMExercise.this, timeList,
                        "Select Time", R.style.DialogAnimations_SmileWindow, true);
                elementDialog.showSpinerDialog();
                elementDialog.bindOnSpinerListener((item1, position1) -> {
                    et_minute.setText(item1);
                });
            }
        });
        swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
            repeatExerciseView(id);
            swipeLayout.close();
        });
        swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
            deleteExerciseView(id);
            swipeLayout.close();
        });
        if (elementList != null && elementList.size() > 0) {
            addFlexElements(fl_elements);
        }
        if (exerciseCategories != null && exerciseCategories.size() > 0) {
            ArrayList<String> exerciseNameList = new ArrayList<>();
            ArrayList<Exercise> exerciseList = new ArrayList<>();
            et_category.setOnClickListener(view -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog(AddOTMExercise.this, workoutCatNameList,
                            "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                    spinnerDialog.showSpinerDialog();
                    spinnerDialog.bindOnSpinerListener((item, position) -> {
                        et_category.setText(item);
                        etSelectExercise.setText("");
                        et_category.setTag(exerciseCategories.get(position).getExerciseCatID());
                        rr_selectExercise.setVisibility(View.VISIBLE);
                        exerciseList.clear();
                        exerciseNameList.clear();
                        if (exerciseCategories.get(position).getExercises().size() > 0) {
                            ExerciseCategory exerciseCat = exerciseCategories.get(position);
                            for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                                exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                                exerciseList.addAll(exerciseCat.getExercises());
                            }
                        }
                    });
                }
            });
            etSelectExercise.setOnClickListener(view -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog exerciseDialog = new SpinnerDialog(AddOTMExercise.this, exerciseNameList,
                            "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                    exerciseDialog.showSpinerDialog();
                    exerciseDialog.bindOnSpinerListener((item, position) -> {
                        etSelectExercise.setText(item);
                        etSelectExercise.setTag(exerciseList.get(position).getExerciseID());
                    });
                }
            });
        }
        rowView.setTag(id);
        llContent.addView(rowView, llContent.getChildCount());
    }

    private void addFlexElements(FlexboxLayout fl_elements) {
        fl_elements.removeAllViews();
        for (int i = 0; i < elementList.size(); i++) {
            Element element = elementList.get(i);
            RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
            rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                    Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
            rr_element_view.setLayoutParams(layoutParams);
            EditText et_element = rr_element_view.findViewById(R.id.et_element);
            et_element.setHint(element.getWorkoutElementName());
            et_element.setTag(element.getWorkoutElementID());
            ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
            et_element.setOnClickListener(view1 -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog elementDialog = new SpinnerDialog(AddOTMExercise.this, workoutElementValue,
                            "Select " + element.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                    elementDialog.showSpinerDialog();
                    elementDialog.bindOnSpinerListener((item1, position1) -> {
                        et_element.setText(item1);
                    });
                }
            });
            fl_elements.addView(rr_element_view, fl_elements.getChildCount());
        }
    }

    private void getAddedExerciseData() {
        List<com.reppsfitness.model.schedule.Exercise> exerciseList = new ArrayList<>();
        for (int i = 0; i < llContent.getChildCount(); i++) {
            com.reppsfitness.model.schedule.Exercise exercise = new com.reppsfitness.model.schedule.Exercise();
            View childAt = llContent.getChildAt(i);
            final EditText etSelectExercise = childAt.findViewById(R.id.et_exercise);
            final EditText et_category = childAt.findViewById(R.id.et_category);
            final EditText et_minute = childAt.findViewById(R.id.et_minute);
            final FlexboxLayout fl_elements = childAt.findViewById(R.id.fl_elements);
            if (et_minute.getText() == null || TextUtils.isEmpty(et_minute.getText().toString())) {
                new MaterialDialog.Builder(mContext).content("Please select time").positiveText("Ok").show();
                return;
            }
            if (et_category.getText() == null || TextUtils.isEmpty(et_category.getText().toString())) {
                new MaterialDialog.Builder(mContext).content("Please select exercise category").positiveText("Ok").show();
                return;
            }
            if (etSelectExercise.getText() == null || TextUtils.isEmpty(etSelectExercise.getText().toString())) {
                new MaterialDialog.Builder(mContext).content("Please select exercise").positiveText("Ok").show();
                return;
            }
            String selectedExercise = etSelectExercise.getText() != null ? etSelectExercise.getText().toString() : "";
            String selectedExerciseID = etSelectExercise.getTag() != null ? (String) etSelectExercise.getTag() : "";
            String selectedCategory = et_category.getText() != null ? et_category.getText().toString() : "";
            String selectedCategoryID = et_category.getTag() != null ? (String) et_category.getTag() : "";
            String selectedTime = et_minute.getText().toString();
            exercise.setExerciseName(selectedExercise);
            exercise.setExerciseID(selectedExerciseID);
            exercise.setWorkoutName(workoutName);
            exercise.setWorkoutTypeID(workoutTypeID);
            exercise.setExerciseCatName(selectedCategory);
            exercise.setExerciseCatID(selectedCategoryID);
            exercise.setExerciseValue(selectedTime);
            exercise.setWorkoutTypeValue(time);
            if (childAt.getTag() != null) {
                if (isTemplate) {
                    if (childAt.getTag() instanceof String) {
                        exercise.setTemplateEID((String) childAt.getTag());
                    } else {
                        exercise.setTemplateEID("");
                    }
                } else {
                    if (childAt.getTag() instanceof String) {
                        exercise.setScheduleEID((String) childAt.getTag());
                    } else {
                        exercise.setScheduleEID("");
                    }
                }
            }
            List<com.reppsfitness.model.schedule.Element> elementList = new ArrayList<>();
            for (int i1 = 0; i1 < fl_elements.getChildCount(); i1++) {
                View childAt1 = fl_elements.getChildAt(i1);
                EditText et_element = childAt1.findViewById(R.id.et_element);
                String selectedElementValue = et_element.getText() != null ? et_element.getText().toString() : "";
                String selectedElementID = et_element.getTag() != null ? (String) et_element.getTag() : "";
                String elementName = et_element.getHint() != null ? et_element.getHint().toString() : "";
                com.reppsfitness.model.schedule.Element element = new com.reppsfitness.model.schedule.Element();
                element.setWorkoutElementID(selectedElementID);
                element.setWorkoutElementName(elementName);
                element.setWorkoutElementValue(selectedElementValue);
                if (!TextUtils.isEmpty(selectedElementValue))
                    elementList.add(element);
            }
            boolean isElementAdded = false;
            for (int j = 0; j < elementList.size(); j++) {
                if (!TextUtils.isEmpty(elementList.get(j).getWorkoutElementValue()))
                    isElementAdded = true;
            }
            if (!isElementAdded) {
                new MaterialDialog.Builder(mContext).content("Please select workout element values").positiveText("Ok").show();
                return;
            }
            exercise.setWorkoutElements(elementList);
            if (!TextUtils.isEmpty(selectedExercise) && !TextUtils.isEmpty(selectedTime))
                exerciseList.add(exercise);
        }
        if (exerciseList.size() > 0) {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(exerciseList);
            Log.e("json", json);
            saveDayExercise(json);
        } else {
            new MaterialDialog.Builder(mContext)
                    .content("Please select exercise first")
                    .positiveText("Ok")
                    .show();
        }
    }

    private void deleteExerciseView(Object deletePositionID) {
        if (deletePositionID != null) {
            Log.e("deletePositionID", "" + deletePositionID);
            if (deletePositionID instanceof String) {
                String getScheduleEID = (String) deletePositionID;
                deleteScheduleExercise(getScheduleEID);
            } else if (deletePositionID instanceof Integer) {
                for (int i = 0; i < llContent.getChildCount(); i++) {
                    if (llContent.getChildAt(i).getTag() instanceof Integer
                            && (int) llContent.getChildAt(i).getTag() == (int) deletePositionID) {
                        llContent.removeViewAt(i);
                        break;
                    }
                }
            }
        }
    }

    private void repeatExerciseView(Object repeatPositionID) {
        View rowView = getLayoutInflater().inflate(R.layout.view_otm, null);
        int id = new Random().nextInt(10000);
        final SwipeLayout swipeLayout = rowView.findViewById(R.id.swipeLayout);
        swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
        final EditText etSelectExercise = rowView.findViewById(R.id.et_exercise);
        final EditText et_category = rowView.findViewById(R.id.et_category);
        final EditText et_minute = rowView.findViewById(R.id.et_minute);
        final FlexboxLayout fl_elements = rowView.findViewById(R.id.fl_elements);
        final RelativeLayout rr_selectExercise = rowView.findViewById(R.id.rr_exercise);
        swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
            repeatExerciseView(id);
            swipeLayout.close();
        });
        swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
            deleteExerciseView(id);
            swipeLayout.close();
        });
        View lastChild = null;
        if (repeatPositionID instanceof String) {
            String scheduleDetailsID = (String) repeatPositionID;
            for (int i = 0; i < llContent.getChildCount(); i++) {
                if (llContent.getChildAt(i).getTag() instanceof String
                        && llContent.getChildAt(i).getTag().equals(scheduleDetailsID)) {
                    lastChild = llContent.getChildAt(i);
                    break;
                }
            }
        } else if (repeatPositionID instanceof Integer) {
            for (int i = 0; i < llContent.getChildCount(); i++) {
                if (llContent.getChildAt(i).getTag() instanceof Integer
                        && (int) llContent.getChildAt(i).getTag() == (int) repeatPositionID) {
                    lastChild = llContent.getChildAt(i);
                    break;
                }
            }
        }
        final EditText etSelectExerciseLast = lastChild.findViewById(R.id.et_exercise);
        final EditText et_categoryLast = lastChild.findViewById(R.id.et_category);
        final EditText et_minuteLast = lastChild.findViewById(R.id.et_minute);
        et_minute.setText(et_minuteLast.getText() != null ? et_minuteLast.getText().toString() : "");
        FlexboxLayout fl_elementsLast = lastChild.findViewById(R.id.fl_elements);
        if (fl_elementsLast.getChildCount() > 0) {
//                swipeLayout.setSwipeEnabled(true);
            if (elementList != null && elementList.size() > 0) {
                for (int i = 0; i < fl_elementsLast.getChildCount(); i++) {
                    View elementView = fl_elementsLast.getChildAt(i);
                    RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                    rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                            Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                    rr_element_view.setLayoutParams(layoutParams);
                    EditText et_element = rr_element_view.findViewById(R.id.et_element);
                    EditText et_elementLast = elementView.findViewById(R.id.et_element);
                    if (et_elementLast != null && et_elementLast.getText() != null)
                        et_element.setText(et_elementLast.getText());
                    if (et_elementLast != null && et_elementLast.getHint() != null)
                        et_element.setHint(et_elementLast.getHint());
                    if (et_elementLast != null && et_elementLast.getTag() != null) {
                        String workoutElementIDLast = (String) et_elementLast.getTag();
                        et_element.setTag(workoutElementIDLast);
                        Element element = null;
                        for (int i1 = 0; i1 < elementList.size(); i1++) {
                            if (elementList.get(i1).getWorkoutElementID().matches(workoutElementIDLast)) {
                                element = elementList.get(i1);
                                break;
                            }
                        }
                        if (element != null && element.getWorkoutElementValue() != null && element.getWorkoutElementValue().size() > 0) {
                            ArrayList<String> workoutElementValue = element.getWorkoutElementValue();
                            Element finalElement = element;
                            et_element.setOnClickListener(view1 -> {
                                if (!app.getPreferences().isClient()) {
                                    SpinnerDialog elementDialog = new SpinnerDialog(AddOTMExercise.this, workoutElementValue,
                                            "Select " + finalElement.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                                    elementDialog.showSpinerDialog();
                                    elementDialog.bindOnSpinerListener((item1, position1) -> {
                                        et_element.setText(item1);
                                    });
                                }
                            });
                        }
                    }
                    fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                }
            }
        }
        if (workoutCatNameList != null && workoutCatNameList.size() > 0) {
            ArrayList<String> exerciseNameList = new ArrayList<>();
            ArrayList<Exercise> exerciseList = new ArrayList<>();
            et_category.setOnClickListener(view -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog spinnerDialog = new SpinnerDialog(AddOTMExercise.this, workoutCatNameList,
                            "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                    spinnerDialog.showSpinerDialog();
                    spinnerDialog.bindOnSpinerListener((item, position) -> {
                        et_category.setText(item);
                        etSelectExercise.setText("");
                        et_category.setTag(exerciseCategories.get(position).getExerciseCatID());
                        rr_selectExercise.setVisibility(View.VISIBLE);
                        exerciseList.clear();
                        exerciseNameList.clear();
                        if (exerciseCategories.get(position).getExercises().size() > 0) {
                            ExerciseCategory exerciseCategory = exerciseCategories.get(position);
                            for (int i = 0; i < exerciseCategory.getExercises().size(); i++) {
                                exerciseNameList.add(exerciseCategory.getExercises().get(i).getExerciseName());
                                exerciseList.addAll(exerciseCategory.getExercises());
                            }
                        }
                    });
                }
            });
            etSelectExercise.setText(etSelectExerciseLast.getText() != null ? etSelectExerciseLast.getText().toString() : "");
            et_category.setText(et_categoryLast.getText() != null ? et_categoryLast.getText().toString() : "");
            et_category.setTag(et_categoryLast.getTag() != null ? et_categoryLast.getTag().toString() : "");
            String exerciseCatIDLast = "";
            if (etSelectExerciseLast.getTag() != null) {
                exerciseCatIDLast = (String) etSelectExerciseLast.getTag();
                etSelectExercise.setTag(etSelectExerciseLast.getTag() != null ? etSelectExerciseLast.getTag().toString() : "");
                if (etSelectExerciseLast.getVisibility() == View.VISIBLE)
                    rr_selectExercise.setVisibility(View.VISIBLE);
                ExerciseCategory exerciseCat = null;
                for (int i = 0; i < exerciseCategories.size(); i++) {
                    if (exerciseCategories.get(i).getExerciseCatID().matches(exerciseCatIDLast)) {
                        exerciseCat = exerciseCategories.get(i);
                        break;
                    }
                }
                if (exerciseCat != null && exerciseCat.getExercises().size() > 0) {
                    for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                        exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                        exerciseList.addAll(exerciseCat.getExercises());
                    }
                }
            }
            etSelectExercise.setOnClickListener(view -> {
                if (!app.getPreferences().isClient()) {
                    SpinnerDialog exerciseDialog = new SpinnerDialog(AddOTMExercise.this, exerciseNameList,
                            "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                    exerciseDialog.showSpinerDialog();
                    exerciseDialog.bindOnSpinerListener((item, position) -> {
                        etSelectExercise.setText(item);
                        etSelectExercise.setTag(exerciseList.get(position).getExerciseID());
                    });
                }
            });
        }
        rowView.setTag(id);
        llContent.addView(rowView, llContent.getChildCount());
    }

    private void displayExcerciseList() {
        if (exerciseList.size() > 0) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> {
                for (int j = 0; j < exerciseList.size(); j++) {
                    Exercise exercise = exerciseList.get(j);
                    View rowView1 = getLayoutInflater().inflate(R.layout.view_otm, null);
                    final SwipeLayout swipeLayout = rowView1.findViewById(R.id.swipeLayout);
                    swipeLayout.setSwipeEnabled(!app.getPreferences().isClient());
                    final EditText etSelectExercise = rowView1.findViewById(R.id.et_exercise);
                    final EditText et_category = rowView1.findViewById(R.id.et_category);
                    final EditText et_minute = rowView1.findViewById(R.id.et_minute);
                    final FlexboxLayout fl_elements = rowView1.findViewById(R.id.fl_elements);

                    et_minute.setText(exercise.getExerciseValue()!= null ? exercise.getExerciseValue() : "");
                    et_minute.setOnClickListener(view -> {
                        if (!app.getPreferences().isClient()) {
                            SpinnerDialog elementDialog = new SpinnerDialog(AddOTMExercise.this, timeList,
                                    "Select Time", R.style.DialogAnimations_SmileWindow, true);
                            elementDialog.showSpinerDialog();
                            elementDialog.bindOnSpinerListener((item1, position1) -> {
                                et_minute.setText(item1);
                            });
                        }
                    });

                    List<com.reppsfitness.model.schedule.Element> workoutElements = exercise.getWorkoutElements();
                    if (workoutElements != null && workoutElements.size() > 0) {
                        for (int k = 0; k < elementList.size(); k++) {
                            Element element1 = elementList.get(k);
                            RelativeLayout rr_element_view = (RelativeLayout) getLayoutInflater().inflate(R.layout.rr_element_view, null);
                            rr_element_view.setPadding(Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4),
                                    Utils.getDpInPx(mContext, 10), Utils.getDpInPx(mContext, 4));
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                    RelativeLayout.LayoutParams.WRAP_CONTENT);
                            layoutParams.setMargins(0, Utils.getDpInPx(mContext, 8), Utils.getDpInPx(mContext, 12), 0);
                            rr_element_view.setLayoutParams(layoutParams);
                            EditText et_element = rr_element_view.findViewById(R.id.et_element);

                            for (int i = 0; i < workoutElements.size(); i++) {
                                com.reppsfitness.model.schedule.Element element = workoutElements.get(i);
                                if (element1.getWorkoutElementID().matches(element.getWorkoutElementID())) {
                                    et_element.setHint(element.getWorkoutElementName());
                                    et_element.setTag(element.getWorkoutElementID());
                                    et_element.setText(element.getWorkoutElementValue());
                                } else {
                                    et_element.setTag(element1.getWorkoutElementID());
                                    et_element.setHint(element1.getWorkoutElementName());
                                }
                            }
                            ArrayList<String> finalWorkoutElementValue = element1.getWorkoutElementValue();
                            et_element.setOnClickListener(view1 -> {
                                if (!app.getPreferences().isClient()) {
                                    SpinnerDialog elementDialog = new SpinnerDialog(AddOTMExercise.this, finalWorkoutElementValue,
                                            "Select " + element1.getWorkoutElementName(), R.style.DialogAnimations_SmileWindow, true);
                                    elementDialog.showSpinerDialog();
                                    elementDialog.bindOnSpinerListener((item1, position1) -> et_element.setText(item1));
                                }
                            });
                            fl_elements.addView(rr_element_view, fl_elements.getChildCount());
                        }
                    }
                    if (exerciseCategories != null && exerciseCategories.size() > 0) {
                        ArrayList<String> exerciseNameList = new ArrayList<>();
                        ArrayList<Exercise> exerciseList1 = new ArrayList<>();
                        et_category.setOnClickListener(view -> {
                            if (!app.getPreferences().isClient()) {
                                SpinnerDialog spinnerDialog = new SpinnerDialog(AddOTMExercise.this, workoutCatNameList,
                                        "Select Workout Category", R.style.DialogAnimations_SmileWindow, true);
                                spinnerDialog.showSpinerDialog();
                                spinnerDialog.bindOnSpinerListener((item, position) -> {
                                    et_category.setText(item);
                                    etSelectExercise.setText("");
                                    et_category.setTag(exerciseCategories.get(position).getExerciseCatID());
                                    exerciseList1.clear();
                                    exerciseNameList.clear();
                                    if (exerciseCategories.get(position).getExercises().size() > 0) {
                                        ExerciseCategory exerciseCat = exerciseCategories.get(position);
                                        for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                                            exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                                            exerciseList1.addAll(exerciseCat.getExercises());
                                        }
                                    }
                                });
                            }
                        });
                        etSelectExercise.setTag(exercise.getExerciseID());
                        etSelectExercise.setText(exercise.getExerciseName());
                        et_category.setText(exercise.getExerciseCatName());
                        et_category.setTag(exercise.getExerciseCatID());
                        String exerciseCatID = exercise.getExerciseCatID();
                        ExerciseCategory exerciseCat = null;
                        for (int i = 0; i < exerciseCategories.size(); i++) {
                            if (exerciseCategories.get(i).getExerciseCatID().matches(exerciseCatID)) {
                                exerciseCat = exerciseCategories.get(i);
                                break;
                            }
                        }
                        if (exerciseCat != null && exerciseCat.getExercises().size() > 0) {
                            for (int i = 0; i < exerciseCat.getExercises().size(); i++) {
                                exerciseNameList.add(exerciseCat.getExercises().get(i).getExerciseName());
                                exerciseList1.addAll(exerciseCat.getExercises());
                            }
                        }
                        etSelectExercise.setOnClickListener(view -> {
                            if (!app.getPreferences().isClient()) {
                                SpinnerDialog exerciseDialog = new SpinnerDialog(AddOTMExercise.this, exerciseNameList,
                                        "Select or Search Exercise", R.style.DialogAnimations_SmileWindow, true);
                                exerciseDialog.showSpinerDialog();
                                exerciseDialog.bindOnSpinerListener((item, position) -> {
                                    etSelectExercise.setText(item);
                                    etSelectExercise.setTag(exerciseList1.get(position).getExerciseID());
                                });
                            }
                        });
                    }
                    rowView1.setTag(isTemplate ? exercise.getTemplateEID() : exercise.getScheduleEID());
                    swipeLayout.findViewById(R.id.tv_repeat).setOnClickListener(view -> {
                        repeatExerciseView(isTemplate ? exercise.getTemplateEID() : exercise.getScheduleEID());
                        swipeLayout.close();
                    });
                    swipeLayout.findViewById(R.id.tv_delete).setOnClickListener(view -> {
                        deleteExerciseView(isTemplate ? exercise.getTemplateEID() : exercise.getScheduleEID());
                        swipeLayout.close();
                    });
                    llContent.addView(rowView1, llContent.getChildCount());
                }
            });
        } else {
            addExerciseView();
        }
    }

    private void saveDayExercise(String json) {
        Map<String, String> params = new HashMap<>();
        if (isTemplate) {
            params.put("xAction", "updateTemplateDayWorkout");
            params.put("templateDayID", day.getTemplateDayID());
            params.put("templateID", templateID);
            params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
            params.put("templateDetailsID", workout != null ? workout.getTemplateDetailsID() : "");
        } else {
            params.put("xAction", "updateScheduleDayWorkoutNew");
            params.put("scheduleDayID", day.getScheduleDayID());
            params.put("clientID", user.getClientID());
            params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
            params.put("scheduleDetailsID", workout != null ? workout.getScheduleDetailsID() : "");
        }
        params.put("workoutDetails", json);
        params.put("workoutTypeID", workoutTypeID);
        params.put("workoutTypeValue", time);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().updateScheduleDayWorkout(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "success");
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            if (CreateWorkoutSchedule.createWorkoutSchedule != null && !CreateWorkoutSchedule.createWorkoutSchedule.isFinishing()) {
                                CreateWorkoutSchedule.createWorkoutSchedule.finish();
                            }
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void deleteScheduleExercise(String scheduleDetailsID) {
        Map<String, String> params = new HashMap<>();
        if (isTemplate) {
            params.put("xAction", "deleteTemplateExercise");
            params.put("templateEID", scheduleDetailsID);
        } else {
            params.put("xAction", "deleteScheduleExercise");
            params.put("scheduleEID", scheduleDetailsID);
        }
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            mCompositeDisposable.add(app.getApiService().deleteScheduleExercise(params)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(response -> {
                        if (pd.isShowing()) pd.dismiss();
                        if (response != null && response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg()))
                                Utils.showLongToast(mContext, response.getMsg());
                            if (llContent.getChildCount() > 0) {
                                for (int i = 0; i < llContent.getChildCount(); i++) {
                                    if (llContent.getChildAt(i).getTag() instanceof String
                                            && llContent.getChildAt(i).getTag().equals(scheduleDetailsID)) {
                                        llContent.removeViewAt(i);
                                        break;
                                    }
                                }
                            }
                        } else {
                            if (response != null && response.getMsg() != null && !TextUtils.isEmpty(response.getMsg()))
                                Utils.showLongToast(mContext, response.getMsg());
                        }
                    }, throwable -> {
                        if (pd.isShowing()) pd.dismiss();
                        Log.e("in", "error " + throwable.getMessage());
                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        MenuItem item = menu.findItem(R.id.action_save);
        if (item != null) {
            item.setVisible(!app.getPreferences().isClient());
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_save:
                getAddedExerciseData();
                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
