package com.reppsfitness.expandable_list;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reppsfitness.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RegularViewHolder extends GroupViewHolder {
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_planTitle)
    TextView tv_planTitle;
    @BindView(R.id.tv_price)
    TextView price;
    @BindView(R.id.iv_arrow)
    ImageView iv_arrow;
    @BindView(R.id.iv_plan)
    ImageView iv_plan;
    @BindDrawable(R.drawable.ic_keyboard_arrow_down)
    Drawable ic_keyboard_arrow_down;
    @BindDrawable(R.drawable.ic_keyboard_arrow_up)
    Drawable ic_keyboard_arrow_up;
    @BindColor(R.color.grey)
    int grey;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.btn_bg_color)
    int btn_bg_color;
    Fragment fragment;

    RegularViewHolder(View itemView, Fragment fragment) {
        super (itemView);
        ButterKnife.bind (this, itemView);
        this.fragment = fragment;
    }

    @Override
    public void expand() {
        iv_arrow.setImageDrawable (ic_keyboard_arrow_up);
        view.setVisibility (View.GONE);
    }

    @Override
    public void collapse() {
        iv_arrow.setImageDrawable (ic_keyboard_arrow_down);
        view.setVisibility (View.VISIBLE);
    }

    void setGroupName(ExpandableGroup group, String reqPackageID) {
        tv_planTitle.setText (group.getTitle ());
        price.setText ("₹ " + group.getPrice ());
        if (group.getPackageID ().equals (reqPackageID)) this.expand ();
        Glide.with (fragment.getActivity ()).load (group.getPackageImage ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (iv_plan);
    }

}