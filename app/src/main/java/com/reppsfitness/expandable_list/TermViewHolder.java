package com.reppsfitness.expandable_list;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.reppsfitness.App;
import com.reppsfitness.BuyPackageActivity;
import com.reppsfitness.R;
import com.reppsfitness.client.fragment.ClientPackageFragment;
import com.reppsfitness.fragment.AddPackageFragment;
import com.reppsfitness.fragment.PackagePlansFragment;
import com.reppsfitness.model.TermItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by MXCPUU11 on 9/28/2017.
 */

public class TermViewHolder extends GroupViewHolder {
    @BindView(R.id.view)
    View view;
    @BindView(R.id.tv_planTitle)
    TextView tv_planTitle;
    @BindView(R.id.tv_price)
    TextView price;
    @BindView(R.id.iv_plan)
    ImageView iv_plan;
    @BindView(R.id.iv_arrow)
    ImageView iv_arrow;
    /*@BindView(R.id.btn_upgrade)
    Button btn_upgrade;*/
    @BindDrawable(R.drawable.ic_keyboard_arrow_down)
    Drawable ic_keyboard_arrow_down;
    @BindDrawable(R.drawable.ic_keyboard_arrow_up)
    Drawable ic_keyboard_arrow_up;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.btn_bg_color)
    int btn_bg_color;
    Fragment fragment;

    TermViewHolder(View itemView, Fragment fragment) {
        super (itemView);
        ButterKnife.bind (this, itemView);
        this.fragment=fragment;
    }

    @Override
    public void expand() {
        iv_arrow.setImageDrawable (ic_keyboard_arrow_up);
        view.setVisibility (View.GONE);
        Log.i ("Adapter", "expand");
    }

    @Override
    public void collapse() {
        Log.i ("Adapter", "collapse");
        iv_arrow.setImageDrawable (ic_keyboard_arrow_down);
        view.setVisibility (View.VISIBLE);
    }

    void setGroupName(ExpandableGroup group/*, boolean isAnyTermBuy, String packageID, Date pkgEndDate, String reqPackageID, boolean isAnyTermRequested, int isTermPkgBuy, boolean isAnyPackageBuy, boolean isAnyPackageRequested*/) {
        tv_planTitle.setText (group.getTitle ());
        price.setText ("₹ " + group.getPrice ());
        Glide.with (fragment.getActivity ()).load (group.getPackageImage ()).diskCacheStrategy (DiskCacheStrategy.NONE).skipMemoryCache (true).into (iv_plan);
        //iv_arrow.setVisibility (fragment instanceof PackagePlansFragment ? VISIBLE : GONE);
        //btn_upgrade.setVisibility (fragment instanceof PackagePlansFragment ? GONE : VISIBLE);
        //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
/*
        if (fragment instanceof ClientPackageFragment || fragment instanceof AddPackageFragment) {
            List items = group.getItems ();
            Object object = items.get (0);
            if (object instanceof TermItem) {
                termItem = (TermItem) object;
                if (isTermPkgBuy == 1) {
                    if (!isAnyTermBuy) {
                        if (isAnyTermRequested) {
                            if (reqPackageID != null && !TextUtils.isEmpty (reqPackageID) && !reqPackageID.equals ("0")) {
                                if (termItem.getPackageID ().equals (reqPackageID)) {
                                    btn_upgrade.setVisibility (VISIBLE);
                                    if (app.getPreferences ().isClient ()) {
                                        btn_upgrade.setOnClickListener (null);
                                        btn_upgrade.setText ("Requested");
                                    }
                                    else {
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                        btn_upgrade.setText ("Approve");
                                        approveRejectFlg="true";
                                    }
                                    iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                } else {
                                    btn_upgrade.setVisibility (GONE);
                                    iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            btn_upgrade.setVisibility (VISIBLE);
                            btn_upgrade.setText ("Buy");
                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                            iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                        }
                    } else {
                        if (termItem.getPackageID () != null && !TextUtils.isEmpty (termItem.getPackageID ()) && !TextUtils.isEmpty (packageID) && !packageID.equals ("0")) {
                            if (pkgEndDate != null) {
                                String strDate = ymdSdf.format (date);
                                String strEndDate = ymdSdf.format (pkgEndDate);
                                try {
                                    currentDate = ymdSdf.parse (strDate);
                                    endDate = ymdSdf.parse (strEndDate);
                                } catch (ParseException e) {
                                    e.printStackTrace ();
                                }
                                if (termItem.getPackageID ().equals (packageID)) {
                                    if (endDate.before (currentDate)) {
                                        btn_upgrade.setText ("Renew");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                        iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                                        btn_upgrade.setVisibility (VISIBLE);

                                    } else {
                                        btn_upgrade.setVisibility (GONE);
                                        iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                                    }
                                } else {
                                    if (isAnyPackageBuy || isAnyTermBuy) {
                                        if (endDate.before (currentDate)) {
                                            btn_upgrade.setText ("Buy");
                                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                            btn_upgrade.setVisibility (VISIBLE);
                                            iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                        } else {
                                            btn_upgrade.setVisibility (GONE);
                                            iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                        }
                                    } else {
                                        btn_upgrade.setText ("Buy");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                        btn_upgrade.setVisibility (VISIBLE);
                                        iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                    }
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            btn_upgrade.setVisibility (GONE);
                            iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                        }
//                    if ((regularItem.getIsCurrentPackage() == 0 && regularItem.getIsRequestPackage() == 1) ||
//                            (regularItem.getIsCurrentPackage() == 1 && regularItem.getIsRequestPackage() == 1)) {
//                        btn_upgrade.setText("Requested");
//                        btn_upgrade.setOnClickListener(null);
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    } else if (regularItem.getIsCurrentPackage() == 1 && regularItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Renew");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(regularItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(colorPrimary, PorterDuff.Mode.SRC_IN);
//                    } else if (regularItem.getIsCurrentPackage() == 0 && regularItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Upgrade");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(regularItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    }
                    }
                } else if (isTermPkgBuy == 0) {
                    if (pkgEndDate != null) {
                        String strDate = ymdSdf.format (date);
                        String strEndDate = ymdSdf.format (pkgEndDate);
                        try {
                            currentDate = ymdSdf.parse (strDate);
                            endDate = ymdSdf.parse (strEndDate);
                        } catch (ParseException e) {
                            e.printStackTrace ();
                        }
                        if (endDate.before (currentDate)) {
                            if (!isAnyPackageRequested) {
                                btn_upgrade.setText ("Buy");
                                btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                btn_upgrade.setVisibility (VISIBLE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            if (!isAnyPackageRequested && !isAnyPackageBuy) {
                                btn_upgrade.setText ("Buy");
                                btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                                btn_upgrade.setVisibility (VISIBLE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                      */
/*  btn_upgrade.setVisibility (GONE);
                        iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);*//*

                        }
                    } else {
                        btn_upgrade.setVisibility (GONE);
                        iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                    }
                } else if (isTermPkgBuy == -1) {
                    btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (termItem));
                }
            }
        }
*/
    }

//    void setGroupName(ExpandableGroup group, boolean isFromAdmin, boolean isAnyTermBuy) {
//        tv_planTitle.setText(group.getTitle());
//        price.setText("₹ " + group.getPrice());
//        iv_arrow.setVisibility(fragment instanceof PackagePlansFragment ? VISIBLE : GONE);
//        btn_upgrade.setVisibility(fragment instanceof PackagePlansFragment ? GONE : VISIBLE);
//        if (fragment instanceof ClientPackageFragment  || fragment instanceof AddPackageFragment) {
//            List items = group.getItems();
//            Object object = items.get(0);
//            if (object instanceof TermItem) {
//                termItem = (TermItem) object;
//                if (!isAnyTermBuy) {
//                    btn_upgrade.setText("Buy");
//                    btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(termItem));
//                    iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                } else {
//                    if ((termItem.getIsCurrentPackage() == 0 && termItem.getIsRequestPackage() == 1) ||
//                            (termItem.getIsCurrentPackage() == 1 && termItem.getIsRequestPackage() == 1)) {
//                        btn_upgrade.setText("Requested");
//                        btn_upgrade.setOnClickListener(null);
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    } else if (termItem.getIsCurrentPackage() == 1 && termItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Renew");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(termItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(colorPrimary, PorterDuff.Mode.SRC_IN);
//                    } else if (termItem.getIsCurrentPackage() == 0 && termItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Upgrade");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(termItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    }
//                }
//            }
//        }
//    }


}
