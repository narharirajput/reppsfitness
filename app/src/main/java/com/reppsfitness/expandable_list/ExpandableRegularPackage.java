package com.reppsfitness.expandable_list;


import com.reppsfitness.model.RegularItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class ExpandableRegularPackage extends ExpandableGroup<RegularItem> {

    public ExpandableRegularPackage(String title, String amount, List<RegularItem> items, String packageImage, String packageID) {
        super(title, amount, items,packageImage,packageID);
    }
}