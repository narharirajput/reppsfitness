package com.reppsfitness.expandable_list;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.reppsfitness.App;
import com.reppsfitness.BuyPackageActivity;
import com.reppsfitness.R;
import com.reppsfitness.client.fragment.ClientPackageFragment;
import com.reppsfitness.fragment.AddPackageFragment;
import com.reppsfitness.fragment.PackagePlansFragment;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.TermItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Created by MXCPUU11 on 9/28/2017.
 */

public class TermDetails extends ChildViewHolder {
    private TextView tv_description;
    @BindView(R.id.btn_upgrade)
    Button btn_upgrade;
    private App app;
    private Activity activity;
    private Fragment fragment;
    private TermItem termItem;
    private Date date;
    SimpleDateFormat ymdSdf = new SimpleDateFormat ("yyyy-MM-dd");
    private Date currentDate = null;
    private Date endDate = null;
    private String approveRejectFlg="false";

    TermDetails(View itemView, App app, Fragment fragment) {
        super (itemView);
        ButterKnife.bind (this, itemView);
        tv_description = itemView.findViewById(R.id.tv_description);
        this.fragment = fragment;
        this.activity = fragment.getActivity ();
        this.app = app;
        date = new Date ();
    }

    void setGroupName(ExpandableGroup group, boolean isAnyTermBuy, String packageID, Date pkgEndDate, String reqPackageID, boolean isAnyTermRequested, int isTermPkgBuy, boolean isAnyPackageBuy, boolean isAnyPackageRequested, TermItem termItem) {
      /*  tv_planTitle.setText (group.getTitle ());
        price.setText ("₹ " + group.getPrice ());
        iv_arrow.setVisibility (fragment instanceof PackagePlansFragment ? VISIBLE : GONE);*/
        tv_description.setText(termItem.getPackageFeatureDesc());
        btn_upgrade.setVisibility (fragment instanceof PackagePlansFragment ? GONE : VISIBLE);
        //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
        if (fragment instanceof ClientPackageFragment || fragment instanceof AddPackageFragment) {
            List items = group.getItems ();
            Object object = items.get (0);
            if (object instanceof TermItem) {
                this.termItem = (TermItem) object;
                if (isTermPkgBuy == 1) {
                    if (!isAnyTermBuy) {
                        if (isAnyTermRequested) {
                            if (reqPackageID != null && !TextUtils.isEmpty (reqPackageID) && !reqPackageID.equals ("0")) {
                                if (this.termItem.getPackageID ().equals (reqPackageID)) {
                                    btn_upgrade.setVisibility (VISIBLE);
                                    if (app.getPreferences ().isClient ()) {
                                        btn_upgrade.setOnClickListener (null);
                                        btn_upgrade.setText ("Requested");
                                    }
                                    else {
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                        btn_upgrade.setText ("Approve");
                                        approveRejectFlg="true";
                                    }
                                    //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                } else {
                                    btn_upgrade.setVisibility (GONE);
                                    //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            btn_upgrade.setVisibility (VISIBLE);
                            btn_upgrade.setText ("Buy");
                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                           // iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                        }
                    } else {
                        if (this.termItem.getPackageID () != null && !TextUtils.isEmpty (this.termItem.getPackageID ()) && !TextUtils.isEmpty (packageID) && !packageID.equals ("0")) {
                            if (pkgEndDate != null) {
                                String strDate = ymdSdf.format (date);
                                String strEndDate = ymdSdf.format (pkgEndDate);
                                try {
                                    currentDate = ymdSdf.parse (strDate);
                                    endDate = ymdSdf.parse (strEndDate);
                                } catch (ParseException e) {
                                    e.printStackTrace ();
                                }
                                if (this.termItem.getPackageID ().equals (packageID)) {
                                    if (endDate.before (currentDate)) {
                                        btn_upgrade.setText ("Renew");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                        //iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                                        btn_upgrade.setVisibility (VISIBLE);

                                    } else {
                                        btn_upgrade.setVisibility (GONE);
                                        //iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                                    }
                                } else {
                                    if (isAnyPackageBuy || isAnyTermBuy) {
                                        if (endDate.before (currentDate)) {
                                            btn_upgrade.setText ("Buy");
                                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                            btn_upgrade.setVisibility (VISIBLE);
                                            //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                        } else {
                                            btn_upgrade.setVisibility (GONE);
                                           // iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                        }
                                    } else {
                                        btn_upgrade.setText ("Buy");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                        btn_upgrade.setVisibility (VISIBLE);
                                        //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                                    }
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (colorPrimary, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            btn_upgrade.setVisibility (GONE);
                            //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                        }
//                    if ((regularItem.getIsCurrentPackage() == 0 && regularItem.getIsRequestPackage() == 1) ||
//                            (regularItem.getIsCurrentPackage() == 1 && regularItem.getIsRequestPackage() == 1)) {
//                        btn_upgrade.setText("Requested");
//                        btn_upgrade.setOnClickListener(null);
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    } else if (regularItem.getIsCurrentPackage() == 1 && regularItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Renew");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(regularItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(colorPrimary, PorterDuff.Mode.SRC_IN);
//                    } else if (regularItem.getIsCurrentPackage() == 0 && regularItem.getIsRequestPackage() == 0) {
//                        btn_upgrade.setText("Upgrade");
//                        btn_upgrade.setOnClickListener(view -> startBuyPackageActivity(regularItem));
//                        iv_plan.getDrawable().mutate().setColorFilter(btn_bg_color, PorterDuff.Mode.SRC_IN);
//                    }
                    }
                } else if (isTermPkgBuy == 0) {
                    if (pkgEndDate != null) {
                        String strDate = ymdSdf.format (date);
                        String strEndDate = ymdSdf.format (pkgEndDate);
                        try {
                            currentDate = ymdSdf.parse (strDate);
                            endDate = ymdSdf.parse (strEndDate);
                        } catch (ParseException e) {
                            e.printStackTrace ();
                        }
                        if (endDate.before (currentDate)) {
                            if (!isAnyPackageRequested) {
                                btn_upgrade.setText ("Buy");
                                btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                btn_upgrade.setVisibility (VISIBLE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                        } else {
                            if (!isAnyPackageRequested && !isAnyPackageBuy) {
                                btn_upgrade.setText ("Buy");
                                btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                                btn_upgrade.setVisibility (VISIBLE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            } else {
                                btn_upgrade.setVisibility (GONE);
                                //iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                            }
                      /*  btn_upgrade.setVisibility (GONE);
                        iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);*/
                        }
                    } else {
                        btn_upgrade.setVisibility (GONE);
                      //  iv_plan.getDrawable ().mutate ().setColorFilter (btn_bg_color, PorterDuff.Mode.SRC_IN);
                    }//
                } else if (isTermPkgBuy == -1) {
                    btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.termItem));
                }
            }
        }
    }

    private void startBuyPackageActivity(TermItem termItem) {
        Intent intent=new Intent (activity, BuyPackageActivity.class);
        intent.putExtra ("termItem", termItem);
        intent.putExtra ("approveRejectFlg", approveRejectFlg);
        activity.startActivity (intent);
        //activity.startActivity (new Intent (activity, BuyPackageActivity.class).putExtra ("termItem", termItem).putExtra ("approveRejectFlg ",approveRejectFlg ));
    }
}

