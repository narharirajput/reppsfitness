package com.reppsfitness.expandable_list;

import com.reppsfitness.model.TermItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by MXCPUU11 on 9/28/2017.
 */

public class ExpandableTermPackage extends ExpandableGroup<TermItem> {

    public ExpandableTermPackage(String title, String amount, List<TermItem> items, String packageImage, String packageID) {
        super(title,amount, items,packageImage);
    }
}
