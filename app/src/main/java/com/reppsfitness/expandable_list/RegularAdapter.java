package com.reppsfitness.expandable_list;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reppsfitness.App;
import com.reppsfitness.R;
import com.reppsfitness.model.RegularItem;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.Date;
import java.util.List;

public class RegularAdapter extends ExpandableRecyclerViewAdapter<RegularViewHolder, RegularDetails> {
    private Fragment fragment;
    private App app;
    private boolean isAnyPackageBuy = false, isAnyPackageRequested = false, isAnyTermBuy=false, isAnyTermRequested=false;
    private int isTermPkgBuy = -1;
    private String packageID, reqPackageID;
    private Date pkgEndDate;

    public RegularAdapter(Fragment fragment, List<? extends ExpandableGroup> groups, boolean isAnyPackageBuy, String packageID, Date pkgEndDate, String reqPackageID, boolean isAnyPackageRequested, int isTermPkgBuy, boolean isAnyTermBuy, boolean isAnyTermRequested) {
        super(groups);
        this.fragment = fragment;
        this.app = (App) fragment.getActivity().getApplication();
        this.isAnyPackageBuy = isAnyPackageBuy;
        this.isAnyPackageRequested = isAnyPackageRequested;
        this.isTermPkgBuy = isTermPkgBuy;
        this.packageID = packageID;
        this.pkgEndDate = pkgEndDate;
        this.reqPackageID = reqPackageID;
        this.isAnyTermBuy=isAnyTermBuy;
        this.isAnyTermRequested=isAnyTermRequested;
    }

    @Override
    public RegularViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.group_view_holder, parent, false);
        return new RegularViewHolder(view,fragment);
    }

    @Override
    public RegularDetails onCreateChildViewHolder(ViewGroup parent, final int viewType) {
        LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.child_view_holder, parent, false);
        return new RegularDetails(view, app, fragment);
    }

    @Override
    public void onBindChildViewHolder(RegularDetails holder, int flatPosition, ExpandableGroup group, int childIndex) {
        RegularItem regularItem = ((ExpandableRegularPackage) group).getItems().get(childIndex);
        holder.setGroupName(group, isAnyPackageBuy, packageID, pkgEndDate, reqPackageID, isAnyPackageRequested, isTermPkgBuy,isAnyTermBuy,isAnyTermRequested,regularItem);
    }

    @Override
    public void onBindGroupViewHolder(RegularViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGroupName(group,reqPackageID);
    }
}