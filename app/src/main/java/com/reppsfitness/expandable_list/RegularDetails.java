package com.reppsfitness.expandable_list;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.reppsfitness.App;
import com.reppsfitness.BuyPackageActivity;
import com.reppsfitness.R;
import com.reppsfitness.client.fragment.ClientPackageFragment;
import com.reppsfitness.fragment.AddPackageFragment;
import com.reppsfitness.fragment.PackagePlansFragment;
import com.reppsfitness.model.RegularItem;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

class RegularDetails extends ChildViewHolder {
    private TextView tv_description;
    @BindView(R.id.btn_upgrade)
    Button btn_upgrade;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindColor(R.color.btn_bg_color)
    int btn_bg_color;
    private App app;
    private Fragment fragment;
    private Activity activity;
    private RegularItem regularItem;
    private Date date;
    private SimpleDateFormat ymdSdf = new SimpleDateFormat ("yyyy-MM-dd");
    private Date currentDate = null;
    private Date endDate = null;
    private String approveRejectFlg="false";

    RegularDetails(View itemView , App app, Fragment fragment) {
        super(itemView);
        ButterKnife.bind (this, itemView);
        tv_description = itemView.findViewById(R.id.tv_description);
        this.fragment = fragment;
        this.app = app;
        this.activity = fragment.getActivity ();
        date = new Date ();
    }

    private void startBuyPackageActivity(RegularItem regularItem) {
        Intent intent=new Intent (activity, BuyPackageActivity.class);
        intent.putExtra ("regularItem", regularItem);
        intent.putExtra ("approveRejectFlg", approveRejectFlg);
        activity.startActivity (intent);
    }

    void setGroupName(ExpandableGroup group, boolean isAnyPackageBuy, String packageID, Date pkgEndDate, String reqPackageID, boolean isAnyPackageRequested, int isTermPkgBuy, boolean isAnyTermBuy, boolean isAnyTermRequested, RegularItem regularItem) {
        tv_description.setText(regularItem.getPackageFeatureDesc());

        btn_upgrade.setVisibility (fragment instanceof PackagePlansFragment ? GONE : VISIBLE);
        if (fragment instanceof ClientPackageFragment || fragment instanceof AddPackageFragment) {
            List items = group.getItems ();
            Object object = items.get (0);
            if (object instanceof RegularItem) {
                this.regularItem = (RegularItem) object;
                if (isTermPkgBuy == 1) {
                    String strDate = ymdSdf.format (date);
                    String strEndDate = ymdSdf.format (pkgEndDate);
                    try {
                        currentDate = ymdSdf.parse (strDate);
                        endDate = ymdSdf.parse (strEndDate);
                    } catch (ParseException e) {
                        e.printStackTrace ();
                    }
                    if (!isAnyTermRequested) {
                        if (endDate.before (currentDate) ) {
                            btn_upgrade.setText ("Buy");
                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                            btn_upgrade.setVisibility (VISIBLE);
                        }else {
                            btn_upgrade.setVisibility (GONE);
                        }
                    } else {
                        btn_upgrade.setVisibility (GONE);
                    }

                } else if (isTermPkgBuy == 0) {
                    if (!isAnyPackageBuy) {
                        if (isAnyPackageRequested) {
                            if (reqPackageID != null && !TextUtils.isEmpty (reqPackageID) && !reqPackageID.equals ("0")) {
                                if (this.regularItem.getPackageID ().equals (reqPackageID)) {
                                    btn_upgrade.setVisibility (VISIBLE);
                                    if (app.getPreferences ().isClient ()) {
                                        btn_upgrade.setOnClickListener (null);
                                        btn_upgrade.setText ("Requested");
                                    }
                                    else {
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                                        btn_upgrade.setText ("Approve");
                                        approveRejectFlg ="true";
                                    }
                                } else {
                                    btn_upgrade.setVisibility (GONE);
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                            }
                        } else {
                            btn_upgrade.setVisibility (VISIBLE);
                            btn_upgrade.setText ("Buy");
                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                        }
                    } else {
                        if (this.regularItem.getPackageID () != null && !TextUtils.isEmpty (this.regularItem.getPackageID ()) && !TextUtils.isEmpty (packageID) && !packageID.equals ("0")) {
                            if (pkgEndDate != null) { // && !isAnyPackageRequested
                                String strDate = ymdSdf.format (date);
                                String strEndDate = ymdSdf.format (pkgEndDate);
                                try {
                                    currentDate = ymdSdf.parse (strDate);
                                    endDate = ymdSdf.parse (strEndDate);
                                } catch (ParseException e) {
                                    e.printStackTrace ();
                                }
                                if (this.regularItem.getPackageID ().equals (packageID)) {
                                    if (endDate.before (currentDate)) {
                                        btn_upgrade.setText ("Renew");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                                        btn_upgrade.setVisibility (VISIBLE);
                                    } else {
                                        btn_upgrade.setVisibility (GONE);
                                    }
                                } else {
                                    if (isAnyPackageBuy || isAnyTermBuy) {
                                        if (endDate.before (currentDate)) {
                                            btn_upgrade.setText ("Buy");
                                            btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                                            btn_upgrade.setVisibility (VISIBLE);
                                        }else {
                                            btn_upgrade.setVisibility (GONE);
                                        }
                                    } else {
                                        btn_upgrade.setText ("Buy");
                                        btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                                        btn_upgrade.setVisibility (VISIBLE);
                                    }
                                }
                            } else {
                                btn_upgrade.setVisibility (GONE);
                            }
                        } else {
                            btn_upgrade.setVisibility (GONE);
                        }
                    }
                } else if (isTermPkgBuy == -1) {
                    btn_upgrade.setOnClickListener (view -> startBuyPackageActivity (this.regularItem));
                }
            }
        }
    }

}