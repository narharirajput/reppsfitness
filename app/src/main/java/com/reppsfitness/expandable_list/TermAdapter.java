package com.reppsfitness.expandable_list;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reppsfitness.App;
import com.reppsfitness.R;
import com.reppsfitness.model.TermItem;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.Date;
import java.util.List;

public class TermAdapter extends ExpandableRecyclerViewAdapter<TermViewHolder, TermDetails> {
    private Fragment fragment;
    private App app;
    //    private boolean isFromAdmin;
    private boolean isAnyTermBuy = false, isAnyTermRequested = false, isAnyPackageBuy = false, isAnyPackageRequested = false;
    int isTermPkgBuy = -1;
    private String packageID, reqPackageID;
    Date pkgEndDate;

    //    public TermAdapter(Fragment fragment, List<? extends ExpandableGroup> groups, boolean isFromAdmin, boolean isAnyTermBuy) {
    public TermAdapter(Fragment fragment, List<? extends ExpandableGroup> groups, boolean isAnyTermBuy, String packageID, Date pkgEndDate, String reqPackageID, boolean isAnyTermRequested, int isTermPkgBuy, boolean isAnyPackageBuy, boolean isAnyPackageRequested) {
        super(groups);
        this.fragment = fragment;
        this.app = (App) fragment.getActivity().getApplication();
//        this.isFromAdmin = isFromAdmin;
        this.isAnyTermBuy = isAnyTermBuy;
        this.isAnyTermRequested = isAnyTermRequested;
        this.isTermPkgBuy = isTermPkgBuy;
        this.packageID = packageID;
        this.pkgEndDate = pkgEndDate;
        this.reqPackageID = reqPackageID;
        this.isAnyPackageBuy = isAnyPackageBuy;
        this.isAnyPackageRequested = isAnyPackageRequested;
    }

    @Override
    public TermViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.group_view_holder, parent, false);
        return new TermViewHolder(view,fragment);
    }

    @Override
    public TermDetails onCreateChildViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.child_view_holder, parent, false);
        return new TermDetails(view, app, fragment);
    }

    @Override
    public void onBindChildViewHolder(TermDetails holder, int flatPosition, ExpandableGroup group, int childIndex) {
        TermItem termItem = ((ExpandableTermPackage) group).getItems().get(childIndex);
        holder.setGroupName(group, isAnyTermBuy, packageID, pkgEndDate, reqPackageID, isAnyTermRequested, isTermPkgBuy,isAnyPackageBuy,isAnyPackageRequested,termItem);
        //holder.onBind(termItem, group);
    }

    @Override
    public void onBindGroupViewHolder(TermViewHolder holder, int flatPosition, ExpandableGroup group) {
//        holder.setGroupName(group, isFromAdmin, isAnyTermBuy);
        holder.setGroupName(group);
    }
}

