package com.reppsfitness;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.reppsfitness.adapter.TemplateDayListAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ClientProfile;
import com.reppsfitness.model.ClientProfileData;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.Template;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.utils.DialogUtils;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by MXCPUU11 on 8/5/2017.
 */

public class TemplateDayListActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    public Template template;
    public User user;
    private String fromDate, toDate;
    private EditText et_from_date, et_to_date;
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    private Date pkgEndDate;
    private TemplateDayListAdapter templateDayListAdapter;
    public MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        template = getIntent().getParcelableExtra("template");
        user = getIntent().getParcelableExtra("user");
        toolbarTitle.setText(template.getTemplateName().toUpperCase());
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if (user != null)
            getClientProfile();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTemplateDayList(template.getTemplateID());
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_workout_schedule;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Utils.ADD_EXERCISE) {
                getTemplateDayList(template.getTemplateID());
            }
        }
    }

    private void getTemplateDayList(String templateID) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "getTemplateDayList");
        params.put("templateID", templateID);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getTemplateDayList(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    WorkoutScheduleResponse response = (WorkoutScheduleResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0 && response.getDayList() != null && response.getDayList().size() > 0) {
                            templateDayListAdapter = new TemplateDayListAdapter(mContext, response.getDayList(), user != null);
                            recyclerView.setAdapter(templateDayListAdapter);
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_assign_template, menu);
        item = menu.findItem(R.id.action_assign_template);
        if (item != null) {
            item.setVisible(user != null);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_assign_template:
                String templateDayIDs = "";
                if (templateDayListAdapter != null && templateDayListAdapter.getDayList() != null &&
                        templateDayListAdapter.getDayList().size() > 0) {
                    List<Day> dayList = templateDayListAdapter.getDayList();
                    List<String> idList = new ArrayList<>();
                    for (int i = 0; i < dayList.size(); i++) {
                        idList.add(dayList.get(i).getTemplateDayID());
                    }
                    Log.e("ids", idList.toString());
                    templateDayIDs = idList.toString();
//                    templateDayIDs = templateDayIDs.replace("]", "");
//                    templateDayIDs = templateDayIDs.replace("[", "");
                    templateDayIDs = templateDayIDs.replace(" ", "");
                    Log.e("templateDayIDs str", templateDayIDs);
                }
                setAssignTemplateOnClickListener(templateDayIDs);
                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                break;
        }
        return true;
    }

    private void setAssignTemplateOnClickListener(String ids) {
        String title = "Assign Template";
        String negativeButton = "CANCEL";
        String positiveButton = "SUBMIT";

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_assign_template, null);
        et_from_date = customView.findViewById(R.id.et_from_date);
        et_to_date = customView.findViewById(R.id.et_to_date);
        et_from_date.setOnClickListener(view1 -> displayDatePickerDialog(true));
        et_to_date.setOnClickListener(view1 -> displayDatePickerDialog(false));

        final Dialog myDialog = DialogUtils.createCustomDialog(mContext, title, customView,
                negativeButton, positiveButton, false, new DialogUtils.DialogListener() {
                    @Override
                    public void onPositiveButton(Dialog dialog) {
                        String fromDate, toDate;
                        fromDate = et_from_date.getText().toString().trim();
                        toDate = et_to_date.getText().toString().trim();
                        if (TextUtils.isEmpty(fromDate)) {
                            et_from_date.setError("Select From Date");
                            et_from_date.requestFocus();
                            return;
                        }
                        if (TextUtils.isEmpty(toDate)) {
                            et_to_date.setError("Select To Date");
                            et_to_date.requestFocus();
                            return;
                        }
                        assignTemplate(dialog, ids);
                    }

                    @Override
                    public void onNegativeButton() {
                    }
                });
        if (myDialog != null && !myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void assignTemplate(Dialog dialog, String ids) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "assignTemplateToSchedule");
        params.put("scheduleFromDate", fromDate);
        params.put("scheduleToDate", toDate);
        params.put("clientID", user.getClientID());
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("templateID", template.getTemplateID());
        params.put("templateDayArray", ids);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().assignTemplateToSchedule(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (dialog.isShowing()) dialog.dismiss();
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (AddClientActivity.addClientActivity != null && !AddClientActivity.addClientActivity.isFinishing()) {
                                AddClientActivity.addClientActivity.finish();
                            }
                            if (SelectTemplateActivity.selectTemplateActivity != null && !SelectTemplateActivity.selectTemplateActivity.isFinishing()) {
                                SelectTemplateActivity.selectTemplateActivity.finish();
                            }
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            Intent intent = new Intent(mContext, AdminMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            startActivity(new Intent(mContext, ClientHomeActivity.class).putExtra("isFromAdmin", true)
                                    .putExtra("user", user));
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (dialog.isShowing()) dialog.dismiss();
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private void getClientProfile() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getClientProfile");
        map.put("clientID", user.getClientID());
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getClientProfile(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    ClientProfileData clientProfileData = (ClientProfileData) object;
                    if (clientProfileData != null && clientProfileData.getCount() > 0) {
                        ClientProfile clientProfile = clientProfileData.getClientProfile();
                        String str_pkgEndDate = clientProfile.getPkgEndDate();
                        String str_pkgStartDate = clientProfile.getPkgStartDate();
                        if (str_pkgEndDate != null && !str_pkgEndDate.equals("0000-00-00")) {
                            try {
                                pkgEndDate = formatter.parse(str_pkgEndDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    private Date date1, date2;

    private void displayDatePickerDialog(boolean isFrom) {
        Calendar now = Calendar.getInstance();
        final Calendar minCalendar = Calendar.getInstance();
        minCalendar.setTime(new Date());
        DatePickerDialog dpd = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
//                    if (isFrom) {
//                        Log.d("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
//                        fromDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                        et_from_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
//                    } else {
//                        Log.d("to date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
//                        toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                        et_to_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
//                    }
                    if (isFrom) {
                        Log.d("from date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
                        try {
                            date1 = formatter.parse(String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                            if (date2 != null) {
                                if (date1.compareTo(date2) < 0 || date1.compareTo(date2) == 0) {
                                    System.out.println("date2 is Greater than my date1");
                                    fromDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    et_from_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
                                } else {
                                    Utils.showShortToast(mContext, "From Date must be before To Date");
                                    date1 = null;
                                    return;
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        fromDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        String date = String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        et_from_date.setText(date);
                    } else {
                        Log.d("to date", "" + year + " " + (monthOfYear + 1) + " " + dayOfMonth);
                        try {
                            date2 = formatter.parse(String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth));
                            if (date1 != null) {
                                if (date1.compareTo(date2) < 0 || date1.compareTo(date2) == 0) {
                                    System.out.println("date2 is Greater than my date1");
                                    toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                    et_to_date.setText(String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year));
                                } else {
                                    Utils.showShortToast(mContext, "To Date must be after From Date");
                                    date2 = null;
                                    return;
                                }
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        toDate = String.valueOf(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                        String date = String.valueOf(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        et_to_date.setText(date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setMinDate(minCalendar);
        if (pkgEndDate != null) {
            final Calendar maxCalendar = Calendar.getInstance();
            maxCalendar.setTime(pkgEndDate);
            dpd.setMaxDate(maxCalendar);
        }
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
