package com.reppsfitness;


import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.RegularItem;
import com.reppsfitness.model.SlotData;
import com.reppsfitness.model.TermItem;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

public class AllotSessionActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    public static AllotSessionActivity addClientActivity;
    String fromDate;
//            , toDate, fromTime, toTime;
//    @BindView(R.id.et_from_date)
//    EditText etFromDate;
//    @BindView(R.id.et_to_date)
//    EditText etToDate;
    @BindView(R.id.et_time)
    EditText etTime;
//    @BindView(R.id.et_fromTime)
//    EditText etFromTime;
//    @BindView(R.id.et_toTime)
//    EditText etToTime;
    RegularItem regularItem;
    TermItem termItem;
    User user;
    private List<String> slotTimings;
    private String selectedTime;
    //    private Date pkgEndDate, pkgStartDate;
    SimpleDateFormat ymdSdf = new SimpleDateFormat("yyyy-MM-dd");
//    SimpleDateFormat dmySdfDisplay = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        termItem = getIntent().getParcelableExtra("termItem");
        regularItem = getIntent().getParcelableExtra("regularItem");
        user = getIntent().getParcelableExtra("user");
        addClientActivity = this;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Allot session");
//        getClientProfile();
        fromDate = ymdSdf.format(new Date());
//        String currDate = dmySdfDisplay.format(new Date());
//        etFromDate.setText(currDate);
        getSessionClientRequestTime();
    }

    @OnClick({R.id.et_time, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
//            case R.id.et_from_date:
////                Date minDate = null;
////                if (regularItem != null) {
////                    minDate = Utils.substractDays(new Date(), Integer.parseInt(regularItem.getPackageDuration()));
////                } else if (termItem != null) {
////                    minDate = Utils.substractDays(new Date(), Integer.parseInt(termItem.getPackageDuration()));
////                }
//                Calendar cal = Calendar.getInstance();
//                final Calendar minCalendar = Calendar.getInstance();
//                minCalendar.setTime(pkgStartDate);
//                DatePickerDialog dpd = DatePickerDialog.newInstance((view12, year, monthOfYear, dayOfMonth) -> {
//                            String dateToDisplay = "" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
//                            fromDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
//                            etFromDate.setText(dateToDisplay);
//                            getSessionClientRequestTime();
////                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
////                            SimpleDateFormat sdfDisplay = new SimpleDateFormat("dd/MM/yyyy");
////                            Date date = null;
////                            try {
////                                date = formatter.parse(fromDate);
////                                Date resultDate = null;
////                                if (regularItem != null) {
////                                    resultDate = Utils.addDays(date, Integer.parseInt(regularItem.getPackageDuration()));
////                                } else if (termItem != null) {
////                                    resultDate = Utils.addDays(date, Integer.parseInt(termItem.getPackageDuration()));
////                                }
////                                if (resultDate != null) {
////                                    String toDateToDisplay = sdfDisplay.format(resultDate);
////                                    toDate = formatter.format(resultDate);
////                                    etToDate.setText(toDateToDisplay);
////                                }
////                            } catch (ParseException e) {
////                                e.printStackTrace();
////                            }
//                        },
//                        cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)
//                );
//                dpd.setMinDate(minCalendar);
//                if (pkgEndDate != null) {
//                    final Calendar maxCalendar = Calendar.getInstance();
//                    maxCalendar.setTime(pkgEndDate);
//                    dpd.setMaxDate(maxCalendar);
//                }
//                dpd.show(getFragmentManager(), "Datepickerdialog");
//                break;
            case R.id.et_time:
                if (slotTimings != null && slotTimings.size() > 0) {
                    new MaterialDialog.Builder(mContext)
                            .items(slotTimings)
                            .itemsCallback((dialog, view1, which, text) -> {
                                selectedTime = text.toString();
                                etTime.setText(selectedTime);
                            })
                            .show();
                }
                break;
//            case R.id.et_fromTime:
//                displayTimePickerDialog(true);
//                break;
//            case R.id.et_toTime:
//                displayTimePickerDialog(false);
//                break;
            case R.id.btn_submit:
                allot_session();
                break;
        }
    }

    private void allot_session() {
//        String str_fromDate = etFromDate.getText().toString();
//        String str_toDate = etToDate.getText().toString();
        String time = etTime.getText().toString();
//        String str_fromTime = etFromTime.getText().toString();
//        String str_toTime = etToTime.getText().toString();
//        if (TextUtils.isEmpty(str_fromDate)) {
//            etFromDate.setError("Select From Date");
//            etFromDate.requestFocus();
//            return;
//        }
        if (TextUtils.isEmpty(time)) {
            etTime.setError("Select Time");
            etTime.requestFocus();
            return;
        }
//        if (TextUtils.isEmpty(str_fromTime)) {
//            etFromTime.setError("Select From Time");
//            etFromTime.requestFocus();
//            return;
//        }
//        if (TextUtils.isEmpty(str_toTime)) {
//            etToTime.setError("Select To Time");
//            etToTime.requestFocus();
//            return;
//        }
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "allottSessionToClient");
//        params.put("fromDate", fromDate);
//        params.put("toDate", toDate);
        params.put("sessionTime", time);
//        params.put("toTime", toTime);
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        if (regularItem != null)
            params.put("packageID", regularItem.getPackageID());
        else if (termItem != null)
            params.put("packageID", termItem.getPackageID());
        params.put("clientID", user.getClientID());
        for (String key : params.keySet()) {
            System.out.println(key + "=" + params.get(key));
        }
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().allottSessionToClient(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            if (AddClientPackageActivity.addClientPackageActivity != null && !AddClientPackageActivity.addClientPackageActivity.isFinishing()) {
                                AddClientPackageActivity.addClientPackageActivity.finish();
                            }
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            } else {
                                Utils.showLongToast(mContext, "Unable to allot session. Please try again later.");
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_allot_session;
    }

    private void getSessionClientRequestTime() {
        Map<String, String> map = new HashMap<>();
        map.put("xAction", "getSessionClientRequestTime");
        map.put("clientID", user.getClientID());
        map.put("requestDate", fromDate);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().getSessionClientRequestTime(map, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    SlotData slotData = (SlotData) object;
                    if (slotData != null && slotData.getCount() > 0) {
                        if (slotData.getData() != null && slotData.getData().getSlotTime() != null && slotData.getData().getSlotTime().size() > 0)
                            slotTimings = slotData.getData().getSlotTime();
                    } else {
                        if (slotData != null && slotData.getMsg() != null && !TextUtils.isEmpty(slotData.getMsg())) {
                            Utils.showLongToast(mContext, slotData.getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

//    private void getClientProfile() {
//        Map<String, String> map = new HashMap<>();
//        map.put("xAction", "getClientProfile");
//        map.put("clientID", user.getClientID());
//        if (cd.isConnectingToInternet()) {
//            CustomProgressDialog pd = new CustomProgressDialog(mContext);
//            pd.show();
//            app.getApiRequestHelper().getClientProfile(map, new ApiRequestHelper.OnRequestComplete() {
//                @Override
//                public void onSuccess(Object object) {
//                    if (pd.isShowing()) pd.dismiss();
//                    ClientProfileData clientProfileData = (ClientProfileData) object;
//                    if (clientProfileData != null && clientProfileData.getCount() > 0) {
//                        ClientProfile clientProfile = clientProfileData.getClientProfile();
//                        String str_pkgEndDate = clientProfile.getPkgEndDate();
//                        String str_pkgStartDate = clientProfile.getPkgStartDate();
//                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//                        if (str_pkgEndDate != null && !str_pkgEndDate.equals("0000-00-00")) {
//                            try {
//                                pkgEndDate = formatter.parse(str_pkgEndDate);
////                                String endDate = dmySdfDisplay.format(pkgEndDate);
////                                etToDate.setText(endDate);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        if (str_pkgStartDate != null && !str_pkgStartDate.equals("0000-00-00")) {
//                            try {
//                                pkgStartDate = formatter.parse(str_pkgStartDate);
//                            } catch (ParseException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    } else {
//                        if (clientProfileData != null && clientProfileData.getMsg() != null && !TextUtils.isEmpty(clientProfileData.getMsg())) {
//                            Utils.showLongToast(mContext, clientProfileData.getMsg());
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(String apiResponse) {
//                    if (pd.isShowing()) pd.dismiss();
//                }
//            });
//        } else {
//            Utils.alert_dialog(mContext);
//        }
//    }

//    private void displayTimePickerDialog(boolean isFrom) {
//        Calendar now = Calendar.getInstance();
//        TimePickerDialog tpd = TimePickerDialog.newInstance(
//                (view, hourOfDay, minute, second) -> {
//                    Log.e("tpd", hourOfDay + "||" + minute + "||" + second);
//                    if (isFrom) {
//                        fromTime = hourOfDay + ":" + minute;
//                        etFromTime.setText(fromTime);
//                    } else {
//                        toTime = hourOfDay + ":" + minute;
//                        etToTime.setText(toTime);
//                    }
//                },
//                now.get(Calendar.HOUR_OF_DAY),
//                now.get(Calendar.MINUTE),
//                true
//        );
//        tpd.show(getFragmentManager(), "Datepickerdialog");
//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_home, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
//            case R.id.action_home:
//                Intent intent = new Intent(this, AdminMainActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//                startActivity(intent);
//                finish();
//                break;
        }
        return true;
    }
}
