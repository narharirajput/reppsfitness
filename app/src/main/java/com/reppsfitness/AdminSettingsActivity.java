package com.reppsfitness;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo;
import com.miguelbcr.ui.rx_paparazzo2.entities.FileData;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ChangePasswordActivity;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.CustomTextViewMedium;
import com.reppsfitness.widget.CustomTextViewRegular;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class AdminSettingsActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    CustomTextViewRegular toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_first_name)
    EditText etFirstName;
    @BindView(R.id.et_last_name)
    EditText etLastName;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.ll_toolbar)
    LinearLayout llToolbar;
    @BindView(R.id.rr_noImage)
    RelativeLayout rrNoImage;
    @BindView(R.id.iv_profile)
    CircleImageView ivProfile;
    @BindView(R.id.tv_addPhoto)
    CustomTextViewMedium tvAddPhoto;
    @BindDrawable(R.drawable.no_image)
    Drawable no_image;
    User data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Settings");
        if (app.getPreferences().getLoggedInUser().getData() != null) {
            data = app.getPreferences().getLoggedInUser().getData();
            if (data.getImageName() != null) {
                Glide.with(mContext).load(data.getImageName()).diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true).into(ivProfile);
                rrNoImage.setVisibility(View.GONE);
                ivProfile.setVisibility(View.VISIBLE);
            } else {
                rrNoImage.setVisibility(View.VISIBLE);
                ivProfile.setVisibility(View.GONE);
            }
            etFirstName.setText(data.getUserName());
            etLastName.setText(data.getDisplayName());
            etEmail.setText(data.getUserEmail());
        }
    }

    @OnClick({R.id.btn_save, R.id.rr_noImage, R.id.iv_profile, R.id.tv_addPhoto,R.id.btn_changePass})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rr_noImage:
                selectPhoto();
                break;
            case R.id.iv_profile:
                selectPhoto();
                break;
            case R.id.tv_addPhoto:
                selectPhoto();
                break;

            case R.id.btn_changePass:
                Intent intent = new Intent (mContext, ChangePasswordActivity.class);
                intent.putExtra ("user", data);
                intent.putExtra ("isAdminProfile", true);
                startActivity (intent);
                break;

            case R.id.btn_save:
                String first_name = etFirstName.getText().toString().trim();
                String last_name = etLastName.getText().toString().trim();
                String email = etEmail.getText().toString().trim();
                if (TextUtils.isEmpty(first_name)) {
                    etFirstName.setError("Enter User Name");
                    etFirstName.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(last_name)) {
                    etLastName.setError("Enter Display Name");
                    etLastName.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    etEmail.setError("Enter Email");
                    etEmail.requestFocus();
                    return;
                }
                if (!Utils.isValidEmail(email)) {
                    etEmail.setError("Enter Valid Email");
                    etEmail.requestFocus();
                    return;
                }
                Map<String, String> params = new HashMap<>();
                params.put("xAction", "updateAdminProfile");
                params.put("userName", first_name);
                params.put("displayName", last_name);
//                params.put("userEmail", email);
                params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
                if (cd.isConnectingToInternet()) {
                    CustomProgressDialog pd = new CustomProgressDialog(mContext);
                    pd.show();
                    app.getApiRequestHelper().updateAdminProfile(params, new ApiRequestHelper.OnRequestComplete() {
                        @Override
                        public void onSuccess(Object object) {
                            if (pd.isShowing()) pd.dismiss();
                            LoginResponse response = (LoginResponse) object;
                            if (response != null) {
                                if (response.getCount() > 0) {
                                    if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                        Utils.showLongToast(mContext, response.getMsg());
                                    }
                                    app.getPreferences().setLoggedInUser(response);
                                    finish();
                                } else {
                                    if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                        Utils.showLongToast(mContext, response.getMsg());
                                    }
                                }
                            } else {
                                Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                            }
                        }

                        @Override
                        public void onFailure(String apiResponse) {
                            if (pd.isShowing()) pd.dismiss();
                            Utils.showLongToast(mContext, apiResponse);
                        }
                    });
                } else {
                    Utils.alert_dialog(mContext);
                }
                break;
        }
    }

    private void selectPhoto() {
        new MaterialDialog.Builder(mContext)
                .items("Capture from Camera", "Select from Gallery")
                .itemsCallback((dialog, view1, which, text) -> {
                    if (which == 0) {
                        RxPaparazzo.single(AdminSettingsActivity.this)
                                .usingCamera()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(response -> {
                                    // See response.resultCode() doc
                                    if (response.resultCode() != RESULT_OK) {
                                        return;
                                    }
                                    FileData data = response.data();
                                    uploadProfilePicture(data.getFile());
                                });
                    } else if (which == 1) {
                        RxPaparazzo.single(AdminSettingsActivity.this)
                                .usingGallery()
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(response -> {
                                    // See response.resultCode() doc
                                    if (response.resultCode() != RESULT_OK) {
                                        return;
                                    }
                                    FileData data = response.data();
                                    uploadProfilePicture(data.getFile());
                                });
                    }
                })
                .show();
    }

    private void uploadProfilePicture(File file) {
        String userID = app.getPreferences().getLoggedInUser().getData().getUserID();
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);
        builder.addFormDataPart("xAction", "uploadProfilePicture");
        builder.addFormDataPart("userType", "1");
        builder.addFormDataPart("userID", userID);
        builder.addFormDataPart("userImage", file.getName(), RequestBody.create(MediaType.parse("image/png"), file));
        MultipartBody multipartBody = builder.build();
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().uploadProfilePicture(multipartBody, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse userData = (LoginResponse) object;
                    if (userData != null) {
                        if (userData.getCount() > 0) {
                            if (userData.getMsg() != null && !TextUtils.isEmpty(userData.getMsg())) {
                                Utils.showLongToast(mContext, userData.getMsg());
                            }
                            rrNoImage.setVisibility(View.GONE);
                            ivProfile.setVisibility(View.VISIBLE);
                            Glide.with(mContext).load(file).diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true).into(ivProfile);
                            //Updating profile image in the preferences
                            if (userData.getData().getUserImage() != null) {
                                LoginResponse loggedInUser = app.getPreferences().getLoggedInUser();
                                User data = loggedInUser.getData();
                                data.setClientProfilePic(userData.getData().getUserImage());
                                loggedInUser.setData(data);
                                app.getPreferences().setLoggedInUser(loggedInUser);
                            }
                        } else {
                            if (userData.getMsg() != null && !TextUtils.isEmpty(userData.getMsg())) {
                                Utils.showLongToast(mContext, userData.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
//            mCompositeDisposable.add(app.getApiService().uploadProfilePictureRx(multipartBody)
//                    .observeOn(AndroidSchedulers.mainThread())
//                    .subscribeOn(Schedulers.io())
//                    .subscribe(userData -> {
//                        if (pd.isShowing()) pd.dismiss();
//                        if (userData != null) {
//                            if (userData.getCount() > 0) {
//                                Utils.showLongToast(mContext, userData.getMsg());
//                                rrNoImage.setVisibility(View.GONE);
//                                ivProfile.setVisibility(View.VISIBLE);
//                                Glide.with(mContext).load(file).into(ivProfile);
//                            } else {
//                                if (userData.getMsg() != null && !TextUtils.isEmpty(userData.getMsg())) {
//                                    Utils.showLongToast(mContext, userData.getMsg());
//                                }
//                            }
//                        } else {
//                            Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                        }
//                    }, throwable -> {
//                        if (pd.isShowing()) pd.dismiss();
//                        Log.e("in", "error " + throwable.getMessage());
//                    }));
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_admin_settings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }
}
