package com.reppsfitness;

import android.widget.CheckBox;

public interface CheckboxClickListener {
    void onCheckboxClick(CheckBox checkBox, int position);
}