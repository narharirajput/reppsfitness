package com.reppsfitness;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reppsfitness.adapter.DayListAdapter;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.Day;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.model.WorkoutScheduleResponse;
import com.reppsfitness.utils.DialogUtils;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by MXCPUU11 on 8/5/2017.
 */

public class WorkoutScheduleActivity extends BaseActivity {
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_toolbar)
    LinearLayout llToolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    public String fromDate;
    String toDate;
    //    WorkoutSchedule workoutSchedule;
    ArrayList<Day> dayList = new ArrayList<>();
    private MenuItem menu_save_template;
    public User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarTitle.setText("Workout Schedule");
        fromDate = getIntent().getStringExtra("fromDate");
        toDate = getIntent().getStringExtra("toDate");
        dayList = getIntent().getParcelableArrayListExtra("dayList");
        user = getIntent().getParcelableExtra("user");
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
//        recyclerView.setAdapter(new DayListAdapter(mContext, dayList));
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_workout_schedule;
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshWorkoutScheduleList();
    }

    private void refreshWorkoutScheduleList() {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "saveWorkoutScheduleDayList");
        params.put("userID", app.getPreferences().getLoggedInUser().getData().getUserID());
        params.put("clientID", user.getClientID());
        params.put("scheduleFromDate", fromDate);
        params.put("scheduleToDate", toDate);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().saveWorkoutScheduleDayList(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (pd.isShowing()) pd.dismiss();
                    WorkoutScheduleResponse response = (WorkoutScheduleResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0 && response.getDayList() != null
                                && response.getDayList().size() > 0) {
                            dayList.clear();
                            dayList.addAll(response.getDayList());
                            recyclerView.setAdapter(new DayListAdapter(mContext, dayList));
                            boolean isAllExercisesAdded = false;
                            if (menu_save_template != null) {
                                for (int i = 0; i < dayList.size(); i++) {
                                    isAllExercisesAdded = !TextUtils.isEmpty(dayList.get(i).getExercise());
                                }
                                menu_save_template.setVisible(isAllExercisesAdded);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Log.e("in", "error " + apiResponse);
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_save_template:
                setBtnOnClick();
                break;
            case R.id.action_home:
                Intent intent;
                if (app.getPreferences().isClient()) {
                    intent = new Intent(this, ClientHomeActivity.class);
                } else {
                    intent = new Intent(this, AdminMainActivity.class);
                }
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
                break;
        }
        return true;
    }

    private void setBtnOnClick() {
        String title = "Save as Template";
        String negativeButton = "CANCEL";
        String positiveButton = "SUBMIT";

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate(R.layout.dialog_save_as_template, null);
        EditText et_templateName = customView.findViewById(R.id.et_templateName);

        final Dialog myDialog = DialogUtils.createCustomDialog(mContext, title, customView,
                negativeButton, positiveButton, false, new DialogUtils.DialogListener() {
                    @Override
                    public void onPositiveButton(Dialog dialog) {
                        String templateName;
                        templateName = et_templateName.getText().toString().trim();
                        if (TextUtils.isEmpty(templateName)) {
                            et_templateName.setError("Enter Template Name");
                            et_templateName.requestFocus();
                            return;
                        }
                        saveAsTemplate(templateName, dialog);
                    }

                    @Override
                    public void onNegativeButton() {

                    }
                });

        if (myDialog != null && !myDialog.isShowing()) {
            myDialog.show();
        }
    }

    private void saveAsTemplate(String templateName, Dialog dialog) {
        Map<String, String> params = new HashMap<>();
        params.put("xAction", "saveAsTemplate");
        params.put("clientID", user.getClientID());
        params.put("scheduleFromDate", fromDate);
        params.put("scheduleToDate", toDate);
        params.put("templateName", templateName);
        if (cd.isConnectingToInternet()) {
            CustomProgressDialog pd = new CustomProgressDialog(mContext);
            pd.show();
            app.getApiRequestHelper().saveAsTemplate(params, new ApiRequestHelper.OnRequestComplete() {
                @Override
                public void onSuccess(Object object) {
                    if (dialog.isShowing()) dialog.dismiss();
                    if (pd.isShowing()) pd.dismiss();
                    LoginResponse response = (LoginResponse) object;
                    if (response != null) {
                        if (response.getCount() > 0) {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                            Intent intent = new Intent(WorkoutScheduleActivity.this, AdminMainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                            startActivity(new Intent(mContext, SelectTemplateActivity.class)
                                    .putExtra("user", user));
                            finish();
                        } else {
                            if (response.getMsg() != null && !TextUtils.isEmpty(response.getMsg())) {
                                Utils.showLongToast(mContext, response.getMsg());
                            }
                        }
                    } else {
                        Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                    if (pd.isShowing()) pd.dismiss();
                    Utils.showLongToast(mContext, apiResponse);
                }
            });
        } else {
            Utils.alert_dialog(mContext);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save_template, menu);
        menu_save_template = menu.findItem(R.id.action_save_template);
        menu_save_template.setVisible(false);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
