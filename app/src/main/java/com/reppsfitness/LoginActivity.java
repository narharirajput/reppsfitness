package com.reppsfitness;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.reppsfitness.api.ApiRequestHelper;
import com.reppsfitness.client.ClientHomeActivity;
import com.reppsfitness.model.ForgotPassword;
import com.reppsfitness.model.LoginResponse;
import com.reppsfitness.model.User;
import com.reppsfitness.utils.DialogUtils;
import com.reppsfitness.utils.Utils;
import com.reppsfitness.widget.materialprogress.CustomProgressDialog;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by MXCPUU11 on 8/9/2017.
 */

public class LoginActivity extends BaseActivity {
    @BindView(R.id.btn_admin)
    Button btnAdmin;
    @BindView(R.id.btn_client)
    Button btnClient;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.et_userEmail)
    EditText etUserEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.tv_terms)
    TextView tv_terms;
    @BindView(R.id.tv_forgot_password)
    TextView tv_forgot_password;
    String termsUrl = "", disclaimerUrl = "";
    String TAG = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        if (app.getPreferences ().isLoggedInUser ()) {
            if (app.getPreferences ().isClient ()) {
                startActivity (new Intent (mContext, ClientHomeActivity.class));
            } else startActivity (new Intent (mContext, AdminMainActivity.class));
            finish ();
            return;
        }
        etUserEmail.setText ("info@reppsfitness.com");
        etPassword.setText ("123456");
//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
//        Log.d("Refreshed token: ", refreshedToken != null ? refreshedToken : "");
        SpannableString ss = new SpannableString ("By continuing I agree to the Terms & Disclaimer.");
        ClickableSpan temrsClickableSpan = new ClickableSpan () {
            @Override
            public void onClick(View textView) {
                if (!TextUtils.isEmpty (termsUrl))
                    startActivity (new Intent (Intent.ACTION_VIEW, Uri.parse (termsUrl)));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState (ds);
                ds.setUnderlineText (false);
                ds.setColor (getResources ().getColor (R.color.toolbar_color));
            }
        };
        ClickableSpan clickableSpan = new ClickableSpan () {
            @Override
            public void onClick(View textView) {
                if (!TextUtils.isEmpty (disclaimerUrl))
                    startActivity (new Intent (Intent.ACTION_VIEW, Uri.parse (disclaimerUrl)));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState (ds);
                ds.setUnderlineText (false);
                ds.setColor (getResources ().getColor (R.color.toolbar_color));
            }
        };
        ss.setSpan (temrsClickableSpan, 29, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan (clickableSpan, 37, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_terms.setText (ss);
        tv_terms.setMovementMethod (LinkMovementMethod.getInstance ());
        getTermsCondLinks ();
    }

    private void getTermsCondLinks() {
        if (cd.isConnectingToInternet ()) {
            app.getApiRequestHelper ().getOtherSettings ("getOtherSettings", new ApiRequestHelper.OnRequestComplete () {
                @Override
                public void onSuccess(Object object) {
                    LoginResponse loginResponse = (LoginResponse) object;
                    Log.e ("in", "success");
                    if (loginResponse != null && loginResponse.getCount () > 0 && loginResponse.getData () != null) {
                        if (loginResponse.getData ().getTermsConditionsUrl () != null && !TextUtils.isEmpty (loginResponse.getData ().getTermsConditionsUrl ())) {
                            termsUrl = loginResponse.getData ().getTermsConditionsUrl ();
                        }
                        if (loginResponse.getData ().getDisclaimerUrl () != null && !TextUtils.isEmpty (loginResponse.getData ().getDisclaimerUrl ())) {
                            disclaimerUrl = loginResponse.getData ().getDisclaimerUrl ();
                        }
                    }
                }

                @Override
                public void onFailure(String apiResponse) {
                }
            });
        } else {
            Utils.alert_dialog (mContext);
        }
    }

    @Override
    protected int getActivityLayout() {
        return R.layout.activity_login;
    }

    @OnClick({R.id.btn_login})
    public void onViewClicked(View view) {
        switch (view.getId ()) {
            case R.id.btn_login:
                final String userEmail = etUserEmail.getText ().toString ().trim ();
                final String password = etPassword.getText ().toString ().trim ();
                if (TextUtils.isEmpty (userEmail)) {
                    etUserEmail.setError ("Enter User Email");
                    etUserEmail.requestFocus ();
                    return;
                }
                if (userEmail.contains ("@")) {
                    if (!Utils.isValidEmail (userEmail)) {
                        etUserEmail.setError ("Enter Valid Email");
                        etUserEmail.requestFocus ();
                        return;
                    }
                } else if (!android.util.Patterns.PHONE.matcher (userEmail).matches () || userEmail.length () != 10) {
                    etUserEmail.setError ("Enter Valid Mobile no");
                    etUserEmail.requestFocus ();
                    return;
                }

                if (TextUtils.isEmpty (password)) {
                    etPassword.setError ("Enter Password");
                    etPassword.requestFocus ();
                    return;
                }
                Map<String, String> params = new HashMap<> ();
                params.put ("xAction", "userLogin");
                params.put ("userEmail", userEmail);
                params.put ("userPass", password);
                params.put ("deviceType", "0");
                if (!TextUtils.isEmpty (app.getPreferences ().getToken ())) {
                    Log.e (TAG, "onViewClicked: 1");
                    params.put ("deviceTokenID", app.getPreferences ().getToken ());

                } else if (FirebaseInstanceId.getInstance ().getToken () != null)
                    Log.e (TAG, "onViewClicked: 2");
                params.put ("deviceTokenID", FirebaseInstanceId.getInstance ().getToken ());

                if (cd.isConnectingToInternet ()) {
                    CustomProgressDialog pd = new CustomProgressDialog (mContext);
                    pd.show ();
                    app.getApiRequestHelper ().login (params, new ApiRequestHelper.OnRequestComplete () {
                        @Override
                        public void onSuccess(Object object) {
                            if (pd.isShowing ()) pd.dismiss ();
                            LoginResponse loginResponse = (LoginResponse) object;
                            Log.e ("in", "success");
                            if (loginResponse != null) {
                                if (loginResponse.getCount () > 0) {
                                    if (loginResponse.getMsg () != null && !TextUtils.isEmpty (loginResponse.getMsg ())) {
                                        Utils.showLongToast (mContext, loginResponse.getMsg ());
                                    }
                                    if (loginResponse.getData ().getUserType ().equals ("1")) {
                                        app.getPreferences ().setClient (false);
                                        app.getPreferences ().setLoggedInUser (loginResponse);
                                        startActivity (new Intent (mContext, AdminMainActivity.class));
                                    } else if (loginResponse.getData ().getUserType ().equals ("2")) {
                                        app.getPreferences ().setClient (true);
                                        User data = loginResponse.getData ();
                                        data.setUserID (data.getClientID ());
                                        loginResponse.setData (data);
                                        app.getPreferences ().setLoggedInUser (loginResponse);
                                        startActivity (new Intent (mContext, ClientHomeActivity.class).putExtra ("isFromAdmin", false));
                                    }
                                    finish ();
                                } else {
                                    if (loginResponse.getMsg () != null && !TextUtils.isEmpty (loginResponse.getMsg ())) {
                                        Utils.showLongToast (mContext, loginResponse.getMsg ());
                                    } else {
                                        Utils.showLongToast (mContext, "Username or password is wrong. Please try again.");
                                    }
                                }
                            } else {
                                Utils.showLongToast (mContext, Utils.UNPROPER_RESPONSE);
                            }
                        }

                        @Override
                        public void onFailure(String apiResponse) {
                            if (pd.isShowing ()) pd.dismiss ();
                            Log.e ("in", "error " + apiResponse);
                            Utils.showLongToast (mContext, apiResponse);
                        }
                    });
                } else {
                    Utils.alert_dialog (mContext);
                }
////                params.put("device_type", "0");
////                if (!TextUtils.isEmpty(app.getPreferences().getToken()))
////                    params.put("device_id", app.getPreferences().getToken());
//                ConnectionDetector cd = new ConnectionDetector(mContext);
//                if (cd.isConnectingToInternet()) {
//                    final CustomProgressDialog pd = new CustomProgressDialog(mContext);
////                    pd.setTitle("Loading...");
//                    if (!pd.isShowing())
//                        pd.show();
//                    app.getApiRequestHelper().login(params, new ApiRequestHelper.OnRequestComplete() {
//                        @Override
//                        public void onSuccess(Object object) {
//                            if (pd.isShowing()) pd.dismiss();
//                            LoginResponse loginResponse = (LoginResponse) object;
//                            if (loginResponse != null) {
//                                if (loginResponse.getCount() > 0) {
//                                    Utils.showLongToast(mContext, loginResponse.getMsg());
//                                    app.getPreferences().setLoggedInUser(loginResponse);
//                                    startActivity(new Intent(mContext, AdminMainActivity.class));
//                                    finish();
//                                } else {
//                                    if (loginResponse.getMsg() != null && !TextUtils.isEmpty(loginResponse.getMsg())) {
//                                        Utils.showLongToast(mContext, loginResponse.getMsg());
//                                    } else {
//                                        Utils.showLongToast(mContext, "Username or password is wrong. Please try again.");
//                                    }
//                                }
//                            } else {
//                                Utils.showLongToast(mContext, Utils.UNPROPER_RESPONSE);
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(String apiResponse) {
//                            if (pd.isShowing()) pd.dismiss();
//                            Utils.showLongToast(mContext, apiResponse);
//                        }
//                    });
//                } else {
//                    Utils.alert_dialog(mContext);
//
                break;
        }
    }

    @OnClick(R.id.tv_forgot_password)
    public void onViewClicked() {
        String title = "Forgot password";
        String negativeButton = "CANCEL";
        String positiveButton = "SUBMIT";

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService (LAYOUT_INFLATER_SERVICE);
        View customView = inflater.inflate (R.layout.activity_forgot_password, null);
        EditText et_email_id = customView.findViewById (R.id.et_email_id);

        final Dialog myDialog = DialogUtils.createCustomDialog (mContext, title, customView, negativeButton, positiveButton, true, new DialogUtils.DialogListener () {
            @Override
            public void onPositiveButton(Dialog dialog) {
                String email_id;
                email_id = et_email_id.getText ().toString ().trim ();
                if (TextUtils.isEmpty (email_id)) {
                    et_email_id.setError ("Enter Email Address");
                    et_email_id.requestFocus ();
                    return;
                }
               /* if (Utils.isValidEmail (email_id)) {
                    et_email_id.setError ("Enter Valid Email Address");
                    et_email_id.requestFocus ();
                    return;
                }*/
                Map<String, String> params = new HashMap<> ();
                params.put ("xAction", "forgetPassword");
                params.put ("clientEmail", email_id);

                if (cd.isConnectingToInternet ()) {
                    CustomProgressDialog pd = new CustomProgressDialog (mContext);
                    pd.show ();

                    app.getApiRequestHelper ().forgotPassword (params, new ApiRequestHelper.OnRequestComplete () {
                        @Override
                        public void onSuccess(Object object) throws JSONException {
                            if (pd.isShowing ()) pd.dismiss ();
                            ForgotPassword forgotPassword = (ForgotPassword) object;
                            Log.e ("in", "success");

                            if (forgotPassword != null) {
                                if (forgotPassword.getCount () > 0) {
                                    if (forgotPassword.getMsg () != null && !TextUtils.isEmpty (forgotPassword.getMsg ())) {
                                        new MaterialDialog.Builder (mContext).content (forgotPassword.getMsg ()).positiveText ("Ok").show ();
                                        dialog.dismiss ();
                                    }
                                } else {
                                    if (forgotPassword.getMsg () != null && !TextUtils.isEmpty (forgotPassword.getMsg ())) {
                                       // Utils.showLongToast (mContext, forgotPassword.getMsg ());
                                        et_email_id.setText ("");
                                        et_email_id.setError (forgotPassword.getMsg ());
                                        et_email_id.requestFocus ();
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(String apiResponse) {
                            if (pd.isShowing ()) pd.dismiss ();
                            Log.e ("in", "error " + apiResponse);
                            Utils.showLongToast (mContext, apiResponse);
                            dialog.dismiss ();
                        }
                    });


                } else {
                    Utils.alert_dialog (mContext);
                    dialog.dismiss ();
                }
            }

            @Override
            public void onNegativeButton() {
                Utils.hideSoftKeyboard ((LoginActivity) mContext);
            }
        });

        if (myDialog != null && !myDialog.isShowing ()) {
            myDialog.show ();
        }
    }
}
